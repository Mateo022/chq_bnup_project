import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ProgressService } from '../services/progress.service';
import { AudioService } from '../services/audio.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  constructor (private router: Router, private progressService: ProgressService, audioService: AudioService) {
    audioService.setBackground('audioSoundtrack');
   }

  startCourse() {
    this.router.navigate(['/section1']);
  }

}
