import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-book-chq',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookChqComponent implements OnInit{
  currentPage = 3;
  @Output() isClicked: EventEmitter<number>;

  constructor() {
    this.isClicked = new EventEmitter<number>();
  }

  ngOnInit(): void { }

  toggleClass(e, toggleClassName) {
    if(e.className.includes(toggleClassName)) {
      e.classList.remove(toggleClassName);
    } else {
      e.classList.add(toggleClassName);
    }
  }

  movePage(e, page) {
    console.log('click--> ', page);
    
    /* console.log('--------\npage: ', page, ' - e: ', <HTMLDivElement>e);   //obtiene el elemento seleccionado completo con toda su información
    console.log('e target:', <HTMLDivElement>e.target);   //obtiene el elemento actual seleccionado
    console.log('e target localName:', <HTMLDivElement>e.target.localName);   //obtiene el nombre de la etiqueta del elemento seleccionado
    console.log('e className: ', <HTMLDivElement>e.target.className);   //obtiene las clases del elemento actual seleccionado
    console.log('e parentElement: ', <HTMLDivElement>e.target.parentElement);   //obtiene el elemento padre del elemento seleccionado */

    //console.log('<HTMLDivElement>e: ', <HTMLDivElement>e);   //obtiene el elemento seleccionado completo con toda su información
    //console.log('<HTMLDivElement>e.target:', <HTMLDivElement>e.target);   //obtiene el elemento actual seleccionado
    //console.log('<HTMLDivElement>e.target.localName:', <HTMLDivElement>e.target.localName);   //obtiene el nombre de la etiqueta del elemento seleccionado
    //console.log('<HTMLDivElement>e.target.className: ', <HTMLDivElement>e.target.className);   //obtiene las clases del elemento actual seleccionado
    //console.log('<HTMLDivElement>e.target.parentElement: ', <HTMLDivElement>e.target.parentElement);   //obtiene el elemento padre del elemento seleccionado
    
    /* el elemento clickeado es la division que contiene la información */
    console.log('----------------------------------------------');
    if(page == 29){
      console.log('cond page == 29');
      
      return;
    }
    console.log(<HTMLDivElement>e);
    console.log( <HTMLDivElement>e.target.classList[0]);
    let className:any = <HTMLDivElement>e.target.classList[0];
    console.log(className);
    if((<HTMLDivElement>e.target).localName == "div" && className == "page"){
      //console.log(<HTMLDivElement>e.target);
      //console.log('<HTMLDivElement>e.target.className: ', <HTMLDivElement>e.target.className); 
      console.log('--> PAGE');
        //obtiene las clases del elemento actual seleccionado
      if (page == this.currentPage) {
        console.log('PAGE | igual --> page: ', page, ' - currentPage: ', this.currentPage);
        this.currentPage+=2;
        this.toggleClass(<HTMLDivElement>e.target, "left-side");
        this.toggleClass((<HTMLDivElement>e.target).nextElementSibling, "left-side");
      }
      else if (page == this.currentPage - 1) {
        console.log('PAGE | -1 --> page: ', page, ' - currentPage: ', this.currentPage);
        this.currentPage-=2;
        this.toggleClass(<HTMLDivElement>e.target, "left-side");
        this.toggleClass((<HTMLDivElement>e.target).previousElementSibling, "left-side");
      }
    }
    /* el elemento clickeado es el contenedor interno (el elemento hijo) */
    else if ((<HTMLDivElement>e.target).localName == "div" && (className == "title" || className == "image" || className == "text") ) {
      console.log('--> CHILD');
      if (page == this.currentPage) {
        console.log('CHILD | igual --> page: ', page, ' - currentPage: ', this.currentPage);
        this.currentPage+=2;
        this.toggleClass(<HTMLDivElement>e.target.parentElement, "left-side");
        this.toggleClass((<HTMLDivElement>e.target.parentElement).nextElementSibling, "left-side");
      }
      else if (page == this.currentPage - 1) {
        console.log('CHILD | -1 --> page: ', page, ' - currentPage: ', this.currentPage);
        this.currentPage-=2;
        this.toggleClass(<HTMLDivElement>e.target.parentElement, "left-side");
        this.toggleClass((<HTMLDivElement>e.target.parentElement).previousElementSibling, "left-side");
      }
    }
    this.isClicked.emit(this.currentPage);
  }

}
