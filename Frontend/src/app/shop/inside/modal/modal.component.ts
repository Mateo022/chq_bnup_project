import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Output() isClicked: EventEmitter<boolean>;
  @Input() modalMessage: string = "";

  constructor() {
    this.isClicked = new EventEmitter<boolean>();
  }

  ngOnInit(): void { }

  startModal(modalMessage) {
    console.log('--- modal --- startModal()')
    this.openModal(modalMessage);
    $(".button_continue").addClass('disabled');
    $(".button_close").addClass('disabled');
    setTimeout(function () {
      $(".button_continue").removeClass('disabled');
      $(".button_close").removeClass('disabled');
    }, 500);
  }
  
  /*
   * Oculta el modal ejecutado al cargar el componente
   */
  closeModal() {
    $("#modal_info").hide();
    this.isClicked.emit(true);
  }

  /*
   * Muestra el modal ejecutado al cargar el componente
   */
  openModal(modalMessage) {
    this.modalMessage = modalMessage;
    console.log('--- modal --- openModal()');
    $("#modal_info").show();
  }

  /* informarPadre() {
    this.isClicked.emit(true);
  } */
}
