import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  @Input() showModalMenu = false;
  stateSoundMenu = 'active';
  hoverAudio = new Audio(`../../assets/section1/sounds/hoverDefault.mp3`);
  clickAudio = new Audio(`../../assets/section1/sounds/clickDefault.mp3`);

  closeMenuModal() {
    this.showModalMenu = false;
  }

  hoverBtnPostMenu() {
    if(this.stateSoundMenu === 'active'){
      this.hoverAudio.play();
    }
  }
  
  leaveBtnPostMenu() {
    this.hoverAudio.pause();
    this.hoverAudio.currentTime = 0;
    //this.clickAudio.play();
  }

}
