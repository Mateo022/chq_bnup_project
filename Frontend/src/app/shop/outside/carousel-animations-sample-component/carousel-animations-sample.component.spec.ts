import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselAnimationsSampleComponent } from './carousel-animations-sample.component';

describe('CarouselAnimationsSampleComponentComponent', () => {
  let component: CarouselAnimationsSampleComponent;
  let fixture: ComponentFixture<CarouselAnimationsSampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarouselAnimationsSampleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CarouselAnimationsSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
