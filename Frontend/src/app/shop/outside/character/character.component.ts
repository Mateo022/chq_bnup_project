import { Component } from '@angular/core';
import { environment as env } from 'src/environments/environment.qa';
import { AudioService } from '../../../services/audio.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent {
  stateSoundMenu = 'active';
  stateCharacterText = 'active';
  stateCharacterPlay = 'active';
  stateCharacterSound = 'active';

  /* caja de la boca del personaje */
  mouthCharacter;
  /* audio al pasar sobre los botones */
  hoverAudio = new Audio(`../../../../assets/section1/sounds/hoverDefault.mp3`);
  /* audio al hacer click en los botones */
  clickAudio = new Audio(`../../../../assets/section1/sounds/clickDefault.mp3`);
  /* Audio del personaje leyendo el texto principal */
  audioCharacter = new Audio(`../../../../assets/section1/sounds/Lesson1_view1.mp3`);

  ngOnDestroy() {
    console.log('DESTRUIDO');
    this.btnCharacterPause();
  }

  changeDirection(direction: string) {
    console.log('CHANGE DIRECTION');
    console.log($('.character'));
    //$('.character').css('border', '1px solid red');
    //$('.character').css('background', 'red');
    if(direction == 'right'){
      $('.character').css('transform', 'scaleX(-1)');
      $('.btns_character').css('left', '3%');
    }else{
      $('.character').css('transform', 'scaleX(1)');
      $('.btns_character').css('left', '56%');
    }
  }

  hoverBtnPostMenu() {
    if(this.stateSoundMenu === 'active'){
      this.hoverAudio.play();
    }
  }
  
  leaveBtnPostMenu() {
    this.hoverAudio.pause();
    this.hoverAudio.currentTime = 0;
    //this.clickAudio.play();
  }

  /*  */
  btnCharacterText() {
    this.clickAudio.currentTime = 0;
    this.clickAudio.play();
    if(this.stateCharacterText === 'active'){
      this.stateCharacterText = 'inactive';
    } else {
      this.stateCharacterText = 'active';
    }
  }

  constructor(private audioService: AudioService){
    this.stateSoundMenu = this.audioService.getStateSoundMenu();
    this.audioService.getStateSoundMenu$().subscribe(stateSoundMenu => {
      this.stateSoundMenu = stateSoundMenu;
      this.stateSoundMenu == 'inactive' ? this.audioCharacter.muted = true : this.audioCharacter.muted = false;
    });
  }

  ngOnInit() {
    //this.mouthCharacter = $('div.character div.mouth');
    this.mouthCharacter = document.getElementsByClassName('mouth');
    console.log('OnInit mouthCharacter: ', this.mouthCharacter);
  }


  /*
   * Reproduce o detiene el audio del personaje
   * Reproduce o detiene la gesticulación de la boca del personaje
   */
  btnCharacterPlayPause() {
    // console.log(' ++ character btnCharacterPlay');
    // this.clickAudio.currentTime = 0;
    // this.clickAudio.play();
    // this.mouthCharacter = $('div.character div.mouth');
    // console.log(this.mouthCharacter.hasClass('close'));
    // console.log(this.mouthCharacter.target);
    if(this.stateCharacterPlay === 'active'){
      this.btnCharacterPlay();
    } else {
      // this.stateCharacterPlay = 'active';
      this.btnCharacterPause();
    }
  }

  btnCharacterPlay(){
    console.log('PLAY mouthCharacter: ', this.mouthCharacter);
    this.stateCharacterPlay = 'inactive';
    this.audioCharacter.play();
    if(this.stateCharacterSound == 'inactive' || this.stateSoundMenu == 'inactive'){
      this.audioCharacter.muted = true;
    }
    this.mouthCharacter[0].classList.remove('close');
    this.mouthCharacter[0].classList.add('gesture');
    //let mouth = this.mouthCharacter;
    this.audioCharacter.onended = () => {
      this.stateCharacterPlay = 'active';
      //mouth[0].classList.remove('gesture');
      //mouth[0].classList.add('close');
      this.mouthCharacter[0].classList.remove('gesture');
      this.mouthCharacter[0].classList.add('close');
    };
  }

  btnCharacterPause(){
    this.stateCharacterPlay = 'active';
    this.audioCharacter.pause();
    this.mouthCharacter[0].classList.remove('gesture');
    this.mouthCharacter[0].classList.add('close');
  }

  btnCharacterReset(){
    // this.stateCharacterPlay = 'active';
    this.btnCharacterPause();
    this.audioCharacter.currentTime = 0;
    // this.mouthCharacter[0].classList.remove('gesture');
    // this.mouthCharacter[0].classList.add('close');
  }

  /*
   * Mutea o activa el sonido del audio del personaje
   */
  btnCharacterSound() {
    this.clickAudio.currentTime = 0;
    this.clickAudio.play();
    if(this.stateCharacterSound === 'active'){
      this.stateCharacterSound = 'inactive';
      this.audioCharacter.muted = true;
    } else {
      this.stateCharacterSound = 'active';
      this.audioCharacter.muted = false;
    }
  }

  drawBoxCharacter(characterObj){
    /* console.log('++ character drawBox characterObj: ', characterObj);
    console.log('++ character drawBox characterObj.left: ', characterObj.left); */
    // $('.box_character').css('left', characterObj.left);
    $('.box_character').css({"left": characterObj.left, "top": characterObj.top, "height": characterObj.height, "width": characterObj.width});
  }

}
