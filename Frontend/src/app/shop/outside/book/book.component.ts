import { Component } from '@angular/core';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent {
  currentPage = 1;

  toggleClass(e, toggleClassName) {
    if(e.className.includes(toggleClassName)) {
      e.classList.remove(toggleClassName);
    } else {
      e.classList.add(toggleClassName);
    }
  }

  movePage(e, page) {
    /* console.log('--------\npage: ', page, ' - e: ', <HTMLDivElement>e);   //obtiene el elemento seleccionado completo con toda su información
    console.log('e target:', <HTMLDivElement>e.target);   //obtiene el elemento actual seleccionado
    console.log('e target localName:', <HTMLDivElement>e.target.localName);   //obtiene el nombre de la etiqueta del elemento seleccionado
    console.log('e className: ', <HTMLDivElement>e.target.className);   //obtiene las clases del elemento actual seleccionado
    console.log('e parentElement: ', <HTMLDivElement>e.target.parentElement);   //obtiene el elemento padre del elemento seleccionado */

    //console.log('<HTMLDivElement>e: ', <HTMLDivElement>e);   //obtiene el elemento seleccionado completo con toda su información
    //console.log('<HTMLDivElement>e.target:', <HTMLDivElement>e.target);   //obtiene el elemento actual seleccionado
    //console.log('<HTMLDivElement>e.target.localName:', <HTMLDivElement>e.target.localName);   //obtiene el nombre de la etiqueta del elemento seleccionado
    //console.log('<HTMLDivElement>e.target.className: ', <HTMLDivElement>e.target.className);   //obtiene las clases del elemento actual seleccionado
    //console.log('<HTMLDivElement>e.target.parentElement: ', <HTMLDivElement>e.target.parentElement);   //obtiene el elemento padre del elemento seleccionado

    /* el elemento clickeado es la division que contiene la información */
    if((<HTMLDivElement>e.target).localName == "div"){
      if (page == this.currentPage) {
        this.currentPage+=2;
        this.toggleClass(<HTMLDivElement>e.target, "left-side");
        this.toggleClass((<HTMLDivElement>e.target).nextElementSibling, "left-side");
      }
      else if (page == this.currentPage - 1) {
        this.currentPage-=2;
        this.toggleClass(<HTMLDivElement>e.target, "left-side");
        this.toggleClass((<HTMLDivElement>e.target).previousElementSibling, "left-side");
      }
    }
    /* el elemento clickeado es el contenedor interno (el elemento hijo) */
    else if ((<HTMLDivElement>e.target).localName == "p") {
      if (page == this.currentPage) {
        this.currentPage+=2;
        this.toggleClass(<HTMLDivElement>e.target.parentElement, "left-side");
        this.toggleClass((<HTMLDivElement>e.target.parentElement).nextElementSibling, "left-side");
        
      }
      else if (page == this.currentPage - 1) {
        this.currentPage-=2;
        this.toggleClass(<HTMLDivElement>e.target.parentElement, "left-side");
        this.toggleClass((<HTMLDivElement>e.target.parentElement).previousElementSibling, "left-side");
      }
    }
  }

}
