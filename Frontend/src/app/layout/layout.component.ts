import { Component, ViewChild } from '@angular/core';
import { MenuComponent } from '../shop/inside/menu/menu.component';
import { AudioService } from '../services/audio.service';
import { ProgressService } from '../services/progress.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {
  @ViewChild('menuId', { static: true }) menuComponent: MenuComponent;
  stateSoundMenu: string;
  showLayout: boolean;
  showModalObjectives = false;
  showModalMenu = false;
  showModalProfile = false;
  lesson1Available: string;
  lesson2Available: string;
  lesson3Available: string;
  lesson4Available: string;
  progressPercentage: number;

  ngOnInit() {
    console.log('layout Init');
  }

  constructor(private router: Router, private audioService: AudioService, private progressService: ProgressService) {
    this.stateSoundMenu = this.audioService.getStateSoundMenu();
    this.showLayout = this.progressService.getShowLayout();
    this.lesson1Available = this.progressService.getLesson1Available();
    this.lesson2Available = this.progressService.getLesson2Available();
    this.lesson3Available = this.progressService.getLesson3Available();
    this.lesson4Available = this.progressService.getLesson4Available();
    this.progressPercentage = this.progressService.getProgressPercentage();
    console.log('layout c: ', this.progressService.getProgressPercentage());
    
    this.audioService.getStateSoundMenu$().subscribe(stateSoundMenu => {
      this.stateSoundMenu = stateSoundMenu;
    });
    this.progressService.getShowLayout$().subscribe(showLayout => {
      this.showLayout = showLayout;
    });
    this.progressService.getLesson1Available$().subscribe(lesson1Available => {
      this.lesson1Available = lesson1Available;
    });
    this.progressService.getLesson2Available$().subscribe(lesson2Available => {
      this.lesson2Available = lesson2Available;
    });
    this.progressService.getLesson3Available$().subscribe(lesson3Available => {
      this.lesson3Available = lesson3Available;
    });
    this.progressService.getLesson4Available$().subscribe(lesson4Available => {
      this.lesson4Available = lesson4Available;
    });
    this.progressService.getProgressPercentage$().subscribe(progressPercentage=> {
      this.progressPercentage = progressPercentage;
      document.getElementById('current_progress').style.height = `${this.progressPercentage}%`;
      console.log('layout c s: ', this.progressService.getProgressPercentage());
    });
    setTimeout(() => {
      document.getElementById('current_progress').style.height = `${this.progressPercentage}%`;
      /* Setear height del progress bar */
      //document.getElementById('current_progress').style.height = `${this.progressPercentage}%`;
      
      // document.getElementById('current_progress').style.height = '80%';
      //$('.current_progress').height(500) ;
      //$('.current_progress').height(`${this.progressPercentage}%`);
    }, 1);

  }

  btnSound() {
    this.audioService.clickAudio.play();
    
    console.log('btnSound\nstateSoundMenu: ', this.audioService.getStateSoundMenu());
    this.audioService.getStateSoundMenu() == 'active' ? this.audioService.pauseBackgroundAudios() : this.audioService.playBackgroundAudios();
    this.audioService.setStateSoundMenu(this.audioService.getStateSoundMenu() == 'active' ? 'inactive' : 'active');
    console.log('stateSoundMenu: ', this.audioService.getStateSoundMenu());

  }

  hoverBtnPostMenu() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.hoverAudio.play();
    }
  }

  leaveBtnPostMenu() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.hoverAudio.pause();
      this.audioService.hoverAudio.currentTime = 0;
    }
  }

  btnPostMainMenu() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    this.showModalMenu = true;
  }

  btnObjectives() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    this.showModalObjectives = true;
  }

  btnProfile() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    this.showModalProfile = true;
  }

  closeMenuModal() {
    this.showModalMenu = false;
  }

  closeObjectivesModal() {
    this.showModalObjectives = false;
  }

  closeProfileModal() {
    this.showModalProfile = false;
  }

  btnMyProfile() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    //pendiente codificación regresar a moodle
  }

  btnReturnHome() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    this.showModalProfile = false;
    this.router.navigate(['/section1']);
  }

  btnCloseSession() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    //pendiente codificación salir de moodle y regresar a la ventara de login
  }

  btnModule(numModule: number) {
    this.closeMenuModal();
    this.router.navigate([`/section1/lesson${numModule}`]);
  }


}
