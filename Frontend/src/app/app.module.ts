import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { routing } from './app.routing';

/* Componentes generales */
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BookComponent } from './shop/outside/book/book.component';
import { BookChqComponent } from './shop/inside/book/book.component';
import { MenuComponent } from './shop/inside/menu/menu.component';

/* Lesson1 (Modulo1) */
import { Lesson1Component } from './section1/lesson1/lesson1.component';
import { S1L1V1Component } from './section1/lesson1/S1L1V1/S1L1V1.component';
import { S1L1V2Component } from './section1/lesson1/S1L1V2/S1L1V2.component';
import { S1L1V3Component } from './section1/lesson1/S1L1V3/S1L1V3.component';
import { S1L1V4Component } from './section1/lesson1/S1L1V4/S1L1V4.component';
import { S1L1V5Component } from './section1/lesson1/S1L1V5/S1L1V5.component';

/* Shop */
import { ModalComponent } from './shop/inside/modal/modal.component';
import { CharacterComponent } from './shop/outside/character/character.component';

/* Carousel */
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  IgxCarouselModule,
  IgxIconModule,
  IgxSelectModule,
  IgxButtonModule,
  IgxCardModule
} from "igniteui-angular";
import { CarouselAnimationsSampleComponent } from './shop/outside/carousel-animations-sample-component/carousel-animations-sample.component';
import { Section1Component } from './section1/section1.component';
import { SoundsManageModule } from './services/sounds-manage/sounds-manage.module';
import { SoftiDirectivesModule } from './directives/softi-directives.module';
import { APP_BASE_HREF, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { ObjectivesComponent } from './objectives/objectives.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    Section1Component,
    Lesson1Component,
    S1L1V1Component,
    S1L1V2Component,
    S1L1V3Component,
    S1L1V4Component,
    S1L1V5Component,
    AppComponent,
    BookComponent,
    BookChqComponent,
    CarouselAnimationsSampleComponent,
    CharacterComponent,
    HomeComponent,
    ModalComponent,
    MenuComponent,
    LayoutComponent,
    ObjectivesComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    routing,
    BrowserAnimationsModule,
    FormsModule,
    IgxCarouselModule,
    IgxIconModule,
    IgxSelectModule,
    IgxButtonModule,
    IgxCardModule,
    SoundsManageModule.forRoot(),
    SoftiDirectivesModule
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }