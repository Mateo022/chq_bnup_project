import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment.qa';

@Injectable()
export class SoundsManageService {

  private hoverSounds: { [key: string]: HTMLAudioElement };
  private clickSounds: { [key: string]: HTMLAudioElement };

  constructor() {
    this.hoverSounds = {
      hoverDefault: new Audio(`../../../assets/section1/sounds/hoverDefault.mp3`)
    };

    this.clickSounds = {
      clickDefault: new Audio(`../../../assets/section1/sounds/clickDefault.mp3`)
    };
  }

  public playHoverSound(key: string){
    if(!this.hoverSounds[key]) { key = 'hoverDefault'; }

    this.hoverSounds[key].currentTime = 0;
    this.hoverSounds[key].play();
  }

  public playClickSound(key: string){
    if(!this.clickSounds[key]) { key = 'clickDefault'; }

    this.clickSounds[key].currentTime = 0;
    this.clickSounds[key].play();
  }
}