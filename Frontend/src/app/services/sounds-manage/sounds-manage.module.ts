import { ModuleWithProviders, NgModule } from '@angular/core';
import { SoundsManageService } from './sounds-manage.service';

@NgModule({})
export class SoundsManageModule {

  private static nameModule: string = 'SoundsManageModule';
  private static isInitialized: boolean = false;

  constructor() {
    /* 
     * Requrimiento 1: llamarlo en al appModule
     */
    if (!SoundsManageModule.isInitialized) {
      throw new Error('Call '+SoundsManageModule.nameModule+'.forRoot() in appModule');
    }
  }

  /* 
   * Unica instancia que se pueda acceder a nivel global  
   */
  static forRoot(): ModuleWithProviders<SoundsManageModule> {

    /* 
     * Requrimiento 2: No se llama nuevamente
     */
    if (this.isInitialized) {
      throw new Error('Do not Call '+SoundsManageModule.nameModule+'.forRoot() multiple times');
    }
  
    this.isInitialized = true;
    return {
      ngModule: SoundsManageModule,
      providers: [ SoundsManageService ]
    }
  }
}