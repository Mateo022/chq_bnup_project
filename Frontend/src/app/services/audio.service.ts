import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  private stateSoundMenu: string;
  stateSoundMenu$: Subject<string>;

  hoverAudio = new Audio(`../assets/section1/sounds/hoverDefault.mp3`);
  lightBulb1Audio = new Audio(`../assets/section1/sounds/lightBulb.mp3`);
  moveBtn = new Audio(`../assets/section1/sounds/SWISH_13.wav`);
  clickAudio = new Audio(`../assets/section1/sounds/clickDefault.mp3`);

  backgroundAudios: any = {
    audioSoundtrack: new Audio(`../../assets/section1/sounds/background_sound.mp3`),
  };
  

  //Musica de fondo
  playBackgroundAudios() {
    this.backgroundAudios["audioSoundtrack"].volume = 0.1; //0.3;
    this.backgroundAudios["audioSoundtrack"].play();
  }
  pauseBackgroundAudios() {
    this.backgroundAudios["audioSoundtrack"].pause();
  }
  
  setStateSoundMenu(stateSoundMenu:string) {
    this.stateSoundMenu = stateSoundMenu;
    this.stateSoundMenu$.next(this.stateSoundMenu);
  }

  getStateSoundMenu() : string {
    return this.stateSoundMenu;
  }

  getStateSoundMenu$(): Observable<string>{
    return this.stateSoundMenu$.asObservable();
  }

  variable = "varInicial"; 
  muted = false;
  current: string[] = [];
  currentBackground = "";
  
  constructor() { 
    this.setOnEndedBackgroundAudios();
    this.stateSoundMenu = 'active';
    this.stateSoundMenu$ = new Subject();
  }

  setBackground(backgroundKey: string) {
    if (this.backgroundAudios[`${this.currentBackground}`]) {
      this.backgroundAudios[`${this.currentBackground}`].pause();
    }
    this.currentBackground = backgroundKey;
    if (this.backgroundAudios[`${this.currentBackground}`] && !this.muted) {
      this.backgroundAudios[`${this.currentBackground}`].volume = 0.3;
      this.backgroundAudios[`${this.currentBackground}`].play();
    }
  }

  setOnEndedBackgroundAudios() {
    for (const audio of Object.keys(this.backgroundAudios)) {
      this.backgroundAudios[`${audio}`].onended = () => {
        this.backgroundAudios[`${audio}`].play();
      }
    }
  }

}
