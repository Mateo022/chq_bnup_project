import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressService {
  private showLayout: boolean;
  showLayout$: Subject<boolean>;
  private showTooltipsSection1: boolean;
  showTooltipsSection1$: Subject<boolean>;
  private lesson1Available: string;
  lesson1Available$: Subject<string>;
  private lesson2Available: string;
  lesson2Available$: Subject<string>;
  private lesson3Available: string;
  lesson3Available$: Subject<string>;
  private lesson4Available: string;
  lesson4Available$: Subject<string>;
  private progressPercentage: number;
  progressPercentage$: Subject<number>;


  constructor() { 
    this.showLayout = true;
    this.showLayout$ = new Subject();
    this.showTooltipsSection1 = true;
    this.showTooltipsSection1$ = new Subject();
    this.lesson1Available = 'active';
    this.lesson1Available$ = new Subject();
    this.lesson2Available = 'active';
    this.lesson2Available$ = new Subject();
    this.lesson3Available = 'active';
    this.lesson3Available$ = new Subject();
    this.lesson4Available = 'active';
    this.lesson4Available$ = new Subject();
    this.progressPercentage = 0;
    this.progressPercentage$ = new Subject();
  }

  /* Observables para showLayout */
  getShowLayout() : boolean {
    return this.showLayout;
  }
  setShowLayout(showLayout:boolean) {
    this.showLayout = showLayout;
    this.showLayout$.next(this.showLayout);
  }
  getShowLayout$(): Observable<boolean>{
    return this.showLayout$.asObservable();
  }

  /* Observables para showTooltipsSection1 */
  getShowTooltipsSection1() : boolean {
    return this.showTooltipsSection1;
  }
  setShowTooltipsSection1(showTooltipsSection1:boolean) {
    this.showTooltipsSection1 = showTooltipsSection1;
    this.showTooltipsSection1$.next(this.showTooltipsSection1);
  }
  getShowTooltipsSection1$(): Observable<boolean>{
    return this.showTooltipsSection1$.asObservable();
  }

  /* Observables para lesson1Available */
  getLesson1Available() : string {
    return this.lesson1Available;
  }
  setLesson1Available(lesson1Available:string) {
    this.lesson1Available = lesson1Available;
    this.lesson1Available$.next(this.lesson1Available);
  }
  getLesson1Available$(): Observable<string>{
    return this.lesson1Available$.asObservable();
  }

  /* Observables para lesson2Available */
  getLesson2Available() : string {
    return this.lesson2Available;
  }
  setLesson2Available(lesson2Available:string) {
    this.lesson2Available = lesson2Available;
    this.lesson2Available$.next(this.lesson2Available);
  }
  getLesson2Available$(): Observable<string>{
    return this.lesson2Available$.asObservable();
  }

  /* Observables para lesson3Available */
  getLesson3Available() : string {
    return this.lesson3Available;
  }
  setLesson3Available(lesson3Available:string) {
    this.lesson3Available = lesson3Available;
    this.lesson3Available$.next(this.lesson3Available);
  }
  getLesson3Available$(): Observable<string>{
    return this.lesson3Available$.asObservable();
  }

  /* Observables para lesson4Available */
  getLesson4Available() : string {
    return this.lesson4Available;
  }
  setLesson4Available(lesson4Available:string) {
    this.lesson4Available = lesson4Available;
    this.lesson4Available$.next(this.lesson4Available);
  }
  getLesson4Available$(): Observable<string>{
    return this.lesson4Available$.asObservable();
  }

  /* Observables para progress bar percentage */
  getProgressPercentage() : number {
    return this.progressPercentage;
  }
  setProgressPercentage(progressPercentage:number) {
    this.progressPercentage = progressPercentage;
    this.progressPercentage$.next(this.progressPercentage);
  }
  getProgressPercentage$(): Observable<number>{
    return this.progressPercentage$.asObservable();
  }
  
}
