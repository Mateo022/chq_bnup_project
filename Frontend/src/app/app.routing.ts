import { RouterModule, Routes } from '@angular/router';
/* General */
/* import { BookComponent } from './shop/outside/book/book.component'; */
import { BookChqComponent } from './shop/inside/book/book.component';
import { HomeComponent  } from './home/home.component';
import { Section1Component } from './section1/section1.component';
import { ModalComponent } from './shop/inside/modal/modal.component';
import { CharacterComponent } from './shop/outside/character/character.component';
import { CarouselAnimationsSampleComponent } from './shop/outside/carousel-animations-sample-component/carousel-animations-sample.component';

/* Lesson1 (Modulo1) */
import { Lesson1Component } from './section1/lesson1/lesson1.component';
import { S1L1V1Component } from './section1/lesson1/S1L1V1/S1L1V1.component';
import { S1L1V2Component } from './section1/lesson1/S1L1V2/S1L1V2.component';
import { S1L1V3Component } from './section1/lesson1/S1L1V3/S1L1V3.component';


const appRoutes = [
    { path: '', component: HomeComponent },
    { path: 'section1', component: Section1Component },
    { path: 'section1/lesson1', component: Lesson1Component },
    { path: 'section1/lesson1/view1', component: S1L1V1Component },
    { path: 'section1/lesson1/view2', component: S1L1V2Component },
    { path: 'section1/lesson1/view3', component: S1L1V3Component },
    /* { path: 'book', component: BookComponent }, */
    { path: 'bookChq', component: BookChqComponent },
    { path: 'modal', component: ModalComponent }, 
    { path: 'character', component:  CharacterComponent },
    { path: 'carousel', component: CarouselAnimationsSampleComponent },
];

export const routing = RouterModule.forRoot(appRoutes, {useHash: true});