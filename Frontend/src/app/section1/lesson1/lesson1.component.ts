import { Component, ElementRef } from '@angular/core';
import { OnInit, ViewChild, Output, Input, EventEmitter } from '@angular/core';
import { ModalComponent } from 'src/app/shop/inside/modal/modal.component';
import { S1L1V1Component } from './S1L1V1/S1L1V1.component';
import { S1L1V2Component } from './S1L1V2/S1L1V2.component';
import { S1L1V3Component } from './S1L1V3/S1L1V3.component';
import { S1L1V4Component } from './S1L1V4/S1L1V4.component';
import { S1L1V5Component } from './S1L1V5/S1L1V5.component';
import { CharacterComponent } from 'src/app/shop/outside/character/character.component';
import { environment as env } from 'src/environments/environment.qa';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { ProgressService } from 'src/app/services/progress.service';

@Component({
  selector: 'app-lesson1',
  templateUrl: './lesson1.component.html',
  styleUrls: ['./lesson1.component.css']
})
export class Lesson1Component {
  stateSoundMenu = 'active';
  stateCharacterText = 'active';
  stateCharacterPlay = 'active';
  stateCharacterSound = 'active';
  currentView: number = 1;
  characterObject: Object = {
    'left': '82%',
    'top': '10%',
    'height': '89%',
    'width': '18.5%'
  };

  /* Mensaje que será enviado al componente modal */
  modalMessage: string;
  /* audio al pasar sobre los botones */
  hoverAudio = new Audio(`../../../assets/section1/sounds/hoverDefault.mp3`);
  /* audio al hacer click en los botones */
  clickAudio = new Audio(`../../../assets/section1/sounds/clickDefault.mp3`);


  @ViewChild('modalId', { static: true }) modalComponent: ModalComponent;
  @ViewChild('view1Id', { static: true }) view1Component: S1L1V1Component;
  @ViewChild('view2Id', { static: true }) view2Component: S1L1V2Component;
  @ViewChild('view3Id', { static: true }) view3Component: S1L1V3Component;
  @ViewChild('view4Id', { static: true }) view4Component: S1L1V4Component;
  @ViewChild('view5Id', { static: true }) view5Component: S1L1V5Component;
  @ViewChild('characterId', { static: true }) characterComponent: CharacterComponent;

  constructor(private router: Router, private progressService: ProgressService) {
  }

  ngOnInit(): void {
    console.log('--- lesson1 --- ngOnInit');
    this.modalMessage = this.view1Component.modalMessage;
    this.modalComponent.startModal(this.modalMessage);
    $(`app-S1L1V2`).css('display', 'none');
    $(`app-S1L1V3`).css('display', 'none');
    $(`app-S1L1V4`).css('display', 'none');
    $(`app-S1L1V5`).css('display', 'none');
    this.characterComponent.drawBoxCharacter(this.view1Component.characterObject);
    this.characterComponent.audioCharacter = this.view1Component.audiosCharacter[1];
  }

  /* Funciones Menu lateral izquierdo */
  btnPostSound() {
    this.clickAudio.play();
    if (this.stateSoundMenu === 'active') {
      this.stateSoundMenu = 'inactive';
    } else {
      this.stateSoundMenu = 'active';
    }
  }

  btnPostMainMenu() {
    this.clickAudio.play();
  }

  btnPostObjectives() {
    this.clickAudio.play();
  }

  btnPostProfile() {
    this.clickAudio.play();
  }

  hoverBtnPostMenu() {
    if (this.stateSoundMenu === 'active') {
      this.hoverAudio.play();
    } 1
  }

  leaveBtnPostMenu() {
    this.hoverAudio.pause();
    this.hoverAudio.currentTime = 0;
    //this.clickAudio.play();
  }

  changeView(numView) {
    console.log('--- lesson1 --- changeView: ', numView, ' - currentView: ', this.currentView);
    if (this.currentView == 2) {
      this.view2Component.stopSlider()
      console.log("STOP-----");
    }
    if (numView == 'prev' && this.currentView > 1) { // previus
      $(`.point${this.currentView}`).removeClass('point_current');
      this.currentView = this.currentView - 1;
      $(`.point${this.currentView}`).addClass('point_current');
      $(`.bganimation`).removeClass('animated fadeInRightBig');
      setTimeout(() => {
        $(`app-S1L1V${this.currentView + 1}`).css('display', 'none');
      }, 1000);
      setTimeout(() => {
        $(`.bganimation`).addClass('animated fadeOutLeftBig');
        setTimeout(() => {
          $(`app-S1L1V${this.currentView}`).css('display', 'block');
          $(`.bganimation`).removeClass('animated fadeOutLeftBig');
          $(`.bganimation`).addClass('animated fadeInRightBig');
          if (this.currentView == 1) {
            this.characterComponent.drawBoxCharacter(this.view1Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view1Component.audiosCharacter[this.view1Component.currentView];
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          } else if (this.currentView == 2) {
            this.characterComponent.drawBoxCharacter(this.view2Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view2Component.audioCharacter;
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
            this.view2Component.sliderAuto();
          } else if (this.currentView == 3) {
            this.characterComponent.drawBoxCharacter(this.view3Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.stateCharacterSound = 'active';
          }
          else if (this.currentView == 4) {
            this.characterComponent.drawBoxCharacter(this.view4Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view4Component.audiosCharacter[1];
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          }
          else {
            this.characterComponent.drawBoxCharacter(this.view5Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view5Component.audioCharacter;
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          }
        }, 1000);
      }, 1);
    }
    else if (numView == 'next' && this.currentView < 5) { // next
      // $(`app-S1L1V${this.currentView}`).css('display', 'none');
      $(`.point${this.currentView}`).removeClass('point_current');
      this.currentView = this.currentView + 1;
      $(`.point${this.currentView}`).addClass('point_current');
      $(`.bganimation`).removeClass('animated fadeInRightBig');
      setTimeout(() => {
        $(`app-S1L1V${this.currentView - 1}`).css('display', 'none');
        if (this.progressService.getProgressPercentage() < (8.5 * (this.currentView - 1))) {
          this.progressService.setProgressPercentage((8.5 * (this.currentView - 1)));
        };
      }, 1000);
      setTimeout(() => {
        $(`.bganimation`).addClass('animated fadeOutLeftBig');
        setTimeout(() => {
          $(`app-S1L1V${this.currentView}`).css('display', 'block');
          $(`.bganimation`).removeClass('animated fadeOutLeftBig');
          $(`.bganimation`).addClass('animated fadeInRightBig');
          if (this.currentView == 1) {
            this.characterComponent.btnCharacterReset();
            this.characterComponent.drawBoxCharacter(this.view1Component.characterObject);
            this.characterComponent.audioCharacter = this.view1Component.audioCharacter;
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          }
          else if (this.currentView == 2) {
            this.characterComponent.drawBoxCharacter(this.view2Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view2Component.audioCharacter;
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
            this.view2Component.sliderAuto();
          }
          else if (this.currentView == 3) {
            this.characterComponent.drawBoxCharacter(this.view3Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.stateCharacterSound = 'active';
          }
          else if (this.currentView == 4) {
            this.characterComponent.drawBoxCharacter(this.view4Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view4Component.audiosCharacter[1];
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          }
          else if (this.currentView == 5) {
            this.characterComponent.drawBoxCharacter(this.view5Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view5Component.audioCharacter;
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          } /* else {
            this.progressService.setLesson2Available('active');
          } */
        }, 1000);
      }, 1);
    } else if (numView == 'next' && this.currentView == 5) {
      this.view5Component.openModal();
      this.characterComponent.btnCharacterReset();
      this.view5Component.showModalFinish = true;
      this.progressService.setLesson2Available('active');
    }
    else if (numView == 1 || numView == 2 || numView == 3 || numView == 4 || numView == 5) {
      let presentView = this.currentView;
      $(`.bganimation`).removeClass('animated fadeInRightBig');
      setTimeout(() => {
        $(`app-S1L1V${presentView}`).css('display', 'none');
        $(`.point${presentView}`).removeClass('point_current');
      }, 1000);
      setTimeout(() => {
        $(`.bganimation`).addClass('animated fadeOutLeftBig');
        setTimeout(() => {
          $(`app-S1L1V${this.currentView}`).css('display', 'block');
          $(`.point${this.currentView}`).addClass('point_current');
          $(`.bganimation`).removeClass('animated fadeOutLeftBig');
          $(`.bganimation`).addClass('animated fadeInRightBig');
          if (this.progressService.getProgressPercentage() < (8.5 * numView - 1)) {
            this.progressService.setProgressPercentage((8.5 * (numView - 1)));
          };
        }, 1000);
      }, 1);
      this.currentView = numView;
      if (numView == 1) {
        this.characterComponent.drawBoxCharacter(this.view1Component.characterObject);
        this.characterComponent.btnCharacterReset();
        this.characterComponent.audioCharacter = this.view1Component.audiosCharacter[this.view1Component.currentView];
        setTimeout(() => {
          this.characterComponent.stateCharacterSound = 'active';
          this.characterComponent.btnCharacterPlay();
        }, 1000);
      } else if (numView == 2) {
        this.characterComponent.drawBoxCharacter(this.view2Component.characterObject);
        this.characterComponent.btnCharacterReset();
        this.characterComponent.audioCharacter = this.view2Component.audioCharacter;
        setTimeout(() => {
          this.characterComponent.stateCharacterSound = 'active';
          this.characterComponent.btnCharacterPlay();
        }, 1000);
        this.view2Component.sliderAuto();
      } else if (numView == 3) {
        this.characterComponent.drawBoxCharacter(this.view3Component.characterObject);
        this.characterComponent.btnCharacterReset();
        this.characterComponent.stateCharacterSound = 'active';
      }
      else if (numView == 4) {
        this.characterComponent.drawBoxCharacter(this.view4Component.characterObject);
        this.characterComponent.btnCharacterReset();
        this.characterComponent.audioCharacter = this.view4Component.audiosCharacter[1];
        setTimeout(() => {
          this.characterComponent.stateCharacterSound = 'active';
          this.characterComponent.btnCharacterPlay();
        }, 1000);
      }
      else if (numView == 5) {
        console.log('NUMERO 5');
        this.characterComponent.drawBoxCharacter(this.view5Component.characterObject);
        this.characterComponent.btnCharacterReset();
        this.characterComponent.audioCharacter = this.view5Component.audioCharacter;
        setTimeout(() => {
          this.characterComponent.stateCharacterSound = 'active';
          this.characterComponent.btnCharacterPlay();
        }, 1000);
      }
    }
  }

  changeAudioCharacterView(numView: string, numPage: number) {
    console.log('lesson1--> numView: ', numView, ' - numPage: ', numPage );
    
    this.characterComponent.btnCharacterReset();
    if(numView == 'view1'){
      console.log('COND view1');
      this.characterComponent.audioCharacter = this.view1Component.audiosCharacter[numPage];
      setTimeout(() => {
        this.characterComponent.btnCharacterPlay();
      }, 200);
    } else if(numView == 'view3'){
      console.log('COND view3');
      this.characterComponent.audioCharacter = this.view3Component.audiosCharacter[numPage];
      setTimeout(() => {
        this.characterComponent.btnCharacterPlay();
      }, 200);
    } else if(numView == 'view4'){
      console.log('COND view4');
      this.characterComponent.audioCharacter = this.view4Component.audiosCharacter[numPage];
      setTimeout(() => {
        this.characterComponent.btnCharacterPlay();
      }, 200);
    }
  }

  /*
   * Oculta el modal ejecutado al cargar el componente
   */
  closeModal(event: boolean) {
    if (event) {
      //console.log('--- lesson1 --- closeModal()');
      //this.modalComponent.closeModal();
      this.characterComponent.btnCharacterPlay();
    }
  }
}
