import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { CharacterComponent } from 'src/app/shop/outside/character/character.component';
import { IgxCarouselComponent } from 'igniteui-angular';
import { environment as env } from 'src/environments/environment.qa';
import * as $ from 'jquery';
import { ProgressService } from 'src/app/services/progress.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-S1L1V5',
  templateUrl: './S1L1V5.component.html',
  styleUrls: ['./S1L1V5.component.css']
})
export class S1L1V5Component {
stateSoundMenu = 'active';
  @Output() isClicked: EventEmitter<boolean>;
  showModalFinish: boolean = false;

  characterObject: Object = {
    'left': '79%',
    'top': '32%',
    'height': '59%',
    'width': '13.5%'
  };

  /* Mensaje que será enviado al componente modal */
  modalMessage: string = "Lee con atención la información que te presentamos, navega con las flechas y visualiza todas las imagenes. Al finalizar continua navegando por el curso.";

  /* Audio del personaje leyendo el texto principal */
  audioCharacter = new Audio(`../../../../assets/section1/sounds/Servicios.mp3`);

  @ViewChild('characterId', { static: true }) characterComponent: CharacterComponent;
  constructor(private router: Router, private progressService: ProgressService) {
  }
  ngOnInit(): void {
    console.log('-- S1L1V5 ngOnInit');
    this.addSlides();
  }

  /* Slide de imagenes (Carousel) */
  @ViewChild('carousel', { static: true }) public carousel: IgxCarouselComponent;

  public slides: any[] = [];
  public animations = ['fade'];
  /* public animations = ['slide', 'fade', 'none']; */

  public addSlides() {
    this.slides.push(
      { image: 'imagen1' },
      { image: 'imagen2' },
      { image: 'imagen3' },
      { image: 'imagen4' },
      { image: 'imagen5' },
      { image: 'imagen6' },
      { image: 'imagen7' },
      { image: 'imagen8' },
      { image: 'imagen9' },
      { image: 'imagen10' },
    );
  }

  /*
 * Muestra el modal de finalización del juego
 */
  openModal() {
    $("#modal_fin").show();
    $(`app-layout.layout1`)[0].childNodes[0].style.zIndex = 1;
  }

  closeModal() {
    $("#modal_fin").hide();
    this.showModalFinish = false;
    if (this.progressService.getProgressPercentage() < 25.5) {
      this.progressService.setProgressPercentage(25.5);
    };
    this.progressService.setLesson2Available('active');
    this.router.navigate(['/section1']);
  }

}