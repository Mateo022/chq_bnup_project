import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { CharacterComponent } from 'src/app/shop/outside/character/character.component';
import { environment as env } from 'src/environments/environment.qa';
import * as $ from 'jquery';

@Component({
  selector: 'app-S1L1V2',
  templateUrl: './S1L1V2.component.html',
  styleUrls: ['./S1L1V2.component.css']
})
export class S1L1V2Component {
  stateSoundMenu = 'active';
  /* Tamaño y posición para dibujar la caja del personaje */
  characterObject: Object = {
    'left': '81%',
    'top': '32%',
    'height': '59%',
    'width': '13.5%'
  };
  timeout = null;

  /* Mensaje que será enviado al componente modal */
  modalMessage: string = "Lee con atención la información que te presentamos, al finalizar continua navegando por el curso.";
  
  /* Audio del personaje leyendo el texto principal */
  audioCharacter = new Audio(`../../../../assets/section1/sounds/Que_son_los_BNUP.mp3`);

  /* @ViewChild('modalId', {static : true}) modalComponent : ModalComponent; */
  @ViewChild('characterId', { static: true }) characterComponent: CharacterComponent;

  ngOnInit(): void {
    console.log('-- S1L1V2 ngOnInit');
  }

  sliderAuto() {
    let front = 1, der = 2, oculto1 = 3, oculto2 = 4, oculto3 = 5, oculto4 = 6, oculto5 = 7, oculto6 = 8, izq = 9;

    $(`#song-1`).removeClass('frontal derecha izquierda oculto');
    $(`#song-2`).removeClass('frontal derecha izquierda oculto');
    $(`#song-3`).removeClass('frontal derecha izquierda oculto');
    $(`#song-4`).removeClass('frontal derecha izquierda oculto');
    $(`#song-5`).removeClass('frontal derecha izquierda oculto');
    $(`#song-6`).removeClass('frontal derecha izquierda oculto');
    $(`#song-7`).removeClass('frontal derecha izquierda oculto');
    $(`#song-8`).removeClass('frontal derecha izquierda oculto');
    $(`#song-9`).removeClass('frontal derecha izquierda oculto');


   this.timeout = setInterval(() => {
      $(`#song-1`).removeClass('frontal derecha izquierda');
      $(`#song-2`).removeClass('frontal derecha izquierda');
      $(`#song-3`).removeClass('frontal derecha izquierda');
      $(`#song-4`).removeClass('frontal derecha izquierda');
      $(`#song-5`).removeClass('frontal derecha izquierda');
      $(`#song-6`).removeClass('frontal derecha izquierda');
      $(`#song-7`).removeClass('frontal derecha izquierda');
      $(`#song-8`).removeClass('frontal derecha izquierda');
      $(`#song-9`).removeClass('frontal derecha izquierda');

      setTimeout(() => {
        $(`#song-${front}`).addClass('frontal');

        setTimeout(() => {

          $(`#song-1`).removeClass('oculto');
          $(`#song-2`).removeClass('oculto');
          $(`#song-3`).removeClass('oculto');
          $(`#song-4`).removeClass('oculto');
          $(`#song-5`).removeClass('oculto');
          $(`#song-6`).removeClass('oculto');
          $(`#song-7`).removeClass('oculto');
          $(`#song-8`).removeClass('oculto');
          $(`#song-9`).removeClass('oculto');
          setTimeout(() => {
            
            $(`#song-${der}`).addClass('derecha');
            $(`#song-${oculto1}`).addClass('oculto');
            $(`#song-${oculto2}`).addClass('oculto');
            $(`#song-${oculto3}`).addClass('oculto');
            $(`#song-${oculto4}`).addClass('oculto');
            $(`#song-${oculto5}`).addClass('oculto');
            $(`#song-${oculto6}`).addClass('oculto');
            $(`#song-${izq}`).addClass('izquierda');
            
            if (front == 1) {
              front = 2;
              der = 3;
              oculto1 = 4;
              oculto2 = 5;
              oculto3 = 6;
              oculto4 = 7;
              oculto5 = 8;
              oculto6 = 9;
              izq = 1;
            } else if (front == 2) {
              front = 3;
              der = 4;
              oculto1 = 5;
              oculto2 = 6;
              oculto3 = 7;
              oculto4 = 8;
              oculto5 = 9;
              oculto6 = 1;
              izq = 2;
            } else if(front == 3) {
              front = 4;
              der = 5;
              oculto1 = 6;
              oculto2 = 7;
              oculto3 = 8;
              oculto4 = 9;
              oculto5 = 1;
              oculto6 = 2;
              izq = 3;
            } else if(front == 4){
              front = 5;
              der = 6;
              oculto1 = 7;
              oculto2 = 8;
              oculto3 = 9;
              oculto4 = 1;
              oculto5 = 2;
              oculto6 = 3;
              izq = 4;
            } 
            else if(front == 5){
              front = 6;
              der = 7;
              oculto1 = 8;
              oculto2 = 9;
              oculto3 = 1;
              oculto4 = 2;
              oculto5 = 3;
              oculto6 = 4;
              izq = 5;
            }
            else if(front == 6){
              front = 7;
              der = 8;
              oculto1 = 9;
              oculto2 = 1;
              oculto3 = 2;
              oculto4 = 3;
              oculto5 = 4;
              oculto6 = 5;
              izq = 6;
            }
            else if(front == 7){
              front = 8;
              der = 9;
              oculto1 = 1;
              oculto2 = 2;
              oculto3 = 3;
              oculto4 = 4;
              oculto5 = 5;
              oculto6 = 6;
              izq = 7;
            }
            else if(front == 8){
              front = 9;
              der = 1;
              oculto1 = 2;
              oculto2 = 3;
              oculto3 = 4;
              oculto4 = 5;
              oculto5 = 6;
              oculto6 = 7;
              izq = 8;
            }else {
              front = 1;
              der = 2;
              oculto1 = 3;
              oculto2 = 4;
              oculto3 = 5;
              oculto4 = 6;
              oculto5 = 7;
              oculto6 = 8;
              izq = 9;
            }
          }, 3);
        }, 100);
      }, 100);
    }, 3000);
  }

  stopSlider(){
    clearInterval(this.timeout)
  }

}
