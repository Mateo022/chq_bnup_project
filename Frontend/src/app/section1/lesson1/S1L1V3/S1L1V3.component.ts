import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
//import { ModalComponent } from 'src/app/shop/inside/modal/modal.component';
import { CharacterComponent } from 'src/app/shop/outside/character/character.component';
import { IgxCarouselComponent } from 'igniteui-angular';
import { environment as env } from 'src/environments/environment.qa';
import * as $ from 'jquery';
import { ProgressService } from 'src/app/services/progress.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-S1L1V3',
  templateUrl: './S1L1V3.component.html',
  styleUrls: ['./S1L1V3.component.css']
})
export class S1L1V3Component {
  stateSoundMenu = 'active';
  @Output() isClicked: EventEmitter<number>;
  showModalFinish: boolean = false;

  /* Tamaño y posición para dibujar la caja del personaje */
  characterObject: Object = {
    'left': '79%',
    'top': '32%',
    'height': '59%',
    'width': '13.5%'
  };

  /* Audios del personaje de sus tres textos */
  audiosCharacter = {
    1: new Audio(`../../../../assets/section1/sounds/lesson1/S1L1V3_1.mp3`),
    2: new Audio(`../../../../assets/section1/sounds/lesson1/S1L1V3_2.mp3`),
    3: new Audio(`../../../../assets/section1/sounds/lesson1/S1L1V3_3.mp3`),
  }

  /* Mensaje que será enviado al componente modal */
  modalMessage: string = "Lee con atención la información que te presentamos, navega con las flechas y visualiza todas las imagenes. Al finalizar continua navegando por el curso.";


  //@ViewChild('modalId', {static : true}) modalComponent : ModalComponent;
  @ViewChild('characterId', { static: true }) characterComponent: CharacterComponent;

  constructor(private router: Router, private progressService: ProgressService) {
    this.isClicked = new EventEmitter<number>();
  }

  ngOnInit(): void {
    console.log('-- S1L1V3 ngOnInit');
  }

  viewInfo(inf: number) {
    $(`.info${inf}`).css('display', 'block');
    $(`.img${inf}`).removeClass('rubberBand');
    if (inf == 3) {
      $(`.warning`).removeClass('rubberBand');
    }
    $(`.img${inf}`).css('hover', 'background-color: #2a6c79k');
    this.isClicked.emit(inf);
  }

  /*
 * Muestra el modal de finalización del juego
 */
  openModal() {
    $("#modal_fin").show();
    $(`app-layout.layout1`)[0].childNodes[0].style.zIndex = 1;
  }

  closeModal() {
    $("#modal_fin").hide();
    this.showModalFinish = false;
    if (this.progressService.getProgressPercentage() < 25.5) {
      this.progressService.setProgressPercentage(25.5);
    };
    this.progressService.setLesson2Available('active');
    this.router.navigate(['/section1']);
  }
}

