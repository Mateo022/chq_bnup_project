import { ComponentFixture, TestBed } from '@angular/core/testing';

import { S1L1V4Component } from './S1L1V4.component';

describe('S1L1V4Component', () => {
  let component: S1L1V4Component;
  let fixture: ComponentFixture<S1L1V4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ S1L1V4Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(S1L1V4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
