import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { CharacterComponent } from 'src/app/shop/outside/character/character.component';
import { IgxCarouselComponent } from 'igniteui-angular';
import { environment as env } from 'src/environments/environment.qa';
import * as $ from 'jquery';
import { ProgressService } from 'src/app/services/progress.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-S1L1V4',
  templateUrl: './S1L1V4.component.html',
  styleUrls: ['./S1L1V4.component.css']
})
export class S1L1V4Component {
  stateSoundMenu = 'active';
  @Output() isClicked: EventEmitter<number>;
  showModalFinish: boolean = false;

  characterObject: Object = {
    'left': '79%',
    'top': '32%',
    'height': '59%',
    'width': '13.5%'
  };

  /* Mensaje que será enviado al componente modal */
  modalMessage: string = "Lee con atención la información que te presentamos, navega con las flechas y visualiza todas las imagenes. Al finalizar continua navegando por el curso.";

  /* Audios del personaje leyendo el texto principal y el texto oculto */
  audiosCharacter = {
    1: new Audio(`../../../../assets/section1/sounds/Estatutos.mp3`),
    2: new Audio(`../../../../assets/section1/sounds/lesson1/S1L1V4_2.mp3`)
  }

  @ViewChild('characterId', { static: true }) characterComponent: CharacterComponent;

  constructor(private router: Router, private progressService: ProgressService) {
    this.isClicked = new  EventEmitter<number>;
  }

  ngOnInit(): void {
    console.log('-- S1L1V4 ngOnInit');
  }

  viewDetails(){
    // $(`.li`).css('display', 'block');
    this.isClicked.emit(2)
    setTimeout(() => {
      $(`.li1`).css('display', 'block');
      setTimeout(() => {
        $(`.li2`).css('display', 'block');
        setTimeout(() => {
          $(`.li3`).css('display', 'block');
          setTimeout(() => {
            $(`.li4`).css('display', 'block');
            setTimeout(() => {
              $(`.li5`).css('display', 'block');
              setTimeout(() => {
                $(`.li6`).css('display', 'block');
              }, 1000);
            }, 1000);
          }, 1000);
        }, 1000);
      }, 1000);
    }, 200);
  }
  
}
