import { Component, OnInit, ViewChild, Output, Input, EventEmitter, ElementRef } from '@angular/core';
import { CharacterComponent } from 'src/app/shop/outside/character/character.component';
import data from '../../../../../server/lesson1/texts.json'
import * as $ from 'jquery';

@Component({
  selector: 'app-S1L1V1',
  templateUrl: './S1L1V1.component.html',
  styleUrls: ['./S1L1V1.component.css']
})
export class S1L1V1Component implements OnInit{
  @Output() isClicked: EventEmitter<number>;

  constructor() {
    this.isClicked = new EventEmitter<number>();
  }
  arrayQuestion = data;
  currentQuestion = 0;
  currentView = 1;
  stateSoundMenu = 'active';
  /* Tamaño y posición para dibujar la caja del personaje */
  characterObject: Object = {
    'left': '79%',
    'top': '32%',
    'height': '59%',
    'width': '13.5%'
  };

  audiosCharacter = {
    1 : new Audio('../../../../assets/section1/sounds/Introduccion.mp3'),
    2 : new Audio(`../../../../assets/section1/sounds/Objetivos.mp3`)
  }

  /* Mensaje que será enviado al componente modal */
  modalMessage: string = "Lee con atención la información que te presentamos, al finalizar mira el video de introducción y continua navegando por el curso.";
  
  /* Audio del personaje leyendo el texto principal */
  audioCharacter = new Audio('../../../../assets/section1/sounds/Introduccion.mp3');

  //textCharacter = new Audio('../../../assets/transcriptions/Lesson1View1.vtt');
  //srcTrack = "Lesson1View1.vtt";
  //soundPath = "Lesson1_view1.mp3";

//  @ViewChild('modalId', {static : true}) modalComponent : ModalComponent;

  ngOnInit(): void {
    console.log('-- S1L1V1 ngOnInit');
  }

  getCurrentQuestion() {
    return this.arrayQuestion[this.currentQuestion];
  }

  changeViews(numView) {
    this.currentView = numView + 1;
    $(`.point0${this.currentQuestion+1}`).removeClass('point_current');
    this.currentQuestion = numView;
    $(`.point0${this.currentQuestion+1}`).addClass('point_current');
    this.isClicked.emit(this.currentView);
  }

}
