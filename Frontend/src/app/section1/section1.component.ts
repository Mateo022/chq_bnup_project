import { Component, ViewChild } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { trigger, transition, style, animate } from '@angular/animations';
import { environment as env } from 'src/environments/environment';
import * as $ from 'jquery';
import { MenuComponent } from '../shop/inside/menu/menu.component';
import { LayoutComponent } from '../layout/layout.component';
import { AudioService } from '../services/audio.service';
import { ProgressService } from '../services/progress.service';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-section1',
  templateUrl: './section1.component.html',
  styleUrls: ['./section1.component.css']
})
export class Section1Component {
  users: any;
  stateSoundMenu: string;
  showTooltips: boolean;
  lesson1Available: string;
  lesson2Available: string;
  lesson3Available: string;
  lesson4Available: string;
  // hoverAudio = new Audio(`../../assets/section1/sounds/hoverDefault.mp3`);
  // clickAudio = new Audio(`../../assets/section1/sounds/clickDefault.mp3`);

  /* Valida si el toolTip debe mostrarse (true) o no (false) */
  tooltipNavigatorBar = false;
  tooltipMenu = false;
  tooltipObjectives = false;
  tooltipSound = false;
  tooltipProfile = false;
  tooltipModulesBar = false;
  tooltipModule1 = false;
  tooltipModule2 = false;
  tooltipProgress = false;

  constructor(private router: Router, private audioService: AudioService, private progressService: ProgressService, private apiService: ApiService ) {
    this.stateSoundMenu = this.audioService.getStateSoundMenu();
    this.showTooltips = this.progressService.getShowTooltipsSection1();
    this.lesson1Available = this.progressService.getLesson1Available();
    this.lesson2Available = this.progressService.getLesson2Available();
    this.lesson3Available = this.progressService.getLesson3Available();
    this.lesson4Available = this.progressService.getLesson4Available();
    this.progressService.getProgressPercentage$().subscribe(progressPercentage => {      
    })
    this.audioService.getStateSoundMenu$().subscribe(stateSoundMenu => {
      this.stateSoundMenu = stateSoundMenu;
    });
    this.progressService.getShowTooltipsSection1$().subscribe(showTooltips => {
      this.showTooltips = showTooltips;
    });
    this.progressService.getLesson1Available$().subscribe(lesson1Available => {
      this.lesson1Available = lesson1Available;
    });
    this.progressService.getLesson2Available$().subscribe(lesson2Available => {
      this.lesson2Available = lesson2Available;
    });
    this.progressService.getLesson3Available$().subscribe(lesson3Available => {
      this.lesson3Available = lesson3Available;
    });
    this.progressService.getLesson4Available$().subscribe(lesson4Available => {
      this.lesson4Available = lesson4Available;
    });
    // this.launchToolTips();
  }
  

  ngOnInit(): void {
    this.repair();
    this.showUser();
  }

  showUser() {
    this.users = this.apiService.listUser().subscribe(user => {
      this.users = user;
      console.log(this.users);

    })
  }

  hoverBtnPostMenu() {
    if(this.stateSoundMenu === 'active'){
      this.audioService.hoverAudio.play();
    }
  }
  
  leaveBtnPostMenu() {
    this.audioService.hoverAudio.pause();
    this.audioService.hoverAudio.currentTime = 0;
  }

  btnModule(numModule: number) {
    this.router.navigate([`/section1/lesson${numModule}`]);
  }

  switchRightWrong() {
    const btn1 = $('div.city_btn1');
    const btn2 = $('div.city_btn2');
    const btn3 = $('div.city_btn3');
    const btn4 = $('div.city_btn4');

    console.log('btn1: ', btn1.hasClass('right'));

    btn1.hasClass('right') ? 
      btn1[0].classList.remove('right') & btn1[0].classList.add('wrong') : 
      btn1[0].classList.remove('wrong') & btn1[0].classList.add('right');

    btn2.hasClass('right') ? 
      btn2[0].classList.remove('right') & btn2[0].classList.add('wrong') : 
      btn2[0].classList.remove('wrong') & btn2[0].classList.add('right');

    btn3.hasClass('right') ? 
      btn3[0].classList.remove('right') & btn3[0].classList.add('wrong') : 
      btn3[0].classList.remove('wrong') & btn3[0].classList.add('right');
      
    btn4.hasClass('right') ? 
      btn4[0].classList.remove('right') & btn4[0].classList.add('wrong') : 
      btn4[0].classList.remove('wrong') & btn4[0].classList.add('right');
  }
  repair(){
    const btn1 = $('div.city_btn1');
    const btn2 = $('div.city_btn2');
    const btn3 = $('div.city_btn3');
    const btn4 = $('div.city_btn4');

    if (this.progressService.getProgressPercentage() >= 25.5) {
      setTimeout(() => {
        btn1[0].classList.remove('wrong')
      }, 1);
      btn1[0].classList.add('right');
    }
    if (this.progressService.getProgressPercentage() >= 68) {
      setTimeout(() => {
        btn2[0].classList.remove('wrong')
      }, 1);
      btn2[0].classList.add('right');
    }
    if (this.progressService.getProgressPercentage() >= 85) {
      setTimeout(() => {
        btn3[0].classList.remove('wrong')
      }, 1);
      btn3[0].classList.add('right');
    }
    if (this.progressService.getProgressPercentage() == 100) {
      setTimeout(() => {
        btn4[0].classList.remove('wrong')
      }, 1);
      btn4[0].classList.add('right');
    }
  }

  launchToolTips() {
    console.log('launchTooltips: ', this.showTooltips);
    if(this.progressService.getProgressPercentage() == 0) {
      this.progressService.setShowTooltipsSection1(false);
      // $(".button").each(function() {
      //   $(this).addClass('disabled');
      // });
      //$('.post_menu').css('z-index', 100);
      this.tooltipNavigatorBar = true;
      setTimeout(() => {
        // $(".button").each(function() {
        //   $(this).removeClass('disabled');
        // });
        this.tooltipNavigatorBar = false;
        // $('.button_objectives').css('opacity', 0.5);
        // $('.button_sound').css('opacity', 0.5);
        // $('.button_profile').css('opacity', 0.5);
        this.tooltipMenu = true;
        setTimeout(() => {
          this.tooltipMenu = false;
          // $('.button_menu').css('opacity', 0.5);
          // $('.button_objectives').css('opacity', '');
          this.tooltipObjectives = true;
          setTimeout(() => {
            this.tooltipObjectives = false;
            // $('.button_objectives').css('opacity', 0.5);
            // $('.button_sound').css('opacity', '');
            this.tooltipSound = true;
            setTimeout(() => {
              this.tooltipSound = false;
              // $('.button_sound').css('opacity', 0.5);
              // $('.button_profile').css('opacity', '');
              this.tooltipProfile = true;
              setTimeout(() => {
                this.tooltipProfile = false;
                // $('.button_menu').css('opacity', '');
                // $('.button_objectives').css('opacity', '');
                // $('.button_sound').css('opacity', '');
                $('.post_menu').css('z-index', 10);
                $('.city_btn1').css('z-index', 101);
                $('.city_btn1').addClass('disabled');
                $('.city_btn2').css('z-index', 100);
                $('.city_btn2').addClass('disabled');
                $('.city_btn3').css('z-index', 100);
                $('.city_btn3').addClass('disabled');
                $('.city_btn4').css('z-index', 100);
                $('.city_btn4').addClass('disabled');
                this.tooltipModulesBar = true;
                setTimeout(() => {
                  this.tooltipModulesBar = false;
                  $('.city_btn2').css('z-index', 3);
                  $('.city_btn3').css('z-index', 5);
                  $('.city_btn4').css('z-index', 3);
                  this.tooltipModule1 = true;
                  setTimeout(() => {
                    this.tooltipModule1 = false;
                    $('.city_btn1').css('z-index', 4);
                    $('.city_btn2').css('z-index', 100);
                    this.tooltipModule2 = true;
                    setTimeout(() => {
                      this.tooltipModule2 = false;
                      $('.city_btn2').css('z-index', 3);
                      this.tooltipProgress = true;
                      setTimeout(() => {
                        this.tooltipProgress = false;
                        $('.city_btn2').css('z-index', 3);
                        $('.city_btn1').removeClass('disabled');
                        $('.city_btn2').removeClass('disabled');
                        $('.city_btn3').removeClass('disabled');
                        $('.city_btn4').removeClass('disabled');
                      }, 7000);
                    }, 7000);
                  }, 7000);
                }, 7000);
              }, 3000);
            }, 3000);
          }, 3000);
        }, 3000);
      }, 7000);
    }
  };
}

