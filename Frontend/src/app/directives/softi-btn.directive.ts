import { Directive, HostBinding, HostListener, Input } from '@angular/core';
import { SoundsManageService } from '../services/sounds-manage/sounds-manage.service';

@Directive({
  selector: 'softi-btn'
})
export class SoftiBtnDirective {

  private hoverSoundPath: string;
  private clickSoundPath: string;

  constructor(private soundsManageService: SoundsManageService) {
    this.hoverSoundPath = 'hoverDefault';
    this.clickSoundPath = 'clickDefault';
  }

  @HostBinding('style.position') bindPosition = 'absolute';

  @Input() set hoverSound(value: string) { if(value) this.hoverSoundPath = value; }
  @Input() set clickSound(value: string) { if(value) this.clickSoundPath = value; }

  @HostListener('mouseover')
  onMouseOver() {
    this.soundsManageService.playHoverSound(this.hoverSoundPath);
  }

  @HostListener('click')
  onMouseClick() {
    this.soundsManageService.playClickSound(this.clickSoundPath);
  }
}