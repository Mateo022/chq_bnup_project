import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: 'softi-txt'
})
export class SoftiTxtDirective {

  constructor() { }

  @HostBinding('style.position') bindPosition = 'absolute';
  @HostBinding('style.width') bindWidth = 'auto';
  @HostBinding('style.height') bindHeight = 'auto';
}