import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: 'softi-div'
})
export class SoftiDivDirective {

  constructor() { }

  @HostBinding('style.position') bindPosition = 'absolute';
  
}