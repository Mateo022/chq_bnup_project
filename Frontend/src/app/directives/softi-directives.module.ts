import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/* DIRECTIVES */
import { SoftiDivDirective } from './softi-div.directive';
import { SoftiTxtDirective } from './softi-txt.directive';
import { SoftiImgDirective } from './softi-img.directive';
import { SoftiBtnDirective } from './softi-btn.directive';

@NgModule({
  declarations: [
    SoftiDivDirective,
    SoftiTxtDirective,
    SoftiImgDirective,
    SoftiBtnDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SoftiDivDirective,
    SoftiTxtDirective,
    SoftiImgDirective,
    SoftiBtnDirective
  ]
})
export class SoftiDirectivesModule { }