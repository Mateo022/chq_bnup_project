import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: 'softi-img'
})
export class SoftiImgDirective {

  constructor() { }

  @HostBinding('style.position') bindPosition = 'absolute';
  @HostBinding('style.backgroundSize') bindBackgroundSize = '100% 100%';
}