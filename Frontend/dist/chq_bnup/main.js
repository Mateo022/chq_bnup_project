"use strict";
(self["webpackChunkchq_bnup"] = self["webpackChunkchq_bnup"] || []).push([["main"],{

/***/ 5041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ 124);


class AppComponent {
  constructor() {
    this.title = 'chq_bnup';
  }
}
AppComponent.ɵfac = function AppComponent_Factory(t) {
  return new (t || AppComponent)();
};
AppComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
  type: AppComponent,
  selectors: [["app-root"]],
  decls: 1,
  vars: 0,
  template: function AppComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    }
  },
  dependencies: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterOutlet],
  styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 6747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/platform-browser */ 4497);
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.routing */ 6738);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component */ 5041);
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home/home.component */ 5067);
/* harmony import */ var _shop_outside_book_book_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shop/outside/book/book.component */ 3020);
/* harmony import */ var _shop_inside_book_book_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shop/inside/book/book.component */ 6085);
/* harmony import */ var _shop_inside_menu_menu_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shop/inside/menu/menu.component */ 9159);
/* harmony import */ var _section1_lesson1_lesson1_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./section1/lesson1/lesson1.component */ 3830);
/* harmony import */ var _section1_lesson1_S1L1V1_S1L1V1_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./section1/lesson1/S1L1V1/S1L1V1.component */ 2854);
/* harmony import */ var _section1_lesson1_S1L1V2_S1L1V2_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./section1/lesson1/S1L1V2/S1L1V2.component */ 4695);
/* harmony import */ var _section1_lesson1_S1L1V3_S1L1V3_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./section1/lesson1/S1L1V3/S1L1V3.component */ 5781);
/* harmony import */ var _section1_lesson1_S1L1V4_S1L1V4_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./section1/lesson1/S1L1V4/S1L1V4.component */ 4790);
/* harmony import */ var _section1_lesson1_S1L1V5_S1L1V5_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./section1/lesson1/S1L1V5/S1L1V5.component */ 2890);
/* harmony import */ var _shop_inside_modal_modal_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./shop/inside/modal/modal.component */ 4988);
/* harmony import */ var _shop_outside_character_character_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shop/outside/character/character.component */ 8619);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/platform-browser/animations */ 7146);
/* harmony import */ var igniteui_angular__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! igniteui-angular */ 480);
/* harmony import */ var _shop_outside_carousel_animations_sample_component_carousel_animations_sample_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shop/outside/carousel-animations-sample-component/carousel-animations-sample.component */ 2065);
/* harmony import */ var _section1_section1_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./section1/section1.component */ 7843);
/* harmony import */ var _services_sounds_manage_sounds_manage_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./services/sounds-manage/sounds-manage.module */ 4325);
/* harmony import */ var _directives_softi_directives_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./directives/softi-directives.module */ 6783);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/common */ 4666);
/* harmony import */ var _layout_layout_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./layout/layout.component */ 6674);
/* harmony import */ var _objectives_objectives_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./objectives/objectives.component */ 2760);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/common/http */ 8987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/router */ 124);


/* Componentes generales */





/* Lesson1 (Modulo1) */






/* Shop */


/* Carousel */














class AppModule {}
AppModule.ɵfac = function AppModule_Factory(t) {
  return new (t || AppModule)();
};
AppModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_20__["ɵɵdefineNgModule"]({
  type: AppModule,
  bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_1__.AppComponent]
});
AppModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_20__["ɵɵdefineInjector"]({
  providers: [{
    provide: _angular_common__WEBPACK_IMPORTED_MODULE_21__.LocationStrategy,
    useClass: _angular_common__WEBPACK_IMPORTED_MODULE_21__.HashLocationStrategy
  }],
  imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_22__.BrowserModule, _angular_common_http__WEBPACK_IMPORTED_MODULE_23__.HttpClientModule, _app_routing__WEBPACK_IMPORTED_MODULE_0__.routing, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_24__.BrowserAnimationsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_25__.FormsModule, igniteui_angular__WEBPACK_IMPORTED_MODULE_26__.IgxCarouselModule, igniteui_angular__WEBPACK_IMPORTED_MODULE_26__.IgxIconModule, igniteui_angular__WEBPACK_IMPORTED_MODULE_26__.IgxSelectModule, igniteui_angular__WEBPACK_IMPORTED_MODULE_26__.IgxButtonModule, igniteui_angular__WEBPACK_IMPORTED_MODULE_26__.IgxCardModule, _services_sounds_manage_sounds_manage_module__WEBPACK_IMPORTED_MODULE_16__.SoundsManageModule.forRoot(), _directives_softi_directives_module__WEBPACK_IMPORTED_MODULE_17__.SoftiDirectivesModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_20__["ɵɵsetNgModuleScope"](AppModule, {
    declarations: [_section1_section1_component__WEBPACK_IMPORTED_MODULE_15__.Section1Component, _section1_lesson1_lesson1_component__WEBPACK_IMPORTED_MODULE_6__.Lesson1Component, _section1_lesson1_S1L1V1_S1L1V1_component__WEBPACK_IMPORTED_MODULE_7__.S1L1V1Component, _section1_lesson1_S1L1V2_S1L1V2_component__WEBPACK_IMPORTED_MODULE_8__.S1L1V2Component, _section1_lesson1_S1L1V3_S1L1V3_component__WEBPACK_IMPORTED_MODULE_9__.S1L1V3Component, _section1_lesson1_S1L1V4_S1L1V4_component__WEBPACK_IMPORTED_MODULE_10__.S1L1V4Component, _section1_lesson1_S1L1V5_S1L1V5_component__WEBPACK_IMPORTED_MODULE_11__.S1L1V5Component, _app_component__WEBPACK_IMPORTED_MODULE_1__.AppComponent, _shop_outside_book_book_component__WEBPACK_IMPORTED_MODULE_3__.BookComponent, _shop_inside_book_book_component__WEBPACK_IMPORTED_MODULE_4__.BookChqComponent, _shop_outside_carousel_animations_sample_component_carousel_animations_sample_component__WEBPACK_IMPORTED_MODULE_14__.CarouselAnimationsSampleComponent, _shop_outside_character_character_component__WEBPACK_IMPORTED_MODULE_13__.CharacterComponent, _home_home_component__WEBPACK_IMPORTED_MODULE_2__.HomeComponent, _shop_inside_modal_modal_component__WEBPACK_IMPORTED_MODULE_12__.ModalComponent, _shop_inside_menu_menu_component__WEBPACK_IMPORTED_MODULE_5__.MenuComponent, _layout_layout_component__WEBPACK_IMPORTED_MODULE_18__.LayoutComponent, _objectives_objectives_component__WEBPACK_IMPORTED_MODULE_19__.ObjectivesComponent],
    imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_22__.BrowserModule, _angular_common_http__WEBPACK_IMPORTED_MODULE_23__.HttpClientModule, _angular_router__WEBPACK_IMPORTED_MODULE_27__.RouterModule, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_24__.BrowserAnimationsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_25__.FormsModule, igniteui_angular__WEBPACK_IMPORTED_MODULE_26__.IgxCarouselModule, igniteui_angular__WEBPACK_IMPORTED_MODULE_26__.IgxIconModule, igniteui_angular__WEBPACK_IMPORTED_MODULE_26__.IgxSelectModule, igniteui_angular__WEBPACK_IMPORTED_MODULE_26__.IgxButtonModule, igniteui_angular__WEBPACK_IMPORTED_MODULE_26__.IgxCardModule, _services_sounds_manage_sounds_manage_module__WEBPACK_IMPORTED_MODULE_16__.SoundsManageModule, _directives_softi_directives_module__WEBPACK_IMPORTED_MODULE_17__.SoftiDirectivesModule]
  });
})();

/***/ }),

/***/ 6738:
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "routing": () => (/* binding */ routing)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var _shop_inside_book_book_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shop/inside/book/book.component */ 6085);
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home/home.component */ 5067);
/* harmony import */ var _section1_section1_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./section1/section1.component */ 7843);
/* harmony import */ var _shop_inside_modal_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shop/inside/modal/modal.component */ 4988);
/* harmony import */ var _shop_outside_character_character_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shop/outside/character/character.component */ 8619);
/* harmony import */ var _shop_outside_carousel_animations_sample_component_carousel_animations_sample_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shop/outside/carousel-animations-sample-component/carousel-animations-sample.component */ 2065);
/* harmony import */ var _section1_lesson1_lesson1_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./section1/lesson1/lesson1.component */ 3830);
/* harmony import */ var _section1_lesson1_S1L1V1_S1L1V1_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./section1/lesson1/S1L1V1/S1L1V1.component */ 2854);
/* harmony import */ var _section1_lesson1_S1L1V2_S1L1V2_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./section1/lesson1/S1L1V2/S1L1V2.component */ 4695);
/* harmony import */ var _section1_lesson1_S1L1V3_S1L1V3_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./section1/lesson1/S1L1V3/S1L1V3.component */ 5781);

/* General */
/* import { BookComponent } from './shop/outside/book/book.component'; */






/* Lesson1 (Modulo1) */




const appRoutes = [{
  path: '',
  component: _home_home_component__WEBPACK_IMPORTED_MODULE_1__.HomeComponent
}, {
  path: 'section1',
  component: _section1_section1_component__WEBPACK_IMPORTED_MODULE_2__.Section1Component
}, {
  path: 'section1/lesson1',
  component: _section1_lesson1_lesson1_component__WEBPACK_IMPORTED_MODULE_6__.Lesson1Component
}, {
  path: 'section1/lesson1/view1',
  component: _section1_lesson1_S1L1V1_S1L1V1_component__WEBPACK_IMPORTED_MODULE_7__.S1L1V1Component
}, {
  path: 'section1/lesson1/view2',
  component: _section1_lesson1_S1L1V2_S1L1V2_component__WEBPACK_IMPORTED_MODULE_8__.S1L1V2Component
}, {
  path: 'section1/lesson1/view3',
  component: _section1_lesson1_S1L1V3_S1L1V3_component__WEBPACK_IMPORTED_MODULE_9__.S1L1V3Component
}, /* { path: 'book', component: BookComponent }, */
{
  path: 'bookChq',
  component: _shop_inside_book_book_component__WEBPACK_IMPORTED_MODULE_0__.BookChqComponent
}, {
  path: 'modal',
  component: _shop_inside_modal_modal_component__WEBPACK_IMPORTED_MODULE_3__.ModalComponent
}, {
  path: 'character',
  component: _shop_outside_character_character_component__WEBPACK_IMPORTED_MODULE_4__.CharacterComponent
}, {
  path: 'carousel',
  component: _shop_outside_carousel_animations_sample_component_carousel_animations_sample_component__WEBPACK_IMPORTED_MODULE_5__.CarouselAnimationsSampleComponent
}];
const routing = _angular_router__WEBPACK_IMPORTED_MODULE_10__.RouterModule.forRoot(appRoutes, {
  useHash: true
});

/***/ }),

/***/ 6279:
/*!***************************************************!*\
  !*** ./src/app/directives/softi-btn.directive.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SoftiBtnDirective": () => (/* binding */ SoftiBtnDirective)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _services_sounds_manage_sounds_manage_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../services/sounds-manage/sounds-manage.service */ 7844);


class SoftiBtnDirective {
  constructor(soundsManageService) {
    this.soundsManageService = soundsManageService;
    this.bindPosition = 'absolute';
    this.hoverSoundPath = 'hoverDefault';
    this.clickSoundPath = 'clickDefault';
  }
  set hoverSound(value) {
    if (value) this.hoverSoundPath = value;
  }
  set clickSound(value) {
    if (value) this.clickSoundPath = value;
  }
  onMouseOver() {
    this.soundsManageService.playHoverSound(this.hoverSoundPath);
  }
  onMouseClick() {
    this.soundsManageService.playClickSound(this.clickSoundPath);
  }
}
SoftiBtnDirective.ɵfac = function SoftiBtnDirective_Factory(t) {
  return new (t || SoftiBtnDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_services_sounds_manage_sounds_manage_service__WEBPACK_IMPORTED_MODULE_0__.SoundsManageService));
};
SoftiBtnDirective.ɵdir = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
  type: SoftiBtnDirective,
  selectors: [["softi-btn"]],
  hostVars: 2,
  hostBindings: function SoftiBtnDirective_HostBindings(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("mouseover", function SoftiBtnDirective_mouseover_HostBindingHandler() {
        return ctx.onMouseOver();
      })("click", function SoftiBtnDirective_click_HostBindingHandler() {
        return ctx.onMouseClick();
      });
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("position", ctx.bindPosition);
    }
  },
  inputs: {
    hoverSound: "hoverSound",
    clickSound: "clickSound"
  }
});

/***/ }),

/***/ 6783:
/*!*******************************************************!*\
  !*** ./src/app/directives/softi-directives.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SoftiDirectivesModule": () => (/* binding */ SoftiDirectivesModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 4666);
/* harmony import */ var _softi_div_directive__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./softi-div.directive */ 9195);
/* harmony import */ var _softi_txt_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./softi-txt.directive */ 9878);
/* harmony import */ var _softi_img_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./softi-img.directive */ 4630);
/* harmony import */ var _softi_btn_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./softi-btn.directive */ 6279);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 2560);

/* DIRECTIVES */





class SoftiDirectivesModule {}
SoftiDirectivesModule.ɵfac = function SoftiDirectivesModule_Factory(t) {
  return new (t || SoftiDirectivesModule)();
};
SoftiDirectivesModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({
  type: SoftiDirectivesModule
});
SoftiDirectivesModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsetNgModuleScope"](SoftiDirectivesModule, {
    declarations: [_softi_div_directive__WEBPACK_IMPORTED_MODULE_0__.SoftiDivDirective, _softi_txt_directive__WEBPACK_IMPORTED_MODULE_1__.SoftiTxtDirective, _softi_img_directive__WEBPACK_IMPORTED_MODULE_2__.SoftiImgDirective, _softi_btn_directive__WEBPACK_IMPORTED_MODULE_3__.SoftiBtnDirective],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule],
    exports: [_softi_div_directive__WEBPACK_IMPORTED_MODULE_0__.SoftiDivDirective, _softi_txt_directive__WEBPACK_IMPORTED_MODULE_1__.SoftiTxtDirective, _softi_img_directive__WEBPACK_IMPORTED_MODULE_2__.SoftiImgDirective, _softi_btn_directive__WEBPACK_IMPORTED_MODULE_3__.SoftiBtnDirective]
  });
})();

/***/ }),

/***/ 9195:
/*!***************************************************!*\
  !*** ./src/app/directives/softi-div.directive.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SoftiDivDirective": () => (/* binding */ SoftiDivDirective)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 2560);

class SoftiDivDirective {
  constructor() {
    this.bindPosition = 'absolute';
  }
}
SoftiDivDirective.ɵfac = function SoftiDivDirective_Factory(t) {
  return new (t || SoftiDivDirective)();
};
SoftiDivDirective.ɵdir = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
  type: SoftiDivDirective,
  selectors: [["softi-div"]],
  hostVars: 2,
  hostBindings: function SoftiDivDirective_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("position", ctx.bindPosition);
    }
  }
});

/***/ }),

/***/ 4630:
/*!***************************************************!*\
  !*** ./src/app/directives/softi-img.directive.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SoftiImgDirective": () => (/* binding */ SoftiImgDirective)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 2560);

class SoftiImgDirective {
  constructor() {
    this.bindPosition = 'absolute';
    this.bindBackgroundSize = '100% 100%';
  }
}
SoftiImgDirective.ɵfac = function SoftiImgDirective_Factory(t) {
  return new (t || SoftiImgDirective)();
};
SoftiImgDirective.ɵdir = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
  type: SoftiImgDirective,
  selectors: [["softi-img"]],
  hostVars: 4,
  hostBindings: function SoftiImgDirective_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("position", ctx.bindPosition)("background-size", ctx.bindBackgroundSize);
    }
  }
});

/***/ }),

/***/ 9878:
/*!***************************************************!*\
  !*** ./src/app/directives/softi-txt.directive.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SoftiTxtDirective": () => (/* binding */ SoftiTxtDirective)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 2560);

class SoftiTxtDirective {
  constructor() {
    this.bindPosition = 'absolute';
    this.bindWidth = 'auto';
    this.bindHeight = 'auto';
  }
}
SoftiTxtDirective.ɵfac = function SoftiTxtDirective_Factory(t) {
  return new (t || SoftiTxtDirective)();
};
SoftiTxtDirective.ɵdir = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
  type: SoftiTxtDirective,
  selectors: [["softi-txt"]],
  hostVars: 6,
  hostBindings: function SoftiTxtDirective_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("position", ctx.bindPosition)("width", ctx.bindWidth)("height", ctx.bindHeight);
    }
  }
});

/***/ }),

/***/ 5067:
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomeComponent": () => (/* binding */ HomeComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var _services_progress_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../services/progress.service */ 8458);
/* harmony import */ var _services_audio_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/audio.service */ 6425);




class HomeComponent {
  constructor(router, progressService, audioService) {
    this.router = router;
    this.progressService = progressService;
    audioService.setBackground('audioSoundtrack');
  }
  startCourse() {
    this.router.navigate(['/section1']);
  }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) {
  return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_services_progress_service__WEBPACK_IMPORTED_MODULE_0__.ProgressService), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_services_audio_service__WEBPACK_IMPORTED_MODULE_1__.AudioService));
};
HomeComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
  type: HomeComponent,
  selectors: [["app-home"]],
  decls: 5,
  vars: 0,
  consts: [[1, "fondo"], ["type", "button", 1, "iniciar", 3, "click"]],
  template: function HomeComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "div", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div")(2, "div", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function HomeComponent_Template_div_click_2_listener() {
        return ctx.startCourse();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](3, " INICIAR ");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](4, "div");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    }
  },
  styles: [".fondo[_ngcontent-%COMP%]{\n    position: absolute;\n    background: url('home.png') no-repeat;  \n    background-size: 100% 100%;\n    width: 100%;\n    height:100%;   \n}\n\n.iniciar[_ngcontent-%COMP%]{\n    position: absolute;\n    background: #1995AD;\n    cursor: pointer; \n    border-radius: calc(var(--frameHeight)*0.1); \n    border-bottom: calc(var(--frameHeight)*0.01) solid #3b3c3d;\n    border-left: calc(var(--frameHeight)*0.01) solid #3b3c3d;\n    border-right: calc(var(--frameHeight)*0.01) solid #3b3c3d;\n    border-top: calc(var(--frameHeight)*0.01) solid #3b3c3d;\n    box-shadow: 0 calc(var(--frameHeight)*0.005) 0 calc(var(--frameHeight)*0.005) #3b3c3d;\n    font-family: var(--font-family-bold);\n    font-size: calc(var(--frameHeight)*0.05);\n    font-weight: 900;\n    height: calc(var(--frameHeight)*0.1);\n    left: 43%;\n    outline: none;\n    top: 86%;\n    color: #fff;\n    z-index: 3;\n    width: calc(var(--frameHeight)*0.28);\n    display: grid;\n    justify-content: center;\n    align-items: center;\n}\n\n.iniciar[_ngcontent-%COMP%]:hover {\n    background-color: #198195\n}\n\n.iniciar[_ngcontent-%COMP%]:active {\n    background-color: #198195;\n    transform: translateY(3px);\n    border-top: calc(var(--frameHeight)*0.01) solid #3b3c3d;\n    border-left: calc(var(--frameHeight)*0.01) solid #3b3c3d;\n    border-right: calc(var(--frameHeight)*0.01) solid #3b3c3d;\n    border-bottom: calc(var(--frameHeight)*0.01) solid #3b3c3d;\n    box-shadow: none;\n}\n.iniciar[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    position: absolute;\n    display: block;\n    height: inherit;\n    width: inherit;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7SUFDbEIscUNBQThEO0lBQzlELDBCQUEwQjtJQUMxQixXQUFXO0lBQ1gsV0FBVztBQUNmOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsMkNBQTJDO0lBQzNDLDBEQUEwRDtJQUMxRCx3REFBd0Q7SUFDeEQseURBQXlEO0lBQ3pELHVEQUF1RDtJQUN2RCxxRkFBcUY7SUFDckYsb0NBQW9DO0lBQ3BDLHdDQUF3QztJQUN4QyxnQkFBZ0I7SUFDaEIsb0NBQW9DO0lBQ3BDLFNBQVM7SUFDVCxhQUFhO0lBQ2IsUUFBUTtJQUNSLFdBQVc7SUFDWCxVQUFVO0lBQ1Ysb0NBQW9DO0lBQ3BDLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0k7QUFDSjs7QUFFQTtJQUNJLHlCQUF5QjtJQUN6QiwwQkFBMEI7SUFDMUIsdURBQXVEO0lBQ3ZELHdEQUF3RDtJQUN4RCx5REFBeUQ7SUFDekQsMERBQTBEO0lBQzFELGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxlQUFlO0lBQ2YsY0FBYztBQUNsQiIsInNvdXJjZXNDb250ZW50IjpbIi5mb25kb3tcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaXR5L2hvbWUucG5nKSBuby1yZXBlYXQ7ICBcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6MTAwJTsgICBcbn1cblxuLmluaWNpYXJ7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6ICMxOTk1QUQ7XG4gICAgY3Vyc29yOiBwb2ludGVyOyBcbiAgICBib3JkZXItcmFkaXVzOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjEpOyBcbiAgICBib3JkZXItYm90dG9tOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxKSBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1sZWZ0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxKSBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1yaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMSkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItdG9wOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxKSBzb2xpZCAjM2IzYzNkO1xuICAgIGJveC1zaGFkb3c6IDAgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDUpIDAgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDUpICMzYjNjM2Q7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWJvbGQpO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNSk7XG4gICAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMSk7XG4gICAgbGVmdDogNDMlO1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgdG9wOiA4NiU7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgei1pbmRleDogMztcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4yOCk7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uaW5pY2lhcjpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE5ODE5NVxufVxuXG4uaW5pY2lhcjphY3RpdmUge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMxOTgxOTU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDNweCk7XG4gICAgYm9yZGVyLXRvcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMSkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItbGVmdDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMSkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItcmlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDEpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLWJvdHRvbTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMSkgc29saWQgIzNiM2MzZDtcbiAgICBib3gtc2hhZG93OiBub25lO1xufVxuLmluaWNpYXIgYXtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgaGVpZ2h0OiBpbmhlcml0O1xuICAgIHdpZHRoOiBpbmhlcml0O1xufSJdLCJzb3VyY2VSb290IjoiIn0= */"]
});

/***/ }),

/***/ 6674:
/*!********************************************!*\
  !*** ./src/app/layout/layout.component.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LayoutComponent": () => (/* binding */ LayoutComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var _services_audio_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../services/audio.service */ 6425);
/* harmony import */ var _services_progress_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/progress.service */ 8458);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 4666);





const _c0 = ["menuId"];
function LayoutComponent_div_0_Template(rf, ctx) {
  if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 4)(1, "div", 5)(2, "div", 6)(3, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_0_Template_div_click_3_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r4.btnPostMainMenu());
    })("mouseenter", function LayoutComponent_div_0_Template_div_mouseenter_3_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r6.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_0_Template_div_mouseleave_3_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r7.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_0_Template_div_click_4_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r8.btnObjectives());
    })("mouseenter", function LayoutComponent_div_0_Template_div_mouseenter_4_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r9.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_0_Template_div_mouseleave_4_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r10.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_0_Template_div_click_5_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r11.btnSound());
    })("mouseenter", function LayoutComponent_div_0_Template_div_mouseenter_5_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r12.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_0_Template_div_mouseleave_5_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r13.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_0_Template_div_click_6_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r14.btnProfile());
    })("mouseenter", function LayoutComponent_div_0_Template_div_mouseenter_6_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r15.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_0_Template_div_mouseleave_6_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5);
      const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r16.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "div", 11)(8, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](9, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()()();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngClass", ctx_r0.stateSoundMenu);
  }
}
function LayoutComponent_div_1_Template(rf, ctx) {
  if (rf & 1) {
    const _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 14)(1, "div", 15)(2, "div", 16)(3, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](4, "MEN\u00DA");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_1_Template_div_click_5_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r17.btnModule(1));
    })("mouseenter", function LayoutComponent_div_1_Template_div_mouseenter_5_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r19.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_1_Template_div_mouseleave_5_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r20.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_1_Template_div_click_6_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r21.btnModule(2));
    })("mouseenter", function LayoutComponent_div_1_Template_div_mouseenter_6_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r22.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_1_Template_div_mouseleave_6_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r23.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "div", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_1_Template_div_click_7_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r24.btnModule(3));
    })("mouseenter", function LayoutComponent_div_1_Template_div_mouseenter_7_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r25.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_1_Template_div_mouseleave_7_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r26.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_1_Template_div_click_8_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r27.btnModule(4));
    })("mouseenter", function LayoutComponent_div_1_Template_div_mouseenter_8_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r28.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_1_Template_div_mouseleave_8_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r29.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](9, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_1_Template_div_click_9_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r18);
      const ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r30.closeMenuModal());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()()();
  }
  if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngClass", ctx_r1.lesson1Available);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngClass", ctx_r1.lesson2Available);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngClass", ctx_r1.lesson3Available);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngClass", ctx_r1.lesson4Available);
  }
}
function LayoutComponent_div_2_Template(rf, ctx) {
  if (rf & 1) {
    const _r32 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 23)(1, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](2, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_2_Template_div_click_3_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r32);
      const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r31.closeObjectivesModal());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()();
  }
}
function LayoutComponent_div_3_Template(rf, ctx) {
  if (rf & 1) {
    const _r34 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 27)(1, "div", 28)(2, "div", 29)(3, "div", 30)(4, "div", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_3_Template_div_click_4_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r34);
      const ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r33.btnMyProfile());
    })("mouseenter", function LayoutComponent_div_3_Template_div_mouseenter_4_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r34);
      const ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r35.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_3_Template_div_mouseleave_4_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r34);
      const ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r36.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](5, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](7, "Mi Perfil");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "div", 34)(9, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_3_Template_div_click_9_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r34);
      const ctx_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r37.btnReturnHome());
    })("mouseenter", function LayoutComponent_div_3_Template_div_mouseenter_9_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r34);
      const ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r38.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_3_Template_div_mouseleave_9_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r34);
      const ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r39.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](10, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](12, "Regresar al inicio");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](13, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](14, "div", 37)(15, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_3_Template_div_click_15_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r34);
      const ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r40.btnCloseSession());
    })("mouseenter", function LayoutComponent_div_3_Template_div_mouseenter_15_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r34);
      const ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r41.hoverBtnPostMenu());
    })("mouseleave", function LayoutComponent_div_3_Template_div_mouseleave_15_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r34);
      const ctx_r42 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r42.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](16, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](17, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](18, "Cerrar Sesi\u00F3n");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](19, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LayoutComponent_div_3_Template_div_click_19_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r34);
      const ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r43.closeProfileModal());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()()();
  }
}
class LayoutComponent {
  constructor(router, audioService, progressService) {
    this.router = router;
    this.audioService = audioService;
    this.progressService = progressService;
    this.showModalObjectives = false;
    this.showModalMenu = false;
    this.showModalProfile = false;
    this.stateSoundMenu = this.audioService.getStateSoundMenu();
    this.showLayout = this.progressService.getShowLayout();
    this.lesson1Available = this.progressService.getLesson1Available();
    this.lesson2Available = this.progressService.getLesson2Available();
    this.lesson3Available = this.progressService.getLesson3Available();
    this.lesson4Available = this.progressService.getLesson4Available();
    this.progressPercentage = this.progressService.getProgressPercentage();
    console.log('layout c: ', this.progressService.getProgressPercentage());
    this.audioService.getStateSoundMenu$().subscribe(stateSoundMenu => {
      this.stateSoundMenu = stateSoundMenu;
    });
    this.progressService.getShowLayout$().subscribe(showLayout => {
      this.showLayout = showLayout;
    });
    this.progressService.getLesson1Available$().subscribe(lesson1Available => {
      this.lesson1Available = lesson1Available;
    });
    this.progressService.getLesson2Available$().subscribe(lesson2Available => {
      this.lesson2Available = lesson2Available;
    });
    this.progressService.getLesson3Available$().subscribe(lesson3Available => {
      this.lesson3Available = lesson3Available;
    });
    this.progressService.getLesson4Available$().subscribe(lesson4Available => {
      this.lesson4Available = lesson4Available;
    });
    this.progressService.getProgressPercentage$().subscribe(progressPercentage => {
      this.progressPercentage = progressPercentage;
      document.getElementById('current_progress').style.height = `${this.progressPercentage}%`;
      console.log('layout c s: ', this.progressService.getProgressPercentage());
    });
    setTimeout(() => {
      document.getElementById('current_progress').style.height = `${this.progressPercentage}%`;
      /* Setear height del progress bar */
      //document.getElementById('current_progress').style.height = `${this.progressPercentage}%`;
      // document.getElementById('current_progress').style.height = '80%';
      //$('.current_progress').height(500) ;
      //$('.current_progress').height(`${this.progressPercentage}%`);
    }, 1);
  }
  ngOnInit() {
    console.log('layout Init');
  }
  btnSound() {
    this.audioService.clickAudio.play();
    console.log('btnSound\nstateSoundMenu: ', this.audioService.getStateSoundMenu());
    this.audioService.getStateSoundMenu() == 'active' ? this.audioService.pauseBackgroundAudios() : this.audioService.playBackgroundAudios();
    this.audioService.setStateSoundMenu(this.audioService.getStateSoundMenu() == 'active' ? 'inactive' : 'active');
    console.log('stateSoundMenu: ', this.audioService.getStateSoundMenu());
  }
  hoverBtnPostMenu() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.hoverAudio.play();
    }
  }
  leaveBtnPostMenu() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.hoverAudio.pause();
      this.audioService.hoverAudio.currentTime = 0;
    }
  }
  btnPostMainMenu() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    this.showModalMenu = true;
  }
  btnObjectives() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    this.showModalObjectives = true;
  }
  btnProfile() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    this.showModalProfile = true;
  }
  closeMenuModal() {
    this.showModalMenu = false;
  }
  closeObjectivesModal() {
    this.showModalObjectives = false;
  }
  closeProfileModal() {
    this.showModalProfile = false;
  }
  btnMyProfile() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    //pendiente codificación regresar a moodle
  }

  btnReturnHome() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    this.showModalProfile = false;
    this.router.navigate(['/section1']);
  }
  btnCloseSession() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.clickAudio.currentTime = 0;
      this.audioService.clickAudio.play();
    }
    //pendiente codificación salir de moodle y regresar a la ventara de login
  }

  btnModule(numModule) {
    this.closeMenuModal();
    this.router.navigate([`/section1/lesson${numModule}`]);
  }
}
LayoutComponent.ɵfac = function LayoutComponent_Factory(t) {
  return new (t || LayoutComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_services_audio_service__WEBPACK_IMPORTED_MODULE_0__.AudioService), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_services_progress_service__WEBPACK_IMPORTED_MODULE_1__.ProgressService));
};
LayoutComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
  type: LayoutComponent,
  selectors: [["app-layout"]],
  viewQuery: function LayoutComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵviewQuery"](_c0, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵloadQuery"]()) && (ctx.menuComponent = _t.first);
    }
  },
  decls: 4,
  vars: 4,
  consts: [["id", "layoutBox", 4, "ngIf"], ["id", "modalMenu", 4, "ngIf"], ["id", "modalObjectives", 4, "ngIf"], ["id", "modalProfile", 4, "ngIf"], ["id", "layoutBox"], ["id", "layoutCenterDiv"], [1, "post_menu"], [1, "button", "button_menu", 3, "click", "mouseenter", "mouseleave"], [1, "button", "button_objectives", 3, "click", "mouseenter", "mouseleave"], [1, "button", "button_sound", 3, "ngClass", "click", "mouseenter", "mouseleave"], [1, "button", "button_profile", 3, "click", "mouseenter", "mouseleave"], [1, "post_progress"], [1, "pipe_progress"], ["id", "current_progress", 1, "current_progress"], ["id", "modalMenu"], [1, "menuCenterDiv"], ["id", "modalMenuContainer"], [1, "menu_title"], [1, "menu", "menu_module_1_info", 3, "ngClass", "click", "mouseenter", "mouseleave"], [1, "menu", "menu_module_2_info", 3, "ngClass", "click", "mouseenter", "mouseleave"], [1, "menu", "menu_module_3_info", 3, "ngClass", "click", "mouseenter", "mouseleave"], [1, "menu", "menu_module_4_info", 3, "ngClass", "click", "mouseenter", "mouseleave"], ["type", "button", 1, "button_close", "button_close_menu", 3, "click"], ["id", "modalObjectives"], [1, "objectivesCenterDiv"], [1, "objectives"], ["type", "button", 1, "button_close", "button_close_objectives", 3, "click"], ["id", "modalProfile"], [1, "profileCenterDiv"], ["id", "modalProfileContainer"], [1, "profile_box_my_profile"], [1, "button_my_profile", 3, "click", "mouseenter", "mouseleave"], [1, "icon", "icon_my_profile"], [1, "text"], [1, "profile_box_return_home"], [1, "button_return_home", 3, "click", "mouseenter", "mouseleave"], [1, "icon", "icon_return_home"], [1, "profile_box_close_session"], [1, "button_close_session", 3, "click", "mouseenter", "mouseleave"], [1, "icon", "icon_close_session"], ["type", "button", 1, "button_close", "button_close_profile", 3, "click"]],
  template: function LayoutComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](0, LayoutComponent_div_0_Template, 10, 1, "div", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, LayoutComponent_div_1_Template, 10, 4, "div", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, LayoutComponent_div_2_Template, 4, 0, "div", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](3, LayoutComponent_div_3_Template, 20, 0, "div", 3);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.showLayout);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.showModalMenu);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.showModalObjectives);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.showModalProfile);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf],
  styles: ["#layoutBox[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 5;\n    clip-path: polygon(0 0, 100% 0, 100% 100%, 93% 100%, 93% 5%, 8% 5%, 8% 100%, 0 100%);\n}\n\n#layoutCenterDiv[_ngcontent-%COMP%], .objectivesCenterDiv[_ngcontent-%COMP%], .menuCenterDiv[_ngcontent-%COMP%] {\n    position: absolute;\n    height: var(--frameHeight);\n    width: var(--frameWidth);\n}\n\n#layoutCenterDiv[_ngcontent-%COMP%] {\n    *clip-path: polygon(0 0, 100% 0, 100% 100%, 93% 100%, 55% 3%, 8% 3%, 8% 100%, 0 100%);\n}\n\n\n.post_menu[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('post.png') no-repeat;\n    background-size: 100% 100%;\n    width: 7%;\n    height: 47%;\n    bottom: 0.3%;\n    *background-color: #9008;\n    display: flex;\n    justify-content: center;\n}\n.post_menu[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]{\n    position: absolute;\n    height: calc(var(--frameHeight)*0.065);\n    left: 44%;\n    width: calc(var(--frameHeight)*0.065);\n    box-shadow: 0 calc(var(--frameHeight)*0.005) 0 calc(var(--frameHeight)*0.0025) #3b3c3d;\n    border-top: calc(var(--frameHeight)*0.005) solid #3b3c3d;\n    border-left: calc(var(--frameHeight)*0.005) solid #3b3c3d;\n    border-right: calc(var(--frameHeight)*0.005) solid #3b3c3d;\n    border-bottom: calc(var(--frameHeight)*0.005) solid #3b3c3d;\n    border-radius: 50%;\n    cursor: pointer;\n}\n.post_menu[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]:active {\n    transform: translateY(3px);\n    border-top: 2px solid #3b3c3d;\n    border-left: 2px solid #3b3c3d;\n    border-right: 2px solid #3b3c3d;\n    border-bottom: 2px solid #3b3c3d;\n    box-shadow: none;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_menu[_ngcontent-%COMP%] {\n    background: url('menu.png') no-repeat;\n    background-size: 100% 100%;\n    top: 11%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_menu[_ngcontent-%COMP%]:hover{\n    background: url('menu_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 11%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_objectives[_ngcontent-%COMP%] {\n    background: url('objetives.png') no-repeat;\n    background-size: 100% 100%;\n    top: 32%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_objectives[_ngcontent-%COMP%]:hover {\n    background: url('objetives_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 32%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_sound.active[_ngcontent-%COMP%] {\n    background: url('sound.png') no-repeat;\n    background-size: 100% 100%;\n    top: 53%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_sound.active[_ngcontent-%COMP%]:hover {\n    background: url('sound_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 53%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_sound.inactive[_ngcontent-%COMP%]{\n    background: url('sound_off_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 53%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_profile[_ngcontent-%COMP%] {\n    background: url('profile.png') no-repeat;\n    background-size: 100% 100%;\n    top: 74.2%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_profile[_ngcontent-%COMP%]:hover {\n    background: url('profile_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 74.2%;\n}\n\n\n\n.post_progress[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('post_progress.png') no-repeat;\n    background-size: 100% 100%;\n    bottom: -0.5%;\n    display: flex;\n    height: 49.2%;\n    justify-content: center;\n    right: -0.2%;\n    width: 7%;\n}\n.post_progress[_ngcontent-%COMP%]   .pipe_progress[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #198195;\n    border: calc(var(--frameHeight)*0.008) solid #3b3c3ddd;\n    border-radius: calc(var(--frameHeight)*0.26);\n    height: 81%;\n    left: 16%;\n    overflow: hidden;\n    top: 9%;\n    width: 40%;\n}\n.post_progress[_ngcontent-%COMP%]   .pipe_progress[_ngcontent-%COMP%]   .current_progress[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #F79D01;\n    border-right: 5px solid #E26723;\n    height: 0%;\n    width: 100%;\n    bottom: 0;\n}\n\n\n\n#modalMenu[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n.menuCenterDiv[_ngcontent-%COMP%], .objectivesCenterDiv[_ngcontent-%COMP%], .profileCenterDiv[_ngcontent-%COMP%] {\n    position: absolute;\n    height: var(--frameHeight);\n    width: var(--frameWidth);\n}\n.menu.inactive[_ngcontent-%COMP%], .button_menu.inactive[_ngcontent-%COMP%], .button_objectives.inactive[_ngcontent-%COMP%], .button_profile.inactive[_ngcontent-%COMP%]\n{\n    pointer-events: none;\n}\n#modalMenuContainer[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('fondo_menu_desplegable.png')\n    no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.8);\n    left: 26%;\n    top: 11%;\n    width: calc(var(--frameHeight)*1);\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_title[_ngcontent-%COMP%] {\n    position: absolute;\n    color: #198195;\n    font-family: var(--font-family-light);\n    font-size: calc(var(--frameHeight)*0.1);\n    left: 12%;\n    top: 2%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    position: absolute;\n    height: 100%;\n    width: 100%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_1_info[_ngcontent-%COMP%] {\n    position: absolute;\n    height: calc(var(--frameHeight)*0.16);\n    left: 32.5%;\n    top: 18%;\n    width: calc(var(--frameHeight)*0.33);\n    transition: all 0.3s ease-in-out;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_1_info.active[_ngcontent-%COMP%] {\n    background: url('Mod1_activado.png') no-repeat;\n    background-size: 100% 100%;\n    cursor: pointer;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_1_info.inactive[_ngcontent-%COMP%]{\n    background: url('Mod1_desactivado.png') no-repeat;\n    background-size: 100% 100%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_1_info.active[_ngcontent-%COMP%]:hover {\n    background: url('Mod1_hover.png') no-repeat;\n    background-size: 100% 100%;\n    transform: scale(1.1) translate(calc(var(--frameHeight)*0.01), calc(var(--frameHeight)*0.023));\n}\n\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_2_info[_ngcontent-%COMP%] {\n    position: absolute;\n    clip-path: polygon(0 0, 100% 0, 100% 100%, 89% 100%, 89% 72%, 0% 72%);\n    height: calc(var(--frameHeight)*0.15);\n    left: 36.2%;\n    top: 40%;\n    width: calc(var(--frameHeight)*0.21);\n    transition: all 0.3s ease-in-out;\n    z-index: 1;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_2_info.active[_ngcontent-%COMP%] {\n    background: url('Mod2_activado.png') no-repeat;\n    background-size: 100% 100%;\n    cursor: pointer;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_2_info.inactive[_ngcontent-%COMP%]{\n    background: url('Mod2_desactivado.png') no-repeat;\n    background-size: 100% 100%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_2_info.active[_ngcontent-%COMP%]:hover {\n    background: url('Mod2_hover.png') no-repeat;\n    background-size: 100% 100%;\n    transform: scale(1.05) translate(calc(var(--frameHeight)*0), calc(var(--frameHeight)*0.023));\n}\n\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_3_info[_ngcontent-%COMP%] {\n    position: absolute;\n    height: calc(var(--frameHeight)*0.13);\n    left: 24.2%;\n    top: 54%;\n    width: calc(var(--frameHeight)*0.21);\n    transition: all 0.3s ease-in-out;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_3_info.active[_ngcontent-%COMP%] {\n    background: url('Mod3_activado.png') no-repeat;\n    background-size: 100% 100%;\n    cursor: pointer;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_3_info.inactive[_ngcontent-%COMP%]{\n    background: url('Mod3_desactivado.png') no-repeat;\n    background-size: 100% 100%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_3_info.active[_ngcontent-%COMP%]:hover {\n    background: url('Mod3_hover.png') no-repeat;\n    background-size: 100% 100%;\n    transform: scale(1.1) translate(calc(var(--frameHeight)*0.01), calc(var(--frameHeight)*0.023));\n}\n\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_4_info[_ngcontent-%COMP%] {\n    position: absolute;\n    height: calc(var(--frameHeight)*0.16);\n    left: 29.6%;\n    top: 78.5%;\n    width: calc(var(--frameHeight)*0.28);\n    transition: all 0.3s ease-in-out;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_4_info.active[_ngcontent-%COMP%] {\n    background: url('Mod4_activado.png') no-repeat;\n    background-size: 100% 100%;\n    cursor: pointer;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_4_info.inactive[_ngcontent-%COMP%]{\n    background: url('Mod4_desactivado.png') no-repeat;\n    background-size: 100% 100%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_4_info.active[_ngcontent-%COMP%]:hover {\n    background: url('Mod4_hover.png') no-repeat;\n    background-size: 100% 100%;\n    transform: scale(1.1) translate(calc(var(--frameHeight)*-0.01), calc(var(--frameHeight)*-0.005));\n}\n\n.button_close[_ngcontent-%COMP%] {\n    position: absolute;\n    background-image: url('equis.png');\n    background-size: 100% 100%;\n    background-repeat: no-repeat;\n    height: calc(var(--frameHeight)*0.05);\n    left: 84%;\n    top: 6%;\n    transition: all 0.1s ease-in-out;\n    width: calc(var(--frameHeight)*0.05);\n}\n.button_close[_ngcontent-%COMP%]:hover {\n    background-image: url('equis_hover.png');\n    background-size: 100% 100%;\n    background-repeat: no-repeat;\n}\n\n\n#modalObjectives[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n.objectives[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('fondo_menu_desplegable.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.8);\n    left: 26%;\n    top: 11%;\n    width: calc(var(--frameHeight)*1);\n}\n.button_close_objectives[_ngcontent-%COMP%] {\n    left: 64.5%;\n    top: 15%;\n}\n\n\n#modalProfile[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n#modalProfileContainer[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #ffffff;\n    border: calc(var(--frameHeight)*0.012) solid #323131;\n    border-radius: calc(var(--frameHeight)*0.03);\n    bottom: 0.5%;\n    display: block;\n    height: calc(var(--frameHeight)*0.25);\n    left: 7.5%;\n    width: calc(var(--frameHeight)*0.4);\n}\n.profile_box_my_profile[_ngcontent-%COMP%], .profile_box_return_home[_ngcontent-%COMP%], .profile_box_close_session[_ngcontent-%COMP%] {\n    height: 33%;\n    padding-left: calc(var(--frameHeight)*0.012);\n    width: 100%;\n}\n.button_my_profile[_ngcontent-%COMP%], .button_return_home[_ngcontent-%COMP%], .button_close_session[_ngcontent-%COMP%] {\n    height: 100%;\n    align-items: center;\n    color: #156270;\n    display: flex;\n    font-size: calc(var(--frameHeight)*0.035);\n    font-family: var(--font-family-bold);\n    gap: calc(var(--frameHeight)*0.013);\n    letter-spacing: calc(var(--frameHeight)*0.001);\n    width: 100%;\n}\n.button_my_profile[_ngcontent-%COMP%]:hover, .button_return_home[_ngcontent-%COMP%]:hover, .button_close_session[_ngcontent-%COMP%]:hover {\n    color: #ED6B06;\n    cursor: pointer;\n}\n.button_my_profile[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%], .button_return_home[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%], .button_close_session[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%]{\n    height: calc(var(--frameHeight)*0.03);\n    width: calc(var(--frameHeight)*0.03);\n}\n.button_my_profile[_ngcontent-%COMP%]   .icon.icon_my_profile[_ngcontent-%COMP%] {\n    background: url('button_my_profile.png') no-repeat;\n    background-size: 100% 100%;\n}\n.button_return_home[_ngcontent-%COMP%]   .icon.icon_return_home[_ngcontent-%COMP%] {\n    background: url('button_back.png') no-repeat;\n    background-size: 100% 100%;\n}\n.button_close_session[_ngcontent-%COMP%]   .icon.icon_close_session[_ngcontent-%COMP%] {\n    background: url('button_close_session.png') no-repeat;\n    background-size: 100% 100%;\n}\n\n.button_close_profile[_ngcontent-%COMP%] {\n    height: calc(var(--frameHeight)*0.03);\n    left: 87.5%;\n    top: 7%;\n    width: calc(var(--frameHeight)*0.03);\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvbGF5b3V0L2xheW91dC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixPQUFPO0lBQ1AsTUFBTTtJQUNOLFlBQVk7SUFDWixVQUFVO0lBQ1Ysb0ZBQW9GO0FBQ3hGOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLDBCQUEwQjtJQUMxQix3QkFBd0I7QUFDNUI7O0FBRUE7S0FDSSxvRkFBcUY7QUFDekY7O0FBRUEsZ0RBQWdEO0FBQ2hEO0lBQ0ksa0JBQWtCO0lBQ2xCLHFDQUE4RDtJQUM5RCwwQkFBMEI7SUFDMUIsU0FBUztJQUNULFdBQVc7SUFDWCxZQUFZO0tBQ1osdUJBQXdCO0lBQ3hCLGFBQWE7SUFDYix1QkFBdUI7QUFDM0I7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixzQ0FBc0M7SUFDdEMsU0FBUztJQUNULHFDQUFxQztJQUNyQyxzRkFBc0Y7SUFDdEYsd0RBQXdEO0lBQ3hELHlEQUF5RDtJQUN6RCwwREFBMEQ7SUFDMUQsMkRBQTJEO0lBQzNELGtCQUFrQjtJQUNsQixlQUFlO0FBQ25CO0FBQ0E7SUFDSSwwQkFBMEI7SUFDMUIsNkJBQTZCO0lBQzdCLDhCQUE4QjtJQUM5QiwrQkFBK0I7SUFDL0IsZ0NBQWdDO0lBQ2hDLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0kscUNBQThEO0lBQzlELDBCQUEwQjtJQUMxQixRQUFRO0FBQ1o7QUFDQTtJQUNJLDJDQUFvRTtJQUNwRSwwQkFBMEI7SUFDMUIsUUFBUTtBQUNaO0FBQ0E7SUFDSSwwQ0FBbUU7SUFDbkUsMEJBQTBCO0lBQzFCLFFBQVE7QUFDWjtBQUNBO0lBQ0ksZ0RBQXlFO0lBQ3pFLDBCQUEwQjtJQUMxQixRQUFRO0FBQ1o7QUFDQTtJQUNJLHNDQUErRDtJQUMvRCwwQkFBMEI7SUFDMUIsUUFBUTtBQUNaO0FBQ0E7SUFDSSw0Q0FBcUU7SUFDckUsMEJBQTBCO0lBQzFCLFFBQVE7QUFDWjtBQUNBO0lBQ0ksZ0RBQXlFO0lBQ3pFLDBCQUEwQjtJQUMxQixRQUFRO0FBQ1o7QUFDQTtJQUNJLHdDQUFpRTtJQUNqRSwwQkFBMEI7SUFDMUIsVUFBVTtBQUNkO0FBQ0E7SUFDSSw4Q0FBdUU7SUFDdkUsMEJBQTBCO0lBQzFCLFVBQVU7QUFDZDs7O0FBR0EsOENBQThDO0FBQzlDO0lBQ0ksa0JBQWtCO0lBQ2xCLDhDQUF1RjtJQUN2RiwwQkFBMEI7SUFDMUIsYUFBYTtJQUNiLGFBQWE7SUFDYixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLFlBQVk7SUFDWixTQUFTO0FBQ2I7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsc0RBQXNEO0lBQ3RELDRDQUE0QztJQUM1QyxXQUFXO0lBQ1gsU0FBUztJQUNULGdCQUFnQjtJQUNoQixPQUFPO0lBQ1AsVUFBVTtBQUNkO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLCtCQUErQjtJQUMvQixVQUFVO0lBQ1YsV0FBVztJQUNYLFNBQVM7QUFDYjs7O0FBR0EsK0JBQStCO0FBQy9CO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsT0FBTztJQUNQLE1BQU07SUFDTixZQUFZO0lBQ1osV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsMEJBQTBCO0lBQzFCLHdCQUF3QjtBQUM1QjtBQUNBOzs7OztJQUtJLG9CQUFvQjtBQUN4QjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCO2FBQ1M7SUFDVCwwQkFBMEI7SUFDMUIsb0NBQW9DO0lBQ3BDLFNBQVM7SUFDVCxRQUFRO0lBQ1IsaUNBQWlDO0FBQ3JDO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLHFDQUFxQztJQUNyQyx1Q0FBdUM7SUFDdkMsU0FBUztJQUNULE9BQU87QUFDWDtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixXQUFXO0FBQ2Y7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixxQ0FBcUM7SUFDckMsV0FBVztJQUNYLFFBQVE7SUFDUixvQ0FBb0M7SUFDcEMsZ0NBQWdDO0FBQ3BDO0FBQ0E7SUFDSSw4Q0FBeUY7SUFDekYsMEJBQTBCO0lBQzFCLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGlEQUE0RjtJQUM1RiwwQkFBMEI7QUFDOUI7QUFDQTtJQUNJLDJDQUFzRjtJQUN0RiwwQkFBMEI7SUFDMUIsOEZBQThGO0FBQ2xHOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLHFFQUFxRTtJQUNyRSxxQ0FBcUM7SUFDckMsV0FBVztJQUNYLFFBQVE7SUFDUixvQ0FBb0M7SUFDcEMsZ0NBQWdDO0lBQ2hDLFVBQVU7QUFDZDtBQUNBO0lBQ0ksOENBQXlGO0lBQ3pGLDBCQUEwQjtJQUMxQixlQUFlO0FBQ25CO0FBQ0E7SUFDSSxpREFBNEY7SUFDNUYsMEJBQTBCO0FBQzlCO0FBQ0E7SUFDSSwyQ0FBc0Y7SUFDdEYsMEJBQTBCO0lBQzFCLDRGQUE0RjtBQUNoRzs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixxQ0FBcUM7SUFDckMsV0FBVztJQUNYLFFBQVE7SUFDUixvQ0FBb0M7SUFDcEMsZ0NBQWdDO0FBQ3BDO0FBQ0E7SUFDSSw4Q0FBeUY7SUFDekYsMEJBQTBCO0lBQzFCLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGlEQUE0RjtJQUM1RiwwQkFBMEI7QUFDOUI7QUFDQTtJQUNJLDJDQUFzRjtJQUN0RiwwQkFBMEI7SUFDMUIsOEZBQThGO0FBQ2xHOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLHFDQUFxQztJQUNyQyxXQUFXO0lBQ1gsVUFBVTtJQUNWLG9DQUFvQztJQUNwQyxnQ0FBZ0M7QUFDcEM7QUFDQTtJQUNJLDhDQUF5RjtJQUN6RiwwQkFBMEI7SUFDMUIsZUFBZTtBQUNuQjtBQUNBO0lBQ0ksaURBQTRGO0lBQzVGLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksMkNBQXNGO0lBQ3RGLDBCQUEwQjtJQUMxQixnR0FBZ0c7QUFDcEc7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsa0NBQThEO0lBQzlELDBCQUEwQjtJQUMxQiw0QkFBNEI7SUFDNUIscUNBQXFDO0lBQ3JDLFNBQVM7SUFDVCxPQUFPO0lBQ1AsZ0NBQWdDO0lBQ2hDLG9DQUFvQztBQUN4QztBQUNBO0lBQ0ksd0NBQW9FO0lBQ3BFLDBCQUEwQjtJQUMxQiw0QkFBNEI7QUFDaEM7O0FBRUEsd0JBQXdCO0FBQ3hCO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsT0FBTztJQUNQLE1BQU07SUFDTixZQUFZO0lBQ1osV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsdURBQWtHO0lBQ2xHLDBCQUEwQjtJQUMxQixvQ0FBb0M7SUFDcEMsU0FBUztJQUNULFFBQVE7SUFDUixpQ0FBaUM7QUFDckM7QUFDQTtJQUNJLFdBQVc7SUFDWCxRQUFRO0FBQ1o7O0FBRUEscUJBQXFCO0FBQ3JCO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsT0FBTztJQUNQLE1BQU07SUFDTixZQUFZO0lBQ1osV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLG9EQUFvRDtJQUNwRCw0Q0FBNEM7SUFDNUMsWUFBWTtJQUNaLGNBQWM7SUFDZCxxQ0FBcUM7SUFDckMsVUFBVTtJQUNWLG1DQUFtQztBQUN2QztBQUNBOzs7SUFHSSxXQUFXO0lBQ1gsNENBQTRDO0lBQzVDLFdBQVc7QUFDZjtBQUNBOzs7SUFHSSxZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxhQUFhO0lBQ2IseUNBQXlDO0lBQ3pDLG9DQUFvQztJQUNwQyxtQ0FBbUM7SUFDbkMsOENBQThDO0lBQzlDLFdBQVc7QUFDZjtBQUNBOzs7SUFHSSxjQUFjO0lBQ2QsZUFBZTtBQUNuQjtBQUNBOzs7SUFHSSxxQ0FBcUM7SUFDckMsb0NBQW9DO0FBQ3hDO0FBQ0E7SUFDSSxrREFBMEY7SUFDMUYsMEJBQTBCO0FBQzlCO0FBQ0E7SUFDSSw0Q0FBb0Y7SUFDcEYsMEJBQTBCO0FBQzlCO0FBQ0E7SUFDSSxxREFBNkY7SUFDN0YsMEJBQTBCO0FBQzlCOztBQUVBO0lBQ0kscUNBQXFDO0lBQ3JDLFdBQVc7SUFDWCxPQUFPO0lBQ1Asb0NBQW9DO0FBQ3hDIiwic291cmNlc0NvbnRlbnQiOlsiI2xheW91dEJveCB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBoZWlnaHQ6IDEwMHZoO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGxlZnQ6IDA7XG4gICAgdG9wOiAwO1xuICAgIHdpZHRoOiAxMDB2dztcbiAgICB6LWluZGV4OiA1O1xuICAgIGNsaXAtcGF0aDogcG9seWdvbigwIDAsIDEwMCUgMCwgMTAwJSAxMDAlLCA5MyUgMTAwJSwgOTMlIDUlLCA4JSA1JSwgOCUgMTAwJSwgMCAxMDAlKTtcbn1cblxuI2xheW91dENlbnRlckRpdiwgLm9iamVjdGl2ZXNDZW50ZXJEaXYsIC5tZW51Q2VudGVyRGl2IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiB2YXIoLS1mcmFtZUhlaWdodCk7XG4gICAgd2lkdGg6IHZhcigtLWZyYW1lV2lkdGgpO1xufVxuXG4jbGF5b3V0Q2VudGVyRGl2IHtcbiAgICAqY2xpcC1wYXRoOiBwb2x5Z29uKDAgMCwgMTAwJSAwLCAxMDAlIDEwMCUsIDkzJSAxMDAlLCA1NSUgMyUsIDglIDMlLCA4JSAxMDAlLCAwIDEwMCUpO1xufVxuXG4vKioqKioqKiAgIENhamEgTWVudSBsYXRlcmFsIGl6cXVpZXJkbyAgICoqKioqKiovXG4ucG9zdF9tZW51IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9tZW51L3Bvc3QucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgd2lkdGg6IDclO1xuICAgIGhlaWdodDogNDclO1xuICAgIGJvdHRvbTogMC4zJTtcbiAgICAqYmFja2dyb3VuZC1jb2xvcjogIzkwMDg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5wb3N0X21lbnUgLmJ1dHRvbntcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA2NSk7XG4gICAgbGVmdDogNDQlO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA2NSk7XG4gICAgYm94LXNoYWRvdzogMCBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwNSkgMCBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwMjUpICMzYjNjM2Q7XG4gICAgYm9yZGVyLXRvcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLWxlZnQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDA1KSBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1yaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLWJvdHRvbTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5wb3N0X21lbnUgLmJ1dHRvbjphY3RpdmUge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgzcHgpO1xuICAgIGJvcmRlci10b3A6IDJweCBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1sZWZ0OiAycHggc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItcmlnaHQ6IDJweCBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjM2IzYzNkO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG59XG4ucG9zdF9tZW51IC5idXR0b24uYnV0dG9uX21lbnUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9tZW51LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogMTElO1xufVxuLnBvc3RfbWVudSAuYnV0dG9uLmJ1dHRvbl9tZW51OmhvdmVye1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9tZW51X2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogMTElO1xufVxuLnBvc3RfbWVudSAuYnV0dG9uLmJ1dHRvbl9vYmplY3RpdmVzIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvb2JqZXRpdmVzLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogMzIlO1xufVxuLnBvc3RfbWVudSAuYnV0dG9uLmJ1dHRvbl9vYmplY3RpdmVzOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvb2JqZXRpdmVzX2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogMzIlO1xufVxuLnBvc3RfbWVudSAuYnV0dG9uLmJ1dHRvbl9zb3VuZC5hY3RpdmUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9zb3VuZC5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB0b3A6IDUzJTtcbn1cbi5wb3N0X21lbnUgLmJ1dHRvbi5idXR0b25fc291bmQuYWN0aXZlOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvc291bmRfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdG9wOiA1MyU7XG59XG4ucG9zdF9tZW51IC5idXR0b24uYnV0dG9uX3NvdW5kLmluYWN0aXZle1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9zb3VuZF9vZmZfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdG9wOiA1MyU7XG59XG4ucG9zdF9tZW51IC5idXR0b24uYnV0dG9uX3Byb2ZpbGUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9wcm9maWxlLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogNzQuMiU7XG59XG4ucG9zdF9tZW51IC5idXR0b24uYnV0dG9uX3Byb2ZpbGU6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9wcm9maWxlX2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogNzQuMiU7XG59XG5cblxuLyoqKioqKiogICBDYWphIE1lbnUgbGF0ZXJhbCBkZXJlY2hvICAgKioqKioqKi9cbi5wb3N0X3Byb2dyZXNzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9nZW5lcmFsX3ZpZXdfbGVzc29ucy9wb3N0X3Byb2dyZXNzLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJvdHRvbTogLTAuNSU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBoZWlnaHQ6IDQ5LjIlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHJpZ2h0OiAtMC4yJTtcbiAgICB3aWR0aDogNyU7XG59XG4ucG9zdF9wcm9ncmVzcyAucGlwZV9wcm9ncmVzcyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMxOTgxOTU7XG4gICAgYm9yZGVyOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgc29saWQgIzNiM2MzZGRkO1xuICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMjYpO1xuICAgIGhlaWdodDogODElO1xuICAgIGxlZnQ6IDE2JTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHRvcDogOSU7XG4gICAgd2lkdGg6IDQwJTtcbn1cbi5wb3N0X3Byb2dyZXNzIC5waXBlX3Byb2dyZXNzIC5jdXJyZW50X3Byb2dyZXNzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0Y3OUQwMTtcbiAgICBib3JkZXItcmlnaHQ6IDVweCBzb2xpZCAjRTI2NzIzO1xuICAgIGhlaWdodDogMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm90dG9tOiAwO1xufVxuXG5cbi8qKioqKioqICAgTW9kYWwgTWVudSAgICoqKioqKiovXG4jbW9kYWxNZW51IHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwYTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogMTAwdmg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMHZ3O1xuICAgIHotaW5kZXg6IDk5O1xufVxuLm1lbnVDZW50ZXJEaXYsIC5vYmplY3RpdmVzQ2VudGVyRGl2LCAucHJvZmlsZUNlbnRlckRpdiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogdmFyKC0tZnJhbWVIZWlnaHQpO1xuICAgIHdpZHRoOiB2YXIoLS1mcmFtZVdpZHRoKTtcbn1cbi5tZW51LmluYWN0aXZlLCBcbi5idXR0b25fbWVudS5pbmFjdGl2ZSxcbi5idXR0b25fb2JqZWN0aXZlcy5pbmFjdGl2ZSxcbi5idXR0b25fcHJvZmlsZS5pbmFjdGl2ZVxue1xuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9mb25kb19tZW51X2Rlc3BsZWdhYmxlLnBuZylcbiAgICBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjgpO1xuICAgIGxlZnQ6IDI2JTtcbiAgICB0b3A6IDExJTtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMSk7XG59XG4jbW9kYWxNZW51Q29udGFpbmVyIC5tZW51X3RpdGxlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgY29sb3I6ICMxOTgxOTU7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWxpZ2h0KTtcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMSk7XG4gICAgbGVmdDogMTIlO1xuICAgIHRvcDogMiU7XG59XG4jbW9kYWxNZW51Q29udGFpbmVyIGEge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4jbW9kYWxNZW51Q29udGFpbmVyIC5tZW51X21vZHVsZV8xX2luZm8ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMTYpO1xuICAgIGxlZnQ6IDMyLjUlO1xuICAgIHRvcDogMTglO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjMzKTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzFfaW5mby5hY3RpdmUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2QxX2FjdGl2YWRvLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzFfaW5mby5pbmFjdGl2ZXtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc01lbnVEZXBsb3kvTW9kMV9kZXNhY3RpdmFkby5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzFfaW5mby5hY3RpdmU6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2QxX2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMS4xKSB0cmFuc2xhdGUoY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMSksIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDIzKSk7XG59XG5cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzJfaW5mbyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGNsaXAtcGF0aDogcG9seWdvbigwIDAsIDEwMCUgMCwgMTAwJSAxMDAlLCA4OSUgMTAwJSwgODklIDcyJSwgMCUgNzIlKTtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMTUpO1xuICAgIGxlZnQ6IDM2LjIlO1xuICAgIHRvcDogNDAlO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjIxKTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcbiAgICB6LWluZGV4OiAxO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfMl9pbmZvLmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9tZW51L2J1dHRvbnNNZW51RGVwbG95L01vZDJfYWN0aXZhZG8ucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfMl9pbmZvLmluYWN0aXZle1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2QyX2Rlc2FjdGl2YWRvLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfMl9pbmZvLmFjdGl2ZTpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9tZW51L2J1dHRvbnNNZW51RGVwbG95L01vZDJfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxLjA1KSB0cmFuc2xhdGUoY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMCksIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDIzKSk7XG59XG5cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzNfaW5mbyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4xMyk7XG4gICAgbGVmdDogMjQuMiU7XG4gICAgdG9wOiA1NCU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMjEpO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfM19pbmZvLmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9tZW51L2J1dHRvbnNNZW51RGVwbG95L01vZDNfYWN0aXZhZG8ucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfM19pbmZvLmluYWN0aXZle1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2QzX2Rlc2FjdGl2YWRvLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfM19pbmZvLmFjdGl2ZTpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9tZW51L2J1dHRvbnNNZW51RGVwbG95L01vZDNfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxLjEpIHRyYW5zbGF0ZShjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxKSwgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjMpKTtcbn1cblxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfNF9pbmZvIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjE2KTtcbiAgICBsZWZ0OiAyOS42JTtcbiAgICB0b3A6IDc4LjUlO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjI4KTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzRfaW5mby5hY3RpdmUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2Q0X2FjdGl2YWRvLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzRfaW5mby5pbmFjdGl2ZXtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc01lbnVEZXBsb3kvTW9kNF9kZXNhY3RpdmFkby5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzRfaW5mby5hY3RpdmU6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2Q0X2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMS4xKSB0cmFuc2xhdGUoY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqLTAuMDEpLCBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSotMC4wMDUpKTtcbn1cblxuLmJ1dHRvbl9jbG9zZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvYnV0dG9ucy9lcXVpcy5wbmcpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA1KTtcbiAgICBsZWZ0OiA4NCU7XG4gICAgdG9wOiA2JTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4xcyBlYXNlLWluLW91dDtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNSk7XG59XG4uYnV0dG9uX2Nsb3NlOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvZXF1aXNfaG92ZXIucG5nKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xufVxuXG4vKioqKiogIE9iamV0aXZvcyAgKioqKiovXG4jbW9kYWxPYmplY3RpdmVzIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwYTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogMTAwdmg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMHZ3O1xuICAgIHotaW5kZXg6IDk5O1xufVxuLm9iamVjdGl2ZXMge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc01lbnVEZXBsb3kvZm9uZG9fbWVudV9kZXNwbGVnYWJsZS5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuOCk7XG4gICAgbGVmdDogMjYlO1xuICAgIHRvcDogMTElO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSoxKTtcbn1cbi5idXR0b25fY2xvc2Vfb2JqZWN0aXZlcyB7XG4gICAgbGVmdDogNjQuNSU7XG4gICAgdG9wOiAxNSU7XG59XG5cbi8qKioqKiAgUGVyZmlsICAqKioqKi9cbiNtb2RhbFByb2ZpbGUge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDBhO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgaGVpZ2h0OiAxMDB2aDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwdnc7XG4gICAgei1pbmRleDogOTk7XG59XG4jbW9kYWxQcm9maWxlQ29udGFpbmVyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBib3JkZXI6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDEyKSBzb2xpZCAjMzIzMTMxO1xuICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDMpO1xuICAgIGJvdHRvbTogMC41JTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMjUpO1xuICAgIGxlZnQ6IDcuNSU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuNCk7XG59XG4ucHJvZmlsZV9ib3hfbXlfcHJvZmlsZSwgXG4ucHJvZmlsZV9ib3hfcmV0dXJuX2hvbWUsIFxuLnByb2ZpbGVfYm94X2Nsb3NlX3Nlc3Npb24ge1xuICAgIGhlaWdodDogMzMlO1xuICAgIHBhZGRpbmctbGVmdDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmJ1dHRvbl9teV9wcm9maWxlLCBcbi5idXR0b25fcmV0dXJuX2hvbWUsIFxuLmJ1dHRvbl9jbG9zZV9zZXNzaW9uIHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBjb2xvcjogIzE1NjI3MDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMzUpO1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1ib2xkKTtcbiAgICBnYXA6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDEzKTtcbiAgICBsZXR0ZXItc3BhY2luZzogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDEpO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmJ1dHRvbl9teV9wcm9maWxlOmhvdmVyLCBcbi5idXR0b25fcmV0dXJuX2hvbWU6aG92ZXIsIFxuLmJ1dHRvbl9jbG9zZV9zZXNzaW9uOmhvdmVyIHtcbiAgICBjb2xvcjogI0VENkIwNjtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uYnV0dG9uX215X3Byb2ZpbGUgLmljb24sIFxuLmJ1dHRvbl9yZXR1cm5faG9tZSAuaWNvbiwgXG4uYnV0dG9uX2Nsb3NlX3Nlc3Npb24gLmljb257XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAzKTtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMyk7XG59XG4uYnV0dG9uX215X3Byb2ZpbGUgLmljb24uaWNvbl9teV9wcm9maWxlIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc1Byb2ZpbGUvYnV0dG9uX215X3Byb2ZpbGUucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG59XG4uYnV0dG9uX3JldHVybl9ob21lIC5pY29uLmljb25fcmV0dXJuX2hvbWUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zUHJvZmlsZS9idXR0b25fYmFjay5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbi5idXR0b25fY2xvc2Vfc2Vzc2lvbiAuaWNvbi5pY29uX2Nsb3NlX3Nlc3Npb24ge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zUHJvZmlsZS9idXR0b25fY2xvc2Vfc2Vzc2lvbi5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cblxuLmJ1dHRvbl9jbG9zZV9wcm9maWxlIHtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDMpO1xuICAgIGxlZnQ6IDg3LjUlO1xuICAgIHRvcDogNyU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDMpO1xufSJdLCJzb3VyY2VSb290IjoiIn0= */"]
});

/***/ }),

/***/ 2760:
/*!****************************************************!*\
  !*** ./src/app/objectives/objectives.component.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ObjectivesComponent": () => (/* binding */ ObjectivesComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 2560);

class ObjectivesComponent {}
ObjectivesComponent.ɵfac = function ObjectivesComponent_Factory(t) {
  return new (t || ObjectivesComponent)();
};
ObjectivesComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
  type: ObjectivesComponent,
  selectors: [["app-objectives"]],
  decls: 0,
  vars: 0,
  template: function ObjectivesComponent_Template(rf, ctx) {},
  styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 2854:
/*!*************************************************************!*\
  !*** ./src/app/section1/lesson1/S1L1V1/S1L1V1.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "S1L1V1Component": () => (/* binding */ S1L1V1Component)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _server_lesson1_texts_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../server/lesson1/texts.json */ 5886);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ 5139);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 4666);





function S1L1V1Component_div_8_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"](" ", ctx_r0.getCurrentQuestion().description1, " ");
  }
}
function S1L1V1Component_div_9_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"](" ", ctx_r1.getCurrentQuestion().description2, " ");
  }
}
function S1L1V1Component_div_10_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"](" ", ctx_r2.getCurrentQuestion().description3, " ");
  }
}
class S1L1V1Component {
  constructor() {
    this.arrayQuestion = _server_lesson1_texts_json__WEBPACK_IMPORTED_MODULE_0__;
    this.currentQuestion = 0;
    this.currentView = 1;
    this.stateSoundMenu = 'active';
    /* Tamaño y posición para dibujar la caja del personaje */
    this.characterObject = {
      'left': '79%',
      'top': '32%',
      'height': '59%',
      'width': '13.5%'
    };
    this.audiosCharacter = {
      1: new Audio('../../../../assets/section1/sounds/Introduccion.mp3'),
      2: new Audio(`../../../../assets/section1/sounds/Objetivos.mp3`)
    };
    /* Mensaje que será enviado al componente modal */
    this.modalMessage = "Lee con atención la información que te presentamos, al finalizar mira el video de introducción y continua navegando por el curso.";
    /* Audio del personaje leyendo el texto principal */
    this.audioCharacter = new Audio('../../../../assets/section1/sounds/Introduccion.mp3');
    this.isClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_2__.EventEmitter();
  }
  //textCharacter = new Audio('../../../assets/transcriptions/Lesson1View1.vtt');
  //srcTrack = "Lesson1View1.vtt";
  //soundPath = "Lesson1_view1.mp3";
  //  @ViewChild('modalId', {static : true}) modalComponent : ModalComponent;
  ngOnInit() {
    console.log('-- S1L1V1 ngOnInit');
  }
  getCurrentQuestion() {
    return this.arrayQuestion[this.currentQuestion];
  }
  changeViews(numView) {
    this.currentView = numView + 1;
    jquery__WEBPACK_IMPORTED_MODULE_1__(`.point0${this.currentQuestion + 1}`).removeClass('point_current');
    this.currentQuestion = numView;
    jquery__WEBPACK_IMPORTED_MODULE_1__(`.point0${this.currentQuestion + 1}`).addClass('point_current');
    this.isClicked.emit(this.currentView);
  }
}
S1L1V1Component.ɵfac = function S1L1V1Component_Factory(t) {
  return new (t || S1L1V1Component)();
};
S1L1V1Component.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
  type: S1L1V1Component,
  selectors: [["app-S1L1V1"]],
  outputs: {
    isClicked: "isClicked"
  },
  decls: 17,
  vars: 5,
  consts: [[1, "S1L1V1_container"], [1, "box_text"], [1, "title_text"], [1, "title_text1"], [1, "title_text2"], [1, "paragraph"], ["class", "paragraph1", 4, "ngIf"], ["class", "paragraph2", 4, "ngIf"], ["class", "paragraph3", 4, "ngIf"], [1, "advance_box"], ["type", "button", 1, "pag_left", 3, "click"], ["type", "button", 1, "point", "point01", "point_current", 3, "click"], ["type", "button", 1, "point", "point02", 3, "click"], ["type", "button", 1, "pag_right", 3, "click"], [1, "video"], [1, "paragraph1"], [1, "paragraph2"], [1, "paragraph3"]],
  template: function S1L1V1Component_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0)(1, "div", 1)(2, "div", 2)(3, "div", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](5, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](7);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](8, S1L1V1Component_div_8_Template, 2, 1, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](9, S1L1V1Component_div_9_Template, 2, 1, "div", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](10, S1L1V1Component_div_10_Template, 2, 1, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "div", 9)(12, "div", 10);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function S1L1V1Component_Template_div_click_12_listener() {
        return ctx.changeViews(0);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](13, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function S1L1V1Component_Template_div_click_13_listener() {
        return ctx.changeViews(0);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](14, "div", 12);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function S1L1V1Component_Template_div_click_14_listener() {
        return ctx.changeViews(1);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](15, "div", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function S1L1V1Component_Template_div_click_15_listener() {
        return ctx.changeViews(1);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](16, "div", 14);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](ctx.getCurrentQuestion().title);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"](" ", ctx.getCurrentQuestion().description, " ");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.currentQuestion == 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.currentQuestion == 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.currentQuestion == 1);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.NgIf],
  styles: [".S1L1V1_container[_ngcontent-%COMP%] {\n    position: absolute;\n    border: calc(var(--frameHeight)*0.012) solid #484849;\n    background-color: #fff;\n    border-radius: 20px;\n    height: 73%;\n    left: 8.5%;\n    top: 18%;\n    width: 83%;\n}\n\n\n.S1L1V1_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%] {\n    position: absolute;\n    font-size: (var(--frameHeight)*0.012);\n    height: 80%;\n    left: 2.5%;\n    top: calc(var(--frameHeight)*0.04);\n    width: 36%;\n}\n.S1L1V1_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%] {\n    width: 100%;\n    color: #ED6B06;\n    display: block;\n}\n.S1L1V1_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text1[_ngcontent-%COMP%]{\n    font-family: var(--font-family-light);\n    font-size: calc(var(--frameHeight)*0.07);\n    text-align: center;\n    margin-bottom: calc(var(--frameHeight)*-0.02);\n    width: 100%;\n}\n.S1L1V1_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text2[_ngcontent-%COMP%]{\n    font-family: var(--font-family-bold);\n    font-size: calc(var(--frameHeight)*0.05);\n    font-weight: bolder;\n    line-height: 95%;\n    text-align: center;\n    width: 100%;\n}\n.S1L1V1_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%] {\n    position: absolute;\n    color: #484849;\n    font-family: var(--font-family-medium);\n    font-size: calc(var(--frameHeight)*0.024);\n    height: -webkit-fill-available;\n    line-height: 125%;\n    \n    overflow-y: auto;\n    padding-top: calc(var(--frameHeight)*0.008);\n    text-align: justify;\n    width: 100%;\n}\n.S1L1V1_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%] {\n    -ms-overflow-style: none;  \n    scrollbar-width: none;  \n}\n.S1L1V1_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%]::-webkit-scrollbar { \n    display: none;  \n}\n\n.S1L1V1_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph1[_ngcontent-%COMP%] {\n    position: absolute;\n    color: #484849;\n    font-family: var(--font-family-medium);\n    font-size: calc(var(--frameHeight)*0.024);\n    height: -webkit-fill-available;\n    line-height: 125%;\n    \n    overflow-y: auto;\n    padding-top: calc(var(--frameHeight)*0.008);\n    text-align: justify;\n    width: 100%;\n    top: 30%;\n}\n.S1L1V1_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph2[_ngcontent-%COMP%] {\n    position: absolute;\n    color: #484849;\n    font-family: var(--font-family-medium);\n    font-size: calc(var(--frameHeight)*0.024);\n    height: -webkit-fill-available;\n    line-height: 125%;\n    \n    overflow-y: auto;\n    padding-top: calc(var(--frameHeight)*0.008);\n    text-align: justify;\n    width: 100%;\n    top: 55%\n}\n.S1L1V1_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph3[_ngcontent-%COMP%] {\n    position: absolute;\n    color: #484849;\n    font-family: var(--font-family-medium);\n    font-size: calc(var(--frameHeight)*0.024);\n    height: -webkit-fill-available;\n    line-height: 125%;\n    \n    overflow-y: auto;\n    padding-top: calc(var(--frameHeight)*0.008);\n    text-align: justify;\n    width: 100%;\n    top: 74%;\n}\n\n.video[_ngcontent-%COMP%] {\n    position: absolute;\n    height: 49.85%;\n    left: 44%;\n    top: 21%;\n    width: 35%;\n    box-shadow: calc(var(--frameHeight)*-0.015) \n                calc(var(--frameHeight)*0.025) \n                calc(var(--frameHeight)*0.075) #3b3b3b;\n    background: url('video.png') no-repeat;\n    background-size: 109% 114%;\n    background-position-x: 98%;\n}\n\n.video[_ngcontent-%COMP%]   video[_ngcontent-%COMP%] {\n    position: absolute;\n    height: 100%;\n    width: 100%;\n    background-color: black;\n}\n\n\n.advance_box[_ngcontent-%COMP%] {\n    height: 16%;\n    margin-left: auto;\n    margin-right: auto;\n    margin-top: calc(var(--frameHeight)*0.26);\n    padding: 0% 1%;\n    top: 50%;\n    width: 100%;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    gap: calc(var(--frameHeight)*0.018);\n    position: absolute;\n}\n\n.point[_ngcontent-%COMP%] {\n    background: url('point.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.015);\n    width: calc(var(--frameHeight)*0.015);\n}\n.point.point_current[_ngcontent-%COMP%] {\n    background: url('point_hover.png') no-repeat;\n    background-size: 100% 100%;\n}\n.point[_ngcontent-%COMP%]:hover {\n    background: url('point_hover.png') no-repeat;\n    background-size: 100% 100%;\n}\n.point[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    display: block;\n    height: inherit;\n    width: inherit;\n}\n.pag_left[_ngcontent-%COMP%] {\n    background: url('arrow_left_1.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.04);\n    width: calc(var(--frameHeight)*0.03);\n    position: absolute;\n    left: 20%;\n}\n.pag_left[_ngcontent-%COMP%]:hover {\n    background: url('hover_arrow_left.png') no-repeat;\n    background-size: 100% 100%;\n}\n.pag_right[_ngcontent-%COMP%] {\n    background: url('arrow_right_1.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.04);\n    width: calc(var(--frameHeight)*0.03);\n    position: absolute;\n    right: 20%;\n}\n.pag_right[_ngcontent-%COMP%]:hover {\n    background: url('hover_arrow_right.png') no-repeat;\n    background-size: 100% 100%;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2VjdGlvbjEvbGVzc29uMS9TMUwxVjEvUzFMMVYxLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFO0FBQ3JFO0lBQ0ksa0JBQWtCO0lBQ2xCLG9EQUFvRDtJQUNwRCxzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxVQUFVO0lBQ1YsUUFBUTtJQUNSLFVBQVU7QUFDZDs7QUFFQSx3Q0FBd0M7QUFDeEM7SUFDSSxrQkFBa0I7SUFDbEIscUNBQXFDO0lBQ3JDLFdBQVc7SUFDWCxVQUFVO0lBQ1Ysa0NBQWtDO0lBQ2xDLFVBQVU7QUFDZDtBQUNBO0lBQ0ksV0FBVztJQUNYLGNBQWM7SUFDZCxjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxxQ0FBcUM7SUFDckMsd0NBQXdDO0lBQ3hDLGtCQUFrQjtJQUNsQiw2Q0FBNkM7SUFDN0MsV0FBVztBQUNmO0FBQ0E7SUFDSSxvQ0FBb0M7SUFDcEMsd0NBQXdDO0lBQ3hDLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLFdBQVc7QUFDZjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxzQ0FBc0M7SUFDdEMseUNBQXlDO0lBQ3pDLDhCQUE4QjtJQUM5QixpQkFBaUI7SUFDakIscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQiwyQ0FBMkM7SUFDM0MsbUJBQW1CO0lBQ25CLFdBQVc7QUFDZjtBQUNBO0lBQ0ksd0JBQXdCLEdBQUcsMEJBQTBCO0lBQ3JELHFCQUFxQixHQUFHLFlBQVk7QUFDeEM7QUFDQTtJQUNJLGFBQWEsR0FBRyxzQkFBc0I7QUFDMUM7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLHNDQUFzQztJQUN0Qyx5Q0FBeUM7SUFDekMsOEJBQThCO0lBQzlCLGlCQUFpQjtJQUNqQixxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLDJDQUEyQztJQUMzQyxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFFBQVE7QUFDWjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxzQ0FBc0M7SUFDdEMseUNBQXlDO0lBQ3pDLDhCQUE4QjtJQUM5QixpQkFBaUI7SUFDakIscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQiwyQ0FBMkM7SUFDM0MsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWDtBQUNKO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLHNDQUFzQztJQUN0Qyx5Q0FBeUM7SUFDekMsOEJBQThCO0lBQzlCLGlCQUFpQjtJQUNqQixxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLDJDQUEyQztJQUMzQyxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFFBQVE7QUFDWjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsU0FBUztJQUNULFFBQVE7SUFDUixVQUFVO0lBQ1Y7O3NEQUVrRDtJQUNsRCxzQ0FBcUY7SUFDckYsMEJBQTBCO0lBQzFCLDBCQUEwQjtBQUM5Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osV0FBVztJQUNYLHVCQUF1QjtBQUMzQjs7QUFFQSwwQ0FBMEM7QUFDMUM7SUFDSSxXQUFXO0lBQ1gsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQix5Q0FBeUM7SUFDekMsY0FBYztJQUNkLFFBQVE7SUFDUixXQUFXO0lBQ1gsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsbUNBQW1DO0lBQ25DLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLHNDQUF3RTtJQUN4RSwwQkFBMEI7SUFDMUIsc0NBQXNDO0lBQ3RDLHFDQUFxQztBQUN6QztBQUNBO0lBQ0ksNENBQThFO0lBQzlFLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksNENBQThFO0lBQzlFLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksY0FBYztJQUNkLGVBQWU7SUFDZixjQUFjO0FBQ2xCO0FBQ0E7SUFDSSw2Q0FBK0U7SUFDL0UsMEJBQTBCO0lBQzFCLHFDQUFxQztJQUNyQyxvQ0FBb0M7SUFDcEMsa0JBQWtCO0lBQ2xCLFNBQVM7QUFDYjtBQUNBO0lBQ0ksaURBQW1GO0lBQ25GLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksOENBQWdGO0lBQ2hGLDBCQUEwQjtJQUMxQixxQ0FBcUM7SUFDckMsb0NBQW9DO0lBQ3BDLGtCQUFrQjtJQUNsQixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGtEQUFvRjtJQUNwRiwwQkFBMEI7QUFDOUIiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKiAgIENhamEgcHJpbmNpcGFsIGNvbiBlbCBjb250ZW5pZG8gZGUgbGFzIGxlY2Npb25lcyAgICoqKioqKiovXG4uUzFMMVYxX2NvbnRhaW5lciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvcmRlcjogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpIHNvbGlkICM0ODQ4NDk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgIGhlaWdodDogNzMlO1xuICAgIGxlZnQ6IDguNSU7XG4gICAgdG9wOiAxOCU7XG4gICAgd2lkdGg6IDgzJTtcbn1cblxuLyoqKioqKiogICBDYWphIGNlbnRyYWwgdGV4dG9zICAgKioqKioqKi9cbi5TMUwxVjFfY29udGFpbmVyIC5ib3hfdGV4dCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGZvbnQtc2l6ZTogKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxMik7XG4gICAgaGVpZ2h0OiA4MCU7XG4gICAgbGVmdDogMi41JTtcbiAgICB0b3A6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDQpO1xuICAgIHdpZHRoOiAzNiU7XG59XG4uUzFMMVYxX2NvbnRhaW5lciAuYm94X3RleHQgLnRpdGxlX3RleHQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAjRUQ2QjA2O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xufVxuLlMxTDFWMV9jb250YWluZXIgLmJveF90ZXh0IC50aXRsZV90ZXh0IC50aXRsZV90ZXh0MXtcbiAgICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktbGlnaHQpO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNyk7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKi0wLjAyKTtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5TMUwxVjFfY29udGFpbmVyIC5ib3hfdGV4dCAudGl0bGVfdGV4dCAudGl0bGVfdGV4dDJ7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWJvbGQpO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNSk7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBsaW5lLWhlaWdodDogOTUlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5TMUwxVjFfY29udGFpbmVyIC5ib3hfdGV4dCAucGFyYWdyYXBoIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgY29sb3I6ICM0ODQ4NDk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LW1lZGl1bSk7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyNCk7XG4gICAgaGVpZ2h0OiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xuICAgIGxpbmUtaGVpZ2h0OiAxMjUlO1xuICAgIC8qIG1hcmdpbi1sZWZ0OiA1JTsgKi9cbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuICAgIHBhZGRpbmctdG9wOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCk7XG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5TMUwxVjFfY29udGFpbmVyIC5ib3hfdGV4dCAucGFyYWdyYXBoIHtcbiAgICAtbXMtb3ZlcmZsb3ctc3R5bGU6IG5vbmU7ICAvKiBJbnRlcm5ldCBFeHBsb3JlciAxMCsgKi9cbiAgICBzY3JvbGxiYXItd2lkdGg6IG5vbmU7ICAvKiBGaXJlZm94ICovXG59XG4uUzFMMVYxX2NvbnRhaW5lciAuYm94X3RleHQgLnBhcmFncmFwaDo6LXdlYmtpdC1zY3JvbGxiYXIgeyBcbiAgICBkaXNwbGF5OiBub25lOyAgLyogU2FmYXJpIGFuZCBDaHJvbWUgKi9cbn1cblxuLlMxTDFWMV9jb250YWluZXIgLmJveF90ZXh0IC5wYXJhZ3JhcGgxIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgY29sb3I6ICM0ODQ4NDk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LW1lZGl1bSk7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyNCk7XG4gICAgaGVpZ2h0OiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xuICAgIGxpbmUtaGVpZ2h0OiAxMjUlO1xuICAgIC8qIG1hcmdpbi1sZWZ0OiA1JTsgKi9cbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuICAgIHBhZGRpbmctdG9wOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCk7XG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0b3A6IDMwJTtcbn1cbi5TMUwxVjFfY29udGFpbmVyIC5ib3hfdGV4dCAucGFyYWdyYXBoMiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGNvbG9yOiAjNDg0ODQ5O1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1tZWRpdW0pO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjQpO1xuICAgIGhlaWdodDogLXdlYmtpdC1maWxsLWF2YWlsYWJsZTtcbiAgICBsaW5lLWhlaWdodDogMTI1JTtcbiAgICAvKiBtYXJnaW4tbGVmdDogNSU7ICovXG4gICAgb3ZlcmZsb3cteTogYXV0bztcbiAgICBwYWRkaW5nLXRvcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDgpO1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdG9wOiA1NSVcbn1cbi5TMUwxVjFfY29udGFpbmVyIC5ib3hfdGV4dCAucGFyYWdyYXBoMyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGNvbG9yOiAjNDg0ODQ5O1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1tZWRpdW0pO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjQpO1xuICAgIGhlaWdodDogLXdlYmtpdC1maWxsLWF2YWlsYWJsZTtcbiAgICBsaW5lLWhlaWdodDogMTI1JTtcbiAgICAvKiBtYXJnaW4tbGVmdDogNSU7ICovXG4gICAgb3ZlcmZsb3cteTogYXV0bztcbiAgICBwYWRkaW5nLXRvcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDgpO1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdG9wOiA3NCU7XG59XG5cbi52aWRlbyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogNDkuODUlO1xuICAgIGxlZnQ6IDQ0JTtcbiAgICB0b3A6IDIxJTtcbiAgICB3aWR0aDogMzUlO1xuICAgIGJveC1zaGFkb3c6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKi0wLjAxNSkgXG4gICAgICAgICAgICAgICAgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjUpIFxuICAgICAgICAgICAgICAgIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDc1KSAjM2IzYjNiO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMS92aWV3MS9pbWFnZXMvdmlkZW8ucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDklIDExNCU7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbi14OiA5OCU7XG59XG5cbi52aWRlbyB2aWRlbyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbn1cblxuLyoqKioqKiogICBDYWphIGNhbWJpbyBkZSB2aXN0YXMgICAqKioqKioqL1xuLmFkdmFuY2VfYm94IHtcbiAgICBoZWlnaHQ6IDE2JTtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLXRvcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4yNik7XG4gICAgcGFkZGluZzogMCUgMSU7XG4gICAgdG9wOiA1MCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGdhcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTgpO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cblxuLnBvaW50IHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvcG9pbnQucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxNSk7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDE1KTtcbn1cbi5wb2ludC5wb2ludF9jdXJyZW50IHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvcG9pbnRfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG59XG4ucG9pbnQ6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvYnV0dG9ucy9wb2ludF9ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbi5wb2ludCBhe1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGhlaWdodDogaW5oZXJpdDtcbiAgICB3aWR0aDogaW5oZXJpdDtcbn1cbi5wYWdfbGVmdCB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9idXR0b25zL2Fycm93X2xlZnRfMS5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDQpO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAzKTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMjAlO1xufVxuLnBhZ19sZWZ0OmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvaG92ZXJfYXJyb3dfbGVmdC5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbi5wYWdfcmlnaHQge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvYnV0dG9ucy9hcnJvd19yaWdodF8xLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNCk7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDMpO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMjAlO1xufVxuLnBhZ19yaWdodDpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9idXR0b25zL2hvdmVyX2Fycm93X3JpZ2h0LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufSJdLCJzb3VyY2VSb290IjoiIn0= */"]
});

/***/ }),

/***/ 4695:
/*!*************************************************************!*\
  !*** ./src/app/section1/lesson1/S1L1V2/S1L1V2.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "S1L1V2Component": () => (/* binding */ S1L1V2Component)
/* harmony export */ });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ 5139);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 2560);


const _c0 = ["characterId"];
class S1L1V2Component {
  constructor() {
    this.stateSoundMenu = 'active';
    /* Tamaño y posición para dibujar la caja del personaje */
    this.characterObject = {
      'left': '81%',
      'top': '32%',
      'height': '59%',
      'width': '13.5%'
    };
    this.timeout = null;
    /* Mensaje que será enviado al componente modal */
    this.modalMessage = "Lee con atención la información que te presentamos, al finalizar continua navegando por el curso.";
    /* Audio del personaje leyendo el texto principal */
    this.audioCharacter = new Audio(`../../../../assets/section1/sounds/Que_son_los_BNUP.mp3`);
  }
  ngOnInit() {
    console.log('-- S1L1V2 ngOnInit');
  }
  sliderAuto() {
    let front = 1,
      der = 2,
      oculto1 = 3,
      oculto2 = 4,
      oculto3 = 5,
      oculto4 = 6,
      oculto5 = 7,
      oculto6 = 8,
      izq = 9;
    jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-1`).removeClass('frontal derecha izquierda oculto');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-2`).removeClass('frontal derecha izquierda oculto');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-3`).removeClass('frontal derecha izquierda oculto');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-4`).removeClass('frontal derecha izquierda oculto');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-5`).removeClass('frontal derecha izquierda oculto');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-6`).removeClass('frontal derecha izquierda oculto');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-7`).removeClass('frontal derecha izquierda oculto');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-8`).removeClass('frontal derecha izquierda oculto');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-9`).removeClass('frontal derecha izquierda oculto');
    this.timeout = setInterval(() => {
      jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-1`).removeClass('frontal derecha izquierda');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-2`).removeClass('frontal derecha izquierda');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-3`).removeClass('frontal derecha izquierda');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-4`).removeClass('frontal derecha izquierda');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-5`).removeClass('frontal derecha izquierda');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-6`).removeClass('frontal derecha izquierda');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-7`).removeClass('frontal derecha izquierda');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-8`).removeClass('frontal derecha izquierda');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-9`).removeClass('frontal derecha izquierda');
      setTimeout(() => {
        jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-${front}`).addClass('frontal');
        setTimeout(() => {
          jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-1`).removeClass('oculto');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-2`).removeClass('oculto');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-3`).removeClass('oculto');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-4`).removeClass('oculto');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-5`).removeClass('oculto');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-6`).removeClass('oculto');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-7`).removeClass('oculto');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-8`).removeClass('oculto');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-9`).removeClass('oculto');
          setTimeout(() => {
            jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-${der}`).addClass('derecha');
            jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-${oculto1}`).addClass('oculto');
            jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-${oculto2}`).addClass('oculto');
            jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-${oculto3}`).addClass('oculto');
            jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-${oculto4}`).addClass('oculto');
            jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-${oculto5}`).addClass('oculto');
            jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-${oculto6}`).addClass('oculto');
            jquery__WEBPACK_IMPORTED_MODULE_0__(`#song-${izq}`).addClass('izquierda');
            if (front == 1) {
              front = 2;
              der = 3;
              oculto1 = 4;
              oculto2 = 5;
              oculto3 = 6;
              oculto4 = 7;
              oculto5 = 8;
              oculto6 = 9;
              izq = 1;
            } else if (front == 2) {
              front = 3;
              der = 4;
              oculto1 = 5;
              oculto2 = 6;
              oculto3 = 7;
              oculto4 = 8;
              oculto5 = 9;
              oculto6 = 1;
              izq = 2;
            } else if (front == 3) {
              front = 4;
              der = 5;
              oculto1 = 6;
              oculto2 = 7;
              oculto3 = 8;
              oculto4 = 9;
              oculto5 = 1;
              oculto6 = 2;
              izq = 3;
            } else if (front == 4) {
              front = 5;
              der = 6;
              oculto1 = 7;
              oculto2 = 8;
              oculto3 = 9;
              oculto4 = 1;
              oculto5 = 2;
              oculto6 = 3;
              izq = 4;
            } else if (front == 5) {
              front = 6;
              der = 7;
              oculto1 = 8;
              oculto2 = 9;
              oculto3 = 1;
              oculto4 = 2;
              oculto5 = 3;
              oculto6 = 4;
              izq = 5;
            } else if (front == 6) {
              front = 7;
              der = 8;
              oculto1 = 9;
              oculto2 = 1;
              oculto3 = 2;
              oculto4 = 3;
              oculto5 = 4;
              oculto6 = 5;
              izq = 6;
            } else if (front == 7) {
              front = 8;
              der = 9;
              oculto1 = 1;
              oculto2 = 2;
              oculto3 = 3;
              oculto4 = 4;
              oculto5 = 5;
              oculto6 = 6;
              izq = 7;
            } else if (front == 8) {
              front = 9;
              der = 1;
              oculto1 = 2;
              oculto2 = 3;
              oculto3 = 4;
              oculto4 = 5;
              oculto5 = 6;
              oculto6 = 7;
              izq = 8;
            } else {
              front = 1;
              der = 2;
              oculto1 = 3;
              oculto2 = 4;
              oculto3 = 5;
              oculto4 = 6;
              oculto5 = 7;
              oculto6 = 8;
              izq = 9;
            }
          }, 3);
        }, 100);
      }, 100);
    }, 3000);
  }
  stopSlider() {
    clearInterval(this.timeout);
  }
}
S1L1V2Component.ɵfac = function S1L1V2Component_Factory(t) {
  return new (t || S1L1V2Component)();
};
S1L1V2Component.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
  type: S1L1V2Component,
  selectors: [["app-S1L1V2"]],
  viewQuery: function S1L1V2Component_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c0, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.characterComponent = _t.first);
    }
  },
  decls: 34,
  vars: 0,
  consts: [[1, "S1L1V2_container"], [1, "box_text"], [1, "title_text"], [1, "title_text1"], [1, "title_text2"], [1, "paragraph"], [1, "container"], [1, "player"], ["type", "radio", "name", "slider", "id", "item-1", "checked", ""], ["type", "radio", "name", "slider", "id", "item-2"], ["type", "radio", "name", "slider", "id", "item-3"], ["type", "radio", "name", "slider", "id", "item-4"], [1, "cards"], ["for", "item-2", "id", "song-2", 1, "card"], ["src", "../../../../assets/section1/lesson1/view2/img1.jpg", "alt", "song"], ["for", "item-3", "id", "song-3", 1, "card"], ["src", "../../../../assets/section1/lesson1/view2/img2.jpg", "alt", "song"], ["for", "item-4", "id", "song-4", 1, "card"], ["src", "../../../../assets/section1/lesson1/view2/img3.jpg", "alt", "song"], ["for", "item-5", "id", "song-5", 1, "card"], ["src", "../../../../assets/section1/lesson1/view2/img4.jpg", "alt", "song"], ["for", "item-6", "id", "song-6", 1, "card"], ["src", "../../../../assets/section1/lesson1/view2/img5.jpg", "alt", "song"], ["for", "item-7", "id", "song-7", 1, "card"], ["src", "../../../../assets/section1/lesson1/view2/img6.jpg", "alt", "song"], ["for", "item-8", "id", "song-8", 1, "card"], ["src", "../../../../assets/section1/lesson1/view2/img7.jpg", "alt", "song"], ["for", "item-9", "id", "song-9", 1, "card"], ["src", "../../../../assets/section1/lesson1/view2/img8.jpg", "alt", "song"], ["for", "item-1", "id", "song-1", 1, "card"], ["src", "../../../../assets/section1/lesson1/view2/img9.jpg", "alt", "song"]],
  template: function S1L1V2Component_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0)(1, "div", 1)(2, "div", 2)(3, "div", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "\u00BFQU\u00C9 SON LOS ");
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "BIENES NACIONALES DE USO P\u00DABLICO (BNUP)?");
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, " Los Bienes Nacionales de Uso P\u00FAblico, son espacios p\u00FAblicos que est\u00E1n destinados al uso y disfrute de toda la poblaci\u00F3n, sin segregaci\u00F3n alguna, siendo alguno de ellos: plazas, parques, jardines, playas, r\u00EDos, lagos, calles y aceras, entre otros. Todos estos espacios est\u00E1n abiertos al p\u00FAblico y su uso no puede ser restringido a un grupo determinado de personas. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](10, "div", 7)(11, "input", 8)(12, "input", 9)(13, "input", 10)(14, "input", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 12)(16, "label", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](17, "img", 14);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "label", 15);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](19, "img", 16);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "label", 17);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](21, "img", 18);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "label", 19);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](23, "img", 20);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "label", 21);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](25, "img", 22);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "label", 23);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](27, "img", 24);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "label", 25);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](29, "img", 26);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "label", 27);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](31, "img", 28);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "label", 29);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](33, "img", 30);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
    }
  },
  styles: [".S1L1V2_container[_ngcontent-%COMP%] {\n    position: absolute;\n    border: calc(var(--frameHeight)*0.012) solid #484849;\n    background-color: #fff;\n    border-radius: 20px;\n    height: 73%;\n    left: 8.5%;\n    overflow: hidden;\n    top: 18%;\n    width: 83%;\n}\n\n.S1L1V2_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%] {\n    position: absolute;\n    font-size: (var(--frameHeight)*0.012);\n    height: 90%;\n    left: 2.5%;\n    top: calc(var(--frameHeight)*0.024);\n    width: 36%;\n}\n.S1L1V2_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%] {\n    width: 100%;\n    color: #ED6B06;\n    display: block;\n}\n.S1L1V2_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text1[_ngcontent-%COMP%]{\n    font-family: var(--font-family-light);\n    font-size: calc(var(--frameHeight)*0.08);\n    font-weight: normal; \n    text-align: center;\n    margin-bottom: calc(var(--frameHeight)*-0.01);\n    width: 100%;\n}\n.S1L1V2_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text2[_ngcontent-%COMP%]{\n    font-family: var(--font-family-bold);\n    font-size: calc(var(--frameHeight)*0.046);\n    font-weight: bolder;\n    line-height: 95%;\n    text-align: center;\n    width: 100%;\n}\n.S1L1V2_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%] {\n    *position: absolute;\n    *height: -webkit-fill-available;\n    color: #484849;\n    font-family: var(--font-family-medium);\n    font-size: calc(var(--frameHeight)*0.024);\n    line-height: 125%;\n    overflow-y: auto;\n    margin: 0 auto;\n    padding-top: calc(var(--frameHeight)*0.024);\n    text-align: justify;\n    width: 78%;\n}\n.S1L1V2_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%] {\n    -ms-overflow-style: none;  \n    scrollbar-width: none;  \n}\n.S1L1V2_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%]::-webkit-scrollbar { \n    display: none;  \n}\n\ninput[type=radio][_ngcontent-%COMP%] {\n    display: none;\n}\n.card[_ngcontent-%COMP%] {\n    position: absolute;\n    width: 60%;\n    height: 100%;\n    left: 0;\n    right: 0;\n    margin: auto;\n    transition: transform 0.4s ease;\n    cursor: pointer;\n}\n.container[_ngcontent-%COMP%] {\n    width: 50%;\n    position: absolute;\n    height: 55%;\n    transform-style: preserve-3d;\n    display: flex;\n    justify-content: center;\n    flex-direction: column;\n    align-items: center;\n    left: 40%;\n    top: 20%;\n}\n.cards[_ngcontent-%COMP%] {\n    position: absolute;\n    width: 100%;\n    height: 100%;\n}\nimg[_ngcontent-%COMP%] {\n    width: 100%;\n    height: 100%;\n    border-radius: 10px;\n    object-fit: cover;\n}\n\n#item-1[_ngcontent-%COMP%]:checked    ~ .cards[_ngcontent-%COMP%]   #song-1[_ngcontent-%COMP%]   img[_ngcontent-%COMP%], #item-2[_ngcontent-%COMP%]:checked    ~ .cards[_ngcontent-%COMP%]   #song-2[_ngcontent-%COMP%]   img[_ngcontent-%COMP%], #item-3[_ngcontent-%COMP%]:checked    ~ .cards[_ngcontent-%COMP%]   #song-3[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    box-shadow: 0px 0px 5px 0px rgba(81, 81, 81, 0.47);\n}\n.player[_ngcontent-%COMP%] {\n    border-radius: 8px;\n    min-width: 320px;\n    padding: 20% 5%;\n}\n\n.frontal[_ngcontent-%COMP%]{\n    transform: translatex(0) scale(1);\n    opacity: 1;\n    z-index: 2;\n}\n.derecha[_ngcontent-%COMP%]{\n    transform: translatex(40%) scale(0.8);\n    opacity: 0.4;\n    z-index: 0;\n}\n.izquierda[_ngcontent-%COMP%]{\n    transform: translatex(-40%) scale(0.8);\n    opacity: 0.4;\n    z-index: 0;\n}\n.oculto[_ngcontent-%COMP%]{\n    transform: translatex(0%) scale(0.8);\n    opacity: 0.2;\n    z-index: 0;\n    display: none;\n}\n\nlabel[_ngcontent-%COMP%]    > p[_ngcontent-%COMP%] {\n    color: white;\n    position: absolute;\n    top: -5px;\n    left: 25px;\n    font-size: 20px;\n    font-weight: bold;\n    height: 30px;\n    width: 30px;\n    background-color: #333;\n    padding-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2VjdGlvbjEvbGVzc29uMS9TMUwxVjIvUzFMMVYyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFO0FBQ3JFO0lBQ0ksa0JBQWtCO0lBQ2xCLG9EQUFvRDtJQUNwRCxzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxVQUFVO0lBQ1YsZ0JBQWdCO0lBQ2hCLFFBQVE7SUFDUixVQUFVO0FBQ2Q7QUFDQSx3Q0FBd0M7QUFDeEM7SUFDSSxrQkFBa0I7SUFDbEIscUNBQXFDO0lBQ3JDLFdBQVc7SUFDWCxVQUFVO0lBQ1YsbUNBQW1DO0lBQ25DLFVBQVU7QUFDZDtBQUNBO0lBQ0ksV0FBVztJQUNYLGNBQWM7SUFDZCxjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxxQ0FBcUM7SUFDckMsd0NBQXdDO0lBQ3hDLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsNkNBQTZDO0lBQzdDLFdBQVc7QUFDZjtBQUNBO0lBQ0ksb0NBQW9DO0lBQ3BDLHlDQUF5QztJQUN6QyxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixXQUFXO0FBQ2Y7QUFDQTtLQUNJLGtCQUFtQjtLQUNuQiw4QkFBK0I7SUFDL0IsY0FBYztJQUNkLHNDQUFzQztJQUN0Qyx5Q0FBeUM7SUFDekMsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsMkNBQTJDO0lBQzNDLG1CQUFtQjtJQUNuQixVQUFVO0FBQ2Q7QUFDQTtJQUNJLHdCQUF3QixHQUFHLDBCQUEwQjtJQUNyRCxxQkFBcUIsR0FBRyxZQUFZO0FBQ3hDO0FBQ0E7SUFDSSxhQUFhLEdBQUcsc0JBQXNCO0FBQzFDOztBQUVBO0lBQ0ksYUFBYTtBQUNqQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixZQUFZO0lBQ1osT0FBTztJQUNQLFFBQVE7SUFDUixZQUFZO0lBQ1osK0JBQStCO0lBQy9CLGVBQWU7QUFDbkI7QUFDQTtJQUNJLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLDRCQUE0QjtJQUM1QixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsU0FBUztJQUNULFFBQVE7QUFDWjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixpQkFBaUI7QUFDckI7QUFDQTs7Ozs7Ozs7Ozs7Ozs7R0FjRztBQUNIO0lBQ0ksa0RBQWtEO0FBQ3REO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxpQ0FBaUM7SUFDakMsVUFBVTtJQUNWLFVBQVU7QUFDZDtBQUNBO0lBQ0kscUNBQXFDO0lBQ3JDLFlBQVk7SUFDWixVQUFVO0FBQ2Q7QUFDQTtJQUNJLHNDQUFzQztJQUN0QyxZQUFZO0lBQ1osVUFBVTtBQUNkO0FBQ0E7SUFDSSxvQ0FBb0M7SUFDcEMsWUFBWTtJQUNaLFVBQVU7SUFDVixhQUFhO0FBQ2pCOztBQUVBO0lBQ0ksWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtJQUNWLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsWUFBWTtJQUNaLFdBQVc7SUFDWCxzQkFBc0I7SUFDdEIsaUJBQWlCO0FBQ3JCIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKiogICBDYWphIHByaW5jaXBhbCBjb24gZWwgY29udGVuaWRvIGRlIGxhcyBsZWNjaW9uZXMgICAqKioqKioqL1xuLlMxTDFWMl9jb250YWluZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3JkZXI6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDEyKSBzb2xpZCAjNDg0ODQ5O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgICBoZWlnaHQ6IDczJTtcbiAgICBsZWZ0OiA4LjUlO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgdG9wOiAxOCU7XG4gICAgd2lkdGg6IDgzJTtcbn1cbi8qKioqKioqICAgQ2FqYSBjZW50cmFsIHRleHRvcyAgICoqKioqKiovXG4uUzFMMVYyX2NvbnRhaW5lciAuYm94X3RleHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBmb250LXNpemU6ICh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpO1xuICAgIGhlaWdodDogOTAlO1xuICAgIGxlZnQ6IDIuNSU7XG4gICAgdG9wOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyNCk7XG4gICAgd2lkdGg6IDM2JTtcbn1cbi5TMUwxVjJfY29udGFpbmVyIC5ib3hfdGV4dCAudGl0bGVfdGV4dCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6ICNFRDZCMDY7XG4gICAgZGlzcGxheTogYmxvY2s7XG59XG4uUzFMMVYyX2NvbnRhaW5lciAuYm94X3RleHQgLnRpdGxlX3RleHQgLnRpdGxlX3RleHQxe1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1saWdodCk7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA4KTtcbiAgICBmb250LXdlaWdodDogbm9ybWFsOyBcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqLTAuMDEpO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLlMxTDFWMl9jb250YWluZXIgLmJveF90ZXh0IC50aXRsZV90ZXh0IC50aXRsZV90ZXh0MntcbiAgICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktYm9sZCk7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA0Nik7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBsaW5lLWhlaWdodDogOTUlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5TMUwxVjJfY29udGFpbmVyIC5ib3hfdGV4dCAucGFyYWdyYXBoIHtcbiAgICAqcG9zaXRpb246IGFic29sdXRlO1xuICAgICpoZWlnaHQ6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7XG4gICAgY29sb3I6ICM0ODQ4NDk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LW1lZGl1bSk7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyNCk7XG4gICAgbGluZS1oZWlnaHQ6IDEyNSU7XG4gICAgb3ZlcmZsb3cteTogYXV0bztcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBwYWRkaW5nLXRvcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjQpO1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgd2lkdGg6IDc4JTtcbn1cbi5TMUwxVjJfY29udGFpbmVyIC5ib3hfdGV4dCAucGFyYWdyYXBoIHtcbiAgICAtbXMtb3ZlcmZsb3ctc3R5bGU6IG5vbmU7ICAvKiBJbnRlcm5ldCBFeHBsb3JlciAxMCsgKi9cbiAgICBzY3JvbGxiYXItd2lkdGg6IG5vbmU7ICAvKiBGaXJlZm94ICovXG59XG4uUzFMMVYyX2NvbnRhaW5lciAuYm94X3RleHQgLnBhcmFncmFwaDo6LXdlYmtpdC1zY3JvbGxiYXIgeyBcbiAgICBkaXNwbGF5OiBub25lOyAgLyogU2FmYXJpIGFuZCBDaHJvbWUgKi9cbn1cblxuaW5wdXRbdHlwZT1yYWRpb10ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG4uY2FyZCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiA2MCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjRzIGVhc2U7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmNvbnRhaW5lciB7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiA1NSU7XG4gICAgdHJhbnNmb3JtLXN0eWxlOiBwcmVzZXJ2ZS0zZDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBsZWZ0OiA0MCU7XG4gICAgdG9wOiAyMCU7XG59XG4uY2FyZHMge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5pbWcge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xufVxuLyogI2l0ZW0tMTpjaGVja2VkIH4gLmNhcmRzICNzb25nLTMsICNpdGVtLTI6Y2hlY2tlZCB+IC5jYXJkcyAjc29uZy0xLCAjaXRlbS0zOmNoZWNrZWQgfiAuY2FyZHMgI3NvbmctMiB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGV4KC00MCUpIHNjYWxlKDAuOCk7XG4gICAgb3BhY2l0eTogMC40O1xuICAgIHotaW5kZXg6IDA7XG59XG4jaXRlbS0xOmNoZWNrZWQgfiAuY2FyZHMgI3NvbmctMiwgI2l0ZW0tMjpjaGVja2VkIH4gLmNhcmRzICNzb25nLTMsICNpdGVtLTM6Y2hlY2tlZCB+IC5jYXJkcyAjc29uZy0xIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXgoNDAlKSBzY2FsZSgwLjgpO1xuICAgIG9wYWNpdHk6IDAuNDtcbiAgICB6LWluZGV4OiAwO1xufVxuI2l0ZW0tMTpjaGVja2VkIH4gLmNhcmRzICNzb25nLTEsICNpdGVtLTI6Y2hlY2tlZCB+IC5jYXJkcyAjc29uZy0yLCAjaXRlbS0zOmNoZWNrZWQgfiAuY2FyZHMgI3NvbmctMyB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGV4KDApIHNjYWxlKDEpO1xuICAgIG9wYWNpdHk6IDE7XG4gICAgei1pbmRleDogMTtcbn0gKi9cbiNpdGVtLTE6Y2hlY2tlZCB+IC5jYXJkcyAjc29uZy0xIGltZywgI2l0ZW0tMjpjaGVja2VkIH4gLmNhcmRzICNzb25nLTIgaW1nLCAjaXRlbS0zOmNoZWNrZWQgfiAuY2FyZHMgI3NvbmctMyBpbWcge1xuICAgIGJveC1zaGFkb3c6IDBweCAwcHggNXB4IDBweCByZ2JhKDgxLCA4MSwgODEsIDAuNDcpO1xufVxuLnBsYXllciB7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIG1pbi13aWR0aDogMzIwcHg7XG4gICAgcGFkZGluZzogMjAlIDUlO1xufVxuXG4uZnJvbnRhbHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXgoMCkgc2NhbGUoMSk7XG4gICAgb3BhY2l0eTogMTtcbiAgICB6LWluZGV4OiAyO1xufVxuLmRlcmVjaGF7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGV4KDQwJSkgc2NhbGUoMC44KTtcbiAgICBvcGFjaXR5OiAwLjQ7XG4gICAgei1pbmRleDogMDtcbn1cbi5penF1aWVyZGF7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGV4KC00MCUpIHNjYWxlKDAuOCk7XG4gICAgb3BhY2l0eTogMC40O1xuICAgIHotaW5kZXg6IDA7XG59XG4ub2N1bHRve1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRleCgwJSkgc2NhbGUoMC44KTtcbiAgICBvcGFjaXR5OiAwLjI7XG4gICAgei1pbmRleDogMDtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG5sYWJlbCA+IHAge1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAtNXB4O1xuICAgIGxlZnQ6IDI1cHg7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGhlaWdodDogMzBweDtcbiAgICB3aWR0aDogMzBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzMzO1xuICAgIHBhZGRpbmctbGVmdDogNXB4O1xufVxuIl0sInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 5781:
/*!*************************************************************!*\
  !*** ./src/app/section1/lesson1/S1L1V3/S1L1V3.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "S1L1V3Component": () => (/* binding */ S1L1V3Component)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ 5139);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var src_app_services_progress_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/progress.service */ 8458);





const _c0 = ["characterId"];
class S1L1V3Component {
  constructor(router, progressService) {
    this.router = router;
    this.progressService = progressService;
    this.stateSoundMenu = 'active';
    this.showModalFinish = false;
    /* Tamaño y posición para dibujar la caja del personaje */
    this.characterObject = {
      'left': '79%',
      'top': '32%',
      'height': '59%',
      'width': '13.5%'
    };
    /* Audios del personaje de sus tres textos */
    this.audiosCharacter = {
      1: new Audio(`../../../../assets/section1/sounds/lesson1/S1L1V3_1.mp3`),
      2: new Audio(`../../../../assets/section1/sounds/lesson1/S1L1V3_2.mp3`),
      3: new Audio(`../../../../assets/section1/sounds/lesson1/S1L1V3_3.mp3`)
    };
    /* Mensaje que será enviado al componente modal */
    this.modalMessage = "Lee con atención la información que te presentamos, navega con las flechas y visualiza todas las imagenes. Al finalizar continua navegando por el curso.";
    this.isClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_2__.EventEmitter();
  }
  ngOnInit() {
    console.log('-- S1L1V3 ngOnInit');
  }
  viewInfo(inf) {
    jquery__WEBPACK_IMPORTED_MODULE_0__(`.info${inf}`).css('display', 'block');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`.img${inf}`).removeClass('rubberBand');
    if (inf == 3) {
      jquery__WEBPACK_IMPORTED_MODULE_0__(`.warning`).removeClass('rubberBand');
    }
    jquery__WEBPACK_IMPORTED_MODULE_0__(`.img${inf}`).css('hover', 'background-color: #2a6c79k');
    this.isClicked.emit(inf);
  }
  /*
  * Muestra el modal de finalización del juego
  */
  openModal() {
    jquery__WEBPACK_IMPORTED_MODULE_0__("#modal_fin").show();
    jquery__WEBPACK_IMPORTED_MODULE_0__(`app-layout.layout1`)[0].childNodes[0].style.zIndex = 1;
  }
  closeModal() {
    jquery__WEBPACK_IMPORTED_MODULE_0__("#modal_fin").hide();
    this.showModalFinish = false;
    if (this.progressService.getProgressPercentage() < 25.5) {
      this.progressService.setProgressPercentage(25.5);
    }
    ;
    this.progressService.setLesson2Available('active');
    this.router.navigate(['/section1']);
  }
}
S1L1V3Component.ɵfac = function S1L1V3Component_Factory(t) {
  return new (t || S1L1V3Component)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_progress_service__WEBPACK_IMPORTED_MODULE_1__.ProgressService));
};
S1L1V3Component.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
  type: S1L1V3Component,
  selectors: [["app-S1L1V3"]],
  viewQuery: function S1L1V3Component_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵviewQuery"](_c0, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵloadQuery"]()) && (ctx.characterComponent = _t.first);
    }
  },
  outputs: {
    isClicked: "isClicked"
  },
  decls: 30,
  vars: 0,
  consts: [[1, "S1L1V3_container"], [1, "box_text"], [1, "title_text"], [1, "title_text1"], [1, "title_text2"], [1, "content"], [1, "text1"], [1, "img1", "rubberBand", 3, "click"], [1, "info1", "animated", "fadeInDownBig"], [1, "paragraph"], [1, "text2"], [1, "img2", "rubberBand", 3, "click"], [1, "info2", "animated", "fadeInDownBig"], [1, "text3"], [1, "warning", "rubberBand", 3, "click"], [1, "info3", "animated", "fadeInDownBig"], [1, "text_warning"]],
  template: function S1L1V3Component_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0)(1, "div", 1)(2, "div", 2)(3, "div", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](4, "LOS BNUP");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](6, "SE CLASIFICAN EN DOS CATEGOR\u00CDAS ");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "div", 5)(8, "div", 6)(9, "div", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function S1L1V3Component_Template_div_click_9_listener() {
        return ctx.viewInfo(1);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](10, "div", 8)(11, "div", 9)(12, "b");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](13, "Bienes fiscales o del estado,");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](14, " son aquellos bienes que pertenecen al Estado y que son administrados por \u00E9l en representaci\u00F3n de toda la sociedad. La gesti\u00F3n de los bienes fiscales es responsabilidad de las autoridades del Estado, quienes deben velar por su correcta utilizaci\u00F3n y conservaci\u00F3n. Estos pueden ser de distinto tipo tales como: terrenos, edificios, maquinarias, veh\u00EDculos, entre otros. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](15, "div", 10)(16, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function S1L1V3Component_Template_div_click_16_listener() {
        return ctx.viewInfo(2);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](17, "div", 12)(18, "div", 9)(19, "b");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](20, "Bienes Nacionales de Uso P\u00FAblico (BNUP),");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](21, " entendiendo por estos aquellos cuyo uso pertenece a todos los habitantes de la naci\u00F3n, tales como: calles, plazas, puentes y caminos, el mar adyacente y sus playas. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](22, "div", 13)(23, "div", 14);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function S1L1V3Component_Template_div_click_23_listener() {
        return ctx.viewInfo(3);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](24, "div", 15)(25, "div", 16);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](26, " El ");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](27, "b");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](28, "articulo 589,");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](29, " del C\u00F3digo Civil chileno, establece que llaman o son bienes nacionales de uso p\u00FAblico, cuyo dominio pertenece a la Naci\u00F3n toda y que, adem\u00E1s su uso pertenece a todos los habitantes de la Naci\u00F3n. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()()()();
    }
  },
  styles: [".S1L1V3_container[_ngcontent-%COMP%] {\n    position: absolute;\n    border: calc(var(--frameHeight)*0.012) solid #484849;\n    background-color: #fff;\n    border-radius: 20px;\n    height: 73%;\n    left: 8.5%;\n    overflow: hidden;\n    top: 18%;\n    width: 83%;\n}\n\n\n.fadeInDownBig[_ngcontent-%COMP%] {\n    animation-name: _ngcontent-%COMP%_fadeInDownBig;\n  }\n  .animated[_ngcontent-%COMP%] {\n    animation-duration: 2s;\n    animation-fill-mode: both;\n  }\n\n.rubberBand[_ngcontent-%COMP%] {\n    animation: rubberBand 1s infinite;\n    animation-duration: 1s;\n    animation-timing-function: ease;\n    animation-delay: 0s;\n    animation-iteration-count: infinite;\n    animation-direction: normal;\n    animation-fill-mode: none;\n    animation-play-state: running;\n    animation-name: rubberBand;\n  }\n\n\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%] {\n    position: absolute;\n    font-family: var(--font-family-light);\n    font-size: (var(--frameHeight)*0.012);\n    height: 90%;\n    top: calc(var(--frameHeight)*0.04);\n    width: 85%;\n  \n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%] {\n    width: 100%;\n    color: #ED6B06;\n    display: block;\n    font-size: calc(var(--frameHeight)*0.04);\n    text-align: center;\n    margin-bottom: 3%;\n\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text1[_ngcontent-%COMP%]{\n    font-family: var(--font-family-light);\n    font-size: calc(var(--frameHeight)*0.09);\n    font-weight: normal; \n    text-align: center;\n    margin-bottom: calc(var(--frameHeight)*-0.02);\n    width: 100%;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text1[_ngcontent-%COMP%]{\n    font-family: var(--font-family-light);\n    font-size: calc(var(--frameHeight)*0.07);\n    font-weight: normal;\n    text-align: initial;\n    margin-bottom: calc(var(--frameHeight)*-0.02);\n    margin-top: -2.5%;\n    width: 100%;\n    margin-left: 18%;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text2[_ngcontent-%COMP%]{\n    font-family: var(--font-family-bold);\n    font-size: calc(var(--frameHeight)*0.04);\n    font-weight: bolder;\n    line-height: 95%;\n    text-align: initial;\n    width: 100%;\n    margin-left: 10%;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .content[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%]{\n    margin-top: 20%;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .content[_ngcontent-%COMP%]{\n    display: grid;\n    grid-template-columns: repeat(2, 1fr);\n    grid-auto-rows: minmax(100px, auto);\n    justify-content: center;\n    height: 30%;\n}\n\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .content[_ngcontent-%COMP%]   .text1[_ngcontent-%COMP%]   .img1[_ngcontent-%COMP%]{\n    position:absolute;\n    background: url('btn_bienes_fiscales.png') no-repeat;\n    background-size: 100% 100%;\n    width: 15%;\n    height: 25%;\n    margin-left: 19%;\n    cursor: pointer;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .content[_ngcontent-%COMP%]   .text1[_ngcontent-%COMP%]   .img1[_ngcontent-%COMP%]:hover{\n    transform: translateY(5px);\n    box-shadow: none;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .content[_ngcontent-%COMP%]   .text1[_ngcontent-%COMP%]   .info1[_ngcontent-%COMP%]{\n    display: none;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .content[_ngcontent-%COMP%]   .text2[_ngcontent-%COMP%]   .img2[_ngcontent-%COMP%]{\n    position:absolute;\n    background: url('btn_bienes_nacionales.png') no-repeat;\n    background-size: 100% 100%;\n    width: 15%;\n    height: 25%;\n    margin-left: 19%;\n    cursor: pointer;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .content[_ngcontent-%COMP%]   .text2[_ngcontent-%COMP%]   .img2[_ngcontent-%COMP%]:hover{\n    transform: translateY(5px);\n    box-shadow: none;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .content[_ngcontent-%COMP%]   .text2[_ngcontent-%COMP%]   .info2[_ngcontent-%COMP%]{\n    display: none;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .text3[_ngcontent-%COMP%]{\n    margin-top: 14.7%;\n    margin-left: 15%;\n}\n\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .text3[_ngcontent-%COMP%]   .warning[_ngcontent-%COMP%]{\n    position:absolute;\n    background: url('btn_admiraci\u00F3n.png') no-repeat;\n    background-size: 100% 100%;\n    left: 11%;\n    top: 92%;\n    cursor: pointer;\n    width: 3%;\n    height: 7%;\n\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .text3[_ngcontent-%COMP%]   .warning[_ngcontent-%COMP%]:hover{\n    transform: translateY(5px);\n    box-shadow: none;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .text3[_ngcontent-%COMP%]   .info3[_ngcontent-%COMP%]{\n    display: none;\n}\n\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .text3[_ngcontent-%COMP%]   .info3[_ngcontent-%COMP%]   .text_warning[_ngcontent-%COMP%]{\n    *position: absolute;\n    *height: -webkit-fill-available;\n    font-family: var(--font-family-regular);\n    font-size: calc(var(--frameHeight)*0.024);\n    line-height: 100%;\n    overflow-y: auto;\n    \n    padding-top: calc(var(--frameHeight)*0.04);\n    text-align: justify;\n    margin-left: 0;\n    width: 90%;\n}\n\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .text3[_ngcontent-%COMP%]   .info3[_ngcontent-%COMP%]   .text_warning[_ngcontent-%COMP%] {\n    -ms-overflow-style: none;  \n    scrollbar-width: none;  \n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .text3[_ngcontent-%COMP%]   .info3[_ngcontent-%COMP%]   .text_warning[_ngcontent-%COMP%]::-webkit-scrollbar { \n    display: none;  \n}\n\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%] {\n    *position: absolute;\n    *height: -webkit-fill-available;\n    font-family: var(--font-family-regular);\n    font-size: calc(var(--frameHeight)*0.024);\n    line-height: 100%;\n    overflow-y: auto;\n    \n    margin-left: 4%;\n    padding-top: calc(var(--frameHeight)*0.04);\n    text-align: justify;\n    width: 95%;\n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%] {\n    -ms-overflow-style: none;  \n    scrollbar-width: none;  \n}\n.S1L1V3_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%]::-webkit-scrollbar { \n    display: none;  \n}\n\n@keyframes _ngcontent-%COMP%_fadeInDownBig {\n    from {\n        opacity: 0;\n        transform: translate3d(0, -100%, 0);\n    }\n\n    to {\n        opacity: 1;\n        transform: none;\n    }\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2VjdGlvbjEvbGVzc29uMS9TMUwxVjMvUzFMMVYzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFO0FBQ3JFO0lBQ0ksa0JBQWtCO0lBQ2xCLG9EQUFvRDtJQUNwRCxzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxVQUFVO0lBQ1YsZ0JBQWdCO0lBQ2hCLFFBQVE7SUFDUixVQUFVO0FBQ2Q7O0FBRUEsK0NBQStDO0FBQy9DO0lBRUksNkJBQTZCO0VBQy9CO0VBQ0E7SUFFRSxzQkFBc0I7SUFFdEIseUJBQXlCO0VBQzNCO0FBQ0YsK0NBQStDO0FBQy9DO0lBQ0ksaUNBQWlDO0lBQ2pDLHNCQUFzQjtJQUN0QiwrQkFBK0I7SUFDL0IsbUJBQW1CO0lBQ25CLG1DQUFtQztJQUNuQywyQkFBMkI7SUFDM0IseUJBQXlCO0lBQ3pCLDZCQUE2QjtJQUM3QiwwQkFBMEI7RUFDNUI7O0FBRUYsd0NBQXdDO0FBQ3hDO0lBQ0ksa0JBQWtCO0lBQ2xCLHFDQUFxQztJQUNyQyxxQ0FBcUM7SUFDckMsV0FBVztJQUNYLGtDQUFrQztJQUNsQyxVQUFVOztBQUVkO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsY0FBYztJQUNkLGNBQWM7SUFDZCx3Q0FBd0M7SUFDeEMsa0JBQWtCO0lBQ2xCLGlCQUFpQjs7QUFFckI7QUFDQTtJQUNJLHFDQUFxQztJQUNyQyx3Q0FBd0M7SUFDeEMsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQiw2Q0FBNkM7SUFDN0MsV0FBVztBQUNmO0FBQ0E7SUFDSSxxQ0FBcUM7SUFDckMsd0NBQXdDO0lBQ3hDLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsNkNBQTZDO0lBQzdDLGlCQUFpQjtJQUNqQixXQUFXO0lBQ1gsZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSxvQ0FBb0M7SUFDcEMsd0NBQXdDO0lBQ3hDLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGFBQWE7SUFDYixxQ0FBcUM7SUFDckMsbUNBQW1DO0lBQ25DLHVCQUF1QjtJQUN2QixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIsb0RBQW1HO0lBQ25HLDBCQUEwQjtJQUMxQixVQUFVO0lBQ1YsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixlQUFlO0FBQ25CO0FBQ0E7SUFDSSwwQkFBMEI7SUFDMUIsZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSxhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsc0RBQXFHO0lBQ3JHLDBCQUEwQjtJQUMxQixVQUFVO0lBQ1YsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixlQUFlO0FBQ25CO0FBQ0E7SUFDSSwwQkFBMEI7SUFDMUIsZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSxhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLCtDQUE4RjtJQUM5RiwwQkFBMEI7SUFDMUIsU0FBUztJQUNULFFBQVE7SUFDUixlQUFlO0lBQ2YsU0FBUztJQUNULFVBQVU7O0FBRWQ7QUFDQTtJQUNJLDBCQUEwQjtJQUMxQixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLGFBQWE7QUFDakI7O0FBRUE7S0FDSSxrQkFBbUI7S0FDbkIsOEJBQStCO0lBQy9CLHVDQUF1QztJQUN2Qyx5Q0FBeUM7SUFDekMsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixvQkFBb0I7SUFDcEIsMENBQTBDO0lBQzFDLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2QsVUFBVTtBQUNkOztBQUVBO0lBQ0ksd0JBQXdCLEdBQUcsMEJBQTBCO0lBQ3JELHFCQUFxQixHQUFHLFlBQVk7QUFDeEM7QUFDQTtJQUNJLGFBQWEsR0FBRyxzQkFBc0I7QUFDMUM7O0FBRUE7S0FDSSxrQkFBbUI7S0FDbkIsOEJBQStCO0lBQy9CLHVDQUF1QztJQUN2Qyx5Q0FBeUM7SUFDekMsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLDBDQUEwQztJQUMxQyxtQkFBbUI7SUFDbkIsVUFBVTtBQUNkO0FBQ0E7SUFDSSx3QkFBd0IsR0FBRywwQkFBMEI7SUFDckQscUJBQXFCLEdBQUcsWUFBWTtBQUN4QztBQUNBO0lBQ0ksYUFBYSxHQUFHLHNCQUFzQjtBQUMxQzs7QUFFQTtJQUNJO1FBQ0ksVUFBVTtRQUNWLG1DQUFtQztJQUN2Qzs7SUFFQTtRQUNJLFVBQVU7UUFDVixlQUFlO0lBQ25CO0FBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKiAgIENhamEgcHJpbmNpcGFsIGNvbiBlbCBjb250ZW5pZG8gZGUgbGFzIGxlY2Npb25lcyAgICoqKioqKiovXG4uUzFMMVYzX2NvbnRhaW5lciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvcmRlcjogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpIHNvbGlkICM0ODQ4NDk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgIGhlaWdodDogNzMlO1xuICAgIGxlZnQ6IDguNSU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB0b3A6IDE4JTtcbiAgICB3aWR0aDogODMlO1xufVxuXG4vKioqKioqKiAgIEFuaW1hY2nDg8KzbiBkZSBsbGVnYWRhIHZpc3RhICAgKioqKioqKi9cbi5mYWRlSW5Eb3duQmlnIHtcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1uYW1lOiBmYWRlSW5Eb3duQmlnO1xuICAgIGFuaW1hdGlvbi1uYW1lOiBmYWRlSW5Eb3duQmlnO1xuICB9XG4gIC5hbmltYXRlZCB7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tZHVyYXRpb246IDJzO1xuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMnM7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tZmlsbC1tb2RlOiBib3RoO1xuICAgIGFuaW1hdGlvbi1maWxsLW1vZGU6IGJvdGg7XG4gIH1cbi8qKioqKioqICAgQW5pbWFjacODwrNuIGRlIGxsZWdhZGEgdmlzdGEgICAqKioqKioqL1xuLnJ1YmJlckJhbmQge1xuICAgIGFuaW1hdGlvbjogcnViYmVyQmFuZCAxcyBpbmZpbml0ZTtcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDFzO1xuICAgIGFuaW1hdGlvbi10aW1pbmctZnVuY3Rpb246IGVhc2U7XG4gICAgYW5pbWF0aW9uLWRlbGF5OiAwcztcbiAgICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcbiAgICBhbmltYXRpb24tZGlyZWN0aW9uOiBub3JtYWw7XG4gICAgYW5pbWF0aW9uLWZpbGwtbW9kZTogbm9uZTtcbiAgICBhbmltYXRpb24tcGxheS1zdGF0ZTogcnVubmluZztcbiAgICBhbmltYXRpb24tbmFtZTogcnViYmVyQmFuZDtcbiAgfVxuXG4vKioqKioqKiAgIENhamEgY2VudHJhbCB0ZXh0b3MgICAqKioqKioqL1xuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWxpZ2h0KTtcbiAgICBmb250LXNpemU6ICh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpO1xuICAgIGhlaWdodDogOTAlO1xuICAgIHRvcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNCk7XG4gICAgd2lkdGg6IDg1JTtcbiAgXG59XG4uUzFMMVYzX2NvbnRhaW5lciAuYm94X3RleHQgLnRpdGxlX3RleHQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAjRUQ2QjA2O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNCk7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xuXG59XG4uUzFMMVYzX2NvbnRhaW5lciAuYm94X3RleHQgLnRpdGxlX3RleHQgLnRpdGxlX3RleHQxe1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1saWdodCk7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA5KTtcbiAgICBmb250LXdlaWdodDogbm9ybWFsOyBcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqLTAuMDIpO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC50aXRsZV90ZXh0IC50aXRsZV90ZXh0MXtcbiAgICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktbGlnaHQpO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNyk7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICB0ZXh0LWFsaWduOiBpbml0aWFsO1xuICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKi0wLjAyKTtcbiAgICBtYXJnaW4tdG9wOiAtMi41JTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tbGVmdDogMTglO1xufVxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC50aXRsZV90ZXh0IC50aXRsZV90ZXh0MntcbiAgICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktYm9sZCk7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA0KTtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGxpbmUtaGVpZ2h0OiA5NSU7XG4gICAgdGV4dC1hbGlnbjogaW5pdGlhbDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tbGVmdDogMTAlO1xufVxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC5jb250ZW50IC5wYXJhZ3JhcGh7XG4gICAgbWFyZ2luLXRvcDogMjAlO1xufVxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC5jb250ZW50e1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMiwgMWZyKTtcbiAgICBncmlkLWF1dG8tcm93czogbWlubWF4KDEwMHB4LCBhdXRvKTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDMwJTtcbn1cblxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC5jb250ZW50IC50ZXh0MSAuaW1nMXtcbiAgICBwb3NpdGlvbjphYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjEvdmlldzMvaW1hZ2VzL2J0bl9iaWVuZXNfZmlzY2FsZXMucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgd2lkdGg6IDE1JTtcbiAgICBoZWlnaHQ6IDI1JTtcbiAgICBtYXJnaW4tbGVmdDogMTklO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5TMUwxVjNfY29udGFpbmVyIC5ib3hfdGV4dCAuY29udGVudCAudGV4dDEgLmltZzE6aG92ZXJ7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDVweCk7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbn1cbi5TMUwxVjNfY29udGFpbmVyIC5ib3hfdGV4dCAuY29udGVudCAudGV4dDEgLmluZm8xe1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG4uUzFMMVYzX2NvbnRhaW5lciAuYm94X3RleHQgLmNvbnRlbnQgLnRleHQyIC5pbWcye1xuICAgIHBvc2l0aW9uOmFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMS92aWV3My9pbWFnZXMvYnRuX2JpZW5lc19uYWNpb25hbGVzLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHdpZHRoOiAxNSU7XG4gICAgaGVpZ2h0OiAyNSU7XG4gICAgbWFyZ2luLWxlZnQ6IDE5JTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uUzFMMVYzX2NvbnRhaW5lciAuYm94X3RleHQgLmNvbnRlbnQgLnRleHQyIC5pbWcyOmhvdmVye1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSg1cHgpO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG59XG4uUzFMMVYzX2NvbnRhaW5lciAuYm94X3RleHQgLmNvbnRlbnQgLnRleHQyIC5pbmZvMntcbiAgICBkaXNwbGF5OiBub25lO1xufVxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC50ZXh0M3tcbiAgICBtYXJnaW4tdG9wOiAxNC43JTtcbiAgICBtYXJnaW4tbGVmdDogMTUlO1xufVxuXG4uUzFMMVYzX2NvbnRhaW5lciAuYm94X3RleHQgLnRleHQzIC53YXJuaW5ne1xuICAgIHBvc2l0aW9uOmFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMS92aWV3My9pbWFnZXMvYnRuX2FkbWlyYWNpw4PCs24ucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgbGVmdDogMTElO1xuICAgIHRvcDogOTIlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB3aWR0aDogMyU7XG4gICAgaGVpZ2h0OiA3JTtcblxufVxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC50ZXh0MyAud2FybmluZzpob3ZlcntcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoNXB4KTtcbiAgICBib3gtc2hhZG93OiBub25lO1xufVxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC50ZXh0MyAuaW5mbzN7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC50ZXh0MyAuaW5mbzMgLnRleHRfd2FybmluZ3tcbiAgICAqcG9zaXRpb246IGFic29sdXRlO1xuICAgICpoZWlnaHQ6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LXJlZ3VsYXIpO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjQpO1xuICAgIGxpbmUtaGVpZ2h0OiAxMDAlO1xuICAgIG92ZXJmbG93LXk6IGF1dG87XG4gICAgLyogbWFyZ2luOiAwIGF1dG87ICovXG4gICAgcGFkZGluZy10b3A6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDQpO1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgd2lkdGg6IDkwJTtcbn1cblxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC50ZXh0MyAuaW5mbzMgLnRleHRfd2FybmluZyB7XG4gICAgLW1zLW92ZXJmbG93LXN0eWxlOiBub25lOyAgLyogSW50ZXJuZXQgRXhwbG9yZXIgMTArICovXG4gICAgc2Nyb2xsYmFyLXdpZHRoOiBub25lOyAgLyogRmlyZWZveCAqL1xufVxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC50ZXh0MyAuaW5mbzMgLnRleHRfd2FybmluZzo6LXdlYmtpdC1zY3JvbGxiYXIgeyBcbiAgICBkaXNwbGF5OiBub25lOyAgLyogU2FmYXJpIGFuZCBDaHJvbWUgKi9cbn1cblxuLlMxTDFWM19jb250YWluZXIgLmJveF90ZXh0IC5wYXJhZ3JhcGgge1xuICAgICpwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgKmhlaWdodDogLXdlYmtpdC1maWxsLWF2YWlsYWJsZTtcbiAgICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktcmVndWxhcik7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyNCk7XG4gICAgbGluZS1oZWlnaHQ6IDEwMCU7XG4gICAgb3ZlcmZsb3cteTogYXV0bztcbiAgICAvKiBtYXJnaW46IDAgYXV0bzsgKi9cbiAgICBtYXJnaW4tbGVmdDogNCU7XG4gICAgcGFkZGluZy10b3A6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDQpO1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgd2lkdGg6IDk1JTtcbn1cbi5TMUwxVjNfY29udGFpbmVyIC5ib3hfdGV4dCAucGFyYWdyYXBoIHtcbiAgICAtbXMtb3ZlcmZsb3ctc3R5bGU6IG5vbmU7ICAvKiBJbnRlcm5ldCBFeHBsb3JlciAxMCsgKi9cbiAgICBzY3JvbGxiYXItd2lkdGg6IG5vbmU7ICAvKiBGaXJlZm94ICovXG59XG4uUzFMMVYzX2NvbnRhaW5lciAuYm94X3RleHQgLnBhcmFncmFwaDo6LXdlYmtpdC1zY3JvbGxiYXIgeyBcbiAgICBkaXNwbGF5OiBub25lOyAgLyogU2FmYXJpIGFuZCBDaHJvbWUgKi9cbn1cblxuQGtleWZyYW1lcyBmYWRlSW5Eb3duQmlnIHtcbiAgICBmcm9tIHtcbiAgICAgICAgb3BhY2l0eTogMDtcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAtMTAwJSwgMCk7XG4gICAgfVxuXG4gICAgdG8ge1xuICAgICAgICBvcGFjaXR5OiAxO1xuICAgICAgICB0cmFuc2Zvcm06IG5vbmU7XG4gICAgfVxufSJdLCJzb3VyY2VSb290IjoiIn0= */"]
});

/***/ }),

/***/ 4790:
/*!*************************************************************!*\
  !*** ./src/app/section1/lesson1/S1L1V4/S1L1V4.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "S1L1V4Component": () => (/* binding */ S1L1V4Component)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ 5139);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var src_app_services_progress_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/progress.service */ 8458);





const _c0 = ["characterId"];
class S1L1V4Component {
  constructor(router, progressService) {
    this.router = router;
    this.progressService = progressService;
    this.stateSoundMenu = 'active';
    this.showModalFinish = false;
    this.characterObject = {
      'left': '79%',
      'top': '32%',
      'height': '59%',
      'width': '13.5%'
    };
    /* Mensaje que será enviado al componente modal */
    this.modalMessage = "Lee con atención la información que te presentamos, navega con las flechas y visualiza todas las imagenes. Al finalizar continua navegando por el curso.";
    /* Audios del personaje leyendo el texto principal y el texto oculto */
    this.audiosCharacter = {
      1: new Audio(`../../../../assets/section1/sounds/Estatutos.mp3`),
      2: new Audio(`../../../../assets/section1/sounds/lesson1/S1L1V4_2.mp3`)
    };
    this.isClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_2__.EventEmitter();
  }
  ngOnInit() {
    console.log('-- S1L1V4 ngOnInit');
  }
  viewDetails() {
    // $(`.li`).css('display', 'block');
    this.isClicked.emit(2);
    setTimeout(() => {
      jquery__WEBPACK_IMPORTED_MODULE_0__(`.li1`).css('display', 'block');
      setTimeout(() => {
        jquery__WEBPACK_IMPORTED_MODULE_0__(`.li2`).css('display', 'block');
        setTimeout(() => {
          jquery__WEBPACK_IMPORTED_MODULE_0__(`.li3`).css('display', 'block');
          setTimeout(() => {
            jquery__WEBPACK_IMPORTED_MODULE_0__(`.li4`).css('display', 'block');
            setTimeout(() => {
              jquery__WEBPACK_IMPORTED_MODULE_0__(`.li5`).css('display', 'block');
              setTimeout(() => {
                jquery__WEBPACK_IMPORTED_MODULE_0__(`.li6`).css('display', 'block');
              }, 1000);
            }, 1000);
          }, 1000);
        }, 1000);
      }, 1000);
    }, 200);
  }
}
S1L1V4Component.ɵfac = function S1L1V4Component_Factory(t) {
  return new (t || S1L1V4Component)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_progress_service__WEBPACK_IMPORTED_MODULE_1__.ProgressService));
};
S1L1V4Component.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
  type: S1L1V4Component,
  selectors: [["app-S1L1V4"]],
  viewQuery: function S1L1V4Component_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵviewQuery"](_c0, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵloadQuery"]()) && (ctx.characterComponent = _t.first);
    }
  },
  outputs: {
    isClicked: "isClicked"
  },
  decls: 26,
  vars: 0,
  consts: [[1, "S1L1V4_container"], [1, "box_text"], [1, "title_text"], [1, "title_text1"], [1, "title_text2"], [1, "paragraph"], ["type", "button", 1, "btnDetails", 3, "click"], [1, "box_text2"], [1, "li", "li1", "animated", "fadeInDownBig"], [1, "li", "li2", "animated", "fadeInDownBig"], [1, "li", "li3", "animated", "fadeInDownBig"], [1, "li", "li4", "animated", "fadeInDownBig"], [1, "li", "li5", "animated", "fadeInDownBig"], [1, "li", "li6", "animated", "fadeInDownBig"]],
  template: function S1L1V4Component_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0)(1, "div", 1)(2, "div", 2)(3, "div", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](4, "ESTATUTOS REGULATORIOS");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](6, "DE LOS BIENES NACIONALES DE USO P\u00DABLICO");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](8, " Los Bienes Nacionales de uso p\u00FAblico, est\u00E1n bajo el control del Ministerio de Bienes Nacionales, es el encargado de formular, coordinar y supervisar la pol\u00EDtica nacional en materia de bienes nacionales de uso p\u00FAblico, sin perjuicio de las potestades de administraci\u00F3n de dichos bienes que seg\u00FAn su naturaleza y fines recaigan sobre otros organismos del Estado. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](9, "br")(10, "br");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](11, " A continuaci\u00F3n, detallaremos los estatutos y entes reguladores de los bienes nacionales de uso p\u00FAblico en Chile que son: ");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function S1L1V4Component_Template_div_click_12_listener() {
        return ctx.viewDetails();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](13, "div", 7)(14, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](15, "\u00B0 La Constituci\u00F3n Pol\u00EDtica de la Rep\u00FAblica");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](16, "div", 9);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](17, "\u00B0 La Ley N\u00B0 19.300 sobre Bases Generales del Medio Ambiente");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](18, "div", 10);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](19, "\u00B0 La Ley N\u00B0 17.288 de Monumentos Nacionales");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](20, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](21, "\u00B0 La Ley N\u00B0 20.240 de Accesibilidad Universal");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](22, "div", 12);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](23, "\u00B0 El Servicio de Bienes Nacionales");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](24, "div", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](25, "\u00B0 El Ministerio de Bienes Nacionales");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()();
    }
  },
  styles: [".S1L1V4_container[_ngcontent-%COMP%] {\n    position: absolute;\n    border: calc(var(--frameHeight)*0.012) solid #484849;\n    background-color: #fff;\n    border-radius: 20px;\n    height: 73%;\n    left: 8.5%;\n    overflow: hidden;\n    top: 18%;\n    width: 83%;\n}\n\n\n\n.S1L1V4_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%] {\n    position: absolute;\n    font-family: var(--font-family-light);\n    font-size: (var(--frameHeight)*0.012);\n    height: 90%;\n    right: 50%;\n    top: calc(var(--frameHeight)*0.04);\n    width: 49%;\n}\n.S1L1V4_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%] {\n    width: 100%;\n    color: #ED6B06;\n    display: block;\n}\n.S1L1V4_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text1[_ngcontent-%COMP%]{\n    font-family: var(--font-family-light);\n    font-size: calc(var(--frameHeight)*0.068);\n    font-weight: normal;\n    text-align: center;\n    margin-bottom: calc(var(--frameHeight)*-0.02);\n    width: 100%;\n}\n.S1L1V4_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text2[_ngcontent-%COMP%]{\n    font-family: var(--font-family-bold);\n    font-size: calc(var(--frameHeight)*0.03);\n    font-weight: bolder;\n    line-height: 95%;\n    text-align: center;\n    width: 100%;\n    margin-top: 2%;\n}\n.S1L1V4_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%] {\n    *position: absolute;\n    *height: -webkit-fill-available;\n    color: #484849;\n    font-family: var(--font-family-medium);\n    font-size: calc(var(--frameHeight)*0.024);\n    line-height: 125%;\n    overflow-y: auto;\n    margin: 0 auto;\n    padding-top: calc(var(--frameHeight)*0.04);\n    text-align: justify;\n    width: 78%;\n}\n.S1L1V4_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%] {\n    -ms-overflow-style: none;  \n    scrollbar-width: none;  \n}\n.S1L1V4_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%]::-webkit-scrollbar { \n    display: none;  \n}\n.S1L1V4_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .btnDetails[_ngcontent-%COMP%] {\n    background: url('flecha_texto.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.04);\n    width: calc(var(--frameHeight)*0.03);\n    position: absolute;\n    top: 77%;\n    left: 39%;\n    width: 12%;\n    height: 16%;\n}\n.S1L1V4_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .btnDetails[_ngcontent-%COMP%]:hover {\n    background: url('hover_flecha_texto.png') no-repeat;\n    background-size: 100% 100%;\n}\n\n\n.box_text2[_ngcontent-%COMP%] {\n    position: absolute;\n    height: 100%;\n    left: 49%;\n    width: 50%;\n    top: 25%;\n}\n.box_text2[_ngcontent-%COMP%]   .li[_ngcontent-%COMP%]{\n    font-size: calc(var(--frameHeight)*0.024);\n    margin-top: 2%;\n    display: none;\n}\n\n\n.fadeInDownBig[_ngcontent-%COMP%] {\n    animation-name: fadeInDownBig;\n  }\n  .animated[_ngcontent-%COMP%] {\n    animation-duration: 1s;\n    animation-fill-mode: both;\n  }\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2VjdGlvbjEvbGVzc29uMS9TMUwxVjQvUzFMMVY0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFO0FBQ3JFO0lBQ0ksa0JBQWtCO0lBQ2xCLG9EQUFvRDtJQUNwRCxzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxVQUFVO0lBQ1YsZ0JBQWdCO0lBQ2hCLFFBQVE7SUFDUixVQUFVO0FBQ2Q7OztBQUdBLHdDQUF3QztBQUN4QztJQUNJLGtCQUFrQjtJQUNsQixxQ0FBcUM7SUFDckMscUNBQXFDO0lBQ3JDLFdBQVc7SUFDWCxVQUFVO0lBQ1Ysa0NBQWtDO0lBQ2xDLFVBQVU7QUFDZDtBQUNBO0lBQ0ksV0FBVztJQUNYLGNBQWM7SUFDZCxjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxxQ0FBcUM7SUFDckMseUNBQXlDO0lBQ3pDLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsNkNBQTZDO0lBQzdDLFdBQVc7QUFDZjtBQUNBO0lBQ0ksb0NBQW9DO0lBQ3BDLHdDQUF3QztJQUN4QyxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsY0FBYztBQUNsQjtBQUNBO0tBQ0ksa0JBQW1CO0tBQ25CLDhCQUErQjtJQUMvQixjQUFjO0lBQ2Qsc0NBQXNDO0lBQ3RDLHlDQUF5QztJQUN6QyxpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCwwQ0FBMEM7SUFDMUMsbUJBQW1CO0lBQ25CLFVBQVU7QUFDZDtBQUNBO0lBQ0ksd0JBQXdCLEdBQUcsMEJBQTBCO0lBQ3JELHFCQUFxQixHQUFHLFlBQVk7QUFDeEM7QUFDQTtJQUNJLGFBQWEsR0FBRyxzQkFBc0I7QUFDMUM7QUFDQTtJQUNJLDZDQUFxRjtJQUNyRiwwQkFBMEI7SUFDMUIscUNBQXFDO0lBQ3JDLG9DQUFvQztJQUNwQyxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCxVQUFVO0lBQ1YsV0FBVztBQUNmO0FBQ0E7SUFDSSxtREFBMkY7SUFDM0YsMEJBQTBCO0FBQzlCOztBQUVBLHlDQUF5QztBQUN6QztJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osU0FBUztJQUNULFVBQVU7SUFDVixRQUFRO0FBQ1o7QUFDQTtJQUNJLHlDQUF5QztJQUN6QyxjQUFjO0lBQ2QsYUFBYTtBQUNqQjs7QUFFQSwrQ0FBK0M7QUFDL0M7SUFFSSw2QkFBNkI7RUFDL0I7RUFDQTtJQUVFLHNCQUFzQjtJQUV0Qix5QkFBeUI7RUFDM0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKiAgIENhamEgcHJpbmNpcGFsIGNvbiBlbCBjb250ZW5pZG8gZGUgbGFzIGxlY2Npb25lcyAgICoqKioqKiovXG4uUzFMMVY0X2NvbnRhaW5lciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvcmRlcjogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpIHNvbGlkICM0ODQ4NDk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgIGhlaWdodDogNzMlO1xuICAgIGxlZnQ6IDguNSU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB0b3A6IDE4JTtcbiAgICB3aWR0aDogODMlO1xufVxuXG5cbi8qKioqKioqICAgQ2FqYSBjZW50cmFsIHRleHRvcyAgICoqKioqKiovXG4uUzFMMVY0X2NvbnRhaW5lciAuYm94X3RleHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktbGlnaHQpO1xuICAgIGZvbnQtc2l6ZTogKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxMik7XG4gICAgaGVpZ2h0OiA5MCU7XG4gICAgcmlnaHQ6IDUwJTtcbiAgICB0b3A6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDQpO1xuICAgIHdpZHRoOiA0OSU7XG59XG4uUzFMMVY0X2NvbnRhaW5lciAuYm94X3RleHQgLnRpdGxlX3RleHQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAjRUQ2QjA2O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xufVxuLlMxTDFWNF9jb250YWluZXIgLmJveF90ZXh0IC50aXRsZV90ZXh0IC50aXRsZV90ZXh0MXtcbiAgICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktbGlnaHQpO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNjgpO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKi0wLjAyKTtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5TMUwxVjRfY29udGFpbmVyIC5ib3hfdGV4dCAudGl0bGVfdGV4dCAudGl0bGVfdGV4dDJ7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWJvbGQpO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMyk7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBsaW5lLWhlaWdodDogOTUlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAyJTtcbn1cbi5TMUwxVjRfY29udGFpbmVyIC5ib3hfdGV4dCAucGFyYWdyYXBoIHtcbiAgICAqcG9zaXRpb246IGFic29sdXRlO1xuICAgICpoZWlnaHQ6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7XG4gICAgY29sb3I6ICM0ODQ4NDk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LW1lZGl1bSk7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyNCk7XG4gICAgbGluZS1oZWlnaHQ6IDEyNSU7XG4gICAgb3ZlcmZsb3cteTogYXV0bztcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBwYWRkaW5nLXRvcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNCk7XG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICB3aWR0aDogNzglO1xufVxuLlMxTDFWNF9jb250YWluZXIgLmJveF90ZXh0IC5wYXJhZ3JhcGgge1xuICAgIC1tcy1vdmVyZmxvdy1zdHlsZTogbm9uZTsgIC8qIEludGVybmV0IEV4cGxvcmVyIDEwKyAqL1xuICAgIHNjcm9sbGJhci13aWR0aDogbm9uZTsgIC8qIEZpcmVmb3ggKi9cbn1cbi5TMUwxVjRfY29udGFpbmVyIC5ib3hfdGV4dCAucGFyYWdyYXBoOjotd2Via2l0LXNjcm9sbGJhciB7IFxuICAgIGRpc3BsYXk6IG5vbmU7ICAvKiBTYWZhcmkgYW5kIENocm9tZSAqL1xufVxuLlMxTDFWNF9jb250YWluZXIgLmJveF90ZXh0IC5idG5EZXRhaWxzIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjEvdmlldzQvZmxlY2hhX3RleHRvLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNCk7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDMpO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDc3JTtcbiAgICBsZWZ0OiAzOSU7XG4gICAgd2lkdGg6IDEyJTtcbiAgICBoZWlnaHQ6IDE2JTtcbn1cbi5TMUwxVjRfY29udGFpbmVyIC5ib3hfdGV4dCAuYnRuRGV0YWlsczpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9sZXNzb24xL3ZpZXc0L2hvdmVyX2ZsZWNoYV90ZXh0by5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cblxuLyoqKioqKiogICBDYWphIHNsaWRlciBpbWFnZW5lcyAgICoqKioqKiovXG4uYm94X3RleHQyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGxlZnQ6IDQ5JTtcbiAgICB3aWR0aDogNTAlO1xuICAgIHRvcDogMjUlO1xufVxuLmJveF90ZXh0MiAubGl7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyNCk7XG4gICAgbWFyZ2luLXRvcDogMiU7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuLyoqKioqKiogICBBbmltYWNpw4PCs24gZGUgbGxlZ2FkYSB2aXN0YSAgICoqKioqKiovXG4uZmFkZUluRG93bkJpZyB7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tbmFtZTogZmFkZUluRG93bkJpZztcbiAgICBhbmltYXRpb24tbmFtZTogZmFkZUluRG93bkJpZztcbiAgfVxuICAuYW5pbWF0ZWQge1xuICAgIC13ZWJraXQtYW5pbWF0aW9uLWR1cmF0aW9uOiAxcztcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDFzO1xuICAgIC13ZWJraXQtYW5pbWF0aW9uLWZpbGwtbW9kZTogYm90aDtcbiAgICBhbmltYXRpb24tZmlsbC1tb2RlOiBib3RoO1xuICB9Il0sInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 2890:
/*!*************************************************************!*\
  !*** ./src/app/section1/lesson1/S1L1V5/S1L1V5.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "S1L1V5Component": () => (/* binding */ S1L1V5Component)
/* harmony export */ });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ 5139);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var src_app_services_progress_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/progress.service */ 8458);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 4666);
/* harmony import */ var igniteui_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! igniteui-angular */ 480);






const _c0 = ["characterId"];
const _c1 = ["carousel"];
function S1L1V5Component_igx_slide_16_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "igx-slide")(1, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](2, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const slide_r5 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngClass", slide_r5.image);
  }
}
function S1L1V5Component_ng_template_17_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "button", 19);
  }
}
function S1L1V5Component_ng_template_18_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "button", 20);
  }
}
function S1L1V5Component_div_19_Template(rf, ctx) {
  if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 21)(1, "div", 22)(2, "div", 23)(3, "div", 24)(4, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](5, " Felicitaciones has completado con \u00E9xito toda la lecci\u00F3n, continua con el m\u00F3dulo 2. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function S1L1V5Component_div_19_Template_div_click_6_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r9);
      const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r8.closeModal());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](7, "CONTINUAR");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function S1L1V5Component_div_19_Template_div_click_8_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r9);
      const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r10.closeModal());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()()()();
  }
}
class S1L1V5Component {
  constructor(router, progressService) {
    this.router = router;
    this.progressService = progressService;
    this.stateSoundMenu = 'active';
    this.showModalFinish = false;
    this.characterObject = {
      'left': '79%',
      'top': '32%',
      'height': '59%',
      'width': '13.5%'
    };
    /* Mensaje que será enviado al componente modal */
    this.modalMessage = "Lee con atención la información que te presentamos, navega con las flechas y visualiza todas las imagenes. Al finalizar continua navegando por el curso.";
    /* Audio del personaje leyendo el texto principal */
    this.audioCharacter = new Audio(`../../../../assets/section1/sounds/Servicios.mp3`);
    this.slides = [];
    this.animations = ['fade'];
  }
  ngOnInit() {
    console.log('-- S1L1V5 ngOnInit');
    this.addSlides();
  }
  /* public animations = ['slide', 'fade', 'none']; */
  addSlides() {
    this.slides.push({
      image: 'imagen1'
    }, {
      image: 'imagen2'
    }, {
      image: 'imagen3'
    }, {
      image: 'imagen4'
    }, {
      image: 'imagen5'
    }, {
      image: 'imagen6'
    }, {
      image: 'imagen7'
    }, {
      image: 'imagen8'
    }, {
      image: 'imagen9'
    }, {
      image: 'imagen10'
    });
  }
  /*
  * Muestra el modal de finalización del juego
  */
  openModal() {
    jquery__WEBPACK_IMPORTED_MODULE_0__("#modal_fin").show();
    jquery__WEBPACK_IMPORTED_MODULE_0__(`app-layout.layout1`)[0].childNodes[0].style.zIndex = 1;
  }
  closeModal() {
    jquery__WEBPACK_IMPORTED_MODULE_0__("#modal_fin").hide();
    this.showModalFinish = false;
    if (this.progressService.getProgressPercentage() < 25.5) {
      this.progressService.setProgressPercentage(25.5);
    }
    ;
    this.progressService.setLesson2Available('active');
    this.router.navigate(['/section1']);
  }
}
S1L1V5Component.ɵfac = function S1L1V5Component_Factory(t) {
  return new (t || S1L1V5Component)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_progress_service__WEBPACK_IMPORTED_MODULE_1__.ProgressService));
};
S1L1V5Component.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
  type: S1L1V5Component,
  selectors: [["app-S1L1V5"]],
  viewQuery: function S1L1V5Component_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵviewQuery"](_c0, 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵviewQuery"](_c1, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵloadQuery"]()) && (ctx.characterComponent = _t.first);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵloadQuery"]()) && (ctx.carousel = _t.first);
    }
  },
  outputs: {
    isClicked: "isClicked"
  },
  decls: 20,
  vars: 2,
  consts: [[1, "S1L1V5_container"], [1, "box_text"], [1, "title_text"], [1, "title_text1"], [1, "title_text2"], [1, "paragraph"], [1, "box_images"], [1, "box_border"], [1, "box_slider"], [1, "carousel-animation-wrapper", "box_slider"], [1, "carousel-wrapper"], ["animationType", "fade", "showIndicators", "'false"], ["carousel", ""], [4, "ngFor", "ngForOf"], ["igxCarouselPrevButton", ""], ["igxCarouselNextButton", ""], ["class", "modal fade", "id", "modal_fin", "aria-labelledby", "exampleModalToggleLabel", "tabindex", "-1", 4, "ngIf"], [1, "slide-wrapper"], [3, "ngClass"], ["igxButton", "fab", "igxRipple", "white", 1, "left_arrow"], ["igxButton", "fab", "igxRipple", "red", 1, "right_arrow"], ["id", "modal_fin", "aria-labelledby", "exampleModalToggleLabel", "tabindex", "-1", 1, "modal", "fade"], [1, "modal-dialog-advanced", "modal-dialog-centered"], [1, "modal-content"], [1, "modal-body"], [1, "modal-body-message"], ["type", "button", 1, "button_continue", 3, "click"], ["type", "button", 1, "button_close", 3, "click"]],
  template: function S1L1V5Component_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0)(1, "div", 1)(2, "div", 2)(3, "div", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](4, "SERVICIOS");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](6, "B\u00C1SICOS");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](8, " Se define como SERVICIOS B\u00C1SICOS al conjunto de servicios prestados por empresas p\u00FAblicas, privadas o mixtas (p\u00FAblico-privado), en materia de agua potable y alcantarillado sanitario, energ\u00EDa el\u00E9ctrica, gas y telefon\u00EDa. Estos servicios se distribuyen en la ciudad de diferentes formas, algunos se pueden distribuir de manera subterr\u00E1nea y/o a\u00E9rea, como lo es la energ\u00EDa el\u00E9ctrica. Otros, como el gas y el agua s\u00F3lo en forma subterr\u00E1nea. Estas redes, que son fuentes de energ\u00EDa, se ubican en nuestras ciudades, comuna o pa\u00EDs, en los Bienes Nacionales de Uso P\u00FAblico (BNUP), es decir, recorren las aceras, veredas, calzadas, plazas, puentes, carreteras, entre otros; para llegar al cliente final y, aunque muchas veces no las vemos, EXISTEN\u201D y est\u00E1n en servicio. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](9, "div", 6)(10, "div", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](11, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "div", 9)(13, "div", 10)(14, "igx-carousel", 11, 12);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](16, S1L1V5Component_igx_slide_16_Template, 3, 1, "igx-slide", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](17, S1L1V5Component_ng_template_17_Template, 1, 0, "ng-template", 14);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](18, S1L1V5Component_ng_template_18_Template, 1, 0, "ng-template", 15);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()()()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](19, S1L1V5Component_div_19_Template, 9, 0, "div", 16);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](16);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", ctx.slides);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.showModalFinish);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgForOf, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, igniteui_angular__WEBPACK_IMPORTED_MODULE_5__.IgxCarouselComponent, igniteui_angular__WEBPACK_IMPORTED_MODULE_5__.IgxSlideComponent, igniteui_angular__WEBPACK_IMPORTED_MODULE_5__.IgxCarouselNextButtonDirective, igniteui_angular__WEBPACK_IMPORTED_MODULE_5__.IgxCarouselPrevButtonDirective, igniteui_angular__WEBPACK_IMPORTED_MODULE_5__.IgxButtonDirective],
  styles: [".S1L1V5_container[_ngcontent-%COMP%] {\n    position: absolute;\n    border: calc(var(--frameHeight)*0.012) solid #484849;\n    background-color: #fff;\n    border-radius: 20px;\n    height: 73%;\n    left: 8.5%;\n    overflow: hidden;\n    top: 18%;\n    width: 83%;\n}\n\n\n\n.S1L1V5_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%] {\n    position: absolute;\n    font-family: var(--font-family-light);\n    font-size: (var(--frameHeight)*0.012);\n    height: 90%;\n    right: 58%;\n    top: calc(var(--frameHeight)*0.04);\n    width: 44%;\n}\n.S1L1V5_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%] {\n    width: 100%;\n    color: #ED6B06;\n    display: block;\n}\n.S1L1V5_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text1[_ngcontent-%COMP%]{\n    font-family: var(--font-family-light);\n    font-size: calc(var(--frameHeight)*0.09);\n    font-weight: normal; \n    text-align: center;\n    margin-bottom: calc(var(--frameHeight)*-0.02);\n    width: 100%;\n}\n.S1L1V5_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .title_text[_ngcontent-%COMP%]   .title_text2[_ngcontent-%COMP%]{\n    font-family: var(--font-family-bold);\n    font-size: calc(var(--frameHeight)*0.05);\n    font-weight: bolder;\n    line-height: 95%;\n    text-align: center;\n    width: 100%;\n}\n.S1L1V5_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%] {\n    *position: absolute;\n    *height: -webkit-fill-available;\n    color: #484849;\n    font-family: var(--font-family-medium);\n    font-size: calc(var(--frameHeight)*0.024);\n    line-height: 125%;\n    overflow-y: auto;\n    margin: 0 auto;\n    padding-top: calc(var(--frameHeight)*0.04);\n    text-align: justify;\n    width: 78%;\n}\n.S1L1V5_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%] {\n    -ms-overflow-style: none;  \n    scrollbar-width: none;  \n}\n.S1L1V5_container[_ngcontent-%COMP%]   .box_text[_ngcontent-%COMP%]   .paragraph[_ngcontent-%COMP%]::-webkit-scrollbar { \n    display: none;  \n}\n\n\n\n.box_images[_ngcontent-%COMP%] {\n    position: absolute;\n    height: 100%;\n    right: 11.5%;\n    width: 51%;\n}\n.box_images[_ngcontent-%COMP%]   .left_arrow[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('arrow_nave_left.png') no-repeat;\n    background-size: 100% 100%;\n    cursor: pointer;\n    left: 2%;\n    top: 46%;\n    padding-right: 1%;\n    min-width: calc(var(--frameHeight)*0.06);\n    min-height: calc(var(--frameHeight)*0.06);\n}\n.box_images[_ngcontent-%COMP%]   .left_arrow[_ngcontent-%COMP%]:hover{\n    background: url('arrow_nave_left_hover.png') no-repeat;\n    background-size: 100% 100%;\n    left: 1%;\n}\n.box_images[_ngcontent-%COMP%]   .right_arrow[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('arrow_nave_right.png') no-repeat;\n    background-size: 100% 100%;\n    cursor: pointer;\n    right: 2%;\n    top: 46%;\n    padding-right: 1%;\n    min-width: calc(var(--frameHeight)*0.06);\n    min-height: calc(var(--frameHeight)*0.06);\n}\n.box_images[_ngcontent-%COMP%]   .right_arrow[_ngcontent-%COMP%]:hover{\n    background: url('arrow_nave_right_hover.png') no-repeat;\n    background-size: 100% 100%;\n    right: 1%;\n}\n.box_images[_ngcontent-%COMP%]   .box_border[_ngcontent-%COMP%] {\n    position: absolute;\n    \n    background: url('cuadro_galeria.png') no-repeat;\n    background-size: 100% 100%;\n    height: 81%;\n    left: 13%;\n    top: 9.6%;\n    width: 74%;\n}\n.box_images[_ngcontent-%COMP%]   .box_border[_ngcontent-%COMP%]   .box_slider[_ngcontent-%COMP%]{\n    position: absolute;\n    *background: url('imagen1.png') no-repeat;\n    background-size: 100% 100%;\n    height: 83.34%;\n    left: 8%;\n    \n    top: 8%;\n    width: 85%;\n    \n}\n\n\n\n.carousel-animation-wrapper[_ngcontent-%COMP%] {\n    display: flex;\n    flex-flow: column;\n}\n.carousel-wrapper[_ngcontent-%COMP%] {\n    height: 100%;\n    width: 100%;\n}\n.slide-wrapper[_ngcontent-%COMP%] {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    height: 100%;\n    \n}\n.igx-card[_ngcontent-%COMP%] {\n    box-shadow: none;\n}\n\nspan#igx-carousel-0-label[_ngcontent-%COMP%] {\n    display: none;\n}\n.lupa[_ngcontent-%COMP%] {\n    border: 2px solid red;\n    height: 100%;\n    width: 100%;\n}\n\n.imagen1[_ngcontent-%COMP%]{\n    background: url('imagen1.png') no-repeat;\n    background-size: 100% 100%;\n    position: absolute;\n    height: 100%;\n    left: 0;\n    top: 0;\n    width: 100%;\n}\n.imagen2[_ngcontent-%COMP%]{\n    background: url('imagen2.png') no-repeat;\n    background-size: 100% 100%;\n    position: absolute;\n    height: 100%;\n    left: 0;\n    top: 0;\n    width: 100%;\n}\n.imagen3[_ngcontent-%COMP%]{\n    background: url('imagen3.png') no-repeat;\n    background-size: 100% 100%;\n    position: absolute;\n    height: 100%;\n    left: 0;\n    top: 0;\n    width: 100%;\n}\n.imagen4[_ngcontent-%COMP%]{\n    background: url('imagen4.png') no-repeat;\n    background-size: 100% 100%;\n    position: absolute;\n    height: 100%;\n    left: 0;\n    top: 0;\n    width: 100%;\n}\n.imagen5[_ngcontent-%COMP%]{\n    background: url('imagen5.png') no-repeat;\n    background-size: 100% 100%;\n    position: absolute;\n    height: 100%;\n    left: 0;\n    top: 0;\n    width: 100%;\n}\n.imagen6[_ngcontent-%COMP%]{\n    background: url('imagen6.png') no-repeat;\n    background-size: 100% 100%;\n    position: absolute;\n    height: 100%;\n    left: 0;\n    top: 0;\n    width: 100%;\n}\n.imagen7[_ngcontent-%COMP%]{\n    background: url('picture1.png') no-repeat;\n    background-size: 100% 100%;\n    position: absolute;\n    height: 100%;\n    left: 0;\n    top: 0;\n    width: 100%;\n}\n.imagen8[_ngcontent-%COMP%]{\n    background: url('picture2.png') no-repeat;\n    background-size: 100% 100%;\n    position: absolute;\n    height: 100%;\n    left: 0;\n    top: 0;\n    width: 100%;\n}\n.imagen9[_ngcontent-%COMP%]{\n    background: url('picture3.png') no-repeat;\n    background-size: 100% 100%;\n    position: absolute;\n    height: 100%;\n    left: 0;\n    top: 0;\n    width: 100%;\n}\n.imagen10[_ngcontent-%COMP%]{\n    background: url('picture4.png') no-repeat;\n    background-size: 100% 100%;\n    position: absolute;\n    height: 100%;\n    left: 0;\n    top: 0;\n    width: 100%;\n}\n\n\n#modal_fin[_ngcontent-%COMP%] {\n    position: absolute;\n    align-items: center;\n    background-color: #5c5c5ccc !important;\n    border: none;\n    color: white;\n    display: block;\n    font-family: var(--font-family-light);\n    opacity: 100%;\n    text-align: center;\n}\n\n.modal-dialog-advanced[_ngcontent-%COMP%] {\n    display: grid;\n    height: 100%;\n    place-items: center;\n    width: 100%;\n}\n\n.modal-dialog-advanced[_ngcontent-%COMP%]   .modal-content[_ngcontent-%COMP%] {\n    border: none;\n    background-image: url('modal_finalizado.png');\n    background-size: 100% 100%;\n    background-color: transparent;\n    height: 90%;\n    padding: 0;\n    width: 70%;\n}\n\n.modal-dialog-advanced[_ngcontent-%COMP%]   .modal-content[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%] {\n    position: absolute;\n    *background-image: url('modal.png');\n    background-size: 100% 100%;\n    background-repeat: no-repeat;\n    background-color: transparent;\n    display: inline-block;\n    height: 78%;\n    justify-content: center;\n    left: 4%;\n    padding: 0;\n    top: 24%;\n    width: 92%;\n}\n\n.modal-dialog-advanced[_ngcontent-%COMP%]   .modal-content[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%]   .modal-body-message[_ngcontent-%COMP%] {\n    color: #333;\n    font-size: calc(var(--frameHeight)*0.05);\n    justify-content: center;\n    text-align: justify;\n    font-family: var(--font-family-regular);\n    height: auto;\n    line-height: 105%;\n    margin-left: 7%;\n    margin-top: 5%;\n    width: 66%;\n}\n\n.modal-dialog-advanced[_ngcontent-%COMP%]   .modal-content[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%]   .modal-body-luck[_ngcontent-%COMP%] {\n    color: #333;\n    font-family: var(--font-family-regular);\n    font-size: calc(var(--frameHeight)*0.05);\n    margin-left: 7%;\n    margin-top: 4%;\n    width: 66%;\n}\n\n.modal-dialog-advanced[_ngcontent-%COMP%]   .modal-content[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%]   .button_continue[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #39a0b4;\n    box-shadow: 0 calc(var(--frameHeight)*0.005) 0 calc(var(--frameHeight)*0.004) #3b3c3d;\n    border-top: calc(var(--frameHeight)*0.0085) solid #3b3c3d;\n    border-left: calc(var(--frameHeight)*0.0085) solid #3b3c3d;\n    border-right: calc(var(--frameHeight)*0.0085) solid #3b3c3d;\n    border-bottom: calc(var(--frameHeight)*0.0085) solid #3b3c3d;\n    border-radius: calc(var(--frameHeight)*1);\n    bottom: 14%;\n    display: block;\n    font-family: var(--font-family-medium);\n    font-size: calc(var(--frameHeight)*0.038);\n    left: 40%;\n    padding: 0.5% 2%;\n    transition: background-color 0.5s ease-in-out;\n}\n\n.modal-dialog-advanced[_ngcontent-%COMP%]   .modal-content[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%]   .button_continue.disabled[_ngcontent-%COMP%] {\n    opacity: 0.4;\n    pointer-events: none;\n}\n\n.modal-dialog-advanced[_ngcontent-%COMP%]   .modal-content[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%]   .button_continue[_ngcontent-%COMP%]:hover {\n    \n    background-color: #2a6c79;\n    \n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2VjdGlvbjEvbGVzc29uMS9TMUwxVjUvUzFMMVY1LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFO0FBQ3JFO0lBQ0ksa0JBQWtCO0lBQ2xCLG9EQUFvRDtJQUNwRCxzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxVQUFVO0lBQ1YsZ0JBQWdCO0lBQ2hCLFFBQVE7SUFDUixVQUFVO0FBQ2Q7OztBQUdBLHdDQUF3QztBQUN4QztJQUNJLGtCQUFrQjtJQUNsQixxQ0FBcUM7SUFDckMscUNBQXFDO0lBQ3JDLFdBQVc7SUFDWCxVQUFVO0lBQ1Ysa0NBQWtDO0lBQ2xDLFVBQVU7QUFDZDtBQUNBO0lBQ0ksV0FBVztJQUNYLGNBQWM7SUFDZCxjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxxQ0FBcUM7SUFDckMsd0NBQXdDO0lBQ3hDLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsNkNBQTZDO0lBQzdDLFdBQVc7QUFDZjtBQUNBO0lBQ0ksb0NBQW9DO0lBQ3BDLHdDQUF3QztJQUN4QyxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixXQUFXO0FBQ2Y7QUFDQTtLQUNJLGtCQUFtQjtLQUNuQiw4QkFBK0I7SUFDL0IsY0FBYztJQUNkLHNDQUFzQztJQUN0Qyx5Q0FBeUM7SUFDekMsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsMENBQTBDO0lBQzFDLG1CQUFtQjtJQUNuQixVQUFVO0FBQ2Q7QUFDQTtJQUNJLHdCQUF3QixHQUFHLDBCQUEwQjtJQUNyRCxxQkFBcUIsR0FBRyxZQUFZO0FBQ3hDO0FBQ0E7SUFDSSxhQUFhLEdBQUcsc0JBQXNCO0FBQzFDOzs7QUFHQSx5Q0FBeUM7QUFDekM7SUFDSSxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFlBQVk7SUFDWixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixnREFBa0Y7SUFDbEYsMEJBQTBCO0lBQzFCLGVBQWU7SUFDZixRQUFRO0lBQ1IsUUFBUTtJQUNSLGlCQUFpQjtJQUNqQix3Q0FBd0M7SUFDeEMseUNBQXlDO0FBQzdDO0FBQ0E7SUFDSSxzREFBd0Y7SUFDeEYsMEJBQTBCO0lBQzFCLFFBQVE7QUFDWjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGlEQUFtRjtJQUNuRiwwQkFBMEI7SUFDMUIsZUFBZTtJQUNmLFNBQVM7SUFDVCxRQUFRO0lBQ1IsaUJBQWlCO0lBQ2pCLHdDQUF3QztJQUN4Qyx5Q0FBeUM7QUFDN0M7QUFDQTtJQUNJLHVEQUF5RjtJQUN6RiwwQkFBMEI7SUFDMUIsU0FBUztBQUNiO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsc0ZBQXNGO0lBQ3RGLCtDQUFpRjtJQUNqRiwwQkFBMEI7SUFDMUIsV0FBVztJQUNYLFNBQVM7SUFDVCxTQUFTO0lBQ1QsVUFBVTtBQUNkO0FBQ0E7SUFDSSxrQkFBa0I7S0FDbEIsd0NBQXdGO0lBQ3hGLDBCQUEwQjtJQUMxQixjQUFjO0lBQ2QsUUFBUTtJQUNSLHNCQUFzQjtJQUN0QixPQUFPO0lBQ1AsVUFBVTtJQUNWLDJCQUEyQjtBQUMvQjs7O0FBR0EsaUNBQWlDO0FBQ2pDO0lBQ0ksYUFBYTtJQUNiLGlCQUFpQjtBQUNyQjtBQUNBO0lBQ0ksWUFBWTtJQUNaLFdBQVc7QUFDZjtBQUNBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsWUFBWTtJQUNaLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksZ0JBQWdCO0FBQ3BCO0FBQ0E7O0dBRUc7QUFDSDtJQUNJLGFBQWE7QUFDakI7QUFDQTtJQUNJLHFCQUFxQjtJQUNyQixZQUFZO0lBQ1osV0FBVztBQUNmOztBQUVBO0lBQ0ksd0NBQXVGO0lBQ3ZGLDBCQUEwQjtJQUMxQixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLE9BQU87SUFDUCxNQUFNO0lBQ04sV0FBVztBQUNmO0FBQ0E7SUFDSSx3Q0FBdUY7SUFDdkYsMEJBQTBCO0lBQzFCLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osT0FBTztJQUNQLE1BQU07SUFDTixXQUFXO0FBQ2Y7QUFDQTtJQUNJLHdDQUF1RjtJQUN2RiwwQkFBMEI7SUFDMUIsa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixPQUFPO0lBQ1AsTUFBTTtJQUNOLFdBQVc7QUFDZjtBQUNBO0lBQ0ksd0NBQXVGO0lBQ3ZGLDBCQUEwQjtJQUMxQixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLE9BQU87SUFDUCxNQUFNO0lBQ04sV0FBVztBQUNmO0FBQ0E7SUFDSSx3Q0FBdUY7SUFDdkYsMEJBQTBCO0lBQzFCLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osT0FBTztJQUNQLE1BQU07SUFDTixXQUFXO0FBQ2Y7QUFDQTtJQUNJLHdDQUF1RjtJQUN2RiwwQkFBMEI7SUFDMUIsa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixPQUFPO0lBQ1AsTUFBTTtJQUNOLFdBQVc7QUFDZjtBQUNBO0lBQ0kseUNBQXdGO0lBQ3hGLDBCQUEwQjtJQUMxQixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLE9BQU87SUFDUCxNQUFNO0lBQ04sV0FBVztBQUNmO0FBQ0E7SUFDSSx5Q0FBd0Y7SUFDeEYsMEJBQTBCO0lBQzFCLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osT0FBTztJQUNQLE1BQU07SUFDTixXQUFXO0FBQ2Y7QUFDQTtJQUNJLHlDQUF3RjtJQUN4RiwwQkFBMEI7SUFDMUIsa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixPQUFPO0lBQ1AsTUFBTTtJQUNOLFdBQVc7QUFDZjtBQUNBO0lBQ0kseUNBQXdGO0lBQ3hGLDBCQUEwQjtJQUMxQixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLE9BQU87SUFDUCxNQUFNO0lBQ04sV0FBVztBQUNmOztBQUVBLHVDQUF1QztBQUN2QztJQUNJLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsc0NBQXNDO0lBQ3RDLFlBQVk7SUFDWixZQUFZO0lBQ1osY0FBYztJQUNkLHFDQUFxQztJQUNyQyxhQUFhO0lBQ2Isa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksYUFBYTtJQUNiLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsV0FBVztBQUNmOztBQUVBO0lBQ0ksWUFBWTtJQUNaLDZDQUE0RjtJQUM1RiwwQkFBMEI7SUFDMUIsNkJBQTZCO0lBQzdCLFdBQVc7SUFDWCxVQUFVO0lBQ1YsVUFBVTtBQUNkOztBQUVBO0lBQ0ksa0JBQWtCO0tBQ2xCLGtDQUFrRjtJQUNsRiwwQkFBMEI7SUFDMUIsNEJBQTRCO0lBQzVCLDZCQUE2QjtJQUM3QixxQkFBcUI7SUFDckIsV0FBVztJQUNYLHVCQUF1QjtJQUN2QixRQUFRO0lBQ1IsVUFBVTtJQUNWLFFBQVE7SUFDUixVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsd0NBQXdDO0lBQ3hDLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsdUNBQXVDO0lBQ3ZDLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLGNBQWM7SUFDZCxVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsdUNBQXVDO0lBQ3ZDLHdDQUF3QztJQUN4QyxlQUFlO0lBQ2YsY0FBYztJQUNkLFVBQVU7QUFDZDs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIscUZBQXFGO0lBQ3JGLHlEQUF5RDtJQUN6RCwwREFBMEQ7SUFDMUQsMkRBQTJEO0lBQzNELDREQUE0RDtJQUM1RCx5Q0FBeUM7SUFDekMsV0FBVztJQUNYLGNBQWM7SUFDZCxzQ0FBc0M7SUFDdEMseUNBQXlDO0lBQ3pDLFNBQVM7SUFDVCxnQkFBZ0I7SUFDaEIsNkNBQTZDO0FBQ2pEOztBQUVBO0lBQ0ksWUFBWTtJQUNaLG9CQUFvQjtBQUN4Qjs7QUFFQTtJQUNJLG1DQUFtQztJQUNuQyx5QkFBeUI7SUFDekIsMEJBQTBCO0FBQzlCIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKiogICBDYWphIHByaW5jaXBhbCBjb24gZWwgY29udGVuaWRvIGRlIGxhcyBsZWNjaW9uZXMgICAqKioqKioqL1xuLlMxTDFWNV9jb250YWluZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3JkZXI6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDEyKSBzb2xpZCAjNDg0ODQ5O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgICBoZWlnaHQ6IDczJTtcbiAgICBsZWZ0OiA4LjUlO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgdG9wOiAxOCU7XG4gICAgd2lkdGg6IDgzJTtcbn1cblxuXG4vKioqKioqKiAgIENhamEgY2VudHJhbCB0ZXh0b3MgICAqKioqKioqL1xuLlMxTDFWNV9jb250YWluZXIgLmJveF90ZXh0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWxpZ2h0KTtcbiAgICBmb250LXNpemU6ICh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpO1xuICAgIGhlaWdodDogOTAlO1xuICAgIHJpZ2h0OiA1OCU7XG4gICAgdG9wOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA0KTtcbiAgICB3aWR0aDogNDQlO1xufVxuLlMxTDFWNV9jb250YWluZXIgLmJveF90ZXh0IC50aXRsZV90ZXh0IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogI0VENkIwNjtcbiAgICBkaXNwbGF5OiBibG9jaztcbn1cbi5TMUwxVjVfY29udGFpbmVyIC5ib3hfdGV4dCAudGl0bGVfdGV4dCAudGl0bGVfdGV4dDF7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWxpZ2h0KTtcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDkpO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7IFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSotMC4wMik7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4uUzFMMVY1X2NvbnRhaW5lciAuYm94X3RleHQgLnRpdGxlX3RleHQgLnRpdGxlX3RleHQye1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1ib2xkKTtcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDUpO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgbGluZS1oZWlnaHQ6IDk1JTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4uUzFMMVY1X2NvbnRhaW5lciAuYm94X3RleHQgLnBhcmFncmFwaCB7XG4gICAgKnBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAqaGVpZ2h0OiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xuICAgIGNvbG9yOiAjNDg0ODQ5O1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1tZWRpdW0pO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjQpO1xuICAgIGxpbmUtaGVpZ2h0OiAxMjUlO1xuICAgIG92ZXJmbG93LXk6IGF1dG87XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gICAgcGFkZGluZy10b3A6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDQpO1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgd2lkdGg6IDc4JTtcbn1cbi5TMUwxVjVfY29udGFpbmVyIC5ib3hfdGV4dCAucGFyYWdyYXBoIHtcbiAgICAtbXMtb3ZlcmZsb3ctc3R5bGU6IG5vbmU7ICAvKiBJbnRlcm5ldCBFeHBsb3JlciAxMCsgKi9cbiAgICBzY3JvbGxiYXItd2lkdGg6IG5vbmU7ICAvKiBGaXJlZm94ICovXG59XG4uUzFMMVY1X2NvbnRhaW5lciAuYm94X3RleHQgLnBhcmFncmFwaDo6LXdlYmtpdC1zY3JvbGxiYXIgeyBcbiAgICBkaXNwbGF5OiBub25lOyAgLyogU2FmYXJpIGFuZCBDaHJvbWUgKi9cbn1cblxuXG4vKioqKioqKiAgIENhamEgc2xpZGVyIGltYWdlbmVzICAgKioqKioqKi9cbi5ib3hfaW1hZ2VzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHJpZ2h0OiAxMS41JTtcbiAgICB3aWR0aDogNTElO1xufVxuLmJveF9pbWFnZXMgLmxlZnRfYXJyb3cge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvYXJyb3dfbmF2ZV9sZWZ0LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBsZWZ0OiAyJTtcbiAgICB0b3A6IDQ2JTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxJTtcbiAgICBtaW4td2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDYpO1xuICAgIG1pbi1oZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDYpO1xufVxuLmJveF9pbWFnZXMgLmxlZnRfYXJyb3c6aG92ZXJ7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9idXR0b25zL2Fycm93X25hdmVfbGVmdF9ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBsZWZ0OiAxJTtcbn1cbi5ib3hfaW1hZ2VzIC5yaWdodF9hcnJvdyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvYnV0dG9ucy9hcnJvd19uYXZlX3JpZ2h0LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICByaWdodDogMiU7XG4gICAgdG9wOiA0NiU7XG4gICAgcGFkZGluZy1yaWdodDogMSU7XG4gICAgbWluLXdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA2KTtcbiAgICBtaW4taGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA2KTtcbn1cbi5ib3hfaW1hZ2VzIC5yaWdodF9hcnJvdzpob3ZlcntcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvYXJyb3dfbmF2ZV9yaWdodF9ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICByaWdodDogMSU7XG59XG4uYm94X2ltYWdlcyAuYm94X2JvcmRlciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIC8qIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvZ2VuZXJhbF92aWV3X2xlc3NvbnMvcGVyc29uYWplLnBuZykgbm8tcmVwZWF0OyAqL1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMS9jdWFkcm9fZ2FsZXJpYS5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDgxJTtcbiAgICBsZWZ0OiAxMyU7XG4gICAgdG9wOiA5LjYlO1xuICAgIHdpZHRoOiA3NCU7XG59XG4uYm94X2ltYWdlcyAuYm94X2JvcmRlciAuYm94X3NsaWRlcntcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgKmJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMS92aWV3My9pbWFnZXMvaW1hZ2VuMS5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDgzLjM0JTtcbiAgICBsZWZ0OiA4JTtcbiAgICAvKiBvdmVyZmxvdzogaGlkZGVuOyAqL1xuICAgIHRvcDogOCU7XG4gICAgd2lkdGg6IDg1JTtcbiAgICAvKiBib3JkZXI6IDFweCBzb2xpZCByZWQ7ICovXG59XG5cblxuLyogU2xpZGUgZGUgaW1hZ2VuZXMgKENhcm91c2VsKSAqL1xuLmNhcm91c2VsLWFuaW1hdGlvbi13cmFwcGVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZmxvdzogY29sdW1uO1xufVxuLmNhcm91c2VsLXdyYXBwZXIge1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5zbGlkZS13cmFwcGVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIC8qICBpbWcgeyB3aWR0aDogMTAwJTsgfSAqL1xufVxuLmlneC1jYXJkIHtcbiAgICBib3gtc2hhZG93OiBub25lO1xufVxuLyogLmlneC1jYXJvdXNlbC1pbmRpY2F0b3JzLS1ib3R0b20ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59ICovXG5zcGFuI2lneC1jYXJvdXNlbC0wLWxhYmVsIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuLmx1cGEge1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHJlZDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5pbWFnZW4xe1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMS92aWV3My9pbWFnZXMvaW1hZ2VuMS5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGxlZnQ6IDA7XG4gICAgdG9wOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmltYWdlbjJ7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9sZXNzb24xL3ZpZXczL2ltYWdlcy9pbWFnZW4yLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4uaW1hZ2VuM3tcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjEvdmlldzMvaW1hZ2VzL2ltYWdlbjMucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5pbWFnZW40e1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMS92aWV3My9pbWFnZXMvaW1hZ2VuNC5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGxlZnQ6IDA7XG4gICAgdG9wOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmltYWdlbjV7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9sZXNzb24xL3ZpZXczL2ltYWdlcy9pbWFnZW41LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4uaW1hZ2VuNntcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjEvdmlldzMvaW1hZ2VzL2ltYWdlbjYucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5pbWFnZW43e1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMS92aWV3My9pbWFnZXMvcGljdHVyZTEucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5pbWFnZW44e1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMS92aWV3My9pbWFnZXMvcGljdHVyZTIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5pbWFnZW45e1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMS92aWV3My9pbWFnZXMvcGljdHVyZTMucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5pbWFnZW4xMHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjEvdmlldzMvaW1hZ2VzL3BpY3R1cmU0LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi8qKioqKioqICAgTW9kYWwgZmluYWxpemFjacODwrNuICAgKioqKioqKi9cbiNtb2RhbF9maW4ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM1YzVjNWNjYyAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWxpZ2h0KTtcbiAgICBvcGFjaXR5OiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm1vZGFsLWRpYWxvZy1hZHZhbmNlZCB7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgcGxhY2UtaXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm1vZGFsLWRpYWxvZy1hZHZhbmNlZCAubW9kYWwtY29udGVudCB7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvZ2VuZXJhbF92aWV3X2xlc3NvbnMvbW9kYWxfZmluYWxpemFkby5wbmcpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGhlaWdodDogOTAlO1xuICAgIHBhZGRpbmc6IDA7XG4gICAgd2lkdGg6IDcwJTtcbn1cblxuLm1vZGFsLWRpYWxvZy1hZHZhbmNlZCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICpiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL21vZGFsLnBuZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgaGVpZ2h0OiA3OCU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbGVmdDogNCU7XG4gICAgcGFkZGluZzogMDtcbiAgICB0b3A6IDI0JTtcbiAgICB3aWR0aDogOTIlO1xufVxuXG4ubW9kYWwtZGlhbG9nLWFkdmFuY2VkIC5tb2RhbC1jb250ZW50IC5tb2RhbC1ib2R5IC5tb2RhbC1ib2R5LW1lc3NhZ2Uge1xuICAgIGNvbG9yOiAjMzMzO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNSk7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktcmVndWxhcik7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIGxpbmUtaGVpZ2h0OiAxMDUlO1xuICAgIG1hcmdpbi1sZWZ0OiA3JTtcbiAgICBtYXJnaW4tdG9wOiA1JTtcbiAgICB3aWR0aDogNjYlO1xufVxuXG4ubW9kYWwtZGlhbG9nLWFkdmFuY2VkIC5tb2RhbC1jb250ZW50IC5tb2RhbC1ib2R5IC5tb2RhbC1ib2R5LWx1Y2sge1xuICAgIGNvbG9yOiAjMzMzO1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1yZWd1bGFyKTtcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDUpO1xuICAgIG1hcmdpbi1sZWZ0OiA3JTtcbiAgICBtYXJnaW4tdG9wOiA0JTtcbiAgICB3aWR0aDogNjYlO1xufVxuXG4ubW9kYWwtZGlhbG9nLWFkdmFuY2VkIC5tb2RhbC1jb250ZW50IC5tb2RhbC1ib2R5IC5idXR0b25fY29udGludWUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzlhMGI0O1xuICAgIGJveC1zaGFkb3c6IDAgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDUpIDAgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDQpICMzYjNjM2Q7XG4gICAgYm9yZGVyLXRvcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDg1KSBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1sZWZ0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwODUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLXJpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwODUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLWJvdHRvbTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDg1KSBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjEpO1xuICAgIGJvdHRvbTogMTQlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1tZWRpdW0pO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMzgpO1xuICAgIGxlZnQ6IDQwJTtcbiAgICBwYWRkaW5nOiAwLjUlIDIlO1xuICAgIHRyYW5zaXRpb246IGJhY2tncm91bmQtY29sb3IgMC41cyBlYXNlLWluLW91dDtcbn1cblxuLm1vZGFsLWRpYWxvZy1hZHZhbmNlZCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAuYnV0dG9uX2NvbnRpbnVlLmRpc2FibGVkIHtcbiAgICBvcGFjaXR5OiAwLjQ7XG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG59XG5cbi5tb2RhbC1kaWFsb2ctYWR2YW5jZWQgLm1vZGFsLWNvbnRlbnQgLm1vZGFsLWJvZHkgLmJ1dHRvbl9jb250aW51ZTpob3ZlciB7XG4gICAgLyogLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEuMSk7ICovXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzJhNmM3OTtcbiAgICAvKiB0cmFuc2Zvcm06IHNjYWxlKDEuMSkgKi9cbn1cbiJdLCJzb3VyY2VSb290IjoiIn0= */"]
});

/***/ }),

/***/ 3830:
/*!*******************************************************!*\
  !*** ./src/app/section1/lesson1/lesson1.component.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Lesson1Component": () => (/* binding */ Lesson1Component)
/* harmony export */ });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ 5139);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var src_app_services_progress_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/progress.service */ 8458);
/* harmony import */ var _S1L1V1_S1L1V1_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./S1L1V1/S1L1V1.component */ 2854);
/* harmony import */ var _S1L1V2_S1L1V2_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./S1L1V2/S1L1V2.component */ 4695);
/* harmony import */ var _S1L1V3_S1L1V3_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./S1L1V3/S1L1V3.component */ 5781);
/* harmony import */ var _S1L1V4_S1L1V4_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./S1L1V4/S1L1V4.component */ 4790);
/* harmony import */ var _S1L1V5_S1L1V5_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./S1L1V5/S1L1V5.component */ 2890);
/* harmony import */ var _shop_outside_character_character_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shop/outside/character/character.component */ 8619);
/* harmony import */ var _shop_inside_modal_modal_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../shop/inside/modal/modal.component */ 4988);
/* harmony import */ var _layout_layout_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../layout/layout.component */ 6674);












const _c0 = ["modalId"];
const _c1 = ["view1Id"];
const _c2 = ["view2Id"];
const _c3 = ["view3Id"];
const _c4 = ["view4Id"];
const _c5 = ["view5Id"];
const _c6 = ["characterId"];
class Lesson1Component {
  constructor(router, progressService) {
    this.router = router;
    this.progressService = progressService;
    this.stateSoundMenu = 'active';
    this.stateCharacterText = 'active';
    this.stateCharacterPlay = 'active';
    this.stateCharacterSound = 'active';
    this.currentView = 1;
    this.characterObject = {
      'left': '82%',
      'top': '10%',
      'height': '89%',
      'width': '18.5%'
    };
    /* audio al pasar sobre los botones */
    this.hoverAudio = new Audio(`../../../assets/section1/sounds/hoverDefault.mp3`);
    /* audio al hacer click en los botones */
    this.clickAudio = new Audio(`../../../assets/section1/sounds/clickDefault.mp3`);
  }
  ngOnInit() {
    console.log('--- lesson1 --- ngOnInit');
    this.modalMessage = this.view1Component.modalMessage;
    this.modalComponent.startModal(this.modalMessage);
    jquery__WEBPACK_IMPORTED_MODULE_0__(`app-S1L1V2`).css('display', 'none');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`app-S1L1V3`).css('display', 'none');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`app-S1L1V4`).css('display', 'none');
    jquery__WEBPACK_IMPORTED_MODULE_0__(`app-S1L1V5`).css('display', 'none');
    this.characterComponent.drawBoxCharacter(this.view1Component.characterObject);
    this.characterComponent.audioCharacter = this.view1Component.audiosCharacter[1];
  }
  /* Funciones Menu lateral izquierdo */
  btnPostSound() {
    this.clickAudio.play();
    if (this.stateSoundMenu === 'active') {
      this.stateSoundMenu = 'inactive';
    } else {
      this.stateSoundMenu = 'active';
    }
  }
  btnPostMainMenu() {
    this.clickAudio.play();
  }
  btnPostObjectives() {
    this.clickAudio.play();
  }
  btnPostProfile() {
    this.clickAudio.play();
  }
  hoverBtnPostMenu() {
    if (this.stateSoundMenu === 'active') {
      this.hoverAudio.play();
    }
    1;
  }
  leaveBtnPostMenu() {
    this.hoverAudio.pause();
    this.hoverAudio.currentTime = 0;
    //this.clickAudio.play();
  }

  changeView(numView) {
    console.log('--- lesson1 --- changeView: ', numView, ' - currentView: ', this.currentView);
    if (this.currentView == 2) {
      this.view2Component.stopSlider();
      console.log("STOP-----");
    }
    if (numView == 'prev' && this.currentView > 1) {
      // previus
      jquery__WEBPACK_IMPORTED_MODULE_0__(`.point${this.currentView}`).removeClass('point_current');
      this.currentView = this.currentView - 1;
      jquery__WEBPACK_IMPORTED_MODULE_0__(`.point${this.currentView}`).addClass('point_current');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).removeClass('animated fadeInRightBig');
      setTimeout(() => {
        jquery__WEBPACK_IMPORTED_MODULE_0__(`app-S1L1V${this.currentView + 1}`).css('display', 'none');
      }, 1000);
      setTimeout(() => {
        jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).addClass('animated fadeOutLeftBig');
        setTimeout(() => {
          jquery__WEBPACK_IMPORTED_MODULE_0__(`app-S1L1V${this.currentView}`).css('display', 'block');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).removeClass('animated fadeOutLeftBig');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).addClass('animated fadeInRightBig');
          if (this.currentView == 1) {
            this.characterComponent.drawBoxCharacter(this.view1Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view1Component.audiosCharacter[this.view1Component.currentView];
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          } else if (this.currentView == 2) {
            this.characterComponent.drawBoxCharacter(this.view2Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view2Component.audioCharacter;
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
            this.view2Component.sliderAuto();
          } else if (this.currentView == 3) {
            this.characterComponent.drawBoxCharacter(this.view3Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.stateCharacterSound = 'active';
          } else if (this.currentView == 4) {
            this.characterComponent.drawBoxCharacter(this.view4Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view4Component.audiosCharacter[1];
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          } else {
            this.characterComponent.drawBoxCharacter(this.view5Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view5Component.audioCharacter;
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          }
        }, 1000);
      }, 1);
    } else if (numView == 'next' && this.currentView < 5) {
      // next
      // $(`app-S1L1V${this.currentView}`).css('display', 'none');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`.point${this.currentView}`).removeClass('point_current');
      this.currentView = this.currentView + 1;
      jquery__WEBPACK_IMPORTED_MODULE_0__(`.point${this.currentView}`).addClass('point_current');
      jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).removeClass('animated fadeInRightBig');
      setTimeout(() => {
        jquery__WEBPACK_IMPORTED_MODULE_0__(`app-S1L1V${this.currentView - 1}`).css('display', 'none');
        if (this.progressService.getProgressPercentage() < 8.5 * (this.currentView - 1)) {
          this.progressService.setProgressPercentage(8.5 * (this.currentView - 1));
        }
        ;
      }, 1000);
      setTimeout(() => {
        jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).addClass('animated fadeOutLeftBig');
        setTimeout(() => {
          jquery__WEBPACK_IMPORTED_MODULE_0__(`app-S1L1V${this.currentView}`).css('display', 'block');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).removeClass('animated fadeOutLeftBig');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).addClass('animated fadeInRightBig');
          if (this.currentView == 1) {
            this.characterComponent.btnCharacterReset();
            this.characterComponent.drawBoxCharacter(this.view1Component.characterObject);
            this.characterComponent.audioCharacter = this.view1Component.audioCharacter;
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          } else if (this.currentView == 2) {
            this.characterComponent.drawBoxCharacter(this.view2Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view2Component.audioCharacter;
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
            this.view2Component.sliderAuto();
          } else if (this.currentView == 3) {
            this.characterComponent.drawBoxCharacter(this.view3Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.stateCharacterSound = 'active';
          } else if (this.currentView == 4) {
            this.characterComponent.drawBoxCharacter(this.view4Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view4Component.audiosCharacter[1];
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          } else if (this.currentView == 5) {
            this.characterComponent.drawBoxCharacter(this.view5Component.characterObject);
            this.characterComponent.btnCharacterReset();
            this.characterComponent.audioCharacter = this.view5Component.audioCharacter;
            setTimeout(() => {
              this.characterComponent.stateCharacterSound = 'active';
              this.characterComponent.btnCharacterPlay();
            }, 1000);
          } /* else {
            this.progressService.setLesson2Available('active');
            } */
        }, 1000);
      }, 1);
    } else if (numView == 'next' && this.currentView == 5) {
      this.view5Component.openModal();
      this.characterComponent.btnCharacterReset();
      this.view5Component.showModalFinish = true;
      this.progressService.setLesson2Available('active');
    } else if (numView == 1 || numView == 2 || numView == 3 || numView == 4 || numView == 5) {
      let presentView = this.currentView;
      jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).removeClass('animated fadeInRightBig');
      setTimeout(() => {
        jquery__WEBPACK_IMPORTED_MODULE_0__(`app-S1L1V${presentView}`).css('display', 'none');
        jquery__WEBPACK_IMPORTED_MODULE_0__(`.point${presentView}`).removeClass('point_current');
      }, 1000);
      setTimeout(() => {
        jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).addClass('animated fadeOutLeftBig');
        setTimeout(() => {
          jquery__WEBPACK_IMPORTED_MODULE_0__(`app-S1L1V${this.currentView}`).css('display', 'block');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`.point${this.currentView}`).addClass('point_current');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).removeClass('animated fadeOutLeftBig');
          jquery__WEBPACK_IMPORTED_MODULE_0__(`.bganimation`).addClass('animated fadeInRightBig');
          if (this.progressService.getProgressPercentage() < 8.5 * numView - 1) {
            this.progressService.setProgressPercentage(8.5 * (numView - 1));
          }
          ;
        }, 1000);
      }, 1);
      this.currentView = numView;
      if (numView == 1) {
        this.characterComponent.drawBoxCharacter(this.view1Component.characterObject);
        this.characterComponent.btnCharacterReset();
        this.characterComponent.audioCharacter = this.view1Component.audiosCharacter[this.view1Component.currentView];
        setTimeout(() => {
          this.characterComponent.stateCharacterSound = 'active';
          this.characterComponent.btnCharacterPlay();
        }, 1000);
      } else if (numView == 2) {
        this.characterComponent.drawBoxCharacter(this.view2Component.characterObject);
        this.characterComponent.btnCharacterReset();
        this.characterComponent.audioCharacter = this.view2Component.audioCharacter;
        setTimeout(() => {
          this.characterComponent.stateCharacterSound = 'active';
          this.characterComponent.btnCharacterPlay();
        }, 1000);
        this.view2Component.sliderAuto();
      } else if (numView == 3) {
        this.characterComponent.drawBoxCharacter(this.view3Component.characterObject);
        this.characterComponent.btnCharacterReset();
        this.characterComponent.stateCharacterSound = 'active';
      } else if (numView == 4) {
        this.characterComponent.drawBoxCharacter(this.view4Component.characterObject);
        this.characterComponent.btnCharacterReset();
        this.characterComponent.audioCharacter = this.view4Component.audiosCharacter[1];
        setTimeout(() => {
          this.characterComponent.stateCharacterSound = 'active';
          this.characterComponent.btnCharacterPlay();
        }, 1000);
      } else if (numView == 5) {
        console.log('NUMERO 5');
        this.characterComponent.drawBoxCharacter(this.view5Component.characterObject);
        this.characterComponent.btnCharacterReset();
        this.characterComponent.audioCharacter = this.view5Component.audioCharacter;
        setTimeout(() => {
          this.characterComponent.stateCharacterSound = 'active';
          this.characterComponent.btnCharacterPlay();
        }, 1000);
      }
    }
  }
  changeAudioCharacterView(numView, numPage) {
    console.log('lesson1--> numView: ', numView, ' - numPage: ', numPage);
    this.characterComponent.btnCharacterReset();
    if (numView == 'view1') {
      console.log('COND view1');
      this.characterComponent.audioCharacter = this.view1Component.audiosCharacter[numPage];
      setTimeout(() => {
        this.characterComponent.btnCharacterPlay();
      }, 200);
    } else if (numView == 'view3') {
      console.log('COND view3');
      this.characterComponent.audioCharacter = this.view3Component.audiosCharacter[numPage];
      setTimeout(() => {
        this.characterComponent.btnCharacterPlay();
      }, 200);
    } else if (numView == 'view4') {
      console.log('COND view4');
      this.characterComponent.audioCharacter = this.view4Component.audiosCharacter[numPage];
      setTimeout(() => {
        this.characterComponent.btnCharacterPlay();
      }, 200);
    }
  }
  /*
   * Oculta el modal ejecutado al cargar el componente
   */
  closeModal(event) {
    if (event) {
      //console.log('--- lesson1 --- closeModal()');
      //this.modalComponent.closeModal();
      this.characterComponent.btnCharacterPlay();
    }
  }
}
Lesson1Component.ɵfac = function Lesson1Component_Factory(t) {
  return new (t || Lesson1Component)(_angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_11__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdirectiveInject"](src_app_services_progress_service__WEBPACK_IMPORTED_MODULE_1__.ProgressService));
};
Lesson1Component.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdefineComponent"]({
  type: Lesson1Component,
  selectors: [["app-lesson1"]],
  viewQuery: function Lesson1Component_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵviewQuery"](_c0, 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵviewQuery"](_c1, 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵviewQuery"](_c2, 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵviewQuery"](_c3, 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵviewQuery"](_c4, 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵviewQuery"](_c5, 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵviewQuery"](_c6, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵloadQuery"]()) && (ctx.modalComponent = _t.first);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵloadQuery"]()) && (ctx.view1Component = _t.first);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵloadQuery"]()) && (ctx.view2Component = _t.first);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵloadQuery"]()) && (ctx.view3Component = _t.first);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵloadQuery"]()) && (ctx.view4Component = _t.first);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵloadQuery"]()) && (ctx.view5Component = _t.first);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵloadQuery"]()) && (ctx.characterComponent = _t.first);
    }
  },
  decls: 33,
  vars: 1,
  consts: [[1, "background"], [1, "bganimation", "animated", "fadeInRightBig"], [1, "title_container"], [1, "title"], [1, "sub_title"], [1, "pipeline_vertical"], [1, "pipeline_horizontal"], [1, "advance_box"], ["type", "button", 1, "pag_left", 3, "click"], ["type", "button", 1, "point", "point1", "point_current", 3, "click"], ["type", "button", 1, "point", "point2", 3, "click"], ["type", "button", 1, "point", "point3", 3, "click"], ["type", "button", 1, "point", "point4", 3, "click"], ["type", "button", 1, "point", "point5", 3, "click"], ["type", "button", 1, "pag_right", 3, "click"], [1, "houses"], [3, "isClicked"], ["view1Id", ""], ["view2Id", ""], ["view3Id", ""], ["view4Id", ""], ["view5Id", ""], ["characterId", ""], [1, "layout1"], [3, "modalMessage", "isClicked"], ["modalId", ""]],
  template: function Lesson1Component_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](0, "div", 0)(1, "div", 1)(2, "div", 2)(3, "div", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵtext"](4, "M\u00D3DULO 1");
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](5, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵtext"](6, "CONSIDERACIONES GENERALES BNUP");
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelement"](7, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](8, "div", 6)(9, "div", 7)(10, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("click", function Lesson1Component_Template_div_click_10_listener() {
        return ctx.changeView("prev");
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](11, "div", 9);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("click", function Lesson1Component_Template_div_click_11_listener() {
        return ctx.changeView(1);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](12, "div", 10);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("click", function Lesson1Component_Template_div_click_12_listener() {
        return ctx.changeView(2);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](13, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("click", function Lesson1Component_Template_div_click_13_listener() {
        return ctx.changeView(3);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](14, "div", 12);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("click", function Lesson1Component_Template_div_click_14_listener() {
        return ctx.changeView(4);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](15, "div", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("click", function Lesson1Component_Template_div_click_15_listener() {
        return ctx.changeView(5);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](16, "div", 14);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("click", function Lesson1Component_Template_div_click_16_listener() {
        return ctx.changeView("next");
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelement"](17, "div", 15);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](18, "app-S1L1V1", 16, 17);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("isClicked", function Lesson1Component_Template_app_S1L1V1_isClicked_18_listener($event) {
        return ctx.changeAudioCharacterView("view1", $event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelement"](20, "app-S1L1V2", null, 18);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](22, "app-S1L1V3", 16, 19);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("isClicked", function Lesson1Component_Template_app_S1L1V3_isClicked_22_listener($event) {
        return ctx.changeAudioCharacterView("view3", $event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](24, "app-S1L1V4", 16, 20);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("isClicked", function Lesson1Component_Template_app_S1L1V4_isClicked_24_listener($event) {
        return ctx.changeAudioCharacterView("view4", $event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelement"](26, "app-S1L1V5", null, 21)(28, "app-character", null, 22);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelement"](30, "app-layout", 23);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementStart"](31, "app-modal", 24, 25);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵlistener"]("isClicked", function Lesson1Component_Template_app_modal_isClicked_31_listener($event) {
        return ctx.closeModal($event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵelementEnd"]();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵadvance"](31);
      _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵproperty"]("modalMessage", ctx.modalMessage);
    }
  },
  dependencies: [_S1L1V1_S1L1V1_component__WEBPACK_IMPORTED_MODULE_2__.S1L1V1Component, _S1L1V2_S1L1V2_component__WEBPACK_IMPORTED_MODULE_3__.S1L1V2Component, _S1L1V3_S1L1V3_component__WEBPACK_IMPORTED_MODULE_4__.S1L1V3Component, _S1L1V4_S1L1V4_component__WEBPACK_IMPORTED_MODULE_5__.S1L1V4Component, _S1L1V5_S1L1V5_component__WEBPACK_IMPORTED_MODULE_6__.S1L1V5Component, _shop_outside_character_character_component__WEBPACK_IMPORTED_MODULE_7__.CharacterComponent, _shop_inside_modal_modal_component__WEBPACK_IMPORTED_MODULE_8__.ModalComponent, _layout_layout_component__WEBPACK_IMPORTED_MODULE_9__.LayoutComponent],
  styles: [".background[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('fondo_contenido.png') no-repeat;  \n    background-size: 100% 100%;\n    width: 100%;\n    height:100%;  \n    top: 0%;\n}\n.bganimation[_ngcontent-%COMP%] {\n    position: absolute;\n    background-size: 100% 100%;\n    width: 100%;\n    height:100%;  \n    top: 0%;\n    z-index: 3;\n}\n\n\n.fadeInRightBig[_ngcontent-%COMP%] {\n    animation-name: fadeInRightBig;\n  }\n  .animated[_ngcontent-%COMP%] {\n    animation-duration: 1s;\n    animation-fill-mode: both;\n  }\n\n  \n\n  .fadeOutLeftBig[_ngcontent-%COMP%] {\n    animation-name: fadeOutLeftBig;\n  }\n\n\n.title_container[_ngcontent-%COMP%] {\n    position: absolute;\n    border: calc(var(--frameHeight)*0.012) solid #484849;\n    background-color: #fff;\n    border-radius: calc(var(--frameHeight)*0.022);\n    height: 11%;\n    left: 5%;\n    top: 4%;\n    padding: 0% 1% 0% 0.5%;\n    color: #198195;\n    z-index: 1;\n    display: grid;\n    align-items: center;\n}\ndiv.title[_ngcontent-%COMP%] {\n    font-size: calc(var(--frameHeight)*0.03);\n    margin-bottom: calc(var(--frameHeight)*-0.018);\n    font-family: var(--font-family-light);\n    font-weight: 600;\n}\ndiv.sub_title[_ngcontent-%COMP%] {\n    font-size: calc(var(--frameHeight)*0.035);\n    font-family: var(--font-family-black);\n}\n\n\n\n.post_menu[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('post.png') no-repeat;\n    background-size: 100% 100%;\n    width: 7%;\n    height: 47%;\n    bottom: 0.3%;\n    *background-color: #9008;\n    display: flex;\n    justify-content: center;\n}\n.post_menu[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]{\n    position: absolute;\n    height: calc(var(--frameHeight)*0.065);\n    left: 44%;\n    width: calc(var(--frameHeight)*0.065);\n    box-shadow: 0 calc(var(--frameHeight)*0.005) 0 calc(var(--frameHeight)*0.0025) #3b3c3d;\n    border-top: calc(var(--frameHeight)*0.005) solid #3b3c3d;\n    border-left: calc(var(--frameHeight)*0.005) solid #3b3c3d;\n    border-right: calc(var(--frameHeight)*0.005) solid #3b3c3d;\n    border-bottom: calc(var(--frameHeight)*0.005) solid #3b3c3d;\n    border-radius: 50%;\n    cursor: pointer;\n}\n.post_menu[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]:active {\n    transform: translateY(3px);\n    border-top: 2px solid #3b3c3d;\n    border-left: 2px solid #3b3c3d;\n    border-right: 2px solid #3b3c3d;\n    border-bottom: 2px solid #3b3c3d;\n    box-shadow: none;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_menu[_ngcontent-%COMP%] {\n    background: url('menu.png') no-repeat;\n    background-size: 100% 100%;\n    top: 11%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_menu[_ngcontent-%COMP%]:hover{\n    background: url('menu_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 11%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_objectives[_ngcontent-%COMP%] {\n    background: url('objetives.png') no-repeat;\n    background-size: 100% 100%;\n    top: 32%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_objectives[_ngcontent-%COMP%]:hover {\n    background: url('objetives_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 32%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_sound.active[_ngcontent-%COMP%] {\n    background: url('sound.png') no-repeat;\n    background-size: 100% 100%;\n    top: 53%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_sound.active[_ngcontent-%COMP%]:hover {\n    background: url('sound_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 53%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_sound.inactive[_ngcontent-%COMP%]{\n    background: url('sound_off_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 53%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_profile[_ngcontent-%COMP%] {\n    background: url('profile.png') no-repeat;\n    background-size: 100% 100%;\n    top: 74.2%;\n}\n.post_menu[_ngcontent-%COMP%]   .button.button_profile[_ngcontent-%COMP%]:hover {\n    background: url('profile_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 74.2%;\n}\n\n\n\n.post_progress[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('post_progress.png') no-repeat;\n    background-size: 100% 100%;\n    bottom: -0.5%;\n    display: flex;\n    height: 49.2%;\n    justify-content: center;\n    right: -0.2%;\n    width: 7%;\n}\n.post_progress[_ngcontent-%COMP%]   .pipe_progress[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #198195;\n    border: calc(var(--frameHeight)*0.008) solid #3b3c3ddd;\n    border-radius: calc(var(--frameHeight)*0.26);\n    height: 81%;\n    left: 16%;\n    overflow: hidden;\n    top: 9%;\n    width: 40%;\n}\n.post_progress[_ngcontent-%COMP%]   .pipe_progress[_ngcontent-%COMP%]   .current_progress[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #F79D01;\n    border-right: 5px solid #E26723;\n    height: 50%;\n    width: 100%;\n    bottom: 0;\n}\n\n\n\n.pipeline_vertical[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('pipeline_vertical.png') no-repeat;  \n    background-size: 100% 100%;\n    width: 6%;\n    height: 37%;\n    top: 8%;\n    left: 3%;\n}\n\n.pipeline_horizontal[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('pipeline_horizontal.png') no-repeat;  \n    background-size: 100% 100%;\n    width: 71.5%;\n    height: 34%;\n    top: 66%;\n    left: 13%;\n}\n\n.houses[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('houses.png') no-repeat;  \n    background-size: 100% 100%;\n    height: 18%;\n    right: 0;\n    width: 34%;\n}\n\n\n\n\n\n.advance_box[_ngcontent-%COMP%] {\n    height: 16%;\n    margin-left: auto;\n    margin-right: auto;\n    margin-top: calc(var(--frameHeight)*0.26);\n    padding: 0% 1%;\n    top: 76%;\n    width: 34%;\n    width: min-content;\n    display: flex;\n    align-items: center;\n    gap: calc(var(--frameHeight)*0.018);\n}\n\n.pag_left[_ngcontent-%COMP%] {\n    background: url('pag_left.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.05);\n    width: calc(var(--frameHeight)*0.05);\n}\n.pag_left[_ngcontent-%COMP%]:hover {\n    background: url('pag_left_hover.png') no-repeat;\n    background-size: 100% 100%;\n}\n.pag_right[_ngcontent-%COMP%] {\n    background: url('pag_right.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.05);\n    width: calc(var(--frameHeight)*0.05);\n}\n.pag_right[_ngcontent-%COMP%]:hover {\n    background: url('pag_right_hover.png') no-repeat;\n    background-size: 100% 100%;\n}\n.point[_ngcontent-%COMP%] {\n    background: url('point.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.015);\n    width: calc(var(--frameHeight)*0.015);\n}\n.point.point_current[_ngcontent-%COMP%] {\n    background: url('point_hover.png') no-repeat;\n    background-size: 100% 100%;\n}\n.point[_ngcontent-%COMP%]:hover {\n    background: url('point_hover.png') no-repeat;\n    background-size: 100% 100%;\n}\n.point[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    display: block;\n    height: inherit;\n    width: inherit;\n}\n\n\n\n.button_menu[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    display: block;\n    height: inherit;\n    width: inherit;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2VjdGlvbjEvbGVzc29uMS9sZXNzb24xLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7SUFDbEIsZ0RBQTRGO0lBQzVGLDBCQUEwQjtJQUMxQixXQUFXO0lBQ1gsV0FBVztJQUNYLE9BQU87QUFDWDtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDBCQUEwQjtJQUMxQixXQUFXO0lBQ1gsV0FBVztJQUNYLE9BQU87SUFDUCxVQUFVO0FBQ2Q7O0FBRUEsK0NBQStDO0FBQy9DO0lBRUksOEJBQThCO0VBQ2hDO0VBQ0E7SUFFRSxzQkFBc0I7SUFFdEIseUJBQXlCO0VBQzNCOztFQUVBLGlEQUFpRDs7RUFFakQ7SUFFRSw4QkFBOEI7RUFDaEM7O0FBRUYsZ0NBQWdDO0FBQ2hDO0lBQ0ksa0JBQWtCO0lBQ2xCLG9EQUFvRDtJQUNwRCxzQkFBc0I7SUFDdEIsNkNBQTZDO0lBQzdDLFdBQVc7SUFDWCxRQUFRO0lBQ1IsT0FBTztJQUNQLHNCQUFzQjtJQUN0QixjQUFjO0lBQ2QsVUFBVTtJQUNWLGFBQWE7SUFDYixtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLHdDQUF3QztJQUN4Qyw4Q0FBOEM7SUFDOUMscUNBQXFDO0lBQ3JDLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0kseUNBQXlDO0lBQ3pDLHFDQUFxQztBQUN6Qzs7O0FBR0EsZ0RBQWdEO0FBQ2hEO0lBQ0ksa0JBQWtCO0lBQ2xCLHFDQUFpRTtJQUNqRSwwQkFBMEI7SUFDMUIsU0FBUztJQUNULFdBQVc7SUFDWCxZQUFZO0tBQ1osdUJBQXdCO0lBQ3hCLGFBQWE7SUFDYix1QkFBdUI7QUFDM0I7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixzQ0FBc0M7SUFDdEMsU0FBUztJQUNULHFDQUFxQztJQUNyQyxzRkFBc0Y7SUFDdEYsd0RBQXdEO0lBQ3hELHlEQUF5RDtJQUN6RCwwREFBMEQ7SUFDMUQsMkRBQTJEO0lBQzNELGtCQUFrQjtJQUNsQixlQUFlO0FBQ25CO0FBQ0E7SUFDSSwwQkFBMEI7SUFDMUIsNkJBQTZCO0lBQzdCLDhCQUE4QjtJQUM5QiwrQkFBK0I7SUFDL0IsZ0NBQWdDO0lBQ2hDLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0kscUNBQWlFO0lBQ2pFLDBCQUEwQjtJQUMxQixRQUFRO0FBQ1o7QUFDQTtJQUNJLDJDQUF1RTtJQUN2RSwwQkFBMEI7SUFDMUIsUUFBUTtBQUNaO0FBQ0E7SUFDSSwwQ0FBc0U7SUFDdEUsMEJBQTBCO0lBQzFCLFFBQVE7QUFDWjtBQUNBO0lBQ0ksZ0RBQTRFO0lBQzVFLDBCQUEwQjtJQUMxQixRQUFRO0FBQ1o7QUFDQTtJQUNJLHNDQUFrRTtJQUNsRSwwQkFBMEI7SUFDMUIsUUFBUTtBQUNaO0FBQ0E7SUFDSSw0Q0FBd0U7SUFDeEUsMEJBQTBCO0lBQzFCLFFBQVE7QUFDWjtBQUNBO0lBQ0ksZ0RBQTRFO0lBQzVFLDBCQUEwQjtJQUMxQixRQUFRO0FBQ1o7QUFDQTtJQUNJLHdDQUFvRTtJQUNwRSwwQkFBMEI7SUFDMUIsVUFBVTtBQUNkO0FBQ0E7SUFDSSw4Q0FBMEU7SUFDMUUsMEJBQTBCO0lBQzFCLFVBQVU7QUFDZDs7O0FBR0EsOENBQThDO0FBQzlDO0lBQ0ksa0JBQWtCO0lBQ2xCLDhDQUEwRjtJQUMxRiwwQkFBMEI7SUFDMUIsYUFBYTtJQUNiLGFBQWE7SUFDYixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLFlBQVk7SUFDWixTQUFTO0FBQ2I7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsc0RBQXNEO0lBQ3RELDRDQUE0QztJQUM1QyxXQUFXO0lBQ1gsU0FBUztJQUNULGdCQUFnQjtJQUNoQixPQUFPO0lBQ1AsVUFBVTtBQUNkO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLCtCQUErQjtJQUMvQixXQUFXO0lBQ1gsV0FBVztJQUNYLFNBQVM7QUFDYjs7O0FBR0EscURBQXFEO0FBQ3JEO0lBQ0ksa0JBQWtCO0lBQ2xCLGtEQUE4RjtJQUM5RiwwQkFBMEI7SUFDMUIsU0FBUztJQUNULFdBQVc7SUFDWCxPQUFPO0lBQ1AsUUFBUTtBQUNaOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLG9EQUFnRztJQUNoRywwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFdBQVc7SUFDWCxRQUFRO0lBQ1IsU0FBUztBQUNiOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLHVDQUFtRjtJQUNuRiwwQkFBMEI7SUFDMUIsV0FBVztJQUNYLFFBQVE7SUFDUixVQUFVO0FBQ2Q7Ozs7O0FBS0EsMENBQTBDO0FBQzFDO0lBQ0ksV0FBVztJQUNYLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIseUNBQXlDO0lBQ3pDLGNBQWM7SUFDZCxRQUFRO0lBQ1IsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLG1DQUFtQztBQUN2Qzs7QUFFQTtJQUNJLHlDQUF3RTtJQUN4RSwwQkFBMEI7SUFDMUIscUNBQXFDO0lBQ3JDLG9DQUFvQztBQUN4QztBQUNBO0lBQ0ksK0NBQThFO0lBQzlFLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksMENBQXlFO0lBQ3pFLDBCQUEwQjtJQUMxQixxQ0FBcUM7SUFDckMsb0NBQW9DO0FBQ3hDO0FBQ0E7SUFDSSxnREFBK0U7SUFDL0UsMEJBQTBCO0FBQzlCO0FBQ0E7SUFDSSxzQ0FBcUU7SUFDckUsMEJBQTBCO0lBQzFCLHNDQUFzQztJQUN0QyxxQ0FBcUM7QUFDekM7QUFDQTtJQUNJLDRDQUEyRTtJQUMzRSwwQkFBMEI7QUFDOUI7QUFDQTtJQUNJLDRDQUEyRTtJQUMzRSwwQkFBMEI7QUFDOUI7QUFDQTtJQUNJLGNBQWM7SUFDZCxlQUFlO0lBQ2YsY0FBYztBQUNsQjs7OztBQUlBO0lBQ0ksY0FBYztJQUNkLGVBQWU7SUFDZixjQUFjO0FBQ2xCIiwic291cmNlc0NvbnRlbnQiOlsiLmJhY2tncm91bmQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL2ZvbmRvX2NvbnRlbmlkby5wbmcpIG5vLXJlcGVhdDsgIFxuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDoxMDAlOyAgXG4gICAgdG9wOiAwJTtcbn1cbi5iZ2FuaW1hdGlvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDoxMDAlOyAgXG4gICAgdG9wOiAwJTtcbiAgICB6LWluZGV4OiAzO1xufVxuXG4vKioqKioqKiAgIEFuaW1hY2nDg8KzbiBkZSBsbGVnYWRhIHZpc3RhICAgKioqKioqKi9cbi5mYWRlSW5SaWdodEJpZyB7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tbmFtZTogZmFkZUluUmlnaHRCaWc7XG4gICAgYW5pbWF0aW9uLW5hbWU6IGZhZGVJblJpZ2h0QmlnO1xuICB9XG4gIC5hbmltYXRlZCB7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tZHVyYXRpb246IDFzO1xuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMXM7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tZmlsbC1tb2RlOiBib3RoO1xuICAgIGFuaW1hdGlvbi1maWxsLW1vZGU6IGJvdGg7XG4gIH1cblxuICAvKioqKioqKiAgIEFuaW1hY2nDg8KzbiBkZSBzYWxpZGEgZGUgdmlzdGEgICAqKioqKioqL1xuXG4gIC5mYWRlT3V0TGVmdEJpZyB7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tbmFtZTogZmFkZU91dExlZnRCaWc7XG4gICAgYW5pbWF0aW9uLW5hbWU6IGZhZGVPdXRMZWZ0QmlnO1xuICB9XG5cbi8qKioqKioqICAgQ2FqYSB0aXR1bG8gICAqKioqKioqL1xuLnRpdGxlX2NvbnRhaW5lciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvcmRlcjogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpIHNvbGlkICM0ODQ4NDk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyMik7XG4gICAgaGVpZ2h0OiAxMSU7XG4gICAgbGVmdDogNSU7XG4gICAgdG9wOiA0JTtcbiAgICBwYWRkaW5nOiAwJSAxJSAwJSAwLjUlO1xuICAgIGNvbG9yOiAjMTk4MTk1O1xuICAgIHotaW5kZXg6IDE7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuZGl2LnRpdGxlIHtcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDMpO1xuICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKi0wLjAxOCk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWxpZ2h0KTtcbiAgICBmb250LXdlaWdodDogNjAwO1xufVxuZGl2LnN1Yl90aXRsZSB7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAzNSk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LWJsYWNrKTtcbn1cblxuXG4vKioqKioqKiAgIENhamEgTWVudSBsYXRlcmFsIGl6cXVpZXJkbyAgICoqKioqKiovXG4ucG9zdF9tZW51IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9tZW51L3Bvc3QucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgd2lkdGg6IDclO1xuICAgIGhlaWdodDogNDclO1xuICAgIGJvdHRvbTogMC4zJTtcbiAgICAqYmFja2dyb3VuZC1jb2xvcjogIzkwMDg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5wb3N0X21lbnUgLmJ1dHRvbntcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA2NSk7XG4gICAgbGVmdDogNDQlO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA2NSk7XG4gICAgYm94LXNoYWRvdzogMCBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwNSkgMCBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwMjUpICMzYjNjM2Q7XG4gICAgYm9yZGVyLXRvcDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLWxlZnQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDA1KSBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1yaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLWJvdHRvbTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5wb3N0X21lbnUgLmJ1dHRvbjphY3RpdmUge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgzcHgpO1xuICAgIGJvcmRlci10b3A6IDJweCBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1sZWZ0OiAycHggc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItcmlnaHQ6IDJweCBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjM2IzYzNkO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG59XG4ucG9zdF9tZW51IC5idXR0b24uYnV0dG9uX21lbnUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9tZW51LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogMTElO1xufVxuLnBvc3RfbWVudSAuYnV0dG9uLmJ1dHRvbl9tZW51OmhvdmVye1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9tZW51X2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogMTElO1xufVxuLnBvc3RfbWVudSAuYnV0dG9uLmJ1dHRvbl9vYmplY3RpdmVzIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvb2JqZXRpdmVzLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogMzIlO1xufVxuLnBvc3RfbWVudSAuYnV0dG9uLmJ1dHRvbl9vYmplY3RpdmVzOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvb2JqZXRpdmVzX2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogMzIlO1xufVxuLnBvc3RfbWVudSAuYnV0dG9uLmJ1dHRvbl9zb3VuZC5hY3RpdmUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9zb3VuZC5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB0b3A6IDUzJTtcbn1cbi5wb3N0X21lbnUgLmJ1dHRvbi5idXR0b25fc291bmQuYWN0aXZlOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvc291bmRfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdG9wOiA1MyU7XG59XG4ucG9zdF9tZW51IC5idXR0b24uYnV0dG9uX3NvdW5kLmluYWN0aXZle1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9zb3VuZF9vZmZfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdG9wOiA1MyU7XG59XG4ucG9zdF9tZW51IC5idXR0b24uYnV0dG9uX3Byb2ZpbGUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9wcm9maWxlLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogNzQuMiU7XG59XG4ucG9zdF9tZW51IC5idXR0b24uYnV0dG9uX3Byb2ZpbGU6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9wcm9maWxlX2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogNzQuMiU7XG59XG5cblxuLyoqKioqKiogICBDYWphIE1lbnUgbGF0ZXJhbCBkZXJlY2hvICAgKioqKioqKi9cbi5wb3N0X3Byb2dyZXNzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9nZW5lcmFsX3ZpZXdfbGVzc29ucy9wb3N0X3Byb2dyZXNzLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJvdHRvbTogLTAuNSU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBoZWlnaHQ6IDQ5LjIlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHJpZ2h0OiAtMC4yJTtcbiAgICB3aWR0aDogNyU7XG59XG4ucG9zdF9wcm9ncmVzcyAucGlwZV9wcm9ncmVzcyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMxOTgxOTU7XG4gICAgYm9yZGVyOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgc29saWQgIzNiM2MzZGRkO1xuICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMjYpO1xuICAgIGhlaWdodDogODElO1xuICAgIGxlZnQ6IDE2JTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHRvcDogOSU7XG4gICAgd2lkdGg6IDQwJTtcbn1cbi5wb3N0X3Byb2dyZXNzIC5waXBlX3Byb2dyZXNzIC5jdXJyZW50X3Byb2dyZXNzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0Y3OUQwMTtcbiAgICBib3JkZXItcmlnaHQ6IDVweCBzb2xpZCAjRTI2NzIzO1xuICAgIGhlaWdodDogNTAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvdHRvbTogMDtcbn1cblxuXG4vKioqKioqKiAgIERpZmVyZW50ZXMgZWxlbWVudG9zIGRlIGxhIHZpc3RhICAgKioqKioqKi9cbi5waXBlbGluZV92ZXJ0aWNhbCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvZ2VuZXJhbF92aWV3X2xlc3NvbnMvcGlwZWxpbmVfdmVydGljYWwucG5nKSBuby1yZXBlYXQ7ICBcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB3aWR0aDogNiU7XG4gICAgaGVpZ2h0OiAzNyU7XG4gICAgdG9wOiA4JTtcbiAgICBsZWZ0OiAzJTtcbn1cblxuLnBpcGVsaW5lX2hvcml6b250YWwge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL3BpcGVsaW5lX2hvcml6b250YWwucG5nKSBuby1yZXBlYXQ7ICBcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB3aWR0aDogNzEuNSU7XG4gICAgaGVpZ2h0OiAzNCU7XG4gICAgdG9wOiA2NiU7XG4gICAgbGVmdDogMTMlO1xufVxuXG4uaG91c2VzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9nZW5lcmFsX3ZpZXdfbGVzc29ucy9ob3VzZXMucG5nKSBuby1yZXBlYXQ7ICBcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDE4JTtcbiAgICByaWdodDogMDtcbiAgICB3aWR0aDogMzQlO1xufVxuXG5cblxuXG4vKioqKioqKiAgIENhamEgY2FtYmlvIGRlIHZpc3RhcyAgICoqKioqKiovXG4uYWR2YW5jZV9ib3gge1xuICAgIGhlaWdodDogMTYlO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICBtYXJnaW4tdG9wOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjI2KTtcbiAgICBwYWRkaW5nOiAwJSAxJTtcbiAgICB0b3A6IDc2JTtcbiAgICB3aWR0aDogMzQlO1xuICAgIHdpZHRoOiBtaW4tY29udGVudDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZ2FwOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxOCk7XG59XG5cbi5wYWdfbGVmdCB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9idXR0b25zL3BhZ19sZWZ0LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNSk7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDUpO1xufVxuLnBhZ19sZWZ0OmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvcGFnX2xlZnRfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG59XG4ucGFnX3JpZ2h0IHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvcGFnX3JpZ2h0LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNSk7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDUpO1xufVxuLnBhZ19yaWdodDpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9idXR0b25zL3BhZ19yaWdodF9ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbi5wb2ludCB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9idXR0b25zL3BvaW50LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTUpO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxNSk7XG59XG4ucG9pbnQucG9pbnRfY3VycmVudCB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9idXR0b25zL3BvaW50X2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuLnBvaW50OmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvcG9pbnRfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG59XG4ucG9pbnQgYXtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBoZWlnaHQ6IGluaGVyaXQ7XG4gICAgd2lkdGg6IGluaGVyaXQ7XG59XG5cblxuXG4uYnV0dG9uX21lbnUgYXtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBoZWlnaHQ6IGluaGVyaXQ7XG4gICAgd2lkdGg6IGluaGVyaXQ7XG59Il0sInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 7843:
/*!************************************************!*\
  !*** ./src/app/section1/section1.component.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Section1Component": () => (/* binding */ Section1Component)
/* harmony export */ });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ 5139);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var _services_audio_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/audio.service */ 6425);
/* harmony import */ var _services_progress_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/progress.service */ 8458);
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/api.service */ 5830);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ 4666);
/* harmony import */ var _layout_layout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../layout/layout.component */ 6674);








function Section1Component_div_15_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 20)(1, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](2, "div", 22)(3, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5, " AC\u00C1 ENCONTRAR\u00C1S LA BARRA ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](7, " DE NAVEGACI\u00D3N ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
  }
}
function Section1Component_div_16_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 25)(1, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](2, "div", 26)(3, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5, " ESTE ES EL BOT\u00D3N MEN\u00DA ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](6, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
  }
}
function Section1Component_div_17_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 30)(1, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](2, "div", 31)(3, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5, " ESTE ES EL BOT\u00D3N OBJETIVOS ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](6, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
  }
}
function Section1Component_div_18_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 35)(1, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](2, "div", 36)(3, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5, " ESTE ES EL BOT\u00D3N SILENCIAR ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](6, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
  }
}
function Section1Component_div_19_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 40)(1, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](2, "div", 41)(3, "div", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5, " ESTE ES EL BOT\u00D3N PERFIL ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](6, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
  }
}
function Section1Component_div_20_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 45)(1, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](2, "div", 46)(3, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5, " AC\u00C1 PUEDES SELECCIONAR ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](7, " EL M\u00D3DULO AL CUAL INGRESAR ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
  }
}
function Section1Component_div_21_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 49)(1, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](2, "div", 50)(3, "div", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "div", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5, " DEBES COMPLETAR LOS M\u00D3DULOS ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](7, " EN ORDEN, EMPEZANDO ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](8, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](9, " POR EL N\u00DAMERO UNO ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
  }
}
function Section1Component_div_22_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 53)(1, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](2, "div", 54)(3, "div", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "div", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5, " AL COMPLETAR UN M\u00D3DULO ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](7, " PUEDES ACCEDER AL SIGUIENTE ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
  }
}
function Section1Component_div_23_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 57)(1, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](2, "div", 58)(3, "div", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "div", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5, " AL AVANZAR EN LAS VISTAS ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](7, " PUEDES VER TU PROGRESO EN LA BARRA ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
  }
}
class Section1Component {
  constructor(router, audioService, progressService, apiService) {
    this.router = router;
    this.audioService = audioService;
    this.progressService = progressService;
    this.apiService = apiService;
    // hoverAudio = new Audio(`../../assets/section1/sounds/hoverDefault.mp3`);
    // clickAudio = new Audio(`../../assets/section1/sounds/clickDefault.mp3`);
    /* Valida si el toolTip debe mostrarse (true) o no (false) */
    this.tooltipNavigatorBar = false;
    this.tooltipMenu = false;
    this.tooltipObjectives = false;
    this.tooltipSound = false;
    this.tooltipProfile = false;
    this.tooltipModulesBar = false;
    this.tooltipModule1 = false;
    this.tooltipModule2 = false;
    this.tooltipProgress = false;
    this.stateSoundMenu = this.audioService.getStateSoundMenu();
    this.showTooltips = this.progressService.getShowTooltipsSection1();
    this.lesson1Available = this.progressService.getLesson1Available();
    this.lesson2Available = this.progressService.getLesson2Available();
    this.lesson3Available = this.progressService.getLesson3Available();
    this.lesson4Available = this.progressService.getLesson4Available();
    this.progressService.getProgressPercentage$().subscribe(progressPercentage => {});
    this.audioService.getStateSoundMenu$().subscribe(stateSoundMenu => {
      this.stateSoundMenu = stateSoundMenu;
    });
    this.progressService.getShowTooltipsSection1$().subscribe(showTooltips => {
      this.showTooltips = showTooltips;
    });
    this.progressService.getLesson1Available$().subscribe(lesson1Available => {
      this.lesson1Available = lesson1Available;
    });
    this.progressService.getLesson2Available$().subscribe(lesson2Available => {
      this.lesson2Available = lesson2Available;
    });
    this.progressService.getLesson3Available$().subscribe(lesson3Available => {
      this.lesson3Available = lesson3Available;
    });
    this.progressService.getLesson4Available$().subscribe(lesson4Available => {
      this.lesson4Available = lesson4Available;
    });
    // this.launchToolTips();
  }

  ngOnInit() {
    this.repair();
    this.showUser();
  }
  showUser() {
    this.users = this.apiService.listUser().subscribe(user => {
      this.users = user;
      console.log(this.users);
    });
  }
  hoverBtnPostMenu() {
    if (this.stateSoundMenu === 'active') {
      this.audioService.hoverAudio.play();
    }
  }
  leaveBtnPostMenu() {
    this.audioService.hoverAudio.pause();
    this.audioService.hoverAudio.currentTime = 0;
  }
  btnModule(numModule) {
    this.router.navigate([`/section1/lesson${numModule}`]);
  }
  switchRightWrong() {
    const btn1 = jquery__WEBPACK_IMPORTED_MODULE_0__('div.city_btn1');
    const btn2 = jquery__WEBPACK_IMPORTED_MODULE_0__('div.city_btn2');
    const btn3 = jquery__WEBPACK_IMPORTED_MODULE_0__('div.city_btn3');
    const btn4 = jquery__WEBPACK_IMPORTED_MODULE_0__('div.city_btn4');
    console.log('btn1: ', btn1.hasClass('right'));
    btn1.hasClass('right') ? btn1[0].classList.remove('right') & btn1[0].classList.add('wrong') : btn1[0].classList.remove('wrong') & btn1[0].classList.add('right');
    btn2.hasClass('right') ? btn2[0].classList.remove('right') & btn2[0].classList.add('wrong') : btn2[0].classList.remove('wrong') & btn2[0].classList.add('right');
    btn3.hasClass('right') ? btn3[0].classList.remove('right') & btn3[0].classList.add('wrong') : btn3[0].classList.remove('wrong') & btn3[0].classList.add('right');
    btn4.hasClass('right') ? btn4[0].classList.remove('right') & btn4[0].classList.add('wrong') : btn4[0].classList.remove('wrong') & btn4[0].classList.add('right');
  }
  repair() {
    const btn1 = jquery__WEBPACK_IMPORTED_MODULE_0__('div.city_btn1');
    const btn2 = jquery__WEBPACK_IMPORTED_MODULE_0__('div.city_btn2');
    const btn3 = jquery__WEBPACK_IMPORTED_MODULE_0__('div.city_btn3');
    const btn4 = jquery__WEBPACK_IMPORTED_MODULE_0__('div.city_btn4');
    if (this.progressService.getProgressPercentage() >= 25.5) {
      setTimeout(() => {
        btn1[0].classList.remove('wrong');
      }, 1);
      btn1[0].classList.add('right');
    }
    if (this.progressService.getProgressPercentage() >= 68) {
      setTimeout(() => {
        btn2[0].classList.remove('wrong');
      }, 1);
      btn2[0].classList.add('right');
    }
    if (this.progressService.getProgressPercentage() >= 85) {
      setTimeout(() => {
        btn3[0].classList.remove('wrong');
      }, 1);
      btn3[0].classList.add('right');
    }
    if (this.progressService.getProgressPercentage() == 100) {
      setTimeout(() => {
        btn4[0].classList.remove('wrong');
      }, 1);
      btn4[0].classList.add('right');
    }
  }
  launchToolTips() {
    console.log('launchTooltips: ', this.showTooltips);
    if (this.progressService.getProgressPercentage() == 0) {
      this.progressService.setShowTooltipsSection1(false);
      // $(".button").each(function() {
      //   $(this).addClass('disabled');
      // });
      //$('.post_menu').css('z-index', 100);
      this.tooltipNavigatorBar = true;
      setTimeout(() => {
        // $(".button").each(function() {
        //   $(this).removeClass('disabled');
        // });
        this.tooltipNavigatorBar = false;
        // $('.button_objectives').css('opacity', 0.5);
        // $('.button_sound').css('opacity', 0.5);
        // $('.button_profile').css('opacity', 0.5);
        this.tooltipMenu = true;
        setTimeout(() => {
          this.tooltipMenu = false;
          // $('.button_menu').css('opacity', 0.5);
          // $('.button_objectives').css('opacity', '');
          this.tooltipObjectives = true;
          setTimeout(() => {
            this.tooltipObjectives = false;
            // $('.button_objectives').css('opacity', 0.5);
            // $('.button_sound').css('opacity', '');
            this.tooltipSound = true;
            setTimeout(() => {
              this.tooltipSound = false;
              // $('.button_sound').css('opacity', 0.5);
              // $('.button_profile').css('opacity', '');
              this.tooltipProfile = true;
              setTimeout(() => {
                this.tooltipProfile = false;
                // $('.button_menu').css('opacity', '');
                // $('.button_objectives').css('opacity', '');
                // $('.button_sound').css('opacity', '');
                jquery__WEBPACK_IMPORTED_MODULE_0__('.post_menu').css('z-index', 10);
                jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn1').css('z-index', 101);
                jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn1').addClass('disabled');
                jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn2').css('z-index', 100);
                jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn2').addClass('disabled');
                jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn3').css('z-index', 100);
                jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn3').addClass('disabled');
                jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn4').css('z-index', 100);
                jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn4').addClass('disabled');
                this.tooltipModulesBar = true;
                setTimeout(() => {
                  this.tooltipModulesBar = false;
                  jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn2').css('z-index', 3);
                  jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn3').css('z-index', 5);
                  jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn4').css('z-index', 3);
                  this.tooltipModule1 = true;
                  setTimeout(() => {
                    this.tooltipModule1 = false;
                    jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn1').css('z-index', 4);
                    jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn2').css('z-index', 100);
                    this.tooltipModule2 = true;
                    setTimeout(() => {
                      this.tooltipModule2 = false;
                      jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn2').css('z-index', 3);
                      this.tooltipProgress = true;
                      setTimeout(() => {
                        this.tooltipProgress = false;
                        jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn2').css('z-index', 3);
                        jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn1').removeClass('disabled');
                        jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn2').removeClass('disabled');
                        jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn3').removeClass('disabled');
                        jquery__WEBPACK_IMPORTED_MODULE_0__('.city_btn4').removeClass('disabled');
                      }, 7000);
                    }, 7000);
                  }, 7000);
                }, 7000);
              }, 3000);
            }, 3000);
          }, 3000);
        }, 3000);
      }, 7000);
    }
  }
}
Section1Component.ɵfac = function Section1Component_Factory(t) {
  return new (t || Section1Component)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_services_audio_service__WEBPACK_IMPORTED_MODULE_1__.AudioService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_services_progress_service__WEBPACK_IMPORTED_MODULE_2__.ProgressService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_services_api_service__WEBPACK_IMPORTED_MODULE_3__.ApiService));
};
Section1Component.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: Section1Component,
  selectors: [["app-section1"]],
  decls: 25,
  vars: 13,
  consts: [[1, "fondo"], [1, "city_btn1", "wrong", 3, "ngClass", "click", "mouseenter", "mouseleave"], [1, "module_1_info"], [1, "city_btn2", "wrong", 3, "ngClass", "click", "mouseenter", "mouseleave"], [1, "module_2_info"], [1, "city_btn3", "wrong", 3, "ngClass", "click", "mouseenter", "mouseleave"], [1, "module_3_info"], [1, "city_btn4", "wrong", 3, "ngClass", "click", "mouseenter", "mouseleave"], [1, "module_4_info"], [1, "pipes"], [1, "title"], ["id", "tooltipNavigatorBar", 4, "ngIf"], ["id", "tooltipMenu", 4, "ngIf"], ["id", "tooltipObjectives", 4, "ngIf"], ["id", "tooltipSound", 4, "ngIf"], ["id", "tooltipProfile", 4, "ngIf"], ["id", "tooltipModulesBar", 4, "ngIf"], ["id", "tooltipModule1", 4, "ngIf"], ["id", "tooltipModule2", 4, "ngIf"], ["id", "tooltipProgress", 4, "ngIf"], ["id", "tooltipNavigatorBar"], [1, "centerDiv"], ["id", "rowNavigatorBar"], ["id", "boxIndicatorNavigatorBar"], ["id", "wordNavigatorBar", 1, "tooltipWords"], ["id", "tooltipMenu"], ["id", "rowMenu"], ["id", "boxIndicatorMenu"], ["id", "wordMenu", 1, "tooltipWords"], [1, "tooltipButtonNav", "button_menu"], ["id", "tooltipObjectives"], ["id", "rowObjectives"], ["id", "boxIndicatorObjectives"], ["id", "wordObjectives", 1, "tooltipWords"], [1, "tooltipButtonNav", "button_objectives"], ["id", "tooltipSound"], ["id", "rowSound"], ["id", "boxIndicatorSound"], ["id", "wordSound", 1, "tooltipWords"], [1, "tooltipButtonNav", "button_sound"], ["id", "tooltipProfile"], ["id", "rowProfile"], ["id", "boxIndicatorProfile"], ["id", "wordProfile", 1, "tooltipWords"], [1, "tooltipButtonNav", "button_profile"], ["id", "tooltipModulesBar"], ["id", "rowModulesBar"], ["id", "boxIndicatorModulesBar"], ["id", "wordModulesBar", 1, "tooltipWords"], ["id", "tooltipModule1"], ["id", "rowModule1"], ["id", "boxIndicatorModule1"], ["id", "wordModule1", 1, "tooltipWords"], ["id", "tooltipModule2"], ["id", "rowModule2"], ["id", "boxIndicatorModule2"], ["id", "wordModule2", 1, "tooltipWords"], ["id", "tooltipProgress"], ["id", "rowProgress"], ["id", "boxIndicatorProgress"], ["id", "wordProgress", 1, "tooltipWords"]],
  template: function Section1Component_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 0)(1, "div", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function Section1Component_Template_div_click_1_listener() {
        return ctx.btnModule(1);
      })("mouseenter", function Section1Component_Template_div_mouseenter_1_listener() {
        return ctx.hoverBtnPostMenu();
      })("mouseleave", function Section1Component_Template_div_mouseleave_1_listener() {
        return ctx.leaveBtnPostMenu();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](2, "div", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](3, "M\u00D3DULO 1");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "div", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function Section1Component_Template_div_click_4_listener() {
        return ctx.btnModule(2);
      })("mouseenter", function Section1Component_Template_div_mouseenter_4_listener() {
        return ctx.hoverBtnPostMenu();
      })("mouseleave", function Section1Component_Template_div_mouseleave_4_listener() {
        return ctx.leaveBtnPostMenu();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](5, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](6, "M\u00D3DULO 2");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](7, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function Section1Component_Template_div_click_7_listener() {
        return ctx.btnModule(3);
      })("mouseenter", function Section1Component_Template_div_mouseenter_7_listener() {
        return ctx.hoverBtnPostMenu();
      })("mouseleave", function Section1Component_Template_div_mouseleave_7_listener() {
        return ctx.leaveBtnPostMenu();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](8, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](9, "M\u00D3DULO 3");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](10, "div", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function Section1Component_Template_div_click_10_listener() {
        return ctx.btnModule(4);
      })("mouseenter", function Section1Component_Template_div_mouseenter_10_listener() {
        return ctx.hoverBtnPostMenu();
      })("mouseleave", function Section1Component_Template_div_mouseleave_10_listener() {
        return ctx.leaveBtnPostMenu();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](11, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](12, "M\u00D3DULO 4");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](13, "div", 9)(14, "div", 10);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](15, Section1Component_div_15_Template, 8, 0, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](16, Section1Component_div_16_Template, 7, 0, "div", 12);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](17, Section1Component_div_17_Template, 7, 0, "div", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](18, Section1Component_div_18_Template, 7, 0, "div", 14);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](19, Section1Component_div_19_Template, 7, 0, "div", 15);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](20, Section1Component_div_20_Template, 8, 0, "div", 16);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](21, Section1Component_div_21_Template, 10, 0, "div", 17);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](22, Section1Component_div_22_Template, 8, 0, "div", 18);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](23, Section1Component_div_23_Template, 8, 0, "div", 19);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](24, "app-layout");
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngClass", ctx.lesson1Available);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngClass", ctx.lesson2Available);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngClass", ctx.lesson3Available);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngClass", ctx.lesson4Available);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](5);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.tooltipNavigatorBar);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.tooltipMenu);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.tooltipObjectives);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.tooltipSound);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.tooltipProfile);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.tooltipModulesBar);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.tooltipModule1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.tooltipModule2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.tooltipProgress);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_7__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_7__.NgIf, _layout_layout_component__WEBPACK_IMPORTED_MODULE_4__.LayoutComponent],
  styles: [".fondo[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('fondo.png') no-repeat;  \n    background-size: 100% 100%;\n    width: 100%;\n    height:100%;  \n    top: 0%;\n}\n\n.disabled[_ngcontent-%COMP%], .inactive[_ngcontent-%COMP%] {\n    pointer-events: none;\n}\n.active[_ngcontent-%COMP%] {\n    cursor: pointer;\n}\n\n\n.city_btn2[_ngcontent-%COMP%]{\n    clip-path: polygon(0 0, 100% 0, 100% 100%, 47% 100%, 47% 38%, 0% 40%);;\n}\n.city_btn2[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    display: block;\n    height: 100%;\n    width: 100%;\n}\n.city_btn2[_ngcontent-%COMP%]   .module_2_info[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #d4e3e3;\n    border: calc(var(--frameHeight)*0.008) solid #3d3d3d;\n    border-radius: calc(var(--frameHeight)*0.01);\n    color: #198195;\n    font-size: calc(var(--frameHeight)*0.035);\n    font-weight: bolder;\n    height: calc(var(--frameHeight)*0.08104);\n    left: 53.5%;\n    text-align: center;\n    top: 38%;\n    width: calc(var(--frameHeight)*0.216);\n    display: flex;\n    align-items: center;\n    justify-content: center;\n}\n.city_btn2.right[_ngcontent-%COMP%]:hover   .module_2_info[_ngcontent-%COMP%], .city_btn2.wrong[_ngcontent-%COMP%]:hover   .module_2_info[_ngcontent-%COMP%]\n{\n    display: none;\n}\n.city_btn2.right[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('btn2_right.png') no-repeat;\n    background-size: 100% 100%;\n    height: 27%;\n    left: -10.4%;\n    top: 42.5%;\n    width: 43.5%;\n    cursor: pointer;\n    z-index: 3;\n}\n.city_btn2.right[_ngcontent-%COMP%]:hover {\n    position: absolute;\n    background: url('btn2_right_hover.png') no-repeat;\n    background-size: 100% 100%;\n    height: 29%;\n    left: -12%;\n    top: 40.5%;\n    width: 45.5%;\n    cursor: pointer;\n    z-index: 10;\n}\n.city_btn2.wrong[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('btn2_wrong.png') no-repeat;\n    background-size: 100% 100%;\n    height: 27%;\n    left: -10.4%;\n    top: 42.5%;\n    width: 43.5%;\n    cursor: pointer;\n    z-index: 3;\n    \n}\n.city_btn2.wrong[_ngcontent-%COMP%]:hover {\n    position: absolute;\n    background: url('btn2_wrong_hover.png') no-repeat;\n    background-size: 100% 100%;\n    height: 29%;\n    left: -12%;\n    top: 40.5%;\n    width: 45.5%;\n    cursor: pointer;\n    z-index: 10;\n}\n\n\n.city_btn3[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    display: block;\n    height: 100%;\n    width: 100%;\n}\n.city_btn3[_ngcontent-%COMP%]   .module_3_info[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #d4e3e3;\n    border: calc(var(--frameHeight)*0.008) solid #3d3d3d;\n    border-radius: calc(var(--frameHeight)*0.01);\n    color: #198195;\n    font-size: calc(var(--frameHeight)*0.035);\n    font-weight: bolder;\n    height: calc(var(--frameHeight)*0.08104);\n    left: 24%;\n    text-align: center;\n    top: 50%;\n    width: calc(var(--frameHeight)*0.216);\n    display: flex;\n    align-items: center;\n    justify-content: center;\n}\n.city_btn3.right[_ngcontent-%COMP%]:hover   .module_3_info[_ngcontent-%COMP%], .city_btn3.wrong[_ngcontent-%COMP%]:hover   .module_3_info[_ngcontent-%COMP%]\n{\n    display: none;\n}\n.city_btn3.right[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('btn3_right.png') no-repeat;\n    background-size: 100% 100%;\n    height: 47%;\n    left: 28.52%;\n    top: 23.8%;\n    width: 22.1%;\n    cursor: pointer;\n    z-index: 5;\n}\n.city_btn3.right[_ngcontent-%COMP%]:hover {\n    position: absolute;\n    background: url('btn3_right_hover.png') no-repeat;\n    background-size: 100% 100%;\n    height: 49%;\n    left: 27.8%;\n    top: 21.95%;\n    width: 23.1%;\n    cursor: pointer;\n    z-index: 10;\n}\n.city_btn3.wrong[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('btn3_wrong.png') no-repeat;\n    background-size: 100% 100%;\n    height: 47%;\n    left: 28.52%;\n    top: 23.8%;\n    width: 22.1%;\n    cursor: pointer;\n    z-index: 5;\n}\n.city_btn3.wrong[_ngcontent-%COMP%]:hover {\n    position: absolute;\n    background: url('btn3_wrong_hover.png') no-repeat;\n    background-size: 100% 100%;\n    height: 49%;\n    left: 27.8%;\n    top: 21.95%;\n    width: 23.1%;\n    cursor: pointer;\n    z-index: 10;\n}\n\n\n.city_btn1[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    display: block;\n    height: 100%;\n    width: 100%;\n}\n.city_btn1[_ngcontent-%COMP%]   .module_1_info[_ngcontent-%COMP%] {\n    \n    position: absolute;\n    background-color: #d4e3e3;\n    border: calc(var(--frameHeight)*0.008) solid #3d3d3d;\n    border-radius: calc(var(--frameHeight)*0.01);\n    color: #198195;\n    font-size: calc(var(--frameHeight)*0.035);\n    font-weight: bolder;\n    height: calc(var(--frameHeight)*0.08);\n    left: 9%;\n    text-align: center;\n    top: 66%;\n    width: calc(var(--frameHeight)*0.216);\n    display: flex;\n    align-items: center;\n    justify-content: center;\n}\n.city_btn1.right[_ngcontent-%COMP%]:hover   .module_1_info[_ngcontent-%COMP%], .city_btn1.wrong[_ngcontent-%COMP%]:hover   .module_1_info[_ngcontent-%COMP%]\n{\n    display: none;\n}\n.city_btn1.right[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('btn1_right.png') no-repeat;\n    background-size: 100% 100%;\n    height: 62%;\n    left: 47.62%;\n    top: 7.5%;\n    width: 11.35%;\n    cursor: pointer;\n    z-index: 4;\n}\n.city_btn1.right[_ngcontent-%COMP%]:hover {\n    position: absolute;\n    background: url('btn1_right_hover.png') no-repeat;\n    background-size: 100% 100%;\n    height: 64%;\n    left: 46.92%;\n    top: 5.5%;\n    width: 12.35%;\n    cursor: pointer;\n    z-index: 10;\n}\n.city_btn1.wrong[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('btn1_wrong.png') no-repeat;\n    background-size: 100% 100%;\n    height: 62%;\n    left: 47.62%;\n    top: 7.5%;\n    width: 11.35%;\n    cursor: pointer;\n    z-index: 4;\n}\n.city_btn1.wrong[_ngcontent-%COMP%]:hover {\n    position: absolute;\n    background: url('btn1_wrong_hover.png') no-repeat;\n    background-size: 100% 100%;\n    height: 64%;\n    left: 46.92%;\n    top: 5.5%;\n    width: 12.35%;\n    cursor: pointer;\n    z-index: 10;\n}\n\n\n.city_btn4[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    display: block;\n    height: 100%;\n    width: 100%;\n}\n.city_btn4[_ngcontent-%COMP%]   .module_4_info[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #d4e3e3;\n    border: calc(var(--frameHeight)*0.008) solid #3d3d3d;\n    border-radius: calc(var(--frameHeight)*0.01);\n    color: #198195;\n    font-size: calc(var(--frameHeight)*0.035);\n    font-weight: bolder;\n    height: calc(var(--frameHeight)*0.08104);\n    left: 16%;\n    text-align: center;\n    top: 0%;\n    width: calc(var(--frameHeight)*0.216);\n    display: flex;\n    align-items: center;\n    justify-content: center;\n}\n.city_btn4.right[_ngcontent-%COMP%]:hover   .module_4_info[_ngcontent-%COMP%], .city_btn4.wrong[_ngcontent-%COMP%]:hover   .module_4_info[_ngcontent-%COMP%]\n{\n    display: none;\n}\n.city_btn4.right[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('btn4_right.png') no-repeat;\n    background-size: 100% 100%;\n    height: 34%;\n    left: 54.35%;\n    top: 35.5%;\n    width: 61.6%;\n    cursor: pointer;\n    z-index: 3;\n}\n.city_btn4.right[_ngcontent-%COMP%]:hover {\n    position: absolute;\n    background: url('btn4_right_hover.png') no-repeat;\n    background-size: 100% 100%;\n    height: 36%;\n    left: 54.35%;\n    top: 33.5%;\n    width: 62.6%;\n    cursor: pointer;\n    z-index: 10;\n}\n.city_btn4.wrong[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('btn4_wrong.png') no-repeat;\n    background-size: 100% 100%;\n    height: 33.7%;\n    left: 54.35%;\n    top: 35.8%;\n    width: 61.6%;\n    cursor: pointer;\n    z-index: 3;\n}\n.city_btn4.wrong[_ngcontent-%COMP%]:hover {\n    position: absolute;\n    background: url('btn4_wrong_hover.png') no-repeat;\n    background-size: 100% 100%;\n    height: 36%;\n    left: 54.35%;\n    top: 33.5%;\n    width: 62.6%;\n    cursor: pointer;\n    z-index: 10;\n}\n\n.pipes[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('tuberias.png') no-repeat;\n    background-size: 90% 70%;\n    width: 87%;\n    height: 45%;\n    top: 69.2%;\n    left: 11.5%;\n}\n\n.title[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('title.png') no-repeat;\n    background-size: 100% 100%;\n    width: 23.5%;\n    height: 28%;\n    top: 1%;\n    right: -0.7%;\n}\n\n\n\n\n\n\n\n\n\n.btnSwitch[_ngcontent-%COMP%] {\n    position: absolute;\n    align-items: center;\n    background-color: #1995AD;\n    border: calc(var(--frameHeight)*0.01) solid #3B3C3D;\n    border-radius: 50%;\n    bottom: 0%;\n    display: flex;\n    font-size: calc(var(--frameHeight)*0.028);\n    height: calc(var(--frameHeight)*0.13);\n    justify-content: center;\n    line-height: 100%;\n    right: 8%;\n    width: calc(var(--frameHeight)*0.13);\n    z-index: 8;\n    transition: 0.5s all ease;\n}\n\n.btnSwitch[_ngcontent-%COMP%]:hover {\n    transform: scale(1.2);\n    background-color: #156270;\n}\n\n\n\n.centerDiv[_ngcontent-%COMP%] {\n    position: absolute;\n    height: var(--frameHeight);\n    width: var(--frameWidth);\n}\n.tooltipWords[_ngcontent-%COMP%] {\n    position: inherit;\n    background-color: rgba(0, 0, 0, 0.8);\n    border-radius: calc(var(--frameHeight) * 0.02);\n    box-shadow: calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0)\n        calc(var(--frameHeight) * 0.025) #fff;\n    color: white;\n    font-family: var(--font-family-regular);\n    font-size: calc(var(--frameHeight) * 0.03);\n    line-height: normal;\n    padding: 0.7%;\n    text-align: center;\n}\n.tooltipButtonNav[_ngcontent-%COMP%]{\n    position: absolute;\n    width: calc(var(--frameHeight)*0.065);\n    height: calc(var(--frameHeight)*0.065);\n    left: 3.15%;\n    box-shadow: 0 calc(var(--frameHeight)*0.008) calc(var(--frameHeight)*0) calc(var(--frameHeight)*0.002) #3b3c3d;\n    border-top:calc(var(--frameHeight)*0.008) solid #3b3c3d;\n    border-left:calc(var(--frameHeight)*0.008) solid #3b3c3d;\n    border-right:calc(var(--frameHeight)*0.008) solid #3b3c3d;\n    border-bottom:calc(var(--frameHeight)*0.008) solid #3b3c3d;\n    border-radius: 50%;\n    z-index: 1;\n}\n\n#tooltipNavigatorBar[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n#rowNavigatorBar[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('toolTipIndicator.png')\n        no-repeat;\n    background-size: 100% 100%;\n    height: 2.27%;\n    left: 8%;\n    top: 76%;\n    width: 12%;\n}\n#boxIndicatorNavigatorBar[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #fffc;\n    border: 1px solid #3338;\n    border-radius: calc(var(--frameHeight) * 0.01);\n    box-shadow: calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0.03) #fff;\n    height: calc(var(--frameHeight) * 0.47);\n    left: 2%;\n    top: 52.5%;\n    width: calc(var(--frameHeight) * 0.117);\n}\n#wordNavigatorBar[_ngcontent-%COMP%] {\n    left: 21%;\n    top: 72%;\n}\n\n#tooltipMenu[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n#rowMenu[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('toolTipIndicator.png')\n        no-repeat;\n    background-size: 100% 100%;\n    height: 2.27%;\n    left: 8%;\n    top: 60.5%;\n    width: 12%;\n}\n#boxIndicatorMenu[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #fffc;\n    border: 1px solid #3338;\n    border-radius: calc(var(--frameHeight) * 0.01);\n    box-shadow: calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0.03) #fff;\n    height: calc(var(--frameHeight) * 0.47);\n    left: 2%;\n    top: 52.5%;\n    width: calc(var(--frameHeight) * 0.117);\n}\n#wordMenu[_ngcontent-%COMP%] {\n    left: 21%;\n    top: 58%;\n}\n\n.tooltipButtonNav.button_menu[_ngcontent-%COMP%] {\n    background: url('menu.png') no-repeat;\n    background-size: 100% 100%;\n    top: 58%;\n}\n\n#tooltipObjectives[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n#rowObjectives[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('toolTipIndicator.png')\n        no-repeat;\n    background-size: 100% 100%;\n    height: 2.27%;\n    left: 8%;\n    top: 70.5%;\n    width: 12%;\n}\n#boxIndicatorObjectives[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #fffc;\n    border: 1px solid #3338;\n    border-radius: calc(var(--frameHeight) * 0.01);\n    box-shadow: calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0.03) #fff;\n    height: calc(var(--frameHeight) * 0.47);\n    left: 2%;\n    top: 52.5%;\n    width: calc(var(--frameHeight) * 0.117);\n}\n#wordObjectives[_ngcontent-%COMP%] {\n    left: 21%;\n    top: 68%;\n}\n.tooltipButtonNav.button_objectives[_ngcontent-%COMP%] {\n    background: url('objetives.png') no-repeat;\n    background-size: 100% 100%;\n    top: 68%;\n}\n\n#tooltipSound[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n#rowSound[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('toolTipIndicator.png')\n        no-repeat;\n    background-size: 100% 100%;\n    height: 2.27%;\n    left: 8%;\n    top: 80.5%;\n    width: 12%;\n}\n#boxIndicatorSound[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #fffc;\n    border: 1px solid #3338;\n    border-radius: calc(var(--frameHeight) * 0.01);\n    box-shadow: calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0.03) #fff;\n    height: calc(var(--frameHeight) * 0.47);\n    left: 2%;\n    top: 52.5%;\n    width: calc(var(--frameHeight) * 0.117);\n}\n#wordSound[_ngcontent-%COMP%] {\n    left: 21%;\n    top: 78.5%;\n}\n.tooltipButtonNav.button_sound[_ngcontent-%COMP%] {\n    background: url('sound.png') no-repeat;\n    background-size: 100% 100%;\n    top: 78%;\n}\n\n#tooltipProfile[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n#rowProfile[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('toolTipIndicator.png')\n        no-repeat;\n    background-size: 100% 100%;\n    height: 2.27%;\n    left: 8%;\n    top: 90.5%;\n    width: 12%;\n}\n#boxIndicatorProfile[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #fffc;\n    border: 1px solid #3338;\n    border-radius: calc(var(--frameHeight) * 0.01);\n    box-shadow: calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0.03) #fff;\n    height: calc(var(--frameHeight) * 0.47);\n    left: 2%;\n    top: 52.5%;\n    width: calc(var(--frameHeight) * 0.117);\n}\n#wordProfile[_ngcontent-%COMP%] {\n    left: 21%;\n    top: 88%;\n}\n.tooltipButtonNav.button_profile[_ngcontent-%COMP%] {\n    background: url('profile.png') no-repeat;\n    background-size: 100% 100%;\n    top: 88%;\n}\n\n#tooltipModulesBar[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n#rowModulesBar[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('toolTipIndicator.png')\n    no-repeat;\n    background-size: 100% 100%;\n    height: 4.27%;\n    left: 47%;\n    top: 77%;\n    transform: rotate(90deg);\n    width: 7%;\n}\n#boxIndicatorModulesBar[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #fffb;\n    border: 1px solid #3338;\n    border-radius: calc(var(--frameHeight) * 0.01);\n    box-shadow: calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0.03) #fff;\n    height: calc(var(--frameHeight) * 0.647);\n    left: 12%;\n    top: 5.5%;\n    width: calc(var(--frameHeight) * 1.7);\n}\n#wordModulesBar[_ngcontent-%COMP%] {\n    left: 40%;\n    top: 89%;\n}\n\n#tooltipModule1[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n#rowModule1[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('toolTipIndicator.png')\n    no-repeat;\n    background-size: 100% 100%;\n    height: 3.27%;\n    left: 50.5%;\n    top: 75%;\n    transform: rotate(90deg);\n    width: 5%;\n}\n#boxIndicatorModule1[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #fffb;\n    border: 1px solid #3338;\n    border-radius: calc(var(--frameHeight) * 0.01);\n    box-shadow: calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0.03) #fff;\n    height: calc(var(--frameHeight) * 0.647);\n    left: 12%;\n    top: 5.5%;\n    width: calc(var(--frameHeight) * 1.7);\n}\n#wordModule1[_ngcontent-%COMP%] {\n    left: 42%;\n    top: 85%;\n}\n\n#tooltipModule2[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n#rowModule2[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('toolTipIndicator.png')\n    no-repeat;\n    background-size: 100% 100%;\n    height: 3.27%;\n    left: 20.5%;\n    top: 75%;\n    transform: rotate(90deg);\n    width: 5%;\n}\n#boxIndicatorModule2[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #fffb;\n    border: 1px solid #3338;\n    border-radius: calc(var(--frameHeight) * 0.01);\n    box-shadow: calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0.03) #fff;\n    height: calc(var(--frameHeight) * 0.647);\n    left: 12%;\n    top: 5.5%;\n    width: calc(var(--frameHeight) * 1.7);\n}\n#wordModule2[_ngcontent-%COMP%] {\n    left: 11%;\n    top: 85%;\n}\n\n#tooltipProgress[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n#rowProgress[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('toolTipIndicator.png')\n    no-repeat;\n    background-size: 100% 100%;\n    height: 3.27%;\n    left: 87.5%;\n    top: 75%;\n    width: 5%;\n}\n#boxIndicatorProgress[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #fffb;\n    border: 1px solid #3338;\n    border-radius: calc(var(--frameHeight) * 0.01);\n    box-shadow: calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0) calc(var(--frameHeight) * 0.03) #fff;\n    height: calc(var(--frameHeight) * 0.48);\n    left: 93.1%;\n    top: 51.8%;\n    width: calc(var(--frameHeight) * 0.112);\n}\n#wordProgress[_ngcontent-%COMP%] {\n    left: 59%;\n    top: 72%;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2VjdGlvbjEvc2VjdGlvbjEuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGtCQUFrQjtJQUNsQixzQ0FBK0Q7SUFDL0QsMEJBQTBCO0lBQzFCLFdBQVc7SUFDWCxXQUFXO0lBQ1gsT0FBTztBQUNYOztBQUVBO0lBQ0ksb0JBQW9CO0FBQ3hCO0FBQ0E7SUFDSSxlQUFlO0FBQ25COztBQUVBLDRCQUE0QjtBQUM1QjtJQUNJLHFFQUFxRTtBQUN6RTtBQUNBO0lBQ0ksY0FBYztJQUNkLFlBQVk7SUFDWixXQUFXO0FBQ2Y7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsb0RBQW9EO0lBQ3BELDRDQUE0QztJQUM1QyxjQUFjO0lBQ2QseUNBQXlDO0lBQ3pDLG1CQUFtQjtJQUNuQix3Q0FBd0M7SUFDeEMsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IscUNBQXFDO0lBQ3JDLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsdUJBQXVCO0FBQzNCO0FBQ0E7OztJQUdJLGFBQWE7QUFDakI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiwyQ0FBb0U7SUFDcEUsMEJBQTBCO0lBQzFCLFdBQVc7SUFDWCxZQUFZO0lBQ1osVUFBVTtJQUNWLFlBQVk7SUFDWixlQUFlO0lBQ2YsVUFBVTtBQUNkO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsaURBQTBFO0lBQzFFLDBCQUEwQjtJQUMxQixXQUFXO0lBQ1gsVUFBVTtJQUNWLFVBQVU7SUFDVixZQUFZO0lBQ1osZUFBZTtJQUNmLFdBQVc7QUFDZjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDJDQUFvRTtJQUNwRSwwQkFBMEI7SUFDMUIsV0FBVztJQUNYLFlBQVk7SUFDWixVQUFVO0lBQ1YsWUFBWTtJQUNaLGVBQWU7SUFDZixVQUFVO0lBQ1YsNERBQTREO0FBQ2hFO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsaURBQTBFO0lBQzFFLDBCQUEwQjtJQUMxQixXQUFXO0lBQ1gsVUFBVTtJQUNWLFVBQVU7SUFDVixZQUFZO0lBQ1osZUFBZTtJQUNmLFdBQVc7QUFDZjs7QUFFQSw0QkFBNEI7QUFDNUI7SUFDSSxjQUFjO0lBQ2QsWUFBWTtJQUNaLFdBQVc7QUFDZjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixvREFBb0Q7SUFDcEQsNENBQTRDO0lBQzVDLGNBQWM7SUFDZCx5Q0FBeUM7SUFDekMsbUJBQW1CO0lBQ25CLHdDQUF3QztJQUN4QyxTQUFTO0lBQ1Qsa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixxQ0FBcUM7SUFDckMsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix1QkFBdUI7QUFDM0I7QUFDQTs7O0lBR0ksYUFBYTtBQUNqQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDJDQUFvRTtJQUNwRSwwQkFBMEI7SUFDMUIsV0FBVztJQUNYLFlBQVk7SUFDWixVQUFVO0lBQ1YsWUFBWTtJQUNaLGVBQWU7SUFDZixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixpREFBMEU7SUFDMUUsMEJBQTBCO0lBQzFCLFdBQVc7SUFDWCxXQUFXO0lBQ1gsV0FBVztJQUNYLFlBQVk7SUFDWixlQUFlO0lBQ2YsV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsMkNBQW9FO0lBQ3BFLDBCQUEwQjtJQUMxQixXQUFXO0lBQ1gsWUFBWTtJQUNaLFVBQVU7SUFDVixZQUFZO0lBQ1osZUFBZTtJQUNmLFVBQVU7QUFDZDtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGlEQUEwRTtJQUMxRSwwQkFBMEI7SUFDMUIsV0FBVztJQUNYLFdBQVc7SUFDWCxXQUFXO0lBQ1gsWUFBWTtJQUNaLGVBQWU7SUFDZixXQUFXO0FBQ2Y7O0FBRUEsNEJBQTRCO0FBQzVCO0lBQ0ksY0FBYztJQUNkLFlBQVk7SUFDWixXQUFXO0FBQ2Y7QUFDQTtJQUNJOzs7Ozs7Ozs4QkFRMEI7SUFDMUIsa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixvREFBb0Q7SUFDcEQsNENBQTRDO0lBQzVDLGNBQWM7SUFDZCx5Q0FBeUM7SUFDekMsbUJBQW1CO0lBQ25CLHFDQUFxQztJQUNyQyxRQUFRO0lBQ1Isa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixxQ0FBcUM7SUFDckMsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix1QkFBdUI7QUFDM0I7QUFDQTs7O0lBR0ksYUFBYTtBQUNqQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDJDQUFvRTtJQUNwRSwwQkFBMEI7SUFDMUIsV0FBVztJQUNYLFlBQVk7SUFDWixTQUFTO0lBQ1QsYUFBYTtJQUNiLGVBQWU7SUFDZixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixpREFBMEU7SUFDMUUsMEJBQTBCO0lBQzFCLFdBQVc7SUFDWCxZQUFZO0lBQ1osU0FBUztJQUNULGFBQWE7SUFDYixlQUFlO0lBQ2YsV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsMkNBQW9FO0lBQ3BFLDBCQUEwQjtJQUMxQixXQUFXO0lBQ1gsWUFBWTtJQUNaLFNBQVM7SUFDVCxhQUFhO0lBQ2IsZUFBZTtJQUNmLFVBQVU7QUFDZDtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGlEQUEwRTtJQUMxRSwwQkFBMEI7SUFDMUIsV0FBVztJQUNYLFlBQVk7SUFDWixTQUFTO0lBQ1QsYUFBYTtJQUNiLGVBQWU7SUFDZixXQUFXO0FBQ2Y7O0FBRUEsNEJBQTRCO0FBQzVCO0lBQ0ksY0FBYztJQUNkLFlBQVk7SUFDWixXQUFXO0FBQ2Y7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsb0RBQW9EO0lBQ3BELDRDQUE0QztJQUM1QyxjQUFjO0lBQ2QseUNBQXlDO0lBQ3pDLG1CQUFtQjtJQUNuQix3Q0FBd0M7SUFDeEMsU0FBUztJQUNULGtCQUFrQjtJQUNsQixPQUFPO0lBQ1AscUNBQXFDO0lBQ3JDLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsdUJBQXVCO0FBQzNCO0FBQ0E7OztJQUdJLGFBQWE7QUFDakI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiwyQ0FBb0U7SUFDcEUsMEJBQTBCO0lBQzFCLFdBQVc7SUFDWCxZQUFZO0lBQ1osVUFBVTtJQUNWLFlBQVk7SUFDWixlQUFlO0lBQ2YsVUFBVTtBQUNkO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsaURBQTBFO0lBQzFFLDBCQUEwQjtJQUMxQixXQUFXO0lBQ1gsWUFBWTtJQUNaLFVBQVU7SUFDVixZQUFZO0lBQ1osZUFBZTtJQUNmLFdBQVc7QUFDZjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDJDQUFvRTtJQUNwRSwwQkFBMEI7SUFDMUIsYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVO0lBQ1YsWUFBWTtJQUNaLGVBQWU7SUFDZixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixpREFBMEU7SUFDMUUsMEJBQTBCO0lBQzFCLFdBQVc7SUFDWCxZQUFZO0lBQ1osVUFBVTtJQUNWLFlBQVk7SUFDWixlQUFlO0lBQ2YsV0FBVztBQUNmOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLHlDQUFrRTtJQUNsRSx3QkFBd0I7SUFDeEIsVUFBVTtJQUNWLFdBQVc7SUFDWCxVQUFVO0lBQ1YsV0FBVztBQUNmOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLHNDQUErRDtJQUMvRCwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLFdBQVc7SUFDWCxPQUFPO0lBQ1AsWUFBWTtBQUNoQjs7O0FBR0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VBbUNFOzs7Ozs7QUFNRiw4Q0FBOEM7QUFDOUM7SUFDSSxrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QixtREFBbUQ7SUFDbkQsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixhQUFhO0lBQ2IseUNBQXlDO0lBQ3pDLHFDQUFxQztJQUNyQyx1QkFBdUI7SUFDdkIsaUJBQWlCO0lBQ2pCLFNBQVM7SUFDVCxvQ0FBb0M7SUFDcEMsVUFBVTtJQUNWLHlCQUF5QjtBQUM3Qjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQix5QkFBeUI7QUFDN0I7OztBQUdBLDZCQUE2QjtBQUM3QjtJQUNJLGtCQUFrQjtJQUNsQiwwQkFBMEI7SUFDMUIsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsb0NBQW9DO0lBQ3BDLDhDQUE4QztJQUM5Qzs2Q0FDeUM7SUFDekMsWUFBWTtJQUNaLHVDQUF1QztJQUN2QywwQ0FBMEM7SUFDMUMsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixxQ0FBcUM7SUFDckMsc0NBQXNDO0lBQ3RDLFdBQVc7SUFDWCw4R0FBOEc7SUFDOUcsdURBQXVEO0lBQ3ZELHdEQUF3RDtJQUN4RCx5REFBeUQ7SUFDekQsMERBQTBEO0lBQzFELGtCQUFrQjtJQUNsQixVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixhQUFhO0lBQ2IsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixPQUFPO0lBQ1AsTUFBTTtJQUNOLFlBQVk7SUFDWixXQUFXO0FBQ2Y7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQjtpQkFDYTtJQUNiLDBCQUEwQjtJQUMxQixhQUFhO0lBQ2IsUUFBUTtJQUNSLFFBQVE7SUFDUixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLDhDQUE4QztJQUM5QywwR0FBMEc7SUFDMUcsdUNBQXVDO0lBQ3ZDLFFBQVE7SUFDUixVQUFVO0lBQ1YsdUNBQXVDO0FBQzNDO0FBQ0E7SUFDSSxTQUFTO0lBQ1QsUUFBUTtBQUNaOztBQUVBO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsT0FBTztJQUNQLE1BQU07SUFDTixZQUFZO0lBQ1osV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEI7aUJBQ2E7SUFDYiwwQkFBMEI7SUFDMUIsYUFBYTtJQUNiLFFBQVE7SUFDUixVQUFVO0lBQ1YsVUFBVTtBQUNkO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsdUJBQXVCO0lBQ3ZCLHVCQUF1QjtJQUN2Qiw4Q0FBOEM7SUFDOUMsMEdBQTBHO0lBQzFHLHVDQUF1QztJQUN2QyxRQUFRO0lBQ1IsVUFBVTtJQUNWLHVDQUF1QztBQUMzQztBQUNBO0lBQ0ksU0FBUztJQUNULFFBQVE7QUFDWjs7QUFFQTtJQUNJLHFDQUE4RDtJQUM5RCwwQkFBMEI7SUFDMUIsUUFBUTtBQUNaOztBQUVBO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsT0FBTztJQUNQLE1BQU07SUFDTixZQUFZO0lBQ1osV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEI7aUJBQ2E7SUFDYiwwQkFBMEI7SUFDMUIsYUFBYTtJQUNiLFFBQVE7SUFDUixVQUFVO0lBQ1YsVUFBVTtBQUNkO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsdUJBQXVCO0lBQ3ZCLHVCQUF1QjtJQUN2Qiw4Q0FBOEM7SUFDOUMsMEdBQTBHO0lBQzFHLHVDQUF1QztJQUN2QyxRQUFRO0lBQ1IsVUFBVTtJQUNWLHVDQUF1QztBQUMzQztBQUNBO0lBQ0ksU0FBUztJQUNULFFBQVE7QUFDWjtBQUNBO0lBQ0ksMENBQW1FO0lBQ25FLDBCQUEwQjtJQUMxQixRQUFRO0FBQ1o7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixhQUFhO0lBQ2IsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixPQUFPO0lBQ1AsTUFBTTtJQUNOLFlBQVk7SUFDWixXQUFXO0FBQ2Y7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQjtpQkFDYTtJQUNiLDBCQUEwQjtJQUMxQixhQUFhO0lBQ2IsUUFBUTtJQUNSLFVBQVU7SUFDVixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLDhDQUE4QztJQUM5QywwR0FBMEc7SUFDMUcsdUNBQXVDO0lBQ3ZDLFFBQVE7SUFDUixVQUFVO0lBQ1YsdUNBQXVDO0FBQzNDO0FBQ0E7SUFDSSxTQUFTO0lBQ1QsVUFBVTtBQUNkO0FBQ0E7SUFDSSxzQ0FBK0Q7SUFDL0QsMEJBQTBCO0lBQzFCLFFBQVE7QUFDWjs7QUFFQTtJQUNJLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsdUJBQXVCO0lBQ3ZCLGFBQWE7SUFDYixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLE9BQU87SUFDUCxNQUFNO0lBQ04sWUFBWTtJQUNaLFdBQVc7QUFDZjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCO2lCQUNhO0lBQ2IsMEJBQTBCO0lBQzFCLGFBQWE7SUFDYixRQUFRO0lBQ1IsVUFBVTtJQUNWLFVBQVU7QUFDZDtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsOENBQThDO0lBQzlDLDBHQUEwRztJQUMxRyx1Q0FBdUM7SUFDdkMsUUFBUTtJQUNSLFVBQVU7SUFDVix1Q0FBdUM7QUFDM0M7QUFDQTtJQUNJLFNBQVM7SUFDVCxRQUFRO0FBQ1o7QUFDQTtJQUNJLHdDQUFpRTtJQUNqRSwwQkFBMEI7SUFDMUIsUUFBUTtBQUNaOztBQUVBO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsT0FBTztJQUNQLE1BQU07SUFDTixZQUFZO0lBQ1osV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEI7YUFDUztJQUNULDBCQUEwQjtJQUMxQixhQUFhO0lBQ2IsU0FBUztJQUNULFFBQVE7SUFDUix3QkFBd0I7SUFDeEIsU0FBUztBQUNiO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsdUJBQXVCO0lBQ3ZCLHVCQUF1QjtJQUN2Qiw4Q0FBOEM7SUFDOUMsMEdBQTBHO0lBQzFHLHdDQUF3QztJQUN4QyxTQUFTO0lBQ1QsU0FBUztJQUNULHFDQUFxQztBQUN6QztBQUNBO0lBQ0ksU0FBUztJQUNULFFBQVE7QUFDWjs7QUFFQTtJQUNJLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsdUJBQXVCO0lBQ3ZCLGFBQWE7SUFDYixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLE9BQU87SUFDUCxNQUFNO0lBQ04sWUFBWTtJQUNaLFdBQVc7QUFDZjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCO2FBQ1M7SUFDVCwwQkFBMEI7SUFDMUIsYUFBYTtJQUNiLFdBQVc7SUFDWCxRQUFRO0lBQ1Isd0JBQXdCO0lBQ3hCLFNBQVM7QUFDYjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsOENBQThDO0lBQzlDLDBHQUEwRztJQUMxRyx3Q0FBd0M7SUFDeEMsU0FBUztJQUNULFNBQVM7SUFDVCxxQ0FBcUM7QUFDekM7QUFDQTtJQUNJLFNBQVM7SUFDVCxRQUFRO0FBQ1o7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixhQUFhO0lBQ2IsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixPQUFPO0lBQ1AsTUFBTTtJQUNOLFlBQVk7SUFDWixXQUFXO0FBQ2Y7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQjthQUNTO0lBQ1QsMEJBQTBCO0lBQzFCLGFBQWE7SUFDYixXQUFXO0lBQ1gsUUFBUTtJQUNSLHdCQUF3QjtJQUN4QixTQUFTO0FBQ2I7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLDhDQUE4QztJQUM5QywwR0FBMEc7SUFDMUcsd0NBQXdDO0lBQ3hDLFNBQVM7SUFDVCxTQUFTO0lBQ1QscUNBQXFDO0FBQ3pDO0FBQ0E7SUFDSSxTQUFTO0lBQ1QsUUFBUTtBQUNaOztBQUVBO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsT0FBTztJQUNQLE1BQU07SUFDTixZQUFZO0lBQ1osV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEI7YUFDUztJQUNULDBCQUEwQjtJQUMxQixhQUFhO0lBQ2IsV0FBVztJQUNYLFFBQVE7SUFDUixTQUFTO0FBQ2I7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLDhDQUE4QztJQUM5QywwR0FBMEc7SUFDMUcsdUNBQXVDO0lBQ3ZDLFdBQVc7SUFDWCxVQUFVO0lBQ1YsdUNBQXVDO0FBQzNDO0FBQ0E7SUFDSSxTQUFTO0lBQ1QsUUFBUTtBQUNaIiwic291cmNlc0NvbnRlbnQiOlsiLmZvbmRvIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaXR5L2ZvbmRvLnBuZykgbm8tcmVwZWF0OyAgXG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OjEwMCU7ICBcbiAgICB0b3A6IDAlO1xufVxuXG4uZGlzYWJsZWQsIC5pbmFjdGl2ZSB7XG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG59XG4uYWN0aXZlIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi8qKioqKioqICAgTGVzc29uMiAgICoqKioqKiovXG4uY2l0eV9idG4ye1xuICAgIGNsaXAtcGF0aDogcG9seWdvbigwIDAsIDEwMCUgMCwgMTAwJSAxMDAlLCA0NyUgMTAwJSwgNDclIDM4JSwgMCUgNDAlKTs7XG59XG4uY2l0eV9idG4yIGF7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmNpdHlfYnRuMiAubW9kdWxlXzJfaW5mbyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkNGUzZTM7XG4gICAgYm9yZGVyOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgc29saWQgIzNkM2QzZDtcbiAgICBib3JkZXItcmFkaXVzOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxKTtcbiAgICBjb2xvcjogIzE5ODE5NTtcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDM1KTtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wODEwNCk7XG4gICAgbGVmdDogNTMuNSU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHRvcDogMzglO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjIxNik7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLmNpdHlfYnRuMi5yaWdodDpob3ZlciAubW9kdWxlXzJfaW5mbyxcbi5jaXR5X2J0bjIud3Jvbmc6aG92ZXIgLm1vZHVsZV8yX2luZm9cbntcbiAgICBkaXNwbGF5OiBub25lO1xufVxuLmNpdHlfYnRuMi5yaWdodCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvY2l0eS9idG4yX3JpZ2h0LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogMjclO1xuICAgIGxlZnQ6IC0xMC40JTtcbiAgICB0b3A6IDQyLjUlO1xuICAgIHdpZHRoOiA0My41JTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgei1pbmRleDogMztcbn1cbi5jaXR5X2J0bjIucmlnaHQ6aG92ZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2NpdHkvYnRuMl9yaWdodF9ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDI5JTtcbiAgICBsZWZ0OiAtMTIlO1xuICAgIHRvcDogNDAuNSU7XG4gICAgd2lkdGg6IDQ1LjUlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB6LWluZGV4OiAxMDtcbn1cbi5jaXR5X2J0bjIud3Jvbmcge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2NpdHkvYnRuMl93cm9uZy5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDI3JTtcbiAgICBsZWZ0OiAtMTAuNCU7XG4gICAgdG9wOiA0Mi41JTtcbiAgICB3aWR0aDogNDMuNSU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHotaW5kZXg6IDM7XG4gICAgLyogY2xpcC1wYXRoOiBwb2x5Z29uKC02JSAwJSwgMTA0JSAwJSwgNTclIDEwMCUsIDQwJSAxMDAlKSAqL1xufVxuLmNpdHlfYnRuMi53cm9uZzpob3ZlciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvY2l0eS9idG4yX3dyb25nX2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogMjklO1xuICAgIGxlZnQ6IC0xMiU7XG4gICAgdG9wOiA0MC41JTtcbiAgICB3aWR0aDogNDUuNSU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHotaW5kZXg6IDEwO1xufVxuXG4vKioqKioqKiAgIExlc3NvbjMgICAqKioqKioqL1xuLmNpdHlfYnRuMyBhe1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5jaXR5X2J0bjMgLm1vZHVsZV8zX2luZm8ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDRlM2UzO1xuICAgIGJvcmRlcjogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDgpIHNvbGlkICMzZDNkM2Q7XG4gICAgYm9yZGVyLXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMSk7XG4gICAgY29sb3I6ICMxOTgxOTU7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAzNSk7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDgxMDQpO1xuICAgIGxlZnQ6IDI0JTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgdG9wOiA1MCU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMjE2KTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uY2l0eV9idG4zLnJpZ2h0OmhvdmVyIC5tb2R1bGVfM19pbmZvLFxuLmNpdHlfYnRuMy53cm9uZzpob3ZlciAubW9kdWxlXzNfaW5mb1xue1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG4uY2l0eV9idG4zLnJpZ2h0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaXR5L2J0bjNfcmlnaHQucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiA0NyU7XG4gICAgbGVmdDogMjguNTIlO1xuICAgIHRvcDogMjMuOCU7XG4gICAgd2lkdGg6IDIyLjElO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB6LWluZGV4OiA1O1xufVxuLmNpdHlfYnRuMy5yaWdodDpob3ZlciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvY2l0eS9idG4zX3JpZ2h0X2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogNDklO1xuICAgIGxlZnQ6IDI3LjglO1xuICAgIHRvcDogMjEuOTUlO1xuICAgIHdpZHRoOiAyMy4xJTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgei1pbmRleDogMTA7XG59XG4uY2l0eV9idG4zLndyb25nIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaXR5L2J0bjNfd3JvbmcucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiA0NyU7XG4gICAgbGVmdDogMjguNTIlO1xuICAgIHRvcDogMjMuOCU7XG4gICAgd2lkdGg6IDIyLjElO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB6LWluZGV4OiA1O1xufVxuLmNpdHlfYnRuMy53cm9uZzpob3ZlciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvY2l0eS9idG4zX3dyb25nX2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogNDklO1xuICAgIGxlZnQ6IDI3LjglO1xuICAgIHRvcDogMjEuOTUlO1xuICAgIHdpZHRoOiAyMy4xJTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgei1pbmRleDogMTA7XG59XG5cbi8qKioqKioqICAgTGVzc29uMSAgICoqKioqKiovXG4uY2l0eV9idG4xIGF7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmNpdHlfYnRuMSAubW9kdWxlXzFfaW5mbyB7XG4gICAgLyogcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvcmRlcjogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTUpIHNvbGlkICMzZDNkM2Q7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMik7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDIpO1xuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyKTtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDIpO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlOyAqL1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDRlM2UzO1xuICAgIGJvcmRlcjogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDgpIHNvbGlkICMzZDNkM2Q7XG4gICAgYm9yZGVyLXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMSk7XG4gICAgY29sb3I6ICMxOTgxOTU7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAzNSk7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDgpO1xuICAgIGxlZnQ6IDklO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB0b3A6IDY2JTtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4yMTYpO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5jaXR5X2J0bjEucmlnaHQ6aG92ZXIgLm1vZHVsZV8xX2luZm8sXG4uY2l0eV9idG4xLndyb25nOmhvdmVyIC5tb2R1bGVfMV9pbmZvXG57XG4gICAgZGlzcGxheTogbm9uZTtcbn1cbi5jaXR5X2J0bjEucmlnaHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2NpdHkvYnRuMV9yaWdodC5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDYyJTtcbiAgICBsZWZ0OiA0Ny42MiU7XG4gICAgdG9wOiA3LjUlO1xuICAgIHdpZHRoOiAxMS4zNSU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHotaW5kZXg6IDQ7XG59XG4uY2l0eV9idG4xLnJpZ2h0OmhvdmVyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaXR5L2J0bjFfcmlnaHRfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiA2NCU7XG4gICAgbGVmdDogNDYuOTIlO1xuICAgIHRvcDogNS41JTtcbiAgICB3aWR0aDogMTIuMzUlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB6LWluZGV4OiAxMDtcbn1cbi5jaXR5X2J0bjEud3Jvbmcge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2NpdHkvYnRuMV93cm9uZy5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDYyJTtcbiAgICBsZWZ0OiA0Ny42MiU7XG4gICAgdG9wOiA3LjUlO1xuICAgIHdpZHRoOiAxMS4zNSU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHotaW5kZXg6IDQ7XG59XG4uY2l0eV9idG4xLndyb25nOmhvdmVyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaXR5L2J0bjFfd3JvbmdfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiA2NCU7XG4gICAgbGVmdDogNDYuOTIlO1xuICAgIHRvcDogNS41JTtcbiAgICB3aWR0aDogMTIuMzUlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB6LWluZGV4OiAxMDtcbn1cblxuLyoqKioqKiogICBMZXNzb240ICAgKioqKioqKi9cbi5jaXR5X2J0bjQgYXtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4uY2l0eV9idG40IC5tb2R1bGVfNF9pbmZvIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Q0ZTNlMztcbiAgICBib3JkZXI6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDA4KSBzb2xpZCAjM2QzZDNkO1xuICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDEpO1xuICAgIGNvbG9yOiAjMTk4MTk1O1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMzUpO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA4MTA0KTtcbiAgICBsZWZ0OiAxNiU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHRvcDogMCU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMjE2KTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uY2l0eV9idG40LnJpZ2h0OmhvdmVyIC5tb2R1bGVfNF9pbmZvLFxuLmNpdHlfYnRuNC53cm9uZzpob3ZlciAubW9kdWxlXzRfaW5mb1xue1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG4uY2l0eV9idG40LnJpZ2h0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaXR5L2J0bjRfcmlnaHQucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiAzNCU7XG4gICAgbGVmdDogNTQuMzUlO1xuICAgIHRvcDogMzUuNSU7XG4gICAgd2lkdGg6IDYxLjYlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB6LWluZGV4OiAzO1xufVxuLmNpdHlfYnRuNC5yaWdodDpob3ZlciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvY2l0eS9idG40X3JpZ2h0X2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogMzYlO1xuICAgIGxlZnQ6IDU0LjM1JTtcbiAgICB0b3A6IDMzLjUlO1xuICAgIHdpZHRoOiA2Mi42JTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgei1pbmRleDogMTA7XG59XG4uY2l0eV9idG40Lndyb25nIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaXR5L2J0bjRfd3JvbmcucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiAzMy43JTtcbiAgICBsZWZ0OiA1NC4zNSU7XG4gICAgdG9wOiAzNS44JTtcbiAgICB3aWR0aDogNjEuNiU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHotaW5kZXg6IDM7XG59XG4uY2l0eV9idG40Lndyb25nOmhvdmVyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaXR5L2J0bjRfd3JvbmdfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiAzNiU7XG4gICAgbGVmdDogNTQuMzUlO1xuICAgIHRvcDogMzMuNSU7XG4gICAgd2lkdGg6IDYyLjYlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB6LWluZGV4OiAxMDtcbn1cblxuLnBpcGVzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaXR5L3R1YmVyaWFzLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogOTAlIDcwJTtcbiAgICB3aWR0aDogODclO1xuICAgIGhlaWdodDogNDUlO1xuICAgIHRvcDogNjkuMiU7XG4gICAgbGVmdDogMTEuNSU7XG59XG5cbi50aXRsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvY2l0eS90aXRsZS5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB3aWR0aDogMjMuNSU7XG4gICAgaGVpZ2h0OiAyOCU7XG4gICAgdG9wOiAxJTtcbiAgICByaWdodDogLTAuNyU7XG59XG5cblxuLyogLnBvc3RfbWVudSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9wb3N0LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHdpZHRoOiA3JTtcbiAgICBoZWlnaHQ6IDQ3JTtcbiAgICBib3R0b206IC0wLjUlO1xuICAgICpiYWNrZ3JvdW5kLWNvbG9yOiAjOTAwODtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHotaW5kZXg6IDEwO1xufVxuXG4ucG9zdF9tZW51IC5idXR0b257XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiA0MyU7XG4gICAgaGVpZ2h0OiAxNC4zJTtcbiAgICBsZWZ0OiA0NCU7XG4gICAgYm94LXNoYWRvdzogMCBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMCkgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDIpICMzYjNjM2Q7XG4gICAgYm9yZGVyLXRvcDpjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItbGVmdDpjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItcmlnaHQ6Y2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDgpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLWJvdHRvbTpjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHotaW5kZXg6IDE7XG59XG5cbi5wb3N0X21lbnUgLmJ1dHRvbjphY3RpdmUge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWShjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkpO1xuICAgIGJvcmRlci10b3A6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDA4KSBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1sZWZ0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItcmlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDA4KSBzb2xpZCAjM2IzYzNkO1xuICAgIGJvcmRlci1ib3R0b206IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDA4KSBzb2xpZCAjM2IzYzNkO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG59Ki9cblxuXG5cblxuXG4vKioqKioqKiAgIENhamEgQ2FtYmlhciBWaXN0YSBDaXVkYWQgICAqKioqKioqL1xuLmJ0blN3aXRjaCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE5OTVBRDtcbiAgICBib3JkZXI6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDEpIHNvbGlkICMzQjNDM0Q7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGJvdHRvbTogMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDI4KTtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMTMpO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGxpbmUtaGVpZ2h0OiAxMDAlO1xuICAgIHJpZ2h0OiA4JTtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4xMyk7XG4gICAgei1pbmRleDogODtcbiAgICB0cmFuc2l0aW9uOiAwLjVzIGFsbCBlYXNlO1xufVxuXG4uYnRuU3dpdGNoOmhvdmVyIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEuMik7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE1NjI3MDtcbn1cblxuXG4vKioqKioqKiAgIFRvb2xUaXBzICAgKioqKioqKi9cbi5jZW50ZXJEaXYge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IHZhcigtLWZyYW1lSGVpZ2h0KTtcbiAgICB3aWR0aDogdmFyKC0tZnJhbWVXaWR0aCk7XG59XG4udG9vbHRpcFdvcmRzIHtcbiAgICBwb3NpdGlvbjogaW5oZXJpdDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuOCk7XG4gICAgYm9yZGVyLXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjAyKTtcbiAgICBib3gtc2hhZG93OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDApIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMClcbiAgICAgICAgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjAyNSkgI2ZmZjtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LXJlZ3VsYXIpO1xuICAgIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjAzKTtcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICAgIHBhZGRpbmc6IDAuNyU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnRvb2x0aXBCdXR0b25OYXZ7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA2NSk7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA2NSk7XG4gICAgbGVmdDogMy4xNSU7XG4gICAgYm94LXNoYWRvdzogMCBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMCkgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDIpICMzYjNjM2Q7XG4gICAgYm9yZGVyLXRvcDpjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItbGVmdDpjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItcmlnaHQ6Y2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDgpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLWJvdHRvbTpjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwOCkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgei1pbmRleDogMTtcbn1cblxuI3Rvb2x0aXBOYXZpZ2F0b3JCYXIge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDBhO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgaGVpZ2h0OiAxMDB2aDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwdnc7XG4gICAgei1pbmRleDogOTk7XG59XG4jcm93TmF2aWdhdG9yQmFyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9nZW5lcmFsX3ZpZXdfbGVzc29ucy90b29sVGlwSW5kaWNhdG9yLnBuZylcbiAgICAgICAgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogMi4yNyU7XG4gICAgbGVmdDogOCU7XG4gICAgdG9wOiA3NiU7XG4gICAgd2lkdGg6IDEyJTtcbn1cbiNib3hJbmRpY2F0b3JOYXZpZ2F0b3JCYXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmYztcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMzMzODtcbiAgICBib3JkZXItcmFkaXVzOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDAuMDEpO1xuICAgIGJveC1zaGFkb3c6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMCkgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwKSBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDAuMDMpICNmZmY7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDAuNDcpO1xuICAgIGxlZnQ6IDIlO1xuICAgIHRvcDogNTIuNSU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC4xMTcpO1xufVxuI3dvcmROYXZpZ2F0b3JCYXIge1xuICAgIGxlZnQ6IDIxJTtcbiAgICB0b3A6IDcyJTtcbn1cblxuI3Rvb2x0aXBNZW51IHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwYTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogMTAwdmg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMHZ3O1xuICAgIHotaW5kZXg6IDk5O1xufVxuI3Jvd01lbnUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL3Rvb2xUaXBJbmRpY2F0b3IucG5nKVxuICAgICAgICBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiAyLjI3JTtcbiAgICBsZWZ0OiA4JTtcbiAgICB0b3A6IDYwLjUlO1xuICAgIHdpZHRoOiAxMiU7XG59XG4jYm94SW5kaWNhdG9yTWVudSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZjO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMzM4O1xuICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC4wMSk7XG4gICAgYm94LXNoYWRvdzogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwKSBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDApIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC4wMykgI2ZmZjtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC40Nyk7XG4gICAgbGVmdDogMiU7XG4gICAgdG9wOiA1Mi41JTtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjExNyk7XG59XG4jd29yZE1lbnUge1xuICAgIGxlZnQ6IDIxJTtcbiAgICB0b3A6IDU4JTtcbn1cblxuLnRvb2x0aXBCdXR0b25OYXYuYnV0dG9uX21lbnUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9tZW51LnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogNTglO1xufVxuXG4jdG9vbHRpcE9iamVjdGl2ZXMge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDBhO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgaGVpZ2h0OiAxMDB2aDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwdnc7XG4gICAgei1pbmRleDogOTk7XG59XG4jcm93T2JqZWN0aXZlcyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvZ2VuZXJhbF92aWV3X2xlc3NvbnMvdG9vbFRpcEluZGljYXRvci5wbmcpXG4gICAgICAgIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDIuMjclO1xuICAgIGxlZnQ6IDglO1xuICAgIHRvcDogNzAuNSU7XG4gICAgd2lkdGg6IDEyJTtcbn1cbiNib3hJbmRpY2F0b3JPYmplY3RpdmVzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmM7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzMzMzg7XG4gICAgYm9yZGVyLXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjAxKTtcbiAgICBib3gtc2hhZG93OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDApIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMCkgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjAzKSAjZmZmO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjQ3KTtcbiAgICBsZWZ0OiAyJTtcbiAgICB0b3A6IDUyLjUlO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDAuMTE3KTtcbn1cbiN3b3JkT2JqZWN0aXZlcyB7XG4gICAgbGVmdDogMjElO1xuICAgIHRvcDogNjglO1xufVxuLnRvb2x0aXBCdXR0b25OYXYuYnV0dG9uX29iamVjdGl2ZXMge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9vYmpldGl2ZXMucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdG9wOiA2OCU7XG59XG5cbiN0b29sdGlwU291bmQge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDBhO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgaGVpZ2h0OiAxMDB2aDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwdnc7XG4gICAgei1pbmRleDogOTk7XG59XG4jcm93U291bmQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL3Rvb2xUaXBJbmRpY2F0b3IucG5nKVxuICAgICAgICBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiAyLjI3JTtcbiAgICBsZWZ0OiA4JTtcbiAgICB0b3A6IDgwLjUlO1xuICAgIHdpZHRoOiAxMiU7XG59XG4jYm94SW5kaWNhdG9yU291bmQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmYztcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMzMzODtcbiAgICBib3JkZXItcmFkaXVzOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDAuMDEpO1xuICAgIGJveC1zaGFkb3c6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMCkgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwKSBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDAuMDMpICNmZmY7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDAuNDcpO1xuICAgIGxlZnQ6IDIlO1xuICAgIHRvcDogNTIuNSU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC4xMTcpO1xufVxuI3dvcmRTb3VuZCB7XG4gICAgbGVmdDogMjElO1xuICAgIHRvcDogNzguNSU7XG59XG4udG9vbHRpcEJ1dHRvbk5hdi5idXR0b25fc291bmQge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9zb3VuZC5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB0b3A6IDc4JTtcbn1cblxuI3Rvb2x0aXBQcm9maWxlIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwYTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogMTAwdmg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMHZ3O1xuICAgIHotaW5kZXg6IDk5O1xufVxuI3Jvd1Byb2ZpbGUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL3Rvb2xUaXBJbmRpY2F0b3IucG5nKVxuICAgICAgICBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiAyLjI3JTtcbiAgICBsZWZ0OiA4JTtcbiAgICB0b3A6IDkwLjUlO1xuICAgIHdpZHRoOiAxMiU7XG59XG4jYm94SW5kaWNhdG9yUHJvZmlsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZjO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMzM4O1xuICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC4wMSk7XG4gICAgYm94LXNoYWRvdzogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwKSBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDApIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC4wMykgI2ZmZjtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC40Nyk7XG4gICAgbGVmdDogMiU7XG4gICAgdG9wOiA1Mi41JTtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjExNyk7XG59XG4jd29yZFByb2ZpbGUge1xuICAgIGxlZnQ6IDIxJTtcbiAgICB0b3A6IDg4JTtcbn1cbi50b29sdGlwQnV0dG9uTmF2LmJ1dHRvbl9wcm9maWxlIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvcHJvZmlsZS5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB0b3A6IDg4JTtcbn1cblxuI3Rvb2x0aXBNb2R1bGVzQmFyIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwYTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogMTAwdmg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMHZ3O1xuICAgIHotaW5kZXg6IDk5O1xufVxuI3Jvd01vZHVsZXNCYXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL3Rvb2xUaXBJbmRpY2F0b3IucG5nKVxuICAgIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDQuMjclO1xuICAgIGxlZnQ6IDQ3JTtcbiAgICB0b3A6IDc3JTtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gICAgd2lkdGg6IDclO1xufVxuI2JveEluZGljYXRvck1vZHVsZXNCYXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmYjtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMzMzODtcbiAgICBib3JkZXItcmFkaXVzOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDAuMDEpO1xuICAgIGJveC1zaGFkb3c6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMCkgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwKSBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDAuMDMpICNmZmY7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDAuNjQ3KTtcbiAgICBsZWZ0OiAxMiU7XG4gICAgdG9wOiA1LjUlO1xuICAgIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDEuNyk7XG59XG4jd29yZE1vZHVsZXNCYXIge1xuICAgIGxlZnQ6IDQwJTtcbiAgICB0b3A6IDg5JTtcbn1cblxuI3Rvb2x0aXBNb2R1bGUxIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwYTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogMTAwdmg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMHZ3O1xuICAgIHotaW5kZXg6IDk5O1xufVxuI3Jvd01vZHVsZTEge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL3Rvb2xUaXBJbmRpY2F0b3IucG5nKVxuICAgIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDMuMjclO1xuICAgIGxlZnQ6IDUwLjUlO1xuICAgIHRvcDogNzUlO1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgICB3aWR0aDogNSU7XG59XG4jYm94SW5kaWNhdG9yTW9kdWxlMSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZiO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMzM4O1xuICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC4wMSk7XG4gICAgYm94LXNoYWRvdzogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwKSBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDApIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC4wMykgI2ZmZjtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC42NDcpO1xuICAgIGxlZnQ6IDEyJTtcbiAgICB0b3A6IDUuNSU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMS43KTtcbn1cbiN3b3JkTW9kdWxlMSB7XG4gICAgbGVmdDogNDIlO1xuICAgIHRvcDogODUlO1xufVxuXG4jdG9vbHRpcE1vZHVsZTIge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDBhO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgaGVpZ2h0OiAxMDB2aDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwdnc7XG4gICAgei1pbmRleDogOTk7XG59XG4jcm93TW9kdWxlMiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvc2VjdGlvbjEvZ2VuZXJhbF92aWV3X2xlc3NvbnMvdG9vbFRpcEluZGljYXRvci5wbmcpXG4gICAgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogMy4yNyU7XG4gICAgbGVmdDogMjAuNSU7XG4gICAgdG9wOiA3NSU7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAgIHdpZHRoOiA1JTtcbn1cbiNib3hJbmRpY2F0b3JNb2R1bGUyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmI7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzMzMzg7XG4gICAgYm9yZGVyLXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjAxKTtcbiAgICBib3gtc2hhZG93OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDApIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMCkgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjAzKSAjZmZmO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjY0Nyk7XG4gICAgbGVmdDogMTIlO1xuICAgIHRvcDogNS41JTtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAxLjcpO1xufVxuI3dvcmRNb2R1bGUyIHtcbiAgICBsZWZ0OiAxMSU7XG4gICAgdG9wOiA4NSU7XG59XG5cbiN0b29sdGlwUHJvZ3Jlc3Mge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDBhO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgaGVpZ2h0OiAxMDB2aDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwdnc7XG4gICAgei1pbmRleDogOTk7XG59XG4jcm93UHJvZ3Jlc3Mge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL3Rvb2xUaXBJbmRpY2F0b3IucG5nKVxuICAgIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IDMuMjclO1xuICAgIGxlZnQ6IDg3LjUlO1xuICAgIHRvcDogNzUlO1xuICAgIHdpZHRoOiA1JTtcbn1cbiNib3hJbmRpY2F0b3JQcm9ncmVzcyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZiO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMzM4O1xuICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC4wMSk7XG4gICAgYm94LXNoYWRvdzogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwKSBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSAqIDApIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC4wMykgI2ZmZjtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpICogMC40OCk7XG4gICAgbGVmdDogOTMuMSU7XG4gICAgdG9wOiA1MS44JTtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkgKiAwLjExMik7XG59XG4jd29yZFByb2dyZXNzIHtcbiAgICBsZWZ0OiA1OSU7XG4gICAgdG9wOiA3MiU7XG59XG5cblxuIl0sInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 5830:
/*!*****************************************!*\
  !*** ./src/app/services/api.service.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ApiService": () => (/* binding */ ApiService)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 8987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 2560);



class ApiService {
  constructor(http) {
    this.http = http;
    this.apiUrl = 'http://localhost:8000';
    this.httpOptions = {
      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }
  listUser() {
    return this.http.get(this.apiUrl + '/user/getTokenByUser');
  }
}
ApiService.ɵfac = function ApiService_Factory(t) {
  return new (t || ApiService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpClient));
};
ApiService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
  token: ApiService,
  factory: ApiService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 6425:
/*!*******************************************!*\
  !*** ./src/app/services/audio.service.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AudioService": () => (/* binding */ AudioService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 228);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 2560);


class AudioService {
  constructor() {
    this.hoverAudio = new Audio(`../assets/section1/sounds/hoverDefault.mp3`);
    this.lightBulb1Audio = new Audio(`../assets/section1/sounds/lightBulb.mp3`);
    this.moveBtn = new Audio(`../assets/section1/sounds/SWISH_13.wav`);
    this.clickAudio = new Audio(`../assets/section1/sounds/clickDefault.mp3`);
    this.backgroundAudios = {
      audioSoundtrack: new Audio(`../../assets/section1/sounds/background_sound.mp3`)
    };
    this.variable = "varInicial";
    this.muted = false;
    this.current = [];
    this.currentBackground = "";
    this.setOnEndedBackgroundAudios();
    this.stateSoundMenu = 'active';
    this.stateSoundMenu$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__.Subject();
  }
  //Musica de fondo
  playBackgroundAudios() {
    this.backgroundAudios["audioSoundtrack"].volume = 0.1; //0.3;
    this.backgroundAudios["audioSoundtrack"].play();
  }
  pauseBackgroundAudios() {
    this.backgroundAudios["audioSoundtrack"].pause();
  }
  setStateSoundMenu(stateSoundMenu) {
    this.stateSoundMenu = stateSoundMenu;
    this.stateSoundMenu$.next(this.stateSoundMenu);
  }
  getStateSoundMenu() {
    return this.stateSoundMenu;
  }
  getStateSoundMenu$() {
    return this.stateSoundMenu$.asObservable();
  }
  setBackground(backgroundKey) {
    if (this.backgroundAudios[`${this.currentBackground}`]) {
      this.backgroundAudios[`${this.currentBackground}`].pause();
    }
    this.currentBackground = backgroundKey;
    if (this.backgroundAudios[`${this.currentBackground}`] && !this.muted) {
      this.backgroundAudios[`${this.currentBackground}`].volume = 0.3;
      this.backgroundAudios[`${this.currentBackground}`].play();
    }
  }
  setOnEndedBackgroundAudios() {
    for (const audio of Object.keys(this.backgroundAudios)) {
      this.backgroundAudios[`${audio}`].onended = () => {
        this.backgroundAudios[`${audio}`].play();
      };
    }
  }
}
AudioService.ɵfac = function AudioService_Factory(t) {
  return new (t || AudioService)();
};
AudioService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
  token: AudioService,
  factory: AudioService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 8458:
/*!**********************************************!*\
  !*** ./src/app/services/progress.service.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressService": () => (/* binding */ ProgressService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 228);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 2560);


class ProgressService {
  constructor() {
    this.showLayout = true;
    this.showLayout$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__.Subject();
    this.showTooltipsSection1 = true;
    this.showTooltipsSection1$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__.Subject();
    this.lesson1Available = 'active';
    this.lesson1Available$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__.Subject();
    this.lesson2Available = 'active';
    this.lesson2Available$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__.Subject();
    this.lesson3Available = 'active';
    this.lesson3Available$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__.Subject();
    this.lesson4Available = 'active';
    this.lesson4Available$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__.Subject();
    this.progressPercentage = 0;
    this.progressPercentage$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__.Subject();
  }
  /* Observables para showLayout */
  getShowLayout() {
    return this.showLayout;
  }
  setShowLayout(showLayout) {
    this.showLayout = showLayout;
    this.showLayout$.next(this.showLayout);
  }
  getShowLayout$() {
    return this.showLayout$.asObservable();
  }
  /* Observables para showTooltipsSection1 */
  getShowTooltipsSection1() {
    return this.showTooltipsSection1;
  }
  setShowTooltipsSection1(showTooltipsSection1) {
    this.showTooltipsSection1 = showTooltipsSection1;
    this.showTooltipsSection1$.next(this.showTooltipsSection1);
  }
  getShowTooltipsSection1$() {
    return this.showTooltipsSection1$.asObservable();
  }
  /* Observables para lesson1Available */
  getLesson1Available() {
    return this.lesson1Available;
  }
  setLesson1Available(lesson1Available) {
    this.lesson1Available = lesson1Available;
    this.lesson1Available$.next(this.lesson1Available);
  }
  getLesson1Available$() {
    return this.lesson1Available$.asObservable();
  }
  /* Observables para lesson2Available */
  getLesson2Available() {
    return this.lesson2Available;
  }
  setLesson2Available(lesson2Available) {
    this.lesson2Available = lesson2Available;
    this.lesson2Available$.next(this.lesson2Available);
  }
  getLesson2Available$() {
    return this.lesson2Available$.asObservable();
  }
  /* Observables para lesson3Available */
  getLesson3Available() {
    return this.lesson3Available;
  }
  setLesson3Available(lesson3Available) {
    this.lesson3Available = lesson3Available;
    this.lesson3Available$.next(this.lesson3Available);
  }
  getLesson3Available$() {
    return this.lesson3Available$.asObservable();
  }
  /* Observables para lesson4Available */
  getLesson4Available() {
    return this.lesson4Available;
  }
  setLesson4Available(lesson4Available) {
    this.lesson4Available = lesson4Available;
    this.lesson4Available$.next(this.lesson4Available);
  }
  getLesson4Available$() {
    return this.lesson4Available$.asObservable();
  }
  /* Observables para progress bar percentage */
  getProgressPercentage() {
    return this.progressPercentage;
  }
  setProgressPercentage(progressPercentage) {
    this.progressPercentage = progressPercentage;
    this.progressPercentage$.next(this.progressPercentage);
  }
  getProgressPercentage$() {
    return this.progressPercentage$.asObservable();
  }
}
ProgressService.ɵfac = function ProgressService_Factory(t) {
  return new (t || ProgressService)();
};
ProgressService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
  token: ProgressService,
  factory: ProgressService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 4325:
/*!****************************************************************!*\
  !*** ./src/app/services/sounds-manage/sounds-manage.module.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SoundsManageModule": () => (/* binding */ SoundsManageModule)
/* harmony export */ });
/* harmony import */ var _sounds_manage_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sounds-manage.service */ 7844);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 2560);


class SoundsManageModule {
  constructor() {
    /*
     * Requrimiento 1: llamarlo en al appModule
     */
    if (!SoundsManageModule.isInitialized) {
      throw new Error('Call ' + SoundsManageModule.nameModule + '.forRoot() in appModule');
    }
  }
  /*
   * Unica instancia que se pueda acceder a nivel global
   */
  static forRoot() {
    /*
     * Requrimiento 2: No se llama nuevamente
     */
    if (this.isInitialized) {
      throw new Error('Do not Call ' + SoundsManageModule.nameModule + '.forRoot() multiple times');
    }
    this.isInitialized = true;
    return {
      ngModule: SoundsManageModule,
      providers: [_sounds_manage_service__WEBPACK_IMPORTED_MODULE_0__.SoundsManageService]
    };
  }
}
SoundsManageModule.nameModule = 'SoundsManageModule';
SoundsManageModule.isInitialized = false;
SoundsManageModule.ɵfac = function SoundsManageModule_Factory(t) {
  return new (t || SoundsManageModule)();
};
SoundsManageModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
  type: SoundsManageModule
});
SoundsManageModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({});

/***/ }),

/***/ 7844:
/*!*****************************************************************!*\
  !*** ./src/app/services/sounds-manage/sounds-manage.service.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SoundsManageService": () => (/* binding */ SoundsManageService)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 2560);

class SoundsManageService {
  constructor() {
    this.hoverSounds = {
      hoverDefault: new Audio(`../../../assets/section1/sounds/hoverDefault.mp3`)
    };
    this.clickSounds = {
      clickDefault: new Audio(`../../../assets/section1/sounds/clickDefault.mp3`)
    };
  }
  playHoverSound(key) {
    if (!this.hoverSounds[key]) {
      key = 'hoverDefault';
    }
    this.hoverSounds[key].currentTime = 0;
    this.hoverSounds[key].play();
  }
  playClickSound(key) {
    if (!this.clickSounds[key]) {
      key = 'clickDefault';
    }
    this.clickSounds[key].currentTime = 0;
    this.clickSounds[key].play();
  }
}
SoundsManageService.ɵfac = function SoundsManageService_Factory(t) {
  return new (t || SoundsManageService)();
};
SoundsManageService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
  token: SoundsManageService,
  factory: SoundsManageService.ɵfac
});

/***/ }),

/***/ 6085:
/*!****************************************************!*\
  !*** ./src/app/shop/inside/book/book.component.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookChqComponent": () => (/* binding */ BookChqComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 2560);


class BookChqComponent {
  constructor() {
    this.currentPage = 3;
    this.isClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
  }
  ngOnInit() {}
  toggleClass(e, toggleClassName) {
    if (e.className.includes(toggleClassName)) {
      e.classList.remove(toggleClassName);
    } else {
      e.classList.add(toggleClassName);
    }
  }
  movePage(e, page) {
    console.log('click--> ', page);
    /* console.log('--------\npage: ', page, ' - e: ', <HTMLDivElement>e);   //obtiene el elemento seleccionado completo con toda su información
    console.log('e target:', <HTMLDivElement>e.target);   //obtiene el elemento actual seleccionado
    console.log('e target localName:', <HTMLDivElement>e.target.localName);   //obtiene el nombre de la etiqueta del elemento seleccionado
    console.log('e className: ', <HTMLDivElement>e.target.className);   //obtiene las clases del elemento actual seleccionado
    console.log('e parentElement: ', <HTMLDivElement>e.target.parentElement);   //obtiene el elemento padre del elemento seleccionado */
    //console.log('<HTMLDivElement>e: ', <HTMLDivElement>e);   //obtiene el elemento seleccionado completo con toda su información
    //console.log('<HTMLDivElement>e.target:', <HTMLDivElement>e.target);   //obtiene el elemento actual seleccionado
    //console.log('<HTMLDivElement>e.target.localName:', <HTMLDivElement>e.target.localName);   //obtiene el nombre de la etiqueta del elemento seleccionado
    //console.log('<HTMLDivElement>e.target.className: ', <HTMLDivElement>e.target.className);   //obtiene las clases del elemento actual seleccionado
    //console.log('<HTMLDivElement>e.target.parentElement: ', <HTMLDivElement>e.target.parentElement);   //obtiene el elemento padre del elemento seleccionado
    /* el elemento clickeado es la division que contiene la información */
    console.log('----------------------------------------------');
    if (page == 29) {
      console.log('cond page == 29');
      return;
    }
    console.log(e);
    console.log(e.target.classList[0]);
    let className = e.target.classList[0];
    console.log(className);
    if (e.target.localName == "div" && className == "page") {
      //console.log(<HTMLDivElement>e.target);
      //console.log('<HTMLDivElement>e.target.className: ', <HTMLDivElement>e.target.className); 
      console.log('--> PAGE');
      //obtiene las clases del elemento actual seleccionado
      if (page == this.currentPage) {
        console.log('PAGE | igual --> page: ', page, ' - currentPage: ', this.currentPage);
        this.currentPage += 2;
        this.toggleClass(e.target, "left-side");
        this.toggleClass(e.target.nextElementSibling, "left-side");
      } else if (page == this.currentPage - 1) {
        console.log('PAGE | -1 --> page: ', page, ' - currentPage: ', this.currentPage);
        this.currentPage -= 2;
        this.toggleClass(e.target, "left-side");
        this.toggleClass(e.target.previousElementSibling, "left-side");
      }
    }
    /* el elemento clickeado es el contenedor interno (el elemento hijo) */else if (e.target.localName == "div" && (className == "title" || className == "image" || className == "text")) {
      console.log('--> CHILD');
      if (page == this.currentPage) {
        console.log('CHILD | igual --> page: ', page, ' - currentPage: ', this.currentPage);
        this.currentPage += 2;
        this.toggleClass(e.target.parentElement, "left-side");
        this.toggleClass(e.target.parentElement.nextElementSibling, "left-side");
      } else if (page == this.currentPage - 1) {
        console.log('CHILD | -1 --> page: ', page, ' - currentPage: ', this.currentPage);
        this.currentPage -= 2;
        this.toggleClass(e.target.parentElement, "left-side");
        this.toggleClass(e.target.parentElement.previousElementSibling, "left-side");
      }
    }
    this.isClicked.emit(this.currentPage);
  }
}
BookChqComponent.ɵfac = function BookChqComponent_Factory(t) {
  return new (t || BookChqComponent)();
};
BookChqComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
  type: BookChqComponent,
  selectors: [["app-book-chq"]],
  outputs: {
    isClicked: "isClicked"
  },
  decls: 133,
  vars: 0,
  consts: [[1, "book"], [1, "page", "left-side", "portada"], [1, "page", "left-side", "page2"], ["disabled", "", 1, "logo1"], ["disabled", "", 1, "image1"], [1, "page", "page3", 3, "click"], ["disabled", "", 1, "text"], [1, "page", "page4", 3, "click"], ["disabled", "", 1, "title"], ["disabled", "", 1, "title2"], [1, "page", "page5", 3, "click"], ["disabled", "", 1, "image"], [1, "page", "page6", 3, "click"], [1, "page", "page7", 3, "click"], [1, "page", "page8", 3, "click"], [1, "page", "page9", 3, "click"], [1, "page", "page10", 3, "click"], [1, "page", "page11", 3, "click"], [1, "page", "page12", 3, "click"], [1, "page", "page13", 3, "click"], [1, "page", "page14", 3, "click"], [1, "page", "page15", 3, "click"], [1, "page", "page16", 3, "click"], [1, "page", "page17", 3, "click"], [1, "page", "page18", 3, "click"], [1, "page", "page19", 3, "click"], [1, "page", "page20", 3, "click"], [1, "page", "page21", 3, "click"], [1, "page", "page22", 3, "click"], [1, "page", "page23", 3, "click"], [1, "page", "page24", 3, "click"], [1, "page", "page25", 3, "click"], [1, "page", "page26", 3, "click"], [1, "page", "page27", 3, "click"], [1, "page", "page28", 3, "click"], [1, "page", "page29", 3, "click"], [1, "video"], ["src", "../../../../assets/section1/lesson3/view1/videos/S1L3V1.mp4", "controls", ""], [1, "border_gray_left"], [1, "border_gray_right"], [1, "divisor"]],
  template: function BookChqComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 3)(4, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_5_listener($event) {
        return ctx.movePage($event, 3);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Acuerdo de coordinaci\u00F3n para trabajos interempresas en el subsuelo de bienes nacionales de uso p\u00FAblico.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_8_listener($event) {
        return ctx.movePage($event, 4);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "ACUERDO DE COORDINACI\u00D3N");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "PARA TRABAJOS INTEREMPRESAS EN EL SUBSUELO DE BIENES NACIONALES DE USO P\u00DABLICO");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " El acuerdo de coordinaci\u00F3n para trabajos Interempresas en el subsuelo de bienes nacionales de uso p\u00FAblico, es un convenio que busca regular y coordinar preventivamente las actividades que se realizan en el subsuelo de los bienes nacionales de uso p\u00FAblico, como las calles y avenidas de las ciudades principalmente. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "br")(16, "br");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " Este acuerdo tiene como objetivo establecer las pautas y lineamientos necesarios para la coordinaci\u00F3n de las actividades de empresas que realizan trabajos en el subsuelo");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 10);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_18_listener($event) {
        return ctx.movePage($event, 5);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, " con el fin de prevenir da\u00F1os, minimizar los riesgos de accidentes y principalmente el cuidado de las personas, as\u00ED como tambi\u00E9n asegurar una adecuada gesti\u00F3n del espacio p\u00FAblico.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 12);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_22_listener($event) {
        return ctx.movePage($event, 6);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "\u00BFEn qu\u00E9 consiste la Solicitud ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 9);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "de Informaci\u00F3n de Interferencias para trabajos programados Anexos A, B, C y D?");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, " La Solicitud de Informaci\u00F3n de Interferencias (SII) es un proceso que se utiliza en proyectos de construcci\u00F3n o trabajos programados que implican la realizaci\u00F3n de actividades en el subsuelo, como excavaciones o instalaci\u00F3n de tuber\u00EDas. El objetivo es prevenir accidentes y da\u00F1os a la infraestructura subterr\u00E1nea existente, como l\u00EDneas de gas, agua, electricidad, entre otras.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "br")(30, "br");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, " El proceso implica que el solicitante del permiso (usualmente la empresa que realizar\u00E1 los trabajos) debe presentar una solicitud a las empresas o entidades propietarias de la infraestructura subterr\u00E1nea existente que pueda verse afectada por los trabajos programados.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_32_listener($event) {
        return ctx.movePage($event, 7);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, " La solicitud debe incluir la descripci\u00F3n detallada de los trabajos a realizar, la ubicaci\u00F3n y duraci\u00F3n estimada, y otros detalles relevantes.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br")(36, "br");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, " La respuesta de las empresas o entidades propietarias de la infraestructura subterr\u00E1nea se presenta en los Anexos A, B, C y D, que son documentos que detallan la ubicaci\u00F3n, tipo y estado de las infraestructuras subterr\u00E1neas existentes, as\u00ED como cualquier otra informaci\u00F3n relevante para garantizar la seguridad durante los trabajos programados.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br")(39, "br");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, " En resumen, la Solicitud de Informaci\u00F3n de Interferencias para trabajos programados y sus anexos A, B, C y D, son un conjunto de documentos y procesos que buscan prevenir accidentes y da\u00F1os a la infraestructura subterr\u00E1nea existente durante la realizaci\u00F3n de trabajos programados en el subsuelo. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 14);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_41_listener($event) {
        return ctx.movePage($event, 8);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "ANEXO A");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " Anexo que hace referencia a la necesidad de realizar un trabajo, sea particular o de Empresa, por lo cual se informa a las C\u00EDa.(s) involucradas que tengan o no, instalaciones en el subsuelo, e informen en un plazo no mayor a 7 d\u00EDas h\u00E1biles a contar de la fecha de recepci\u00F3n. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "br")(47, "br");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, " De no informar se entender\u00E1 que en los puntos consultados no existen redes.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 15);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_49_listener($event) {
        return ctx.movePage($event, 9);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 16);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_51_listener($event) {
        return ctx.movePage($event, 10);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "ANEXO A");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 17);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_55_listener($event) {
        return ctx.movePage($event, 11);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 18);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_57_listener($event) {
        return ctx.movePage($event, 12);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "ANEXO B");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, " Este anexo es el enviado por la (s) Cia. (s) requeridas quienes informaran respecto de la existencia o no de instalaciones, de haber, se le asignara un T\u00E9cnico dedicado con quien se podr\u00E1n contactar - prevenir y dar soluciones a inconvenientes si procede. Debe ser entregada hasta el s\u00E9ptimo d\u00EDa h\u00E1bil.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 19);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_62_listener($event) {
        return ctx.movePage($event, 13);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 20);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_64_listener($event) {
        return ctx.movePage($event, 14);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "ANEXO B");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 21);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_68_listener($event) {
        return ctx.movePage($event, 15);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "div", 22);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_70_listener($event) {
        return ctx.movePage($event, 16);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "ANEXO C");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, " Este anexo es el remitido por el solicitante, 48 horas antes, informando el inicio formal de sus actividades de intervenci\u00F3n, es responsabilidad de la (s) requeridas (s) asistir - cautelar- orientar- prevenir y hacer respetar las normativas que garanticen una correcta ejecuci\u00F3n de los trabajos en el subsuelo.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 23);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_75_listener($event) {
        return ctx.movePage($event, 17);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 24);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_77_listener($event) {
        return ctx.movePage($event, 18);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "ANEXO D");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, " Este anexo fue dise\u00F1ado para aquellos eventos que nacen s\u00FAbitamente transform\u00E1ndose en contingencia, la asistencia se hace obligatoria aportando en terreno toda la informaci\u00F3n que se requiera. La no asistencia se entiende como la no existencia de redes y se proceder\u00E1 en consecuencia., de existir red la consultada deber\u00E1 hacerse responsable. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 25);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_82_listener($event) {
        return ctx.movePage($event, 19);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 26);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_84_listener($event) {
        return ctx.movePage($event, 20);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, " Estos anexos se encuentran en un portal web, al cual se ingresa desde la p\u00E1gina www.chilquinta.cl, siendo una plataforma automatizada, que permite tener el control de las solicitudes de trabajos programados en una plataforma web, siendo algunos de sus beneficios: ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 27);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_87_listener($event) {
        return ctx.movePage($event, 21);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 6)(89, "b");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "Mayor eficiencia:");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, " se ha logrado automatizar el proceso de solicitud y programaci\u00F3n, lo que conlleva la reducci\u00F3n del tiempo y los costos asociados con la administraci\u00F3n manual de la programaci\u00F3n de trabajos. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](92, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div", 28);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_93_listener($event) {
        return ctx.movePage($event, 22);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](94, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 6)(96, "b");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Mayor visibilidad y seguimiento:");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, " seguimiento de los trabajos solicitados y programados en tiempo real, lo cual permite garantizar que se est\u00E9n cumpliendo los plazos y que los trabajos se est\u00E9n realizando seg\u00FAn lo acordado. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 29);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_99_listener($event) {
        return ctx.movePage($event, 23);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 6)(101, "b");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "Comunicaci\u00F3n mejorada:");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, " una mejora importante es la comunicaci\u00F3n entre los solicitantes y los programadores de trabajos, permitiendo en algunos casos respuesta en l\u00EDnea de forma inmediata (en tiempo real), de acuerdo a los protocolos definidos ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](104, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "div", 30);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_105_listener($event) {
        return ctx.movePage($event, 24);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](106, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 6)(108, "b");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "Facilidad de uso:");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, " Al utilizar una plataforma web, los solicitantes pueden solicitar y programar trabajos f\u00E1cilmente sin necesidad de comunicarse directamente con los programadores. Esto reduce el tiempo y el esfuerzo necesarios para solicitar trabajos y aumentar la satisfacci\u00F3n del cliente. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 31);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_111_listener($event) {
        return ctx.movePage($event, 25);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 6)(113, "b");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "Mayor precisi\u00F3n y calidad:");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, " Al ser un proceso automatizado, se pueden reducir los errores humanos y mejorar la calidad de los trabajos programados ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "div", 32);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_117_listener($event) {
        return ctx.movePage($event, 26);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](119, "Comunicaci\u00F3n | Volantes");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](120, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 33);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_121_listener($event) {
        return ctx.movePage($event, 27);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](122, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 34);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_123_listener($event) {
        return ctx.movePage($event, 28);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "\u00BFC\u00F3mo realiza este proceso Chilquinta Distribuci\u00F3n?");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](126, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "div", 35);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookChqComponent_Template_div_click_127_listener($event) {
        return ctx.movePage($event, 29);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "div", 36);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "video", 37);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](130, "div", 38)(131, "div", 39)(132, "div", 40);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    }
  },
  styles: ["@import url(https://fonts.googleapis.com/css2?family=Lora&display=swap);@import url(https://fonts.googleapis.com/css2?family=EB+Garamond&display=swap);*[_ngcontent-%COMP%] {\n  margin: 0;\n  box-sizing: border-box;\n}\n\n\n\n\n.divisor[_ngcontent-%COMP%] {\n  position: absolute;\n  background-color: #3a9acebb;\n  height: 100%;\n  left: 50%;\n  top: 0;\n  width: calc(var(--frameHeight)*0.0002);\n}\n.border_book[_ngcontent-%COMP%] {\n  position: absolute;\n  border: calc(var(--frameHeight)*0.01) solid #3a9ace;\n  border-radius: calc(var(--frameHeight)*0.02);\n  height: 90%;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  transform-style: preserve-3d;\n  width: 65%;\n}\n.book[_ngcontent-%COMP%] {\n  position: absolute;\n  border: calc(var(--frameHeight)*0.012) solid #3a9ace;\n  border-radius: calc(var(--frameHeight)*0.02);\n  height: 90%;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  transform-style: preserve-3d;\n  width: 65%;\n  \n}\n\ndiv.border_gray_left[_ngcontent-%COMP%] {\n  position: absolute;\n  border-radius: calc(var(--frameHeight)*0.012);\n  border-left: calc(var(--frameHeight)*0.03) solid #d6d6d6;\n  height: 100%;\n  left: 0%;\n}\ndiv.border_gray_right[_ngcontent-%COMP%] {\n  position: absolute;\n  border-radius: calc(var(--frameHeight)*0.012);\n  border-right: calc(var(--frameHeight)*0.03) solid #d6d6d6;\n  height: 100%;\n  right: 0%;\n}\n\n.page[_ngcontent-%COMP%] {\n  position: absolute;\n  background: #fff;\n  border-right: calc(var(--frameHeight)*0.03) solid #d6d6d6;\n  border-left: calc(var(--frameHeight)*0.03) solid #d6d6d6;\n  border-left: none;\n  height: 100%;\n  overflow: hidden;\n  right: 0;\n  top: 0;\n  transition: transform 1s;\n  width: 50%;\n}\n\n.left-side[_ngcontent-%COMP%] {\n  border-right: none;\n  border-left: calc(var(--frameHeight)*0.03) solid #d6d6d6;\n}\n\n.page[_ngcontent-ayc-c16][_ngcontent-%COMP%]:nth-child(2n+1) {\n  border-left: 1px solid #3335;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(1) {\n  padding: 3% 3% 3% 3%;\n  transform-origin: 0% 50%;\n  transform: translatez(-1px);\n  background: #9bc8d6;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(1) {\n  transform: translatez(1px) rotatey(-180deg);\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(2) {\n  transform-origin: 100% 50%;\n  transform: translatez(-2px) scalex(-1) translatex(100%);\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(2) {\n  transform: translatez(2px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page2[_ngcontent-%COMP%]   .logo1[_ngcontent-%COMP%] {\n  background: url('LogoValparaiso.png') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.2);\n  width: calc(var(--frameHeight)*0.35);\n}\n.page.page2[_ngcontent-%COMP%]   .image1[_ngcontent-%COMP%] {\n  background: url('logos1.png') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.07);\n  width: 95%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(3) {\n  transform-origin: 0% 50%;\n  transform: translatez(-3px);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(3) {\n  transform: translatez(3px) rotatey(-180deg);\n}\n.page.page3[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  position: absolute;\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.08);\n  font-weight: normal;\n  left: 0;\n  text-align: center;\n  top: 40%;\n  width: 100%;\n}\n.page.page3[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]{\n  font-size: calc(var(--frameHeight)*0.025);\n  line-height: normal;\n  text-align: justify;\n  width: 75.3%;\n  color: #000\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(4) {\n  transform-origin: 100% 50%;\n  transform: translatez(-4px) scalex(-1) translatex(100%);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(4) {\n  transform: translatez(4px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page4[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  position: absolute;\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.038);\n  font-weight: normal;\n  left: 0;\n  text-align: center;\n  top: 5%;\n  width: 100%;\n}\n.page.page4[_ngcontent-%COMP%]   .title2[_ngcontent-%COMP%]{\n  position: absolute;\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.02);\n  font-weight: bold;\n  left: 0;\n  text-align: center;\n  top: 13%;\n\n  width: 100%;\n}\n.page.page4[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  position: absolute;\n  font-size: calc(var(--frameHeight)*0.025);\n  left: 5%;\n  line-height: normal;\n  text-align: justify;\n  top: 28%;\n  width: 90%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(5) {\n  transform-origin: 0% 50%;\n  transform: translatez(-5px);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(5) {\n  transform: translatez(5px) rotatey(-180deg);\n}\n.page.page5[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  position: absolute;\n  font-size: calc(var(--frameHeight)*0.025);\n  left: 5%;\n  line-height: normal;\n  text-align: justify;\n  top: 6%;\n  width: 90%;\n}\n\n.page.page5[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  position: absolute;\n  background: url('gif_de_hojas.gif') no-repeat;\n  background-size: 100% 100%;\n  height: 50%;\n  left: 14%;\n  top: 37%;\n  width: 70%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(6) {\n  transform-origin: 100% 50%;\n  transform: translatez(-6px) scalex(-1) translatex(100%);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(6) {\n  transform: translatez(6px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page6[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.038);\n  font-weight: normal;\n  text-align: center;\n  width: 100%;\n}\n.page.page6[_ngcontent-%COMP%]   .title2[_ngcontent-%COMP%]{\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.02);\n  font-weight: bold;\n  text-align: center;\n  width: 100%;\n}\n.page.page6[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  font-size: calc(var(--frameHeight)*0.023);\n  line-height: normal;\n  text-align: justify;\n  width: 90%;\n}\n\n\n\n.page[_ngcontent-%COMP%]:nth-child(7) {\n  transform-origin: 0% 50%;\n  transform: translatez(-7px);\n  background: #ffffff;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(7) {\n  transform: translatez(7px) rotatey(-180deg);\n}\n.page.page7[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  position: absolute;\n  font-size: calc(var(--frameHeight)*0.023);\n  left: 5%;\n  line-height: normal;\n  text-align: justify;\n  top: 5%;\n  width: 90%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(8) {\n  transform-origin: 100% 50%;\n  transform: translatez(-8px) scalex(-1) translatex(100%);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(8) {\n  transform: translatez(8px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page8[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  position: absolute;\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.08);\n  font-weight: normal;\n  left: 0;\n  text-align: center;\n  top: 5%;\n  width: 100%;\n}\n.page.page8[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  position: absolute;\n  font-size: calc(var(--frameHeight)*0.025);\n  left: 5%;\n  line-height: normal;\n  text-align: justify;\n  top: 28%;\n  width: 90%;\n}\n\n.page[_ngcontent-%COMP%]:nth-child(9) {\n  transform-origin: 0% 50%;\n  transform: translatez(-9px);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(9) {\n  transform: translatez(9px) rotatey(-180deg);\n}\n.page.page9[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  position: absolute;\n  background: url('picture20.png') no-repeat;\n  background-size: 100% 100%;\n  height: 85%;\n  left: 5%;\n  top: 9%;\n  width: 90%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(10) {\n  transform-origin: 100% 50%;\n  transform: translatez(-10px) scalex(-1) translatex(100%);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(10) {\n  transform: translatez(10px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page10[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.08);\n  font-weight: normal;\n  text-align: center;\n  width: 100%;\n}\n.page.page10[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('AnexoA-2.jpg') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.42);\n  width: calc(var(--frameHeight)*0.40);\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(11) {\n  transform-origin: 0% 50%;\n  transform: translatez(-11px);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(11) {\n  transform: translatez(11px) rotatey(-180deg);\n}\n.page.page11[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  position: absolute;\n  background: url('picture22.png') no-repeat;\n  background-size: 100% 100%;\n  height: 85%;\n  left: 5%;\n  top: 9%;\n  width: 90%;\n}\n\n.page[_ngcontent-%COMP%]:nth-child(12) {\n  transform-origin: 100% 50%;\n  transform: translatez(-12px) scalex(-1) translatex(100%);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(12) {\n  transform: translatez(12px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page12[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  position: absolute;\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.08);\n  font-weight: normal;\n  left: 0;\n  text-align: center;\n  top: 5%;\n  width: 100%;\n}\n.page.page12[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  position: absolute;\n  font-size: calc(var(--frameHeight)*0.025);\n  left: 10%;\n  line-height: normal;\n  text-align: justify;\n  top: 28%;\n  width: 80%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(13) {\n  transform-origin: 0% 50%;\n  transform: translatez(-13px);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(13) {\n  transform: translatez(13px) rotatey(-180deg);\n}\n.page.page13[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('picture20.png') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.6);\n  width: calc(var(--frameHeight)*0.47);\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(14) {\n  transform-origin: 100% 50%;\n  transform: translatez(-14px) scalex(-1) translatex(100%);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n  padding-top: 6%;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(14) {\n  transform: translatez(14px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page14[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.08);\n  font-weight: normal;\n  text-align: center;\n  width: 100%;\n}\n.page.page14[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('AnexoB-2.jpg') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.50);\n  width: calc(var(--frameHeight)*0.35);\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(15) {\n  transform-origin: 0% 50%;\n  transform: translatez(-15px);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(15) {\n  transform: translatez(15px) rotatey(-180deg);\n}\n.page.page15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  position: absolute;\n  background: url('picture19.png') no-repeat;\n  background-size: 100% 100%;\n  height: 85%;\n  left: 5%;\n  top: 9%;\n  width: 90%;\n}\n\n\n\n.page[_ngcontent-%COMP%]:nth-child(16) {\n  transform-origin: 100% 50%;\n  transform: translatez(-16px) scalex(-1) translatex(100%);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(16) {\n  transform: translatez(16px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page16[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  position: absolute;\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.08);\n  font-weight: normal;\n  left: 0;\n  text-align: center;\n  top: 5%;\n  width: 100%;\n}\n.page.page16[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  position: absolute;\n  font-size: calc(var(--frameHeight)*0.025);\n  left: 10%;\n  line-height: normal;\n  text-align: justify;\n  top: 28%;\n  width: 80%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(17) {\n  transform-origin: 0% 50%;\n  transform: translatez(-17px);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(17) {\n  transform: translatez(17px) rotatey(-180deg);\n}\n.page.page17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  position: absolute;\n  background: url('picture17.png') no-repeat;\n  background-size: 100% 100%;\n  height: 85%;\n  left: 5%;\n  top: 9%;\n  width: 90%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(18) {\n  transform-origin: 100% 50%;\n  transform: translatez(-18px) scalex(-1) translatex(100%);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(18) {\n  transform: translatez(18px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page18[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  position: absolute;\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.08);\n  font-weight: normal;\n  left: 0;\n  text-align: center;\n  top: 5%;\n  width: 100%;\n}\n.page.page18[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  position: absolute;\n  font-size: calc(var(--frameHeight)*0.025);\n  left: 10%;\n  line-height: normal;\n  text-align: justify;\n  top: 28%;\n  width: 80%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(19) {\n  transform-origin: 0% 50%;\n  transform: translatez(-19px);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(19) {\n  transform: translatez(19px) rotatey(-180deg);\n}\n.page.page19[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('picture16.png') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.55);\n  width: calc(var(--frameHeight)*0.35);\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(20) {\n  transform-origin: 100% 50%;\n  transform: translatez(-20px) scalex(-1) translatex(100%);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(20) {\n  transform: translatez(20px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page20[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]{\n  position: absolute;\n  font-size: calc(var(--frameHeight)*0.025);\n  left: 10%;\n  line-height: normal;\n  text-align: justify;\n  top: 28%;\n  width: 80%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(21) {\n  transform-origin: 0% 50%;\n  transform: translatez(-21px);\n  background: #ffffff;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(21) {\n  transform: translatez(21px) rotatey(-180deg);\n}\n.page.page21[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]{\n  position: absolute;\n  font-size: calc(var(--frameHeight)*0.025);\n  left: 5%;\n  line-height: normal;\n  text-align: justify;\n  top: 18%;\n  width: 90%;\n}\n.page.page21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  position: absolute;\n  background: url('gif_medidor.gif') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.30);\n  left: 14%;\n  top: 42%;\n  width: calc(var(--frameHeight)*0.40);\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(22) {\n  transform-origin: 100% 50%;\n  transform: translatez(-22px) scalex(-1) translatex(100%);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n  gap: 2%;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(22) {\n  transform: translatez(22px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page22[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('gif_visibility.gif') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.25);;\n  width: calc(var(--frameHeight)*0.30);;\n}\n.page.page22[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]{\n  font-size: calc(var(--frameHeight)*0.025);\n  line-height: normal;\n  text-align: justify;\n  width: 88%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(23) {\n  transform-origin: 0% 50%;\n  transform: translatez(-23px);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n  gap: 5%;\n  padding-top: 3%;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(23) {\n  transform: translatez(23px) rotatey(-180deg);\n}\n.page.page23[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]{\n  font-size: calc(var(--frameHeight)*0.025);\n  line-height: normal;\n  text-align: justify;\n  width: 88%;\n}\n.page.page23[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('gif_comunication.gif') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.22);\n  width: calc(var(--frameHeight)*0.30);\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(24) {\n  transform-origin: 100% 50%;\n  transform: translatez(-24px) scalex(-1) translatex(100%);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n  gap: 4%;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(24) {\n  transform: translatez(24px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page24[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('gif_facility.gif') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.30);\n  width: calc(var(--frameHeight)*0.40);\n}\n.page.page24[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]{\n  font-size: calc(var(--frameHeight)*0.025);\n  line-height: normal;\n  text-align: justify;\n  width: 88%;\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(25) {\n  transform-origin: 0% 50%;\n  transform: translatez(-25px);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n  gap: 6%;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(25) {\n  transform: translatez(25px) rotatey(-180deg);\n}\n.page.page25[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]{\n  font-size: calc(var(--frameHeight)*0.025);\n  line-height: normal;\n  text-align: justify;\n  width: 88%;\n  \n}\n.page.page25[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('gif_precision.gif') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.30);\n  width: calc(var(--frameHeight)*0.40);\n  \n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(26) {\n  transform-origin: 100% 50%;\n  transform: translatez(-26px) scalex(-1) translatex(100%);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n  gap: 3%;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(26) {\n  transform: translatez(26px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page26[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.04);\n  font-weight: normal;\n  text-align: center;\n  width: 100%;\n}\n.page.page26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('picture15.png') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.5);\n  width: calc(var(--frameHeight)*0.36);\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(27) {\n  transform-origin: 0% 50%;\n  transform: translatez(-27px);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(27) {\n  transform: translatez(27px) rotatey(-180deg);\n}\n.page.page27[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('picture14.png') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.5);\n  width: calc(var(--frameHeight)*0.35);\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(28) {\n  transform-origin: 100% 50%;\n  transform: translatez(-28px) scalex(-1) translatex(100%);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n  gap: 8%;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(28) {\n  transform: translatez(28px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n.page.page28[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%] {\n  color: #ED6B06;\n  display: block;\n  font-size: calc(var(--frameHeight)*0.04);\n  font-weight: normal;\n  line-height: 110%;\n  text-align: center;\n  width: 100%;\n}\n.page.page28[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\n  background: url('gif_formulario.gif') no-repeat;\n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.35);\n  width: calc(var(--frameHeight)*0.35);\n}\n\n\n.page[_ngcontent-%COMP%]:nth-child(29) {\n  transform-origin: 0% 50%;\n  transform: translatez(-29px);\n  background: #ffffff;\n  display: grid;\n  align-content: center;\n  align-items: center;\n  justify-items: center;\n}\n.left-side[_ngcontent-%COMP%]:nth-child(29) {\n  transform: translatez(29px) rotatey(-180deg);\n}\n.page.page29[_ngcontent-%COMP%]   .video[_ngcontent-%COMP%] {\n  height: calc(var(--frameHeight)*0.278);\n  width: 90%;\n  box-shadow: calc(var(--frameHeight)*-0.015) \n              calc(var(--frameHeight)*0.025) \n              calc(var(--frameHeight)*0.075) #3b3b3b;\n}\n.page.page29[_ngcontent-%COMP%]   .video[_ngcontent-%COMP%]   video[_ngcontent-%COMP%] {\n  height: 100%;\n  width: 100%;\n  background-color: black;\n}\n\n\n\n.page[_ngcontent-%COMP%]:nth-child(30) {\n  padding: 5% 5% 4% 4%;\n  transform-origin: 100% 50%;\n  transform: translatez(-29px) scalex(-1) translatex(100%);\n  background: #ccc;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(30) {\n  transform: translatez(29px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(31) {\n  padding: 5% 4% 4% 5%;\n  transform-origin: 0% 50%;\n  transform: translatez(-25px);\n  background: #ccc;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(31) {\n  transform: translatez(25px) rotatey(-180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(32) {\n  padding: 5% 5% 4% 4%;\n  transform-origin: 100% 50%;\n  transform: translatez(-26px) scalex(-1) translatex(100%);\n  background: #ccc;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(32) {\n  transform: translatez(26px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n\n\n\n\n\n\n\n\n\n\n.page.portada[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n  color: #3d7d9e;\n  font-size: calc(var(--frameHeight)*0.085);\n  font-weight: 900;\n  text-align: center;\n}\n\ndiv.image_portada[_ngcontent-%COMP%] {\n  position: absolute;\n  background: url('protocolo.png') no-repeat;  \n  background-size: 100% 100%;\n  height: calc(var(--frameHeight)*0.20);\n  left: 33%;\n  top: 37%;\n  width: calc(var(--frameHeight)*0.20);\n}\n\n.page.portada[_ngcontent-%COMP%]   .sub_title[_ngcontent-%COMP%]{\n  position: absolute;\n  left: 3%;\n  text-align: center;\n  top: 72%;\n}\n\n\n\np[_ngcontent-%COMP%] {\n  margin-bottom: 4%;\n  font-family: \"EB Garamond\", serif;\n  font-size: 16px;\n  \n}\n\n.page[_ngcontent-%COMP%]:nth-child(3)   p[_ngcontent-%COMP%]:first-of-type:first-letter {\n  font-size: 32px;\n}\n\n.instruction[_ngcontent-%COMP%] {\n  position: absolute;\n  color: #0009;\n  font-size: calc(var(--frameHeight)*0.04);\n  left: calc(var(--frameHeight)*0.63);\n  padding: 10px 15px;\n  text-align: center;\n  top: 49%;\n  transform: translate(-50%, -50%);\n  width: 27%;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hvcC9pbnNpZGUvYm9vay9ib29rLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxTQUFTO0VBQ1Qsc0JBQXNCO0FBQ3hCOztBQUVBOzs7Ozs7R0FNRzs7QUFFSDs7Ozs7OztHQU9HO0FBQ0g7RUFDRSxrQkFBa0I7RUFDbEIsMkJBQTJCO0VBQzNCLFlBQVk7RUFDWixTQUFTO0VBQ1QsTUFBTTtFQUNOLHNDQUFzQztBQUN4QztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLG1EQUFtRDtFQUNuRCw0Q0FBNEM7RUFDNUMsV0FBVztFQUNYLFNBQVM7RUFDVCxRQUFRO0VBQ1IsZ0NBQWdDO0VBQ2hDLDRCQUE0QjtFQUM1QixVQUFVO0FBQ1o7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixvREFBb0Q7RUFDcEQsNENBQTRDO0VBQzVDLFdBQVc7RUFDWCxTQUFTO0VBQ1QsUUFBUTtFQUNSLGdDQUFnQztFQUNoQyw0QkFBNEI7RUFDNUIsVUFBVTtFQUNWLHNDQUFzQztBQUN4Qzs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQiw2Q0FBNkM7RUFDN0Msd0RBQXdEO0VBQ3hELFlBQVk7RUFDWixRQUFRO0FBQ1Y7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQiw2Q0FBNkM7RUFDN0MseURBQXlEO0VBQ3pELFlBQVk7RUFDWixTQUFTO0FBQ1g7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLHlEQUF5RDtFQUN6RCx3REFBd0Q7RUFDeEQsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsUUFBUTtFQUNSLE1BQU07RUFDTix3QkFBd0I7RUFDeEIsVUFBVTtBQUNaOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLHdEQUF3RDtBQUMxRDs7QUFFQTtFQUNFLDRCQUE0QjtBQUM5Qjs7QUFFQSxzQkFBc0I7QUFDdEI7RUFDRSxvQkFBb0I7RUFDcEIsd0JBQXdCO0VBQ3hCLDJCQUEyQjtFQUMzQixtQkFBbUI7QUFDckI7QUFDQTtFQUNFLDJDQUEyQztBQUM3Qzs7QUFFQSx5QkFBeUI7QUFDekI7RUFDRSwwQkFBMEI7RUFDMUIsdURBQXVEO0VBQ3ZELGFBQWE7RUFDYixxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLHFCQUFxQjtBQUN2QjtBQUNBO0VBQ0Usc0VBQXNFO0FBQ3hFO0FBQ0E7RUFDRSwrQ0FBOEY7RUFDOUYsMEJBQTBCO0VBQzFCLG9DQUFvQztFQUNwQyxvQ0FBb0M7QUFDdEM7QUFDQTtFQUNFLHVDQUFzRjtFQUN0RiwwQkFBMEI7RUFDMUIscUNBQXFDO0VBQ3JDLFVBQVU7QUFDWjs7QUFFQSx1QkFBdUI7QUFDdkI7RUFDRSx3QkFBd0I7RUFDeEIsMkJBQTJCO0VBQzNCLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixxQkFBcUI7QUFDdkI7QUFDQTtFQUNFLDJDQUEyQztBQUM3QztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsd0NBQXdDO0VBQ3hDLG1CQUFtQjtFQUNuQixPQUFPO0VBQ1Asa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixXQUFXO0FBQ2I7QUFDQTtFQUNFLHlDQUF5QztFQUN6QyxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWjtBQUNGOztBQUVBLHlCQUF5QjtBQUN6QjtFQUNFLDBCQUEwQjtFQUMxQix1REFBdUQ7RUFDdkQsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSxzRUFBc0U7QUFDeEU7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsY0FBYztFQUNkLHlDQUF5QztFQUN6QyxtQkFBbUI7RUFDbkIsT0FBTztFQUNQLGtCQUFrQjtFQUNsQixPQUFPO0VBQ1AsV0FBVztBQUNiO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGNBQWM7RUFDZCx3Q0FBd0M7RUFDeEMsaUJBQWlCO0VBQ2pCLE9BQU87RUFDUCxrQkFBa0I7RUFDbEIsUUFBUTs7RUFFUixXQUFXO0FBQ2I7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQix5Q0FBeUM7RUFDekMsUUFBUTtFQUNSLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsUUFBUTtFQUNSLFVBQVU7QUFDWjs7QUFFQSx1QkFBdUI7QUFDdkI7RUFDRSx3QkFBd0I7RUFDeEIsMkJBQTJCO0VBQzNCLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsMkNBQTJDO0FBQzdDO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIseUNBQXlDO0VBQ3pDLFFBQVE7RUFDUixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLE9BQU87RUFDUCxVQUFVO0FBQ1o7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsNkNBQTRGO0VBQzVGLDBCQUEwQjtFQUMxQixXQUFXO0VBQ1gsU0FBUztFQUNULFFBQVE7RUFDUixVQUFVO0FBQ1o7O0FBRUEseUJBQXlCO0FBQ3pCO0VBQ0UsMEJBQTBCO0VBQzFCLHVEQUF1RDtFQUN2RCxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIscUJBQXFCO0FBQ3ZCO0FBQ0E7RUFDRSxzRUFBc0U7QUFDeEU7QUFDQTtFQUNFLGNBQWM7RUFDZCxjQUFjO0VBQ2QseUNBQXlDO0VBQ3pDLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsV0FBVztBQUNiO0FBQ0E7RUFDRSxjQUFjO0VBQ2QsY0FBYztFQUNkLHdDQUF3QztFQUN4QyxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLFdBQVc7QUFDYjtBQUNBO0VBQ0UseUNBQXlDO0VBQ3pDLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsVUFBVTtBQUNaOzs7QUFHQSx1QkFBdUI7QUFDdkI7RUFDRSx3QkFBd0I7RUFDeEIsMkJBQTJCO0VBQzNCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLDJDQUEyQztBQUM3QztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLHlDQUF5QztFQUN6QyxRQUFRO0VBQ1IsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixPQUFPO0VBQ1AsVUFBVTtBQUNaOztBQUVBLHlCQUF5QjtBQUN6QjtFQUNFLDBCQUEwQjtFQUMxQix1REFBdUQ7RUFDdkQsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSxzRUFBc0U7QUFDeEU7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsY0FBYztFQUNkLHdDQUF3QztFQUN4QyxtQkFBbUI7RUFDbkIsT0FBTztFQUNQLGtCQUFrQjtFQUNsQixPQUFPO0VBQ1AsV0FBVztBQUNiO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIseUNBQXlDO0VBQ3pDLFFBQVE7RUFDUixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLFFBQVE7RUFDUixVQUFVO0FBQ1o7QUFDQSx1QkFBdUI7QUFDdkI7RUFDRSx3QkFBd0I7RUFDeEIsMkJBQTJCO0VBQzNCLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsMkNBQTJDO0FBQzdDO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsMENBQXlGO0VBQ3pGLDBCQUEwQjtFQUMxQixXQUFXO0VBQ1gsUUFBUTtFQUNSLE9BQU87RUFDUCxVQUFVO0FBQ1o7O0FBRUEsMEJBQTBCO0FBQzFCO0VBQ0UsMEJBQTBCO0VBQzFCLHdEQUF3RDtFQUN4RCxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIscUJBQXFCO0FBQ3ZCO0FBQ0E7RUFDRSx1RUFBdUU7QUFDekU7QUFDQTtFQUNFLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsd0NBQXdDO0VBQ3hDLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsV0FBVztBQUNiO0FBQ0E7RUFDRSx5Q0FBd0Y7RUFDeEYsMEJBQTBCO0VBQzFCLHFDQUFxQztFQUNyQyxvQ0FBb0M7QUFDdEM7O0FBRUEsd0JBQXdCO0FBQ3hCO0VBQ0Usd0JBQXdCO0VBQ3hCLDRCQUE0QjtFQUM1QixtQkFBbUI7QUFDckI7QUFDQTtFQUNFLDRDQUE0QztBQUM5QztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLDBDQUF5RjtFQUN6RiwwQkFBMEI7RUFDMUIsV0FBVztFQUNYLFFBQVE7RUFDUixPQUFPO0VBQ1AsVUFBVTtBQUNaO0FBQ0EsMEJBQTBCO0FBQzFCO0VBQ0UsMEJBQTBCO0VBQzFCLHdEQUF3RDtFQUN4RCxtQkFBbUI7QUFDckI7QUFDQTtFQUNFLHVFQUF1RTtBQUN6RTtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsd0NBQXdDO0VBQ3hDLG1CQUFtQjtFQUNuQixPQUFPO0VBQ1Asa0JBQWtCO0VBQ2xCLE9BQU87RUFDUCxXQUFXO0FBQ2I7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQix5Q0FBeUM7RUFDekMsU0FBUztFQUNULG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsUUFBUTtFQUNSLFVBQVU7QUFDWjs7QUFFQSx3QkFBd0I7QUFDeEI7RUFDRSx3QkFBd0I7RUFDeEIsNEJBQTRCO0VBQzVCLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixxQkFBcUI7QUFDdkI7QUFDQTtFQUNFLDRDQUE0QztBQUM5QztBQUNBO0VBQ0UsMENBQXlGO0VBQ3pGLDBCQUEwQjtFQUMxQixvQ0FBb0M7RUFDcEMsb0NBQW9DO0FBQ3RDOztBQUVBLDBCQUEwQjtBQUMxQjtFQUNFLDBCQUEwQjtFQUMxQix3REFBd0Q7RUFDeEQsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixlQUFlO0FBQ2pCO0FBQ0E7RUFDRSx1RUFBdUU7QUFDekU7QUFDQTtFQUNFLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsd0NBQXdDO0VBQ3hDLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsV0FBVztBQUNiO0FBQ0E7RUFDRSx5Q0FBd0Y7RUFDeEYsMEJBQTBCO0VBQzFCLHFDQUFxQztFQUNyQyxvQ0FBb0M7QUFDdEM7O0FBRUEsd0JBQXdCO0FBQ3hCO0VBQ0Usd0JBQXdCO0VBQ3hCLDRCQUE0QjtFQUM1QixtQkFBbUI7QUFDckI7QUFDQTtFQUNFLDRDQUE0QztBQUM5QztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLDBDQUF5RjtFQUN6RiwwQkFBMEI7RUFDMUIsV0FBVztFQUNYLFFBQVE7RUFDUixPQUFPO0VBQ1AsVUFBVTtBQUNaOzs7QUFHQSwwQkFBMEI7QUFDMUI7RUFDRSwwQkFBMEI7RUFDMUIsd0RBQXdEO0VBQ3hELG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsdUVBQXVFO0FBQ3pFO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGNBQWM7RUFDZCx3Q0FBd0M7RUFDeEMsbUJBQW1CO0VBQ25CLE9BQU87RUFDUCxrQkFBa0I7RUFDbEIsT0FBTztFQUNQLFdBQVc7QUFDYjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLHlDQUF5QztFQUN6QyxTQUFTO0VBQ1QsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixRQUFRO0VBQ1IsVUFBVTtBQUNaOztBQUVBLHdCQUF3QjtBQUN4QjtFQUNFLHdCQUF3QjtFQUN4Qiw0QkFBNEI7RUFDNUIsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSw0Q0FBNEM7QUFDOUM7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQiwwQ0FBeUY7RUFDekYsMEJBQTBCO0VBQzFCLFdBQVc7RUFDWCxRQUFRO0VBQ1IsT0FBTztFQUNQLFVBQVU7QUFDWjs7QUFFQSwwQkFBMEI7QUFDMUI7RUFDRSwwQkFBMEI7RUFDMUIsd0RBQXdEO0VBQ3hELG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsdUVBQXVFO0FBQ3pFO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGNBQWM7RUFDZCx3Q0FBd0M7RUFDeEMsbUJBQW1CO0VBQ25CLE9BQU87RUFDUCxrQkFBa0I7RUFDbEIsT0FBTztFQUNQLFdBQVc7QUFDYjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLHlDQUF5QztFQUN6QyxTQUFTO0VBQ1QsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixRQUFRO0VBQ1IsVUFBVTtBQUNaOztBQUVBLHdCQUF3QjtBQUN4QjtFQUNFLHdCQUF3QjtFQUN4Qiw0QkFBNEI7RUFDNUIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLHFCQUFxQjtBQUN2QjtBQUNBO0VBQ0UsNENBQTRDO0FBQzlDO0FBQ0E7RUFDRSwwQ0FBeUY7RUFDekYsMEJBQTBCO0VBQzFCLHFDQUFxQztFQUNyQyxvQ0FBb0M7QUFDdEM7O0FBRUEsMEJBQTBCO0FBQzFCO0VBQ0UsMEJBQTBCO0VBQzFCLHdEQUF3RDtFQUN4RCxtQkFBbUI7QUFDckI7QUFDQTtFQUNFLHVFQUF1RTtBQUN6RTtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLHlDQUF5QztFQUN6QyxTQUFTO0VBQ1QsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixRQUFRO0VBQ1IsVUFBVTtBQUNaOztBQUVBLHdCQUF3QjtBQUN4QjtFQUNFLHdCQUF3QjtFQUN4Qiw0QkFBNEI7RUFDNUIsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSw0Q0FBNEM7QUFDOUM7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQix5Q0FBeUM7RUFDekMsUUFBUTtFQUNSLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsUUFBUTtFQUNSLFVBQVU7QUFDWjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLDRDQUEyRjtFQUMzRiwwQkFBMEI7RUFDMUIscUNBQXFDO0VBQ3JDLFNBQVM7RUFDVCxRQUFRO0VBQ1Isb0NBQW9DO0FBQ3RDOztBQUVBLDBCQUEwQjtBQUMxQjtFQUNFLDBCQUEwQjtFQUMxQix3REFBd0Q7RUFDeEQsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixPQUFPO0FBQ1Q7QUFDQTtFQUNFLHVFQUF1RTtBQUN6RTtBQUNBO0VBQ0UsK0NBQThGO0VBQzlGLDBCQUEwQjtFQUMxQixxQ0FBcUM7RUFDckMsb0NBQW9DO0FBQ3RDO0FBQ0E7RUFDRSx5Q0FBeUM7RUFDekMsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixVQUFVO0FBQ1o7O0FBRUEsd0JBQXdCO0FBQ3hCO0VBQ0Usd0JBQXdCO0VBQ3hCLDRCQUE0QjtFQUM1QixtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLE9BQU87RUFDUCxlQUFlO0FBQ2pCO0FBQ0E7RUFDRSw0Q0FBNEM7QUFDOUM7QUFDQTtFQUNFLHlDQUF5QztFQUN6QyxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLFVBQVU7QUFDWjtBQUNBO0VBQ0UsaURBQWdHO0VBQ2hHLDBCQUEwQjtFQUMxQixxQ0FBcUM7RUFDckMsb0NBQW9DO0FBQ3RDOztBQUVBLDBCQUEwQjtBQUMxQjtFQUNFLDBCQUEwQjtFQUMxQix3REFBd0Q7RUFDeEQsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixPQUFPO0FBQ1Q7QUFDQTtFQUNFLHVFQUF1RTtBQUN6RTtBQUNBO0VBQ0UsNkNBQTRGO0VBQzVGLDBCQUEwQjtFQUMxQixxQ0FBcUM7RUFDckMsb0NBQW9DO0FBQ3RDO0FBQ0E7RUFDRSx5Q0FBeUM7RUFDekMsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixVQUFVO0FBQ1o7O0FBRUEsd0JBQXdCO0FBQ3hCO0VBQ0Usd0JBQXdCO0VBQ3hCLDRCQUE0QjtFQUM1QixtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLE9BQU87QUFDVDtBQUNBO0VBQ0UsNENBQTRDO0FBQzlDO0FBQ0E7RUFDRSx5Q0FBeUM7RUFDekMsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsMkJBQTJCO0FBQzdCO0FBQ0E7RUFDRSw4Q0FBNkY7RUFDN0YsMEJBQTBCO0VBQzFCLHFDQUFxQztFQUNyQyxvQ0FBb0M7RUFDcEMsNEJBQTRCO0FBQzlCOztBQUVBLDBCQUEwQjtBQUMxQjtFQUNFLDBCQUEwQjtFQUMxQix3REFBd0Q7RUFDeEQsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixPQUFPO0FBQ1Q7QUFDQTtFQUNFLHVFQUF1RTtBQUN6RTtBQUNBO0VBQ0UsY0FBYztFQUNkLGNBQWM7RUFDZCx3Q0FBd0M7RUFDeEMsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixXQUFXO0FBQ2I7QUFDQTtFQUNFLDBDQUF5RjtFQUN6RiwwQkFBMEI7RUFDMUIsb0NBQW9DO0VBQ3BDLG9DQUFvQztBQUN0Qzs7QUFFQSx3QkFBd0I7QUFDeEI7RUFDRSx3QkFBd0I7RUFDeEIsNEJBQTRCO0VBQzVCLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixxQkFBcUI7QUFDdkI7QUFDQTtFQUNFLDRDQUE0QztBQUM5QztBQUNBO0VBQ0UsMENBQXlGO0VBQ3pGLDBCQUEwQjtFQUMxQixvQ0FBb0M7RUFDcEMsb0NBQW9DO0FBQ3RDOztBQUVBLDBCQUEwQjtBQUMxQjtFQUNFLDBCQUEwQjtFQUMxQix3REFBd0Q7RUFDeEQsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixPQUFPO0FBQ1Q7QUFDQTtFQUNFLHVFQUF1RTtBQUN6RTtBQUNBO0VBQ0UsY0FBYztFQUNkLGNBQWM7RUFDZCx3Q0FBd0M7RUFDeEMsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsV0FBVztBQUNiO0FBQ0E7RUFDRSwrQ0FBOEY7RUFDOUYsMEJBQTBCO0VBQzFCLHFDQUFxQztFQUNyQyxvQ0FBb0M7QUFDdEM7O0FBRUEsd0JBQXdCO0FBQ3hCO0VBQ0Usd0JBQXdCO0VBQ3hCLDRCQUE0QjtFQUM1QixtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIscUJBQXFCO0FBQ3ZCO0FBQ0E7RUFDRSw0Q0FBNEM7QUFDOUM7QUFDQTtFQUNFLHNDQUFzQztFQUN0QyxVQUFVO0VBQ1Y7O29EQUVrRDtBQUNwRDtBQUNBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7RUFDWCx1QkFBdUI7QUFDekI7OztBQUdBLGdCQUFnQjtBQUNoQjtFQUNFLG9CQUFvQjtFQUNwQiwwQkFBMEI7RUFDMUIsd0RBQXdEO0VBQ3hELGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLHVFQUF1RTtBQUN6RTs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQix3QkFBd0I7RUFDeEIsNEJBQTRCO0VBQzVCLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLDRDQUE0QztBQUM5Qzs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQiwwQkFBMEI7RUFDMUIsd0RBQXdEO0VBQ3hELGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLHVFQUF1RTtBQUN6RTs7Ozs7QUFLQSxLQUFLO0FBQ0w7Ozs7Ozs7Ozs7O0dBV0c7Ozs7QUFJSCxxQkFBcUI7QUFDckI7RUFDRSxjQUFjO0VBQ2QseUNBQXlDO0VBQ3pDLGdCQUFnQjtFQUNoQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsMENBQXlGO0VBQ3pGLDBCQUEwQjtFQUMxQixxQ0FBcUM7RUFDckMsU0FBUztFQUNULFFBQVE7RUFDUixvQ0FBb0M7QUFDdEM7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLGtCQUFrQjtFQUNsQixRQUFRO0FBQ1Y7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBdUJHOztBQUVIO0VBQ0UsaUJBQWlCO0VBQ2pCLGlDQUFpQztFQUNqQyxlQUFlO0VBQ2YsNEJBQTRCO0FBQzlCOztBQUVBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osd0NBQXdDO0VBQ3hDLG1DQUFtQztFQUNuQyxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixnQ0FBZ0M7RUFDaEMsVUFBVTtBQUNaIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PUxvcmEmZGlzcGxheT1zd2FwXCIpO1xuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PUVCK0dhcmFtb25kJmRpc3BsYXk9c3dhcFwiKTtcbioge1xuICBtYXJnaW46IDA7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG5cbi8qIC5pbnN0cnVjdGlvbiwgLmJvb2sge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICBib3JkZXI6IDFweCBzb2xpZCAjMTViO1xufSAqL1xuXG4vKiBib2R5IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwdmg7XG4gIG1pbi13aWR0aDogOTAwcHg7XG4gIG1pbi1oZWlnaHQ6IDcwMHB4O1xuICBiYWNrZ3JvdW5kOiAjZmJlY2I0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59ICovXG4uZGl2aXNvciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNhOWFjZWJiO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGxlZnQ6IDUwJTtcbiAgdG9wOiAwO1xuICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDAyKTtcbn1cbi5ib3JkZXJfYm9vayB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm9yZGVyOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxKSBzb2xpZCAjM2E5YWNlO1xuICBib3JkZXItcmFkaXVzOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyKTtcbiAgaGVpZ2h0OiA5MCU7XG4gIGxlZnQ6IDUwJTtcbiAgdG9wOiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICB0cmFuc2Zvcm0tc3R5bGU6IHByZXNlcnZlLTNkO1xuICB3aWR0aDogNjUlO1xufVxuLmJvb2sge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvcmRlcjogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpIHNvbGlkICMzYTlhY2U7XG4gIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDIpO1xuICBoZWlnaHQ6IDkwJTtcbiAgbGVmdDogNTAlO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIHRyYW5zZm9ybS1zdHlsZTogcHJlc2VydmUtM2Q7XG4gIHdpZHRoOiA2NSU7XG4gIC8qIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpOyAqL1xufVxuXG5kaXYuYm9yZGVyX2dyYXlfbGVmdCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm9yZGVyLXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpO1xuICBib3JkZXItbGVmdDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMykgc29saWQgI2Q2ZDZkNjtcbiAgaGVpZ2h0OiAxMDAlO1xuICBsZWZ0OiAwJTtcbn1cbmRpdi5ib3JkZXJfZ3JheV9yaWdodCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm9yZGVyLXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTIpO1xuICBib3JkZXItcmlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDMpIHNvbGlkICNkNmQ2ZDY7XG4gIGhlaWdodDogMTAwJTtcbiAgcmlnaHQ6IDAlO1xufVxuXG4ucGFnZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgYm9yZGVyLXJpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAzKSBzb2xpZCAjZDZkNmQ2O1xuICBib3JkZXItbGVmdDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMykgc29saWQgI2Q2ZDZkNjtcbiAgYm9yZGVyLWxlZnQ6IG5vbmU7XG4gIGhlaWdodDogMTAwJTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogMDtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDFzO1xuICB3aWR0aDogNTAlO1xufVxuXG4ubGVmdC1zaWRlIHtcbiAgYm9yZGVyLXJpZ2h0OiBub25lO1xuICBib3JkZXItbGVmdDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMykgc29saWQgI2Q2ZDZkNjtcbn1cblxuLnBhZ2VbX25nY29udGVudC1heWMtYzE2XTpudGgtY2hpbGQoMm4rMSkge1xuICBib3JkZXItbGVmdDogMXB4IHNvbGlkICMzMzM1O1xufVxuXG4vKiBQw4PCoWdpbmEgMSAob2N1bHRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDEpIHtcbiAgcGFkZGluZzogMyUgMyUgMyUgMyU7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDAlIDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KC0xcHgpO1xuICBiYWNrZ3JvdW5kOiAjOWJjOGQ2O1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMSkge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooMXB4KSByb3RhdGV5KC0xODBkZWcpO1xufVxuXG4vKiBQw4PCoWdpbmEgMiAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDIpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMTAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtMnB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSk7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1pdGVtczogY2VudGVyO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMikge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooMnB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufVxuLnBhZ2UucGFnZTIgLmxvZ28xIHtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9sZXNzb24zL3ZpZXcxL2ltYWdlcy9Mb2dvVmFscGFyYWlzby5wbmcpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4yKTtcbiAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMzUpO1xufVxuLnBhZ2UucGFnZTIgLmltYWdlMSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMy92aWV3MS9pbWFnZXMvbG9nb3MxLnBuZykgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA3KTtcbiAgd2lkdGg6IDk1JTtcbn1cblxuLyogUMODwqFnaW5hIDMgKGRlcmVjaGEpICovXG4ucGFnZTpudGgtY2hpbGQoMykge1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtM3B4KTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgZGlzcGxheTogZ3JpZDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG59XG4ubGVmdC1zaWRlOm50aC1jaGlsZCgzKSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigzcHgpIHJvdGF0ZXkoLTE4MGRlZyk7XG59XG4ucGFnZS5wYWdlMyAudGl0bGV7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgY29sb3I6ICNFRDZCMDY7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDgpO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBsZWZ0OiAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRvcDogNDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5wYWdlLnBhZ2UzIC50ZXh0e1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDI1KTtcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgd2lkdGg6IDc1LjMlO1xuICBjb2xvcjogIzAwMFxufVxuXG4vKiBQw4PCoWdpbmEgNCAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDQpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMTAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtNHB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSk7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG4ubGVmdC1zaWRlOm50aC1jaGlsZCg0KSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleig0cHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKSByb3RhdGV5KDE4MGRlZyk7XG59XG4ucGFnZS5wYWdlNCAudGl0bGV7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgY29sb3I6ICNFRDZCMDY7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDM4KTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgbGVmdDogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0b3A6IDUlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5wYWdlLnBhZ2U0IC50aXRsZTJ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgY29sb3I6ICNFRDZCMDY7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDIpO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbGVmdDogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0b3A6IDEzJTtcblxuICB3aWR0aDogMTAwJTtcbn1cbi5wYWdlLnBhZ2U0IC50ZXh0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDI1KTtcbiAgbGVmdDogNSU7XG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gIHRvcDogMjglO1xuICB3aWR0aDogOTAlO1xufVxuXG4vKiBQw4PCoWdpbmEgNSAoZGVyZWNoYSkgKi9cbi5wYWdlOm50aC1jaGlsZCg1KSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDAlIDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KC01cHgpO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoNSkge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooNXB4KSByb3RhdGV5KC0xODBkZWcpO1xufVxuLnBhZ2UucGFnZTUgLnRleHQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjUpO1xuICBsZWZ0OiA1JTtcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgdG9wOiA2JTtcbiAgd2lkdGg6IDkwJTtcbn1cblxuLnBhZ2UucGFnZTUgLmltYWdlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjMvdmlldzEvaW1hZ2VzL2dpZl9kZV9ob2phcy5naWYpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogNTAlO1xuICBsZWZ0OiAxNCU7XG4gIHRvcDogMzclO1xuICB3aWR0aDogNzAlO1xufVxuXG4vKiBQw4PCoWdpbmEgNiAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDYpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMTAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtNnB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSk7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1pdGVtczogY2VudGVyO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoNikge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooNnB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufVxuLnBhZ2UucGFnZTYgLnRpdGxle1xuICBjb2xvcjogI0VENkIwNjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMzgpO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnBhZ2UucGFnZTYgLnRpdGxlMntcbiAgY29sb3I6ICNFRDZCMDY7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDIpO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbn1cbi5wYWdlLnBhZ2U2IC50ZXh0IHtcbiAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyMyk7XG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gIHdpZHRoOiA5MCU7XG59XG5cblxuLyogUMODwqFnaW5hIDcgKGRlcmVjaGEpICovXG4ucGFnZTpudGgtY2hpbGQoNykge1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtN3B4KTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLmxlZnQtc2lkZTpudGgtY2hpbGQoNykge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooN3B4KSByb3RhdGV5KC0xODBkZWcpO1xufVxuLnBhZ2UucGFnZTcgLnRleHQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjMpO1xuICBsZWZ0OiA1JTtcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgdG9wOiA1JTtcbiAgd2lkdGg6IDkwJTtcbn1cblxuLyogUMODwqFnaW5hIDggKGl6cXVpZXJkYSkgKi9cbi5wYWdlOm50aC1jaGlsZCg4KSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLThweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoOCkge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooOHB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufVxuLnBhZ2UucGFnZTggLnRpdGxle1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGNvbG9yOiAjRUQ2QjA2O1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA4KTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgbGVmdDogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0b3A6IDUlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5wYWdlLnBhZ2U4IC50ZXh0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDI1KTtcbiAgbGVmdDogNSU7XG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gIHRvcDogMjglO1xuICB3aWR0aDogOTAlO1xufVxuLyogUMODwqFnaW5hIDkgKGRlcmVjaGEpICovXG4ucGFnZTpudGgtY2hpbGQoOSkge1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtOXB4KTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDkpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDlweCkgcm90YXRleSgtMTgwZGVnKTtcbn1cbi5wYWdlLnBhZ2U5IC5pbWFnZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9sZXNzb24zL3ZpZXcxL2ltYWdlcy9waWN0dXJlMjAucG5nKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBoZWlnaHQ6IDg1JTtcbiAgbGVmdDogNSU7XG4gIHRvcDogOSU7XG4gIHdpZHRoOiA5MCU7XG59XG5cbi8qIFDDg8KhZ2luYSAxMCAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDEwKSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTEwcHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgZGlzcGxheTogZ3JpZDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG59XG4ubGVmdC1zaWRlOm50aC1jaGlsZCgxMCkge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooMTBweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpIHJvdGF0ZXkoMTgwZGVnKTtcbn1cbi5wYWdlLnBhZ2UxMCAudGl0bGV7XG4gIGNvbG9yOiAjRUQ2QjA2O1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA4KTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbn1cbi5wYWdlLnBhZ2UxMCAuaW1hZ2Uge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjMvdmlldzEvaW1hZ2VzL0FuZXhvQS0yLmpwZykgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjQyKTtcbiAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuNDApO1xufVxuXG4vKiBQw4PCoWdpbmEgMTEgKGRlcmVjaGEpICovXG4ucGFnZTpudGgtY2hpbGQoMTEpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTExcHgpO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMTEpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDExcHgpIHJvdGF0ZXkoLTE4MGRlZyk7XG59XG4ucGFnZS5wYWdlMTEgLmltYWdlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjMvdmlldzEvaW1hZ2VzL3BpY3R1cmUyMi5wbmcpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogODUlO1xuICBsZWZ0OiA1JTtcbiAgdG9wOiA5JTtcbiAgd2lkdGg6IDkwJTtcbn1cbi8qIFDDg8KhZ2luYSAxMiAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDEyKSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTEycHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDEyKSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigxMnB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufVxuLnBhZ2UucGFnZTEyIC50aXRsZXtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBjb2xvcjogI0VENkIwNjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wOCk7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGxlZnQ6IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdG9wOiA1JTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ucGFnZS5wYWdlMTIgLnRleHQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjUpO1xuICBsZWZ0OiAxMCU7XG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gIHRvcDogMjglO1xuICB3aWR0aDogODAlO1xufVxuXG4vKiBQw4PCoWdpbmEgMTMgKGRlcmVjaGEpICovXG4ucGFnZTpudGgtY2hpbGQoMTMpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTEzcHgpO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBkaXNwbGF5OiBncmlkO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktaXRlbXM6IGNlbnRlcjtcbn1cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDEzKSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigxM3B4KSByb3RhdGV5KC0xODBkZWcpO1xufVxuLnBhZ2UucGFnZTEzIC5pbWFnZSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMy92aWV3MS9pbWFnZXMvcGljdHVyZTIwLnBuZykgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjYpO1xuICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC40Nyk7XG59XG5cbi8qIFDDg8KhZ2luYSAxNCAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDE0KSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTE0cHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgZGlzcGxheTogZ3JpZDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiA2JTtcbn1cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDE0KSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigxNHB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufVxuLnBhZ2UucGFnZTE0IC50aXRsZXtcbiAgY29sb3I6ICNFRDZCMDY7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDgpO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnBhZ2UucGFnZTE0IC5pbWFnZSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMy92aWV3MS9pbWFnZXMvQW5leG9CLTIuanBnKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuNTApO1xuICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4zNSk7XG59XG5cbi8qIFDDg8KhZ2luYSAxNSAoZGVyZWNoYSkgKi9cbi5wYWdlOm50aC1jaGlsZCgxNSkge1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtMTVweCk7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG4ubGVmdC1zaWRlOm50aC1jaGlsZCgxNSkge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooMTVweCkgcm90YXRleSgtMTgwZGVnKTtcbn1cbi5wYWdlLnBhZ2UxNSAuaW1hZ2Uge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMy92aWV3MS9pbWFnZXMvcGljdHVyZTE5LnBuZykgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgaGVpZ2h0OiA4NSU7XG4gIGxlZnQ6IDUlO1xuICB0b3A6IDklO1xuICB3aWR0aDogOTAlO1xufVxuXG5cbi8qIFDDg8KhZ2luYSAxNiAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDE2KSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTE2cHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDE2KSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigxNnB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufVxuLnBhZ2UucGFnZTE2IC50aXRsZXtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBjb2xvcjogI0VENkIwNjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wOCk7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGxlZnQ6IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdG9wOiA1JTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ucGFnZS5wYWdlMTYgLnRleHQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjUpO1xuICBsZWZ0OiAxMCU7XG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gIHRvcDogMjglO1xuICB3aWR0aDogODAlO1xufVxuXG4vKiBQw4PCoWdpbmEgMTcgKGRlcmVjaGEpICovXG4ucGFnZTpudGgtY2hpbGQoMTcpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTE3cHgpO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMTcpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDE3cHgpIHJvdGF0ZXkoLTE4MGRlZyk7XG59XG4ucGFnZS5wYWdlMTcgLmltYWdlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjMvdmlldzEvaW1hZ2VzL3BpY3R1cmUxNy5wbmcpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogODUlO1xuICBsZWZ0OiA1JTtcbiAgdG9wOiA5JTtcbiAgd2lkdGg6IDkwJTtcbn1cblxuLyogUMODwqFnaW5hIDE4IChpenF1aWVyZGEpICovXG4ucGFnZTpudGgtY2hpbGQoMTgpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMTAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtMThweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMTgpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDE4cHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKSByb3RhdGV5KDE4MGRlZyk7XG59XG4ucGFnZS5wYWdlMTggLnRpdGxle1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGNvbG9yOiAjRUQ2QjA2O1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA4KTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgbGVmdDogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0b3A6IDUlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5wYWdlLnBhZ2UxOCAudGV4dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyNSk7XG4gIGxlZnQ6IDEwJTtcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgdG9wOiAyOCU7XG4gIHdpZHRoOiA4MCU7XG59XG5cbi8qIFDDg8KhZ2luYSAxOSAoZGVyZWNoYSkgKi9cbi5wYWdlOm50aC1jaGlsZCgxOSkge1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtMTlweCk7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1pdGVtczogY2VudGVyO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMTkpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDE5cHgpIHJvdGF0ZXkoLTE4MGRlZyk7XG59XG4ucGFnZS5wYWdlMTkgLmltYWdlIHtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9sZXNzb24zL3ZpZXcxL2ltYWdlcy9waWN0dXJlMTYucG5nKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuNTUpO1xuICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4zNSk7XG59XG5cbi8qIFDDg8KhZ2luYSAyMCAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDIwKSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTIwcHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDIwKSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigyMHB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufVxuLnBhZ2UucGFnZTIwIC50ZXh0e1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjUpO1xuICBsZWZ0OiAxMCU7XG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gIHRvcDogMjglO1xuICB3aWR0aDogODAlO1xufVxuXG4vKiBQw4PCoWdpbmEgMjEgKGRlcmVjaGEpICovXG4ucGFnZTpudGgtY2hpbGQoMjEpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTIxcHgpO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMjEpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDIxcHgpIHJvdGF0ZXkoLTE4MGRlZyk7XG59XG4ucGFnZS5wYWdlMjEgLnRleHR7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyNSk7XG4gIGxlZnQ6IDUlO1xuICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICB0b3A6IDE4JTtcbiAgd2lkdGg6IDkwJTtcbn1cbi5wYWdlLnBhZ2UyMSAuaW1hZ2Uge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMy92aWV3MS9pbWFnZXMvZ2lmX21lZGlkb3IuZ2lmKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMzApO1xuICBsZWZ0OiAxNCU7XG4gIHRvcDogNDIlO1xuICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC40MCk7XG59XG5cbi8qIFDDg8KhZ2luYSAyMiAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDIyKSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTIycHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgZGlzcGxheTogZ3JpZDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gIGdhcDogMiU7XG59XG4ubGVmdC1zaWRlOm50aC1jaGlsZCgyMikge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooMjJweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpIHJvdGF0ZXkoMTgwZGVnKTtcbn1cbi5wYWdlLnBhZ2UyMiAuaW1hZ2Uge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjMvdmlldzEvaW1hZ2VzL2dpZl92aXNpYmlsaXR5LmdpZikgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjI1KTs7XG4gIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjMwKTs7XG59XG4ucGFnZS5wYWdlMjIgLnRleHR7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjUpO1xuICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICB3aWR0aDogODglO1xufVxuXG4vKiBQw4PCoWdpbmEgMjMgKGRlcmVjaGEpICovXG4ucGFnZTpudGgtY2hpbGQoMjMpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTIzcHgpO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBkaXNwbGF5OiBncmlkO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktaXRlbXM6IGNlbnRlcjtcbiAgZ2FwOiA1JTtcbiAgcGFkZGluZy10b3A6IDMlO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMjMpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDIzcHgpIHJvdGF0ZXkoLTE4MGRlZyk7XG59XG4ucGFnZS5wYWdlMjMgLnRleHR7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjUpO1xuICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICB3aWR0aDogODglO1xufVxuLnBhZ2UucGFnZTIzIC5pbWFnZSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMy92aWV3MS9pbWFnZXMvZ2lmX2NvbXVuaWNhdGlvbi5naWYpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4yMik7XG4gIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjMwKTtcbn1cblxuLyogUMODwqFnaW5hIDI0IChpenF1aWVyZGEpICovXG4ucGFnZTpudGgtY2hpbGQoMjQpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMTAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtMjRweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBkaXNwbGF5OiBncmlkO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktaXRlbXM6IGNlbnRlcjtcbiAgZ2FwOiA0JTtcbn1cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDI0KSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigyNHB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufVxuLnBhZ2UucGFnZTI0IC5pbWFnZSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMy92aWV3MS9pbWFnZXMvZ2lmX2ZhY2lsaXR5LmdpZikgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjMwKTtcbiAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuNDApO1xufVxuLnBhZ2UucGFnZTI0IC50ZXh0e1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDI1KTtcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgd2lkdGg6IDg4JTtcbn1cblxuLyogUMODwqFnaW5hIDI1IChkZXJlY2hhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDI1KSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDAlIDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KC0yNXB4KTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgZGlzcGxheTogZ3JpZDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gIGdhcDogNiU7XG59XG4ubGVmdC1zaWRlOm50aC1jaGlsZCgyNSkge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooMjVweCkgcm90YXRleSgtMTgwZGVnKTtcbn1cbi5wYWdlLnBhZ2UyNSAudGV4dHtcbiAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyNSk7XG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gIHdpZHRoOiA4OCU7XG4gIC8qIGJvcmRlcjogMXB4IHNvbGlkIHJlZDsgKi9cbn1cbi5wYWdlLnBhZ2UyNSAuaW1hZ2Uge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjMvdmlldzEvaW1hZ2VzL2dpZl9wcmVjaXNpb24uZ2lmKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMzApO1xuICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC40MCk7XG4gIC8qIGJvcmRlcjogMXB4IHNvbGlkIGJsdWU7ICovXG59XG5cbi8qIFDDg8KhZ2luYSAyNiAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDI2KSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTI2cHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgZGlzcGxheTogZ3JpZDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gIGdhcDogMyU7XG59XG4ubGVmdC1zaWRlOm50aC1jaGlsZCgyNikge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooMjZweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpIHJvdGF0ZXkoMTgwZGVnKTtcbn1cbi5wYWdlLnBhZ2UyNiAudGl0bGV7XG4gIGNvbG9yOiAjRUQ2QjA2O1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA0KTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbn1cbi5wYWdlLnBhZ2UyNiAuaW1hZ2Uge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2xlc3NvbjMvdmlldzEvaW1hZ2VzL3BpY3R1cmUxNS5wbmcpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC41KTtcbiAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMzYpO1xufVxuXG4vKiBQw4PCoWdpbmEgMjcgKGRlcmVjaGEpICovXG4ucGFnZTpudGgtY2hpbGQoMjcpIHtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTI3cHgpO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBkaXNwbGF5OiBncmlkO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktaXRlbXM6IGNlbnRlcjtcbn1cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDI3KSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigyN3B4KSByb3RhdGV5KC0xODBkZWcpO1xufVxuLnBhZ2UucGFnZTI3IC5pbWFnZSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMy92aWV3MS9pbWFnZXMvcGljdHVyZTE0LnBuZykgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjUpO1xuICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4zNSk7XG59XG5cbi8qIFDDg8KhZ2luYSAyOCAoaXpxdWllcmRhKSAqL1xuLnBhZ2U6bnRoLWNoaWxkKDI4KSB7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTI4cHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgZGlzcGxheTogZ3JpZDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gIGdhcDogOCU7XG59XG4ubGVmdC1zaWRlOm50aC1jaGlsZCgyOCkge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooMjhweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpIHJvdGF0ZXkoMTgwZGVnKTtcbn1cbi5wYWdlLnBhZ2UyOCAudGl0bGUge1xuICBjb2xvcjogI0VENkIwNjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNCk7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxMTAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnBhZ2UucGFnZTI4IC5pbWFnZSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbGVzc29uMy92aWV3MS9pbWFnZXMvZ2lmX2Zvcm11bGFyaW8uZ2lmKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMzUpO1xuICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4zNSk7XG59XG5cbi8qIFDDg8KhZ2luYSAyOSAoZGVyZWNoYSkgKi9cbi5wYWdlOm50aC1jaGlsZCgyOSkge1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtMjlweCk7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1pdGVtczogY2VudGVyO1xufVxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMjkpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDI5cHgpIHJvdGF0ZXkoLTE4MGRlZyk7XG59XG4ucGFnZS5wYWdlMjkgLnZpZGVvIHtcbiAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjI3OCk7XG4gIHdpZHRoOiA5MCU7XG4gIGJveC1zaGFkb3c6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKi0wLjAxNSkgXG4gICAgICAgICAgICAgIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDI1KSBcbiAgICAgICAgICAgICAgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNzUpICMzYjNiM2I7XG59XG4ucGFnZS5wYWdlMjkgLnZpZGVvIHZpZGVvIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG59XG5cblxuLyogRmluYWwgdmFjaWEgKi9cbi5wYWdlOm50aC1jaGlsZCgzMCkge1xuICBwYWRkaW5nOiA1JSA1JSA0JSA0JTtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMTAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtMjlweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpO1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xufVxuXG4ubGVmdC1zaWRlOm50aC1jaGlsZCgzMCkge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooMjlweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpIHJvdGF0ZXkoMTgwZGVnKTtcbn1cblxuLnBhZ2U6bnRoLWNoaWxkKDMxKSB7XG4gIHBhZGRpbmc6IDUlIDQlIDQlIDUlO1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtMjVweCk7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG59XG5cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDMxKSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigyNXB4KSByb3RhdGV5KC0xODBkZWcpO1xufVxuXG4ucGFnZTpudGgtY2hpbGQoMzIpIHtcbiAgcGFkZGluZzogNSUgNSUgNCUgNCU7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTI2cHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2NjYztcbn1cblxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMzIpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDI2cHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKSByb3RhdGV5KDE4MGRlZyk7XG59XG5cblxuXG5cbi8qICAqL1xuLyogLnBhZ2UubGFzdCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG4ucGFnZTpudGgtY2hpbGQoMjIpIHtcbiAgcGFkZGluZzogNSUgNSUgNCUgNCU7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTEycHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2NjYztcbn1cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDIyKSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigxMnB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufSAqL1xuXG5cblxuLyogUG9ydGFkYSAob2N1bHRhKSAqL1xuLnBhZ2UucG9ydGFkYSAudGl0bGV7XG4gIGNvbG9yOiAjM2Q3ZDllO1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDg1KTtcbiAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5kaXYuaW1hZ2VfcG9ydGFkYSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9sZXNzb24zL3ZpZXcxL2ltYWdlcy9wcm90b2NvbG8ucG5nKSBuby1yZXBlYXQ7ICBcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4yMCk7XG4gIGxlZnQ6IDMzJTtcbiAgdG9wOiAzNyU7XG4gIHdpZHRoOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjIwKTtcbn1cblxuLnBhZ2UucG9ydGFkYSAuc3ViX3RpdGxle1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDMlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRvcDogNzIlO1xufVxuXG4vKiAuY292ZXItZnJvbnQ6bnRoLWNoaWxkKDEpIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoI2FmZDJkZCwgI2ExZDVlNSkgNTAlIDUwJS84MCUgODYlIG5vLXJlcGVhdCwgbGluZWFyLWdyYWRpZW50KCNmZmZmZmYsICNmZmZmZmYpIDUwJSA1MCUvOTAlIDkzJSBuby1yZXBlYXQsIGxpbmVhci1ncmFkaWVudCgjOWJjOGQ2LCAjOWJjOGQ2KSBuby1yZXBlYXQ7XG59XG4uY292ZXItZnJvbnQ6bnRoLWNoaWxkKDEpIGgxIHtcbiAgZm9udC1mYW1pbHk6IFwiTG9yYVwiLCBzZXJpZjtcbiAgZm9udC1zaXplOiA0NXB4O1xuICBjb2xvcjogIzEzNGE3ODtcbiAgbWFyZ2luLXRvcDogMzBweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5jb3Zlci1mcm9udDpudGgtY2hpbGQoMSkgaDIge1xuICBmb250LWZhbWlseTogXCJFQiBHYXJhbW9uZFwiLCBzZXJpZjtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBjb2xvcjogIzEzNGE3ODtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDQwJTtcbn1cbi5jb3Zlci1mcm9udDpudGgtY2hpbGQoMSkgLmhhdCB7XG4gIGZvbnQtc2l6ZTogMTAwcHg7XG4gIGNvbG9yOiAjMTM0YTc4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMyU7XG59ICovXG5cbnAge1xuICBtYXJnaW4tYm90dG9tOiA0JTtcbiAgZm9udC1mYW1pbHk6IFwiRUIgR2FyYW1vbmRcIiwgc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgLyogYm9yZGVyOiAxcHggc29saWQgYmx1ZTsgKi9cbn1cblxuLnBhZ2U6bnRoLWNoaWxkKDMpIHA6Zmlyc3Qtb2YtdHlwZTpmaXJzdC1sZXR0ZXIge1xuICBmb250LXNpemU6IDMycHg7XG59XG5cbi5pbnN0cnVjdGlvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgY29sb3I6ICMwMDA5O1xuICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDQpO1xuICBsZWZ0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjYzKTtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRvcDogNDklO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgd2lkdGg6IDI3JTtcbn1cblxuIl0sInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 9159:
/*!****************************************************!*\
  !*** ./src/app/shop/inside/menu/menu.component.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MenuComponent": () => (/* binding */ MenuComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 4666);


function MenuComponent_div_0_Template(rf, ctx) {
  if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 1)(1, "div", 2)(2, "div", 3)(3, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "MEN\u00DA");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mouseenter", function MenuComponent_div_0_Template_div_mouseenter_5_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2);
      const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](ctx_r1.hoverBtnPostMenu());
    })("mouseleave", function MenuComponent_div_0_Template_div_mouseleave_5_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2);
      const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](ctx_r3.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mouseenter", function MenuComponent_div_0_Template_div_mouseenter_6_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2);
      const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](ctx_r4.hoverBtnPostMenu());
    })("mouseleave", function MenuComponent_div_0_Template_div_mouseleave_6_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2);
      const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](ctx_r5.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mouseenter", function MenuComponent_div_0_Template_div_mouseenter_7_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2);
      const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](ctx_r6.hoverBtnPostMenu());
    })("mouseleave", function MenuComponent_div_0_Template_div_mouseleave_7_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2);
      const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](ctx_r7.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mouseenter", function MenuComponent_div_0_Template_div_mouseenter_8_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2);
      const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](ctx_r8.hoverBtnPostMenu());
    })("mouseleave", function MenuComponent_div_0_Template_div_mouseleave_8_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2);
      const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](ctx_r9.leaveBtnPostMenu());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MenuComponent_div_0_Template_div_click_9_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2);
      const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](ctx_r10.closeMenuModal());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()()()();
  }
}
class MenuComponent {
  constructor() {
    this.showModalMenu = false;
    this.stateSoundMenu = 'active';
    this.hoverAudio = new Audio(`../../assets/section1/sounds/hoverDefault.mp3`);
    this.clickAudio = new Audio(`../../assets/section1/sounds/clickDefault.mp3`);
  }
  closeMenuModal() {
    this.showModalMenu = false;
  }
  hoverBtnPostMenu() {
    if (this.stateSoundMenu === 'active') {
      this.hoverAudio.play();
    }
  }
  leaveBtnPostMenu() {
    this.hoverAudio.pause();
    this.hoverAudio.currentTime = 0;
    //this.clickAudio.play();
  }
}

MenuComponent.ɵfac = function MenuComponent_Factory(t) {
  return new (t || MenuComponent)();
};
MenuComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
  type: MenuComponent,
  selectors: [["app-menu"]],
  inputs: {
    showModalMenu: "showModalMenu"
  },
  decls: 1,
  vars: 1,
  consts: [["id", "modalMenu", 4, "ngIf"], ["id", "modalMenu"], [1, "centerDiv"], ["id", "modalMenuContainer"], [1, "menu_title"], [1, "menu_module_1_info", "active", 3, "mouseenter", "mouseleave"], [1, "menu_module_2_info", "active", 3, "mouseenter", "mouseleave"], [1, "menu_module_3_info", "active", 3, "mouseenter", "mouseleave"], [1, "menu_module_4_info", "active", 3, "mouseenter", "mouseleave"], ["type", "button", 1, "button_close", 3, "click"]],
  template: function MenuComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, MenuComponent_div_0_Template, 10, 0, "div", 0);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.showModalMenu);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.NgIf],
  styles: ["#modalMenu[_ngcontent-%COMP%] {\n    position: fixed;\n    align-items: center;\n    background-color: #000a;\n    display: flex;\n    height: 100vh;\n    justify-content: center;\n    left: 0;\n    top: 0;\n    width: 100vw;\n    z-index: 99;\n}\n.centerDiv[_ngcontent-%COMP%] {\n    position: absolute;\n    height: var(--frameHeight);\n    width: var(--frameWidth);\n}\n#modalMenuContainer[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('fondo_menu_desplegable.png')\n    no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.8);\n    left: 26%;\n    top: 11%;\n    width: calc(var(--frameHeight)*1);\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_title[_ngcontent-%COMP%] {\n    position: absolute;\n    color: #198195;\n    font-family: var(--font-family-light);\n    font-size: calc(var(--frameHeight)*0.1);\n    left: 12%;\n    top: 2%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_1_info.active[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('Mod1_activado.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.16);\n    left: 32.5%;\n    top: 18%;\n    width: calc(var(--frameHeight)*0.33);\n    transition: all 0.3s ease-in-out;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_1_info.inactive[_ngcontent-%COMP%]{\n    background: url('Mod1_desactivado.png') no-repeat;\n    background-size: 100% 100%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_1_info[_ngcontent-%COMP%]:hover {\n    background: url('Mod1_hover.png') no-repeat;\n    background-size: 100% 100%;\n    cursor: pointer;\n    transform: scale(1.1) translate(calc(var(--frameHeight)*0.01), calc(var(--frameHeight)*0.023));\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_2_info.active[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('Mod2_activado.png') no-repeat;\n    background-size: 100% 100%;\n    clip-path: polygon(0 0, 100% 0, 100% 100%, 89% 100%, 89% 72%, 0% 72%);\n    height: calc(var(--frameHeight)*0.15);\n    left: 36.2%;\n    top: 40%;\n    width: calc(var(--frameHeight)*0.21);\n    transition: all 0.3s ease-in-out;\n    z-index: 1;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_2_info.inactive[_ngcontent-%COMP%]{\n    background: url('Mod2_desactivado.png') no-repeat;\n    background-size: 100% 100%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_2_info[_ngcontent-%COMP%]:hover {\n    background: url('Mod2_hover.png') no-repeat;\n    background-size: 100% 100%;\n    cursor: pointer;\n    transform: scale(1.05) translate(calc(var(--frameHeight)*0), calc(var(--frameHeight)*0.023));\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_3_info.active[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('Mod3_activado.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.13);\n    left: 24.2%;\n    top: 54%;\n    width: calc(var(--frameHeight)*0.21);\n    transition: all 0.3s ease-in-out;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_3_info.inactive[_ngcontent-%COMP%]{\n    background: url('Mod3_desactivado.png') no-repeat;\n    background-size: 100% 100%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_3_info[_ngcontent-%COMP%]:hover {\n    background: url('Mod3_hover.png') no-repeat;\n    background-size: 100% 100%;\n    cursor: pointer;\n    transform: scale(1.1) translate(calc(var(--frameHeight)*0.01), calc(var(--frameHeight)*0.023));\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_4_info.active[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('Mod4_activado.png') no-repeat;\n    background-size: 100% 100%;\n    height: calc(var(--frameHeight)*0.16);\n    left: 29.6%;\n    top: 78.5%;\n    width: calc(var(--frameHeight)*0.28);\n    transition: all 0.3s ease-in-out;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_4_info.inactive[_ngcontent-%COMP%]{\n    background: url('Mod4_desactivado.png') no-repeat;\n    background-size: 100% 100%;\n}\n#modalMenuContainer[_ngcontent-%COMP%]   .menu_module_4_info[_ngcontent-%COMP%]:hover {\n    background: url('Mod4_hover.png') no-repeat;\n    background-size: 100% 100%;\n    cursor: pointer;\n    transform: scale(1.1) translate(calc(var(--frameHeight)*-0.01), calc(var(--frameHeight)*-0.005));\n}\n.button_close[_ngcontent-%COMP%] {\n    position: absolute;\n    background-image: url('equis.png');\n    background-size: 100% 100%;\n    background-repeat: no-repeat;\n    height: calc(var(--frameHeight)*0.05);\n    left: 84%;\n    top: 6%;\n    transition: all 0.1s ease-in-out;\n    width: calc(var(--frameHeight)*0.05);\n}\n.button_close[_ngcontent-%COMP%]:hover {\n    background-image: url('equis_hover.png');\n    background-size: 100% 100%;\n    background-repeat: no-repeat;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hvcC9pbnNpZGUvbWVudS9tZW51LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsK0JBQStCO0FBQy9CO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsT0FBTztJQUNQLE1BQU07SUFDTixZQUFZO0lBQ1osV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsMEJBQTBCO0lBQzFCLHdCQUF3QjtBQUM1QjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCO2FBQ1M7SUFDVCwwQkFBMEI7SUFDMUIsb0NBQW9DO0lBQ3BDLFNBQVM7SUFDVCxRQUFRO0lBQ1IsaUNBQWlDO0FBQ3JDO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLHFDQUFxQztJQUNyQyx1Q0FBdUM7SUFDdkMsU0FBUztJQUNULE9BQU87QUFDWDtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDhDQUErRjtJQUMvRiwwQkFBMEI7SUFDMUIscUNBQXFDO0lBQ3JDLFdBQVc7SUFDWCxRQUFRO0lBQ1Isb0NBQW9DO0lBQ3BDLGdDQUFnQztBQUNwQztBQUNBO0lBQ0ksaURBQWtHO0lBQ2xHLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksMkNBQTRGO0lBQzVGLDBCQUEwQjtJQUMxQixlQUFlO0lBQ2YsOEZBQThGO0FBQ2xHO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsOENBQStGO0lBQy9GLDBCQUEwQjtJQUMxQixxRUFBcUU7SUFDckUscUNBQXFDO0lBQ3JDLFdBQVc7SUFDWCxRQUFRO0lBQ1Isb0NBQW9DO0lBQ3BDLGdDQUFnQztJQUNoQyxVQUFVO0FBQ2Q7QUFDQTtJQUNJLGlEQUFrRztJQUNsRywwQkFBMEI7QUFDOUI7QUFDQTtJQUNJLDJDQUE0RjtJQUM1RiwwQkFBMEI7SUFDMUIsZUFBZTtJQUNmLDRGQUE0RjtBQUNoRztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDhDQUErRjtJQUMvRiwwQkFBMEI7SUFDMUIscUNBQXFDO0lBQ3JDLFdBQVc7SUFDWCxRQUFRO0lBQ1Isb0NBQW9DO0lBQ3BDLGdDQUFnQztBQUNwQztBQUNBO0lBQ0ksaURBQWtHO0lBQ2xHLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksMkNBQTRGO0lBQzVGLDBCQUEwQjtJQUMxQixlQUFlO0lBQ2YsOEZBQThGO0FBQ2xHO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsOENBQStGO0lBQy9GLDBCQUEwQjtJQUMxQixxQ0FBcUM7SUFDckMsV0FBVztJQUNYLFVBQVU7SUFDVixvQ0FBb0M7SUFDcEMsZ0NBQWdDO0FBQ3BDO0FBQ0E7SUFDSSxpREFBa0c7SUFDbEcsMEJBQTBCO0FBQzlCO0FBQ0E7SUFDSSwyQ0FBNEY7SUFDNUYsMEJBQTBCO0lBQzFCLGVBQWU7SUFDZixnR0FBZ0c7QUFDcEc7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixrQ0FBb0U7SUFDcEUsMEJBQTBCO0lBQzFCLDRCQUE0QjtJQUM1QixxQ0FBcUM7SUFDckMsU0FBUztJQUNULE9BQU87SUFDUCxnQ0FBZ0M7SUFDaEMsb0NBQW9DO0FBQ3hDO0FBQ0E7SUFDSSx3Q0FBMEU7SUFDMUUsMEJBQTBCO0lBQzFCLDRCQUE0QjtBQUNoQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqICAgTW9kYWwgTWVudSAgICoqKioqKiovXG4jbW9kYWxNZW51IHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwYTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogMTAwdmg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMHZ3O1xuICAgIHotaW5kZXg6IDk5O1xufVxuLmNlbnRlckRpdiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogdmFyKC0tZnJhbWVIZWlnaHQpO1xuICAgIHdpZHRoOiB2YXIoLS1mcmFtZVdpZHRoKTtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc01lbnVEZXBsb3kvZm9uZG9fbWVudV9kZXNwbGVnYWJsZS5wbmcpXG4gICAgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC44KTtcbiAgICBsZWZ0OiAyNiU7XG4gICAgdG9wOiAxMSU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjEpO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV90aXRsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGNvbG9yOiAjMTk4MTk1O1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1saWdodCk7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjEpO1xuICAgIGxlZnQ6IDEyJTtcbiAgICB0b3A6IDIlO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfMV9pbmZvLmFjdGl2ZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2QxX2FjdGl2YWRvLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4xNik7XG4gICAgbGVmdDogMzIuNSU7XG4gICAgdG9wOiAxOCU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMzMpO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfMV9pbmZvLmluYWN0aXZle1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2QxX2Rlc2FjdGl2YWRvLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfMV9pbmZvOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc01lbnVEZXBsb3kvTW9kMV9ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxLjEpIHRyYW5zbGF0ZShjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxKSwgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjMpKTtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzJfaW5mby5hY3RpdmUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc01lbnVEZXBsb3kvTW9kMl9hY3RpdmFkby5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBjbGlwLXBhdGg6IHBvbHlnb24oMCAwLCAxMDAlIDAsIDEwMCUgMTAwJSwgODklIDEwMCUsIDg5JSA3MiUsIDAlIDcyJSk7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjE1KTtcbiAgICBsZWZ0OiAzNi4yJTtcbiAgICB0b3A6IDQwJTtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4yMSk7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQ7XG4gICAgei1pbmRleDogMTtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzJfaW5mby5pbmFjdGl2ZXtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc01lbnVEZXBsb3kvTW9kMl9kZXNhY3RpdmFkby5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzJfaW5mbzpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9tZW51L2J1dHRvbnNNZW51RGVwbG95L01vZDJfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMS4wNSkgdHJhbnNsYXRlKGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjApLCBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyMykpO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfM19pbmZvLmFjdGl2ZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2QzX2FjdGl2YWRvLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4xMyk7XG4gICAgbGVmdDogMjQuMiU7XG4gICAgdG9wOiA1NCU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMjEpO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfM19pbmZvLmluYWN0aXZle1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2QzX2Rlc2FjdGl2YWRvLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfM19pbmZvOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc01lbnVEZXBsb3kvTW9kM19ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxLjEpIHRyYW5zbGF0ZShjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAxKSwgY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMjMpKTtcbn1cbiNtb2RhbE1lbnVDb250YWluZXIgLm1lbnVfbW9kdWxlXzRfaW5mby5hY3RpdmUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc01lbnVEZXBsb3kvTW9kNF9hY3RpdmFkby5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBoZWlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMTYpO1xuICAgIGxlZnQ6IDI5LjYlO1xuICAgIHRvcDogNzguNSU7XG4gICAgd2lkdGg6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMjgpO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfNF9pbmZvLmluYWN0aXZle1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvbWVudS9idXR0b25zTWVudURlcGxveS9Nb2Q0X2Rlc2FjdGl2YWRvLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuI21vZGFsTWVudUNvbnRhaW5lciAubWVudV9tb2R1bGVfNF9pbmZvOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL21lbnUvYnV0dG9uc01lbnVEZXBsb3kvTW9kNF9ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxLjEpIHRyYW5zbGF0ZShjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSotMC4wMSksIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKi0wLjAwNSkpO1xufVxuLmJ1dHRvbl9jbG9zZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvYnV0dG9ucy9lcXVpcy5wbmcpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA1KTtcbiAgICBsZWZ0OiA4NCU7XG4gICAgdG9wOiA2JTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4xcyBlYXNlLWluLW91dDtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNSk7XG59XG4uYnV0dG9uX2Nsb3NlOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvZXF1aXNfaG92ZXIucG5nKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xufSJdLCJzb3VyY2VSb290IjoiIn0= */"]
});

/***/ }),

/***/ 4988:
/*!******************************************************!*\
  !*** ./src/app/shop/inside/modal/modal.component.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ModalComponent": () => (/* binding */ ModalComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ 5139);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);



class ModalComponent {
  constructor() {
    this.modalMessage = "";
    this.isClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__.EventEmitter();
  }
  ngOnInit() {}
  startModal(modalMessage) {
    console.log('--- modal --- startModal()');
    this.openModal(modalMessage);
    jquery__WEBPACK_IMPORTED_MODULE_0__(".button_continue").addClass('disabled');
    jquery__WEBPACK_IMPORTED_MODULE_0__(".button_close").addClass('disabled');
    setTimeout(function () {
      jquery__WEBPACK_IMPORTED_MODULE_0__(".button_continue").removeClass('disabled');
      jquery__WEBPACK_IMPORTED_MODULE_0__(".button_close").removeClass('disabled');
    }, 500);
  }
  /*
   * Oculta el modal ejecutado al cargar el componente
   */
  closeModal() {
    jquery__WEBPACK_IMPORTED_MODULE_0__("#modal_info").hide();
    this.isClicked.emit(true);
  }
  /*
   * Muestra el modal ejecutado al cargar el componente
   */
  openModal(modalMessage) {
    this.modalMessage = modalMessage;
    console.log('--- modal --- openModal()');
    jquery__WEBPACK_IMPORTED_MODULE_0__("#modal_info").show();
  }
}
ModalComponent.ɵfac = function ModalComponent_Factory(t) {
  return new (t || ModalComponent)();
};
ModalComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
  type: ModalComponent,
  selectors: [["app-modal"]],
  inputs: {
    modalMessage: "modalMessage"
  },
  outputs: {
    isClicked: "isClicked"
  },
  decls: 11,
  vars: 1,
  consts: [["id", "modal_info", "aria-hidden", "true", "aria-labelledby", "exampleModalToggleLabel", "tabindex", "-1", 1, "modal", "fade"], [1, "modal-dialog-advanced", "modal-dialog-centered"], [1, "modal-content", "animated", "fadeInRightBig"], [1, "modal-body"], [1, "modal-body-message"], [1, "modal-body-luck"], ["type", "button", 1, "button_continue", 3, "click"], ["type", "button", 1, "button_close", 3, "click"]],
  template: function ModalComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0)(1, "div", 1)(2, "div", 2)(3, "div", 3)(4, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, " \u00A1 Mucha suerte ! ");
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ModalComponent_Template_div_click_8_listener() {
        return ctx.closeModal();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "CONTINUAR");
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ModalComponent_Template_div_click_10_listener() {
        return ctx.closeModal();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.modalMessage, " ");
    }
  },
  styles: [".fadeInRightBig[_ngcontent-%COMP%] {\n  animation-name: fadeInRightBig;\n  \n}\n.animated[_ngcontent-%COMP%] {\n  animation-duration: 1s;\n  animation-fill-mode: both;\n}\n\n#modal_info[_ngcontent-%COMP%]{\n    position: absolute;\n    align-items: center;\n    background-color: #5c5c5ccc !important;\n    border: none;\n    color: white;\n    display: block;\n    font-family: var(--font-family-light);\n    opacity: 100%;\n    text-align: center;  \n}\n\n.modal-dialog-advanced[_ngcontent-%COMP%] {\n    display: grid;\n    height: 100%;\n    place-items: center;\n    width: 100%;\n}\n\n.modal-content[_ngcontent-%COMP%] {\n    border: none;\n    \n    background-image: url('modal_completo.png');\n    background-size: 100% 100%;\n    background-color: transparent;\n    height: 90%;\n    padding: 0;\n    width: 70%;\n}\n\n.modal-title-1[_ngcontent-%COMP%] {\n    position: absolute;\n    color: #ED6B06;\n    font-size: calc(var(--frameHeight)*0.108);\n    font-weight: 900;\n    left: 7%;\n    line-height: calc(var(--frameHeight)*0.078);\n    top: 2%;\n    z-index: 1;\n}\n.modal-title-2[_ngcontent-%COMP%] {\n    position: absolute;\n    color: #ED6B06;\n    font-size: calc(var(--frameHeight)*0.075);\n    font-weight: 500;\n    left: 14%;\n    line-height: calc(var(--frameHeight)*0.078);\n    bottom: 13%;\n    z-index: 1;\n}\n.modal-body[_ngcontent-%COMP%] {\n    position: absolute;\n    *background-image: url('modal.png');\n    background-size: 100% 100%;\n    background-repeat: no-repeat;\n    background-color: transparent;\n    display: inline-block;\n    height: 78%;\n    justify-content: center;\n    left: 4%;\n    padding: 0;\n    top: 24%;\n    width: 92%;\n}\n.modal-body-message[_ngcontent-%COMP%]{\n    color: #333;\n    font-size: calc(var(--frameHeight)*0.05);\n    justify-content: center;\n    text-align: justify;\n    font-family: var(--font-family-regular);\n    height: auto;\n    line-height: 105%;\n    margin-left: 7%;\n    margin-top: 5%;\n    width: 66%;\n}\n.modal-body-luck[_ngcontent-%COMP%] {\n    color: #333;\n    font-family: var(--font-family-regular);\n    font-size: calc(var(--frameHeight)*0.05);\n    margin-left: 7%;\n    margin-top: 4%;\n    width: 66%;\n}\n.button_continue[_ngcontent-%COMP%] {\n    position: absolute;\n    background-color: #39a0b4;\n    box-shadow: 0 calc(var(--frameHeight)*0.005) 0 calc(var(--frameHeight)*0.004) #3b3c3d;\n    border-top: calc(var(--frameHeight)*0.0085) solid #3b3c3d;\n    border-left: calc(var(--frameHeight)*0.0085) solid #3b3c3d;\n    border-right: calc(var(--frameHeight)*0.0085) solid #3b3c3d;\n    border-bottom: calc(var(--frameHeight)*0.0085) solid #3b3c3d;\n    border-radius: calc(var(--frameHeight)*1);\n    bottom: 14%;\n    display: block;\n    font-family: var(--font-family-medium);\n    font-size: calc(var(--frameHeight)*0.038);\n    left: 40%;\n    padding: 0.5% 2%;\n    transition: background-color 0.5s ease-in-out;\n}\n.button_continue.disabled[_ngcontent-%COMP%] {\n    opacity: 0.4;\n    pointer-events: none;\n}\n.button_continue[_ngcontent-%COMP%]:hover {\n    \n    background-color: #2a6c79;\n    \n}\n.button_continue[_ngcontent-%COMP%]:active {\n    transform: translateY(3px);\n    border-top: calc(var(--frameHeight)*0.007) solid #3b3c3d;\n    border-left: calc(var(--frameHeight)*0.007) solid #3b3c3d;\n    border-right: calc(var(--frameHeight)*0.007) solid #3b3c3d;\n    border-bottom: calc(var(--frameHeight)*0.007) solid #3b3c3d;\n    box-shadow: none;\n}   \n.button_show_modal[_ngcontent-%COMP%] {\n    position: absolute;\n    top: 45%;\n    left: 2%;\n    background-color: #105360;\n    color: white;\n    padding: 0.3%;\n}\n\n.button_close[_ngcontent-%COMP%] {\n    position: absolute;\n    background-image: url('equis.png');\n    background-size: 100% 100%;\n    background-repeat: no-repeat;\n    height: calc(var(--frameHeight)*0.05);\n    left: 97%;\n    top: 6%;\n    transition: all 0.1s ease-in-out;\n    width: calc(var(--frameHeight)*0.05);\n}\n.button_close.disabled[_ngcontent-%COMP%] {\n    opacity: 0.4;\n    pointer-events: none;\n}\n.button_close[_ngcontent-%COMP%]:hover {\n    background-image: url('equis_hover.png');\n    background-size: 100% 100%;\n    background-repeat: no-repeat;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hvcC9pbnNpZGUvbW9kYWwvbW9kYWwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx1QkFBdUI7O0FBRXZCO0VBRUUsOEJBQThCOztBQUVoQztBQUNBO0VBRUUsc0JBQXNCO0VBRXRCLHlCQUF5QjtBQUMzQjtBQUNBLGtCQUFrQjtBQUNsQjtJQUNJLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsc0NBQXNDO0lBQ3RDLFlBQVk7SUFDWixZQUFZO0lBQ1osY0FBYztJQUNkLHFDQUFxQztJQUNyQyxhQUFhO0lBQ2Isa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksYUFBYTtJQUNiLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsV0FBVztBQUNmOztBQUVBO0lBQ0ksWUFBWTtJQUNaLGdDQUFnQztJQUNoQywyQ0FBMEY7SUFDMUYsMEJBQTBCO0lBQzFCLDZCQUE2QjtJQUM3QixXQUFXO0lBQ1gsVUFBVTtJQUNWLFVBQVU7QUFDZDtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXFERztBQUNIO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCx5Q0FBeUM7SUFDekMsZ0JBQWdCO0lBQ2hCLFFBQVE7SUFDUiwyQ0FBMkM7SUFDM0MsT0FBTztJQUNQLFVBQVU7QUFDZDtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCx5Q0FBeUM7SUFDekMsZ0JBQWdCO0lBQ2hCLFNBQVM7SUFDVCwyQ0FBMkM7SUFDM0MsV0FBVztJQUNYLFVBQVU7QUFDZDtBQUNBO0lBQ0ksa0JBQWtCO0tBQ2xCLGtDQUFrRjtJQUNsRiwwQkFBMEI7SUFDMUIsNEJBQTRCO0lBQzVCLDZCQUE2QjtJQUM3QixxQkFBcUI7SUFDckIsV0FBVztJQUNYLHVCQUF1QjtJQUN2QixRQUFRO0lBQ1IsVUFBVTtJQUNWLFFBQVE7SUFDUixVQUFVO0FBQ2Q7QUFDQTtJQUNJLFdBQVc7SUFDWCx3Q0FBd0M7SUFDeEMsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQix1Q0FBdUM7SUFDdkMsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsY0FBYztJQUNkLFVBQVU7QUFDZDtBQUNBO0lBQ0ksV0FBVztJQUNYLHVDQUF1QztJQUN2Qyx3Q0FBd0M7SUFDeEMsZUFBZTtJQUNmLGNBQWM7SUFDZCxVQUFVO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIscUZBQXFGO0lBQ3JGLHlEQUF5RDtJQUN6RCwwREFBMEQ7SUFDMUQsMkRBQTJEO0lBQzNELDREQUE0RDtJQUM1RCx5Q0FBeUM7SUFDekMsV0FBVztJQUNYLGNBQWM7SUFDZCxzQ0FBc0M7SUFDdEMseUNBQXlDO0lBQ3pDLFNBQVM7SUFDVCxnQkFBZ0I7SUFDaEIsNkNBQTZDO0FBQ2pEO0FBQ0E7SUFDSSxZQUFZO0lBQ1osb0JBQW9CO0FBQ3hCO0FBQ0E7SUFDSSxtQ0FBbUM7SUFDbkMseUJBQXlCO0lBQ3pCLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksMEJBQTBCO0lBQzFCLHdEQUF3RDtJQUN4RCx5REFBeUQ7SUFDekQsMERBQTBEO0lBQzFELDJEQUEyRDtJQUMzRCxnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsUUFBUTtJQUNSLHlCQUF5QjtJQUN6QixZQUFZO0lBQ1osYUFBYTtBQUNqQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixrQ0FBb0U7SUFDcEUsMEJBQTBCO0lBQzFCLDRCQUE0QjtJQUM1QixxQ0FBcUM7SUFDckMsU0FBUztJQUNULE9BQU87SUFDUCxnQ0FBZ0M7SUFDaEMsb0NBQW9DO0FBQ3hDO0FBQ0E7SUFDSSxZQUFZO0lBQ1osb0JBQW9CO0FBQ3hCO0FBQ0E7SUFDSSx3Q0FBMEU7SUFDMUUsMEJBQTBCO0lBQzFCLDRCQUE0QjtBQUNoQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKiogICBBbmltYWNpw4PCs24gICAgKioqL1xuXG4uZmFkZUluUmlnaHRCaWcge1xuICAtd2Via2l0LWFuaW1hdGlvbi1uYW1lOiBmYWRlSW5SaWdodEJpZztcbiAgYW5pbWF0aW9uLW5hbWU6IGZhZGVJblJpZ2h0QmlnO1xuICBcbn1cbi5hbmltYXRlZCB7XG4gIC13ZWJraXQtYW5pbWF0aW9uLWR1cmF0aW9uOiAxcztcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAxcztcbiAgLXdlYmtpdC1hbmltYXRpb24tZmlsbC1tb2RlOiBib3RoO1xuICBhbmltYXRpb24tZmlsbC1tb2RlOiBib3RoO1xufVxuLyoqKiAgIE1vZGFsICAgKioqL1xuI21vZGFsX2luZm97XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzVjNWM1Y2NjICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBmb250LWZhbWlseTogdmFyKC0tZm9udC1mYW1pbHktbGlnaHQpO1xuICAgIG9wYWNpdHk6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyAgXG59XG5cbi5tb2RhbC1kaWFsb2ctYWR2YW5jZWQge1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHBsYWNlLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5tb2RhbC1jb250ZW50IHtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgLyogYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7ICovXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9nZW5lcmFsX3ZpZXdfbGVzc29ucy9tb2RhbF9jb21wbGV0by5wbmcpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGhlaWdodDogOTAlO1xuICAgIHBhZGRpbmc6IDA7XG4gICAgd2lkdGg6IDcwJTtcbn1cbi8qIC5tb2RhbC1oZWFkZXJ7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDMyKTtcbiAgICBoZWlnaHQ6IDE5JTtcbiAgICBsZWZ0OiAzJTtcbiAgICBwYWRkaW5nOiAwO1xuICAgIHRvcDogMSU7XG4gICAgd2lkdGg6IDI3JTtcbn1cbi5tb2RhbC1oZWFkZXItMSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvcmRlcjogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTUpIHNvbGlkICMzZDNkM2Q7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMik7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDIpO1xuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XG4gICAgaGVpZ2h0OiA0NCU7XG4gICAgdG9wOiAwO1xuICAgIHdpZHRoOiA4OCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG4ubW9kYWwtaGVhZGVyLTIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3JkZXI6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDE1KSBzb2xpZCAjM2QzZDNkO1xuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDIpO1xuICAgIGJvcmRlci10b3A6IG5vbmU7XG4gICAgYm9yZGVyLXJpZ2h0OiBub25lO1xuICAgIGhlaWdodDogNjElO1xuICAgIGJvdHRvbTogLTElO1xuICAgIHdpZHRoOiA5MiU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG4ubW9kYWwtaGVhZGVyLTMge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3JkZXI6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDE1KSBzb2xpZCAjM2QzZDNkO1xuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAyKTtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMik7XG4gICAgYm9yZGVyLWxlZnQ6IG5vbmU7XG4gICAgaGVpZ2h0OiA2MSU7XG4gICAgcmlnaHQ6IDA7XG4gICAgYm90dG9tOiAtMSU7XG4gICAgd2lkdGg6IDEyJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cbi5tb2RhbC1oZWFkZXItNCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvcmRlcjogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMTUpIHNvbGlkICMzZDNkM2Q7XG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMik7XG4gICAgYm9yZGVyLXJpZ2h0OiBub25lO1xuICAgIGJvcmRlci10b3A6IG5vbmU7XG4gICAgaGVpZ2h0OiA5JTtcbiAgICByaWdodDogMy4xJTtcbiAgICB0b3A6IDQwJTtcbiAgICB3aWR0aDogMTIlO1xufSAqL1xuLm1vZGFsLXRpdGxlLTEge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBjb2xvcjogI0VENkIwNjtcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMTA4KTtcbiAgICBmb250LXdlaWdodDogOTAwO1xuICAgIGxlZnQ6IDclO1xuICAgIGxpbmUtaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA3OCk7XG4gICAgdG9wOiAyJTtcbiAgICB6LWluZGV4OiAxO1xufVxuLm1vZGFsLXRpdGxlLTIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBjb2xvcjogI0VENkIwNjtcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDc1KTtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGxlZnQ6IDE0JTtcbiAgICBsaW5lLWhlaWdodDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNzgpO1xuICAgIGJvdHRvbTogMTMlO1xuICAgIHotaW5kZXg6IDE7XG59XG4ubW9kYWwtYm9keSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICpiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL21vZGFsLnBuZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgaGVpZ2h0OiA3OCU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbGVmdDogNCU7XG4gICAgcGFkZGluZzogMDtcbiAgICB0b3A6IDI0JTtcbiAgICB3aWR0aDogOTIlO1xufVxuLm1vZGFsLWJvZHktbWVzc2FnZXtcbiAgICBjb2xvcjogIzMzMztcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDUpO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LXJlZ3VsYXIpO1xuICAgIGhlaWdodDogYXV0bztcbiAgICBsaW5lLWhlaWdodDogMTA1JTtcbiAgICBtYXJnaW4tbGVmdDogNyU7XG4gICAgbWFyZ2luLXRvcDogNSU7XG4gICAgd2lkdGg6IDY2JTtcbn1cbi5tb2RhbC1ib2R5LWx1Y2sge1xuICAgIGNvbG9yOiAjMzMzO1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1yZWd1bGFyKTtcbiAgICBmb250LXNpemU6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDUpO1xuICAgIG1hcmdpbi1sZWZ0OiA3JTtcbiAgICBtYXJnaW4tdG9wOiA0JTtcbiAgICB3aWR0aDogNjYlO1xufVxuLmJ1dHRvbl9jb250aW51ZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzOWEwYjQ7XG4gICAgYm94LXNoYWRvdzogMCBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwNSkgMCBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwNCkgIzNiM2MzZDtcbiAgICBib3JkZXItdG9wOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwODUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLWxlZnQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDA4NSkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItcmlnaHQ6IGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDA4NSkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItYm90dG9tOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwODUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLXJhZGl1czogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMSk7XG4gICAgYm90dG9tOiAxNCU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LW1lZGl1bSk7XG4gICAgZm9udC1zaXplOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAzOCk7XG4gICAgbGVmdDogNDAlO1xuICAgIHBhZGRpbmc6IDAuNSUgMiU7XG4gICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAwLjVzIGVhc2UtaW4tb3V0O1xufVxuLmJ1dHRvbl9jb250aW51ZS5kaXNhYmxlZCB7XG4gICAgb3BhY2l0eTogMC40O1xuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuLmJ1dHRvbl9jb250aW51ZTpob3ZlciB7XG4gICAgLyogLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEuMSk7ICovXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzJhNmM3OTtcbiAgICAvKiB0cmFuc2Zvcm06IHNjYWxlKDEuMSkgKi9cbn1cbi5idXR0b25fY29udGludWU6YWN0aXZlIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoM3B4KTtcbiAgICBib3JkZXItdG9wOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwNykgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItbGVmdDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDcpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLXJpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwNykgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItYm90dG9tOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwNykgc29saWQgIzNiM2MzZDtcbiAgICBib3gtc2hhZG93OiBub25lO1xufSAgIFxuLmJ1dHRvbl9zaG93X21vZGFsIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA0NSU7XG4gICAgbGVmdDogMiU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzEwNTM2MDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogMC4zJTtcbn1cblxuLmJ1dHRvbl9jbG9zZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvYnV0dG9ucy9lcXVpcy5wbmcpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgaGVpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjA1KTtcbiAgICBsZWZ0OiA5NyU7XG4gICAgdG9wOiA2JTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4xcyBlYXNlLWluLW91dDtcbiAgICB3aWR0aDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wNSk7XG59XG4uYnV0dG9uX2Nsb3NlLmRpc2FibGVkIHtcbiAgICBvcGFjaXR5OiAwLjQ7XG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG59XG4uYnV0dG9uX2Nsb3NlOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2J1dHRvbnMvZXF1aXNfaG92ZXIucG5nKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xufSJdLCJzb3VyY2VSb290IjoiIn0= */"]
});

/***/ }),

/***/ 3020:
/*!*****************************************************!*\
  !*** ./src/app/shop/outside/book/book.component.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookComponent": () => (/* binding */ BookComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 2560);

class BookComponent {
  constructor() {
    this.currentPage = 1;
  }
  toggleClass(e, toggleClassName) {
    if (e.className.includes(toggleClassName)) {
      e.classList.remove(toggleClassName);
    } else {
      e.classList.add(toggleClassName);
    }
  }
  movePage(e, page) {
    /* console.log('--------\npage: ', page, ' - e: ', <HTMLDivElement>e);   //obtiene el elemento seleccionado completo con toda su información
    console.log('e target:', <HTMLDivElement>e.target);   //obtiene el elemento actual seleccionado
    console.log('e target localName:', <HTMLDivElement>e.target.localName);   //obtiene el nombre de la etiqueta del elemento seleccionado
    console.log('e className: ', <HTMLDivElement>e.target.className);   //obtiene las clases del elemento actual seleccionado
    console.log('e parentElement: ', <HTMLDivElement>e.target.parentElement);   //obtiene el elemento padre del elemento seleccionado */
    //console.log('<HTMLDivElement>e: ', <HTMLDivElement>e);   //obtiene el elemento seleccionado completo con toda su información
    //console.log('<HTMLDivElement>e.target:', <HTMLDivElement>e.target);   //obtiene el elemento actual seleccionado
    //console.log('<HTMLDivElement>e.target.localName:', <HTMLDivElement>e.target.localName);   //obtiene el nombre de la etiqueta del elemento seleccionado
    //console.log('<HTMLDivElement>e.target.className: ', <HTMLDivElement>e.target.className);   //obtiene las clases del elemento actual seleccionado
    //console.log('<HTMLDivElement>e.target.parentElement: ', <HTMLDivElement>e.target.parentElement);   //obtiene el elemento padre del elemento seleccionado
    /* el elemento clickeado es la division que contiene la información */
    if (e.target.localName == "div") {
      if (page == this.currentPage) {
        this.currentPage += 2;
        this.toggleClass(e.target, "left-side");
        this.toggleClass(e.target.nextElementSibling, "left-side");
      } else if (page == this.currentPage - 1) {
        this.currentPage -= 2;
        this.toggleClass(e.target, "left-side");
        this.toggleClass(e.target.previousElementSibling, "left-side");
      }
    }
    /* el elemento clickeado es el contenedor interno (el elemento hijo) */else if (e.target.localName == "p") {
      if (page == this.currentPage) {
        this.currentPage += 2;
        this.toggleClass(e.target.parentElement, "left-side");
        this.toggleClass(e.target.parentElement.nextElementSibling, "left-side");
      } else if (page == this.currentPage - 1) {
        this.currentPage -= 2;
        this.toggleClass(e.target.parentElement, "left-side");
        this.toggleClass(e.target.parentElement.previousElementSibling, "left-side");
      }
    }
  }
}
BookComponent.ɵfac = function BookComponent_Factory(t) {
  return new (t || BookComponent)();
};
BookComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
  type: BookComponent,
  selectors: [["app-book"]],
  decls: 44,
  vars: 0,
  consts: [[1, "instruction"], [1, "book"], [1, "page", "cover-front", 3, "click"], [1, "hat"], [1, "fab", "fa-pied-piper-hat"], [1, "page", "text-page", 3, "click"], ["disabled", ""], [1, "page"]],
  template: function BookComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Flip the page");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1)(3, "div", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookComponent_Template_div_click_3_listener($event) {
        return ctx.movePage($event, 1);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Mrs. Dalloway");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "i", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h2");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Virginia Woolf");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookComponent_Template_div_click_10_listener($event) {
        return ctx.movePage($event, 2);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookComponent_Template_div_click_11_listener($event) {
        return ctx.movePage($event, 3);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "P\u00E1gina 03 - Mrs. Dalloway said she would buy the flowers herself.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "For Lucy had her work cut out for her. The doors would be taken off their hinges; Rumpelmayer's men were coming. And then, thought Clarissa Dalloway, what a morning--fresh as if issued to children on a beach.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "What a lark! What a plunge! For so it had always seemed to her, when, with a little squeak of the hinges, which she could hear now, she had burst open the French windows and plunged at Bourton into the open air. How fresh, how calm, stiller than event of course, the air was in the early morning; like the flap of a wave; the kiss of a wave; chill and sharp and yet (for a girl of eighteen as she then was) solemn, feeling as she did, standing there at the open window, that something awful was about to happen; looking at the flowers, at the trees with the smoke winding off them and the rooks ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookComponent_Template_div_click_18_listener($event) {
        return ctx.movePage($event, 4);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "P\u00E1gina 04 - vanished--how strange it was!--a few sayings like this about cabbages.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "She stiffened a little on the kerb, waiting for Durtnall's van to pass. A charming woman, Scrope Purvis thought her (knowing her as one does know people who live next door to one in Westminster); a touch of the bird about her, of the jay, blue-green, light, vivacious, though she was over fifty, and grown very white since her illness. There she perched, never seeing him, waiting to cross, very upright.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "For having lived in Westminster--how many years now? over twenty,--one feels even in the midst of the traffic, or waking at night, Clarissa was positive, a particular hush, or solemnity; an indescribable pause; a suspense (but that might be her heart, affected, they said, by influenza) before Big Ben strikes. There! Out it boomed. First a warning, musical; then the hour, irrevocable. The leaden circles dissolved in the air. ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookComponent_Template_div_click_25_listener($event) {
        return ctx.movePage($event, 5);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "P\u00E1gina 05 - the uproar; the carriages, motor cars, omnibuses, vans, sandwich men shuffling and swinging; brass bands; barrel organs; in the triumph and the jingle and the strange high singing of some aeroplane overhead was what she loved; life; London; this moment of June.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "For it was the middle of June. The War was over, except for some one like Mrs. Foxcroft at the Embassy last night eating her heart out because that nice boy was killed and now the old Manor House must go to a cousin; or Lady Bexborough who opened a bazaar, they said, with the telegram in her hand, John, her favourite, killed; but it was over; thank Heaven--over. It was June. The King and Queen were at the Palace. And everywhere, though it was still so early, there was a beating, a stirring of galloping ponies, tapping of cricket bats; ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookComponent_Template_div_click_30_listener($event) {
        return ctx.movePage($event, 6);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "P\u00E1gina 06 - eighteenth-century settings to tempt Americans (but one must economise, not buy things rashly for Elizabeth), and she, too, loving it as she did with an absurd and faithful passion, being part of it, since her people were courtiers once in the time of the Georges, she, too, was going that very night to kindle and illuminate; to give her party. But how strange, on entering the Park, the silence; the mist; the hum; the slow-swimming happy ducks; ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "\"Good-morning to you, Clarissa!\" said Hugh, rather extravagantly, for they had known each other as children. \"Where are you off to?\"");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "\"I love walking in London,\" said Mrs. Dalloway. \"Really it's better than walking in the country.\"");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookComponent_Template_div_click_37_listener($event) {
        return ctx.movePage($event, 7);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "P\u00E1gina 07 - Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis nihil, iure possimus fugiat dignissimos est adipisci porro, dicta eaque ipsum eos, accusamus velit architecto eius illo rerum nisi suscipit aperiam! Vel nobis veritatis iure maxime incidunt perspiciatis, odit, veniam impedit alias, minus distinctio aperiam in. Repudiandae, voluptate nam esse consectetur adipisci quod praesentium ex numquam eligendi obcaecati laudantium saepe hic, unde animi dolore harum corrupti perspiciatis. Hic, similique obcaecati molestias nihil id architecto dolorem rem distinctio voluptatum quaerat reprehenderit odit soluta, vel numquam ab earum, dolorum recusandae ullam. Veniam expedita ducimus nihil quo accusantium repudiandae fugit ea omnis, magnam voluptatibus? Hic, sunt. Cumque repellat cupiditate dolor, nulla iusto corporis modi quos tenetur quibusdam possimus culpa architecto, mollitia sequi quia odio excepturi ea saepe vitae eum quis quo, quas quisquam accusamus? Vitae maiores quidem beatae repellat voluptate. Unde sunt illum asperiores aperiam laudantium nisi perspiciatis! Corrupti ipsam hic laborum ea eos maiores, recusandae eveniet quaerat magni? Magnam, placeat commodi! Numquam asperiores neque at eligendi minima nemo saepe perspiciatis enim, corporis nobis consequatur harum voluptate aliquam explicabo cum voluptates. Vitae, possimus itaque eaque, earum quia veniam laboriosam tenetur labore expedita eos esse natus officiis obcaecati enim neque vel minus animi odio ipsa?");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BookComponent_Template_div_click_40_listener($event) {
        return ctx.movePage($event, 8);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "p", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "P\u00E1gina 08 - Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus, magni ab mollitia molestias velit iusto voluptatum temporibus suscipit explicabo. Minus culpa itaque omnis ipsam sed iste inventore reprehenderit ex veniam aut debitis, dolore magnam deleniti assumenda perferendis qui perspiciatis, vitae dolorum obcaecati optio, voluptatibus magni vero repellat. Architecto quos perspiciatis eum mollitia delectus veritatis error possimus, minima nihil animi, molestiae velit, asperiores consectetur quaerat adipisci esse perferendis eos vel impedit soluta sit quisquam. Quasi laborum sequi, possimus doloremque quos consequatur! Quae alias velit et a ipsum quo harum dicta ipsa quibusdam porro adipisci nulla, omnis id? Quaerat, eaque consequatur? Obcaecati saepe nobis, magni non accusantium, assumenda incidunt blanditiis autem pariatur ipsum officiis corporis rerum officia amet ipsam cumque, deserunt ratione numquam quo ut repudiandae harum cum consequatur veniam? Asperiores placeat ducimus, dicta cumque aut beatae illo ipsum vero ipsa quasi dolor, optio delectus vitae ex dolore tempora exercitationem tenetur deserunt explicabo ipsam consequuntur excepturi nobis! Saepe corrupti earum reiciendis quod quisquam mollitia voluptates, nostrum quidem cupiditate nihil sit molestiae dolores reprehenderit nulla quibusdam animi nisi, enim pariatur, vel praesentium! Animi libero odio ducimus modi! Exercitationem aspernatur saepe molestias quo explicabo, ratione hic velit ipsa deleniti laboriosam ullam accusantium quidem odit.");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "div", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    }
  },
  styles: ["@import url(https://fonts.googleapis.com/css2?family=Lora&display=swap);@import url(https://fonts.googleapis.com/css2?family=EB+Garamond&display=swap);*[_ngcontent-%COMP%] {\n  margin: 0;\n  box-sizing: border-box;\n}\n\n.instruction[_ngcontent-%COMP%], .book[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\n\n\n.book[_ngcontent-%COMP%] {\n  width: 847px;\n  height: 654.5px;\n  transform: translate(-50%, -50%) rotatex(10deg) rotatey(-10deg);\n  transform-style: preserve-3d;\n}\n\n.page[_ngcontent-%COMP%] {\n  width: 423.5px;\n  height: 654.5px;\n  background: #fff;\n  position: absolute;\n  top: 0;\n  right: 0;\n  transition: transform 1s;\n  overflow: hidden;\n}\n\n.page[_ngcontent-%COMP%]:nth-child(1) {\n  padding: 5% 4% 4% 5%;\n  transform-origin: 0% 50%;\n  transform: translatez(-1px);\n  background: #9bc8d6;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(1) {\n  transform: translatez(1px) rotatey(-180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(2) {\n  padding: 5% 5% 4% 4%;\n  transform-origin: 100% 50%;\n  transform: translatez(-2px) scalex(-1) translatex(100%);\n  background: #9bc8d6;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(2) {\n  transform: translatez(2px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(3) {\n  padding: 5% 4% 4% 5%;\n  transform-origin: 0% 50%;\n  transform: translatez(-3px);\n  background: #f5f5f5;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(3) {\n  transform: translatez(3px) rotatey(-180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(4) {\n  padding: 5% 5% 4% 4%;\n  transform-origin: 100% 50%;\n  transform: translatez(-4px) scalex(-1) translatex(100%);\n  background: #f5f5f5;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(4) {\n  transform: translatez(4px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(5) {\n  padding: 5% 4% 4% 5%;\n  transform-origin: 0% 50%;\n  transform: translatez(-5px);\n  background: #f5f5f5;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(5) {\n  transform: translatez(5px) rotatey(-180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(6) {\n  padding: 5% 5% 4% 4%;\n  transform-origin: 100% 50%;\n  transform: translatez(-6px) scalex(-1) translatex(100%);\n  background: #f5f5f5;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(6) {\n  transform: translatez(6px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(7) {\n  padding: 5% 4% 4% 5%;\n  transform-origin: 0% 50%;\n  transform: translatez(-7px);\n  background: #f5f5f5;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(7) {\n  transform: translatez(7px) rotatey(-180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(8) {\n  padding: 5% 5% 4% 4%;\n  transform-origin: 100% 50%;\n  transform: translatez(-8px) scalex(-1) translatex(100%);\n  background: #f5f5f5;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(8) {\n  transform: translatez(8px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(9) {\n  padding: 5% 4% 4% 5%;\n  transform-origin: 0% 50%;\n  transform: translatez(-9px);\n  background: #f5f5f5;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(9) {\n  transform: translatez(9px) rotatey(-180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(10) {\n  padding: 5% 5% 4% 4%;\n  transform-origin: 100% 50%;\n  transform: translatez(-10px) scalex(-1) translatex(100%);\n  background: #ccc;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(10) {\n  transform: translatez(10px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(11) {\n  padding: 5% 4% 4% 5%;\n  transform-origin: 0% 50%;\n  transform: translatez(-11px);\n  background: #ccc;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(11) {\n  transform: translatez(11px) rotatey(-180deg);\n}\n\n.page[_ngcontent-%COMP%]:nth-child(12) {\n  padding: 5% 5% 4% 4%;\n  transform-origin: 100% 50%;\n  transform: translatez(-12px) scalex(-1) translatex(100%);\n  background: #ccc;\n}\n\n.left-side[_ngcontent-%COMP%]:nth-child(12) {\n  transform: translatez(12px) scalex(-1) translatex(100%) rotatey(180deg);\n}\n\n.cover-front[_ngcontent-%COMP%]:nth-child(1) {\n  text-align: center;\n  background: linear-gradient(#afd2dd, #a1d5e5) 50% 50%/80% 86% no-repeat, linear-gradient(#ffffff, #ffffff) 50% 50%/90% 93% no-repeat, linear-gradient(#9bc8d6, #9bc8d6) no-repeat;\n}\n.cover-front[_ngcontent-%COMP%]:nth-child(1)   h1[_ngcontent-%COMP%] {\n  font-family: \"Lora\", serif;\n  font-size: 45px;\n  color: #134a78;\n  margin-top: 30px;\n  text-transform: uppercase;\n}\n.cover-front[_ngcontent-%COMP%]:nth-child(1)   h2[_ngcontent-%COMP%] {\n  font-family: \"EB Garamond\", serif;\n  font-size: 24px;\n  color: #134a78;\n  position: relative;\n  top: 40%;\n}\n.cover-front[_ngcontent-%COMP%]:nth-child(1)   .hat[_ngcontent-%COMP%] {\n  font-size: 100px;\n  color: #134a78;\n  position: relative;\n  top: 3%;\n}\n\np[_ngcontent-%COMP%] {\n  margin-bottom: 4%;\n  font-family: \"EB Garamond\", serif;\n  font-size: 16px;\n}\n\n.page[_ngcontent-%COMP%]:nth-child(3)   p[_ngcontent-%COMP%]:first-of-type:first-letter {\n  font-size: 32px;\n}\n\n.instruction[_ngcontent-%COMP%] {\n  left: calc(50% - 150px);\n  font-size: 26px;\n  font-family: \"EB Garamond\", serif;\n  color: #0009;\n  background: #fff7;\n  border-radius: 15px;\n  padding: 10px 15px;\n}\n\np[_ngcontent-%COMP%] {\n  border: 1px solid blue;\n}\n\ndiv.page[_ngcontent-%COMP%] {\n  border: 1px solid red;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hvcC9vdXRzaWRlL2Jvb2svYm9vay5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsU0FBUztFQUNULHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FBUztFQUNULGdDQUFnQztBQUNsQzs7QUFFQTs7Ozs7OztHQU9HOztBQUVIO0VBQ0UsWUFBWTtFQUNaLGVBQWU7RUFDZiwrREFBK0Q7RUFDL0QsNEJBQTRCO0FBQzlCOztBQUVBO0VBQ0UsY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixRQUFRO0VBQ1Isd0JBQXdCO0VBQ3hCLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQix3QkFBd0I7RUFDeEIsMkJBQTJCO0VBQzNCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLDJDQUEyQztBQUM3Qzs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQiwwQkFBMEI7RUFDMUIsdURBQXVEO0VBQ3ZELG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLHNFQUFzRTtBQUN4RTs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQix3QkFBd0I7RUFDeEIsMkJBQTJCO0VBQzNCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLDJDQUEyQztBQUM3Qzs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQiwwQkFBMEI7RUFDMUIsdURBQXVEO0VBQ3ZELG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLHNFQUFzRTtBQUN4RTs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQix3QkFBd0I7RUFDeEIsMkJBQTJCO0VBQzNCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLDJDQUEyQztBQUM3Qzs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQiwwQkFBMEI7RUFDMUIsdURBQXVEO0VBQ3ZELG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLHNFQUFzRTtBQUN4RTs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQix3QkFBd0I7RUFDeEIsMkJBQTJCO0VBQzNCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLDJDQUEyQztBQUM3Qzs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQiwwQkFBMEI7RUFDMUIsdURBQXVEO0VBQ3ZELG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLHNFQUFzRTtBQUN4RTs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQix3QkFBd0I7RUFDeEIsMkJBQTJCO0VBQzNCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLDJDQUEyQztBQUM3Qzs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQiwwQkFBMEI7RUFDMUIsd0RBQXdEO0VBQ3hELGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLHVFQUF1RTtBQUN6RTs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQix3QkFBd0I7RUFDeEIsNEJBQTRCO0VBQzVCLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLDRDQUE0QztBQUM5Qzs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQiwwQkFBMEI7RUFDMUIsd0RBQXdEO0VBQ3hELGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLHVFQUF1RTtBQUN6RTs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixpTEFBaUw7QUFDbkw7QUFDQTtFQUNFLDBCQUEwQjtFQUMxQixlQUFlO0VBQ2YsY0FBYztFQUNkLGdCQUFnQjtFQUNoQix5QkFBeUI7QUFDM0I7QUFDQTtFQUNFLGlDQUFpQztFQUNqQyxlQUFlO0VBQ2YsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixRQUFRO0FBQ1Y7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLE9BQU87QUFDVDs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixpQ0FBaUM7RUFDakMsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGVBQWU7QUFDakI7O0FBRUE7RUFDRSx1QkFBdUI7RUFDdkIsZUFBZTtFQUNmLGlDQUFpQztFQUNqQyxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxxQkFBcUI7QUFDdkIiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzMj9mYW1pbHk9TG9yYSZkaXNwbGF5PXN3YXBcIik7XG5AaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzMj9mYW1pbHk9RUIrR2FyYW1vbmQmZGlzcGxheT1zd2FwXCIpO1xuKiB7XG4gIG1hcmdpbjogMDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbn1cblxuLmluc3RydWN0aW9uLCAuYm9vayB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG59XG5cbi8qIGJvZHkge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgbWluLXdpZHRoOiA5MDBweDtcbiAgbWluLWhlaWdodDogNzAwcHg7XG4gIGJhY2tncm91bmQ6ICNmYmVjYjQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn0gKi9cblxuLmJvb2sge1xuICB3aWR0aDogODQ3cHg7XG4gIGhlaWdodDogNjU0LjVweDtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSkgcm90YXRleCgxMGRlZykgcm90YXRleSgtMTBkZWcpO1xuICB0cmFuc2Zvcm0tc3R5bGU6IHByZXNlcnZlLTNkO1xufVxuXG4ucGFnZSB7XG4gIHdpZHRoOiA0MjMuNXB4O1xuICBoZWlnaHQ6IDY1NC41cHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDFzO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4ucGFnZTpudGgtY2hpbGQoMSkge1xuICBwYWRkaW5nOiA1JSA0JSA0JSA1JTtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTFweCk7XG4gIGJhY2tncm91bmQ6ICM5YmM4ZDY7XG59XG5cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDEpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDFweCkgcm90YXRleSgtMTgwZGVnKTtcbn1cblxuLnBhZ2U6bnRoLWNoaWxkKDIpIHtcbiAgcGFkZGluZzogNSUgNSUgNCUgNCU7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTJweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpO1xuICBiYWNrZ3JvdW5kOiAjOWJjOGQ2O1xufVxuXG4ubGVmdC1zaWRlOm50aC1jaGlsZCgyKSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigycHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKSByb3RhdGV5KDE4MGRlZyk7XG59XG5cbi5wYWdlOm50aC1jaGlsZCgzKSB7XG4gIHBhZGRpbmc6IDUlIDQlIDQlIDUlO1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtM3B4KTtcbiAgYmFja2dyb3VuZDogI2Y1ZjVmNTtcbn1cblxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMykge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooM3B4KSByb3RhdGV5KC0xODBkZWcpO1xufVxuXG4ucGFnZTpudGgtY2hpbGQoNCkge1xuICBwYWRkaW5nOiA1JSA1JSA0JSA0JTtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMTAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtNHB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSk7XG4gIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG59XG5cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDQpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDRweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpIHJvdGF0ZXkoMTgwZGVnKTtcbn1cblxuLnBhZ2U6bnRoLWNoaWxkKDUpIHtcbiAgcGFkZGluZzogNSUgNCUgNCUgNSU7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDAlIDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KC01cHgpO1xuICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xufVxuXG4ubGVmdC1zaWRlOm50aC1jaGlsZCg1KSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleig1cHgpIHJvdGF0ZXkoLTE4MGRlZyk7XG59XG5cbi5wYWdlOm50aC1jaGlsZCg2KSB7XG4gIHBhZGRpbmc6IDUlIDUlIDQlIDQlO1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAxMDAlIDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KC02cHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2Y1ZjVmNTtcbn1cblxuLmxlZnQtc2lkZTpudGgtY2hpbGQoNikge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooNnB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufVxuXG4ucGFnZTpudGgtY2hpbGQoNykge1xuICBwYWRkaW5nOiA1JSA0JSA0JSA1JTtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTdweCk7XG4gIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG59XG5cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDcpIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDdweCkgcm90YXRleSgtMTgwZGVnKTtcbn1cblxuLnBhZ2U6bnRoLWNoaWxkKDgpIHtcbiAgcGFkZGluZzogNSUgNSUgNCUgNCU7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLThweCkgc2NhbGV4KC0xKSB0cmFuc2xhdGV4KDEwMCUpO1xuICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xufVxuXG4ubGVmdC1zaWRlOm50aC1jaGlsZCg4KSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleig4cHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKSByb3RhdGV5KDE4MGRlZyk7XG59XG5cbi5wYWdlOm50aC1jaGlsZCg5KSB7XG4gIHBhZGRpbmc6IDUlIDQlIDQlIDUlO1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigtOXB4KTtcbiAgYmFja2dyb3VuZDogI2Y1ZjVmNTtcbn1cblxuLmxlZnQtc2lkZTpudGgtY2hpbGQoOSkge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooOXB4KSByb3RhdGV5KC0xODBkZWcpO1xufVxuXG4ucGFnZTpudGgtY2hpbGQoMTApIHtcbiAgcGFkZGluZzogNSUgNSUgNCUgNCU7XG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTEwcHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKTtcbiAgYmFja2dyb3VuZDogI2NjYztcbn1cblxuLmxlZnQtc2lkZTpudGgtY2hpbGQoMTApIHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KDEwcHgpIHNjYWxleCgtMSkgdHJhbnNsYXRleCgxMDAlKSByb3RhdGV5KDE4MGRlZyk7XG59XG5cbi5wYWdlOm50aC1jaGlsZCgxMSkge1xuICBwYWRkaW5nOiA1JSA0JSA0JSA1JTtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooLTExcHgpO1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xufVxuXG4ubGVmdC1zaWRlOm50aC1jaGlsZCgxMSkge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXooMTFweCkgcm90YXRleSgtMTgwZGVnKTtcbn1cblxuLnBhZ2U6bnRoLWNoaWxkKDEyKSB7XG4gIHBhZGRpbmc6IDUlIDUlIDQlIDQlO1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAxMDAlIDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV6KC0xMnB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSk7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG59XG5cbi5sZWZ0LXNpZGU6bnRoLWNoaWxkKDEyKSB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRleigxMnB4KSBzY2FsZXgoLTEpIHRyYW5zbGF0ZXgoMTAwJSkgcm90YXRleSgxODBkZWcpO1xufVxuXG4uY292ZXItZnJvbnQ6bnRoLWNoaWxkKDEpIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoI2FmZDJkZCwgI2ExZDVlNSkgNTAlIDUwJS84MCUgODYlIG5vLXJlcGVhdCwgbGluZWFyLWdyYWRpZW50KCNmZmZmZmYsICNmZmZmZmYpIDUwJSA1MCUvOTAlIDkzJSBuby1yZXBlYXQsIGxpbmVhci1ncmFkaWVudCgjOWJjOGQ2LCAjOWJjOGQ2KSBuby1yZXBlYXQ7XG59XG4uY292ZXItZnJvbnQ6bnRoLWNoaWxkKDEpIGgxIHtcbiAgZm9udC1mYW1pbHk6IFwiTG9yYVwiLCBzZXJpZjtcbiAgZm9udC1zaXplOiA0NXB4O1xuICBjb2xvcjogIzEzNGE3ODtcbiAgbWFyZ2luLXRvcDogMzBweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5jb3Zlci1mcm9udDpudGgtY2hpbGQoMSkgaDIge1xuICBmb250LWZhbWlseTogXCJFQiBHYXJhbW9uZFwiLCBzZXJpZjtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBjb2xvcjogIzEzNGE3ODtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDQwJTtcbn1cbi5jb3Zlci1mcm9udDpudGgtY2hpbGQoMSkgLmhhdCB7XG4gIGZvbnQtc2l6ZTogMTAwcHg7XG4gIGNvbG9yOiAjMTM0YTc4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMyU7XG59XG5cbnAge1xuICBtYXJnaW4tYm90dG9tOiA0JTtcbiAgZm9udC1mYW1pbHk6IFwiRUIgR2FyYW1vbmRcIiwgc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cblxuLnBhZ2U6bnRoLWNoaWxkKDMpIHA6Zmlyc3Qtb2YtdHlwZTpmaXJzdC1sZXR0ZXIge1xuICBmb250LXNpemU6IDMycHg7XG59XG5cbi5pbnN0cnVjdGlvbiB7XG4gIGxlZnQ6IGNhbGMoNTAlIC0gMTUwcHgpO1xuICBmb250LXNpemU6IDI2cHg7XG4gIGZvbnQtZmFtaWx5OiBcIkVCIEdhcmFtb25kXCIsIHNlcmlmO1xuICBjb2xvcjogIzAwMDk7XG4gIGJhY2tncm91bmQ6ICNmZmY3O1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBwYWRkaW5nOiAxMHB4IDE1cHg7XG59XG5cbnAge1xuICBib3JkZXI6IDFweCBzb2xpZCBibHVlO1xufVxuXG5kaXYucGFnZSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJlZDtcbn1cbiJdLCJzb3VyY2VSb290IjoiIn0= */"]
});

/***/ }),

/***/ 2065:
/*!***********************************************************************************************************!*\
  !*** ./src/app/shop/outside/carousel-animations-sample-component/carousel-animations-sample.component.ts ***!
  \***********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CarouselAnimationsSampleComponent": () => (/* binding */ CarouselAnimationsSampleComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 4666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var igniteui_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! igniteui-angular */ 480);




const _c0 = ["carousel"];
function CarouselAnimationsSampleComponent_igx_select_item_7_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "igx-select-item", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "titlecase");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const animation_r5 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", animation_r5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 2, animation_r5), " ");
  }
}
function CarouselAnimationsSampleComponent_igx_slide_11_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "igx-slide")(1, "div", 12)(2, "igx-card")(3, "igx-card-header")(4, "h4", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "igx-card-content")(7, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "igx-card-media");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "img", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "igx-card-actions")(12, "a", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "visit page");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()()()()();
  }
  if (rf & 2) {
    const slide_r6 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](slide_r6.heading);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](slide_r6.description);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", slide_r6.image, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("href", slide_r6.link, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
  }
}
function CarouselAnimationsSampleComponent_ng_template_12_Template(rf, ctx) {}
class CarouselAnimationsSampleComponent {
  constructor() {
    this.slides = [];
    this.animations = ['slide', 'fade', 'none'];
  }
  ngOnInit() {
    this.addSlides();
  }
  addSlides() {
    this.slides.push({
      description: '30+ Material-based Angular components to code speedy web apps faster.',
      heading: 'Ignite UI for Angular',
      image: 'https://www.infragistics.com/angular-demos-lob/assets/images/carousel/slide1-angular.png',
      link: 'https://www.infragistics.com/products/ignite-ui-angular'
    }, {
      description: 'A complete JavaScript UI library empowering you to build data-rich responsive web apps.',
      heading: 'Ignite UI for Javascript',
      image: 'https://www.infragistics.com/angular-demos-lob/assets/images/carousel/slide2-ignite.png',
      link: 'https://www.infragistics.com/products/ignite-ui'
    }, {
      description: 'Build full-featured business apps with the most versatile set of ASP.NET AJAX UI controls',
      heading: 'Ultimate UI for ASP.NET',
      image: 'https://www.infragistics.com/angular-demos-lob/assets/images/carousel/slide3-aspnet.png',
      link: 'https://www.infragistics.com/products/aspnet'
    });
  }
}
CarouselAnimationsSampleComponent.ɵfac = function CarouselAnimationsSampleComponent_Factory(t) {
  return new (t || CarouselAnimationsSampleComponent)();
};
CarouselAnimationsSampleComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
  type: CarouselAnimationsSampleComponent,
  selectors: [["app-carousel-animations-sample"]],
  viewQuery: function CarouselAnimationsSampleComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.carousel = _t.first);
    }
  },
  decls: 13,
  vars: 3,
  consts: [[1, "carousel-animation-wrapper"], [1, "action-wrapper"], [2, "margin", "0 5px 0 10px"], [2, "max-width", "100px"], [3, "ngModel", "ngModelChange"], ["select", ""], [3, "value", 4, "ngFor", "ngForOf"], [1, "carousel-wrapper"], ["carousel", ""], [4, "ngFor", "ngForOf"], ["igxCarouselIndicator", ""], [3, "value"], [1, "slide-wrapper"], ["igxCardHeaderTitle", ""], [3, "src"], ["igxButton", "", "target", "_blank", "rel", "noopener", 2, "text-decoration", "none", 3, "href"]],
  template: function CarouselAnimationsSampleComponent_Template(rf, ctx) {
    if (rf & 1) {
      const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0)(1, "div", 1)(2, "span", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Current animation: ");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 3)(5, "igx-select", 4, 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CarouselAnimationsSampleComponent_Template_igx_select_ngModelChange_5_listener($event) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7);
        const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](10);
        return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](_r2.animationType = $event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, CarouselAnimationsSampleComponent_igx_select_item_7_Template, 3, 4, "igx-select-item", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 7)(9, "igx-carousel", null, 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, CarouselAnimationsSampleComponent_igx_slide_11_Template, 14, 4, "igx-slide", 9);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, CarouselAnimationsSampleComponent_ng_template_12_Template, 0, 0, "ng-template", 10);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()()();
    }
    if (rf & 2) {
      const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](10);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", _r2.animationType);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.animations);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.slides);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.NgForOf, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.NgModel, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxCarouselComponent, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxSlideComponent, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxCarouselIndicatorDirective, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxSelectComponent, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxSelectItemComponent, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxButtonDirective, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxCardComponent, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxCardHeaderComponent, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxCardMediaDirective, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxCardContentDirective, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxCardActionsComponent, igniteui_angular__WEBPACK_IMPORTED_MODULE_3__.IgxCardHeaderTitleDirective, _angular_common__WEBPACK_IMPORTED_MODULE_1__.TitleCasePipe],
  styles: [".slide-wrapper[_ngcontent-%COMP%] {\n    max-height: 600px;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    height: 100%;\n    padding: 10px;\n\n   \n}\n\n.carousel-wrapper[_ngcontent-%COMP%] {\n    max-height: 600px;\n    height: 100%;\n    width: 90%;\n    margin: 20px auto;\n}\n\n.action-wrapper[_ngcontent-%COMP%] {\n    height: 40px;\n    width: 70%;\n    display: flex;\n    align-items: baseline;\n    margin-bottom: 1rem;\n    margin-left: 1rem;\n}\n\n.carousel-animation-wrapper[_ngcontent-%COMP%] {\n    height: 700px;\n    margin: 16px auto;\n    display: flex;\n    flex-flow: column;\n}\n\n.igx-card[_ngcontent-%COMP%] {\n    box-shadow: none;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hvcC9vdXRzaWRlL2Nhcm91c2VsLWFuaW1hdGlvbnMtc2FtcGxlLWNvbXBvbmVudC9jYXJvdXNlbC1hbmltYXRpb25zLXNhbXBsZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0lBQ2pCLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsdUJBQXVCO0lBQ3ZCLFlBQVk7SUFDWixhQUFhOztHQUVkOztPQUVJO0FBQ1A7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIsWUFBWTtJQUNaLFVBQVU7SUFDVixpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxZQUFZO0lBQ1osVUFBVTtJQUNWLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsYUFBYTtJQUNiLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQiIsInNvdXJjZXNDb250ZW50IjpbIi5zbGlkZS13cmFwcGVyIHtcbiAgICBtYXgtaGVpZ2h0OiA2MDBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHBhZGRpbmc6IDEwcHg7XG5cbiAgIC8qICBpbWcge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICB9ICovXG59XG5cbi5jYXJvdXNlbC13cmFwcGVyIHtcbiAgICBtYXgtaGVpZ2h0OiA2MDBweDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBtYXJnaW46IDIwcHggYXV0bztcbn1cblxuLmFjdGlvbi13cmFwcGVyIHtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgd2lkdGg6IDcwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBiYXNlbGluZTtcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICAgIG1hcmdpbi1sZWZ0OiAxcmVtO1xufVxuXG4uY2Fyb3VzZWwtYW5pbWF0aW9uLXdyYXBwZXIge1xuICAgIGhlaWdodDogNzAwcHg7XG4gICAgbWFyZ2luOiAxNnB4IGF1dG87XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWZsb3c6IGNvbHVtbjtcbn1cblxuLmlneC1jYXJkIHtcbiAgICBib3gtc2hhZG93OiBub25lO1xufSJdLCJzb3VyY2VSb290IjoiIn0= */"]
});

/***/ }),

/***/ 8619:
/*!***************************************************************!*\
  !*** ./src/app/shop/outside/character/character.component.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CharacterComponent": () => (/* binding */ CharacterComponent)
/* harmony export */ });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ 5139);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _services_audio_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/audio.service */ 6425);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 4666);
/* harmony import */ var _directives_softi_div_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../directives/softi-div.directive */ 9195);





class CharacterComponent {
  constructor(audioService) {
    this.audioService = audioService;
    this.stateSoundMenu = 'active';
    this.stateCharacterText = 'active';
    this.stateCharacterPlay = 'active';
    this.stateCharacterSound = 'active';
    /* audio al pasar sobre los botones */
    this.hoverAudio = new Audio(`../../../../assets/section1/sounds/hoverDefault.mp3`);
    /* audio al hacer click en los botones */
    this.clickAudio = new Audio(`../../../../assets/section1/sounds/clickDefault.mp3`);
    /* Audio del personaje leyendo el texto principal */
    this.audioCharacter = new Audio(`../../../../assets/section1/sounds/Lesson1_view1.mp3`);
    this.stateSoundMenu = this.audioService.getStateSoundMenu();
    this.audioService.getStateSoundMenu$().subscribe(stateSoundMenu => {
      this.stateSoundMenu = stateSoundMenu;
      this.stateSoundMenu == 'inactive' ? this.audioCharacter.muted = true : this.audioCharacter.muted = false;
    });
  }
  ngOnDestroy() {
    console.log('DESTRUIDO');
    this.btnCharacterPause();
  }
  changeDirection(direction) {
    console.log('CHANGE DIRECTION');
    console.log(jquery__WEBPACK_IMPORTED_MODULE_0__('.character'));
    //$('.character').css('border', '1px solid red');
    //$('.character').css('background', 'red');
    if (direction == 'right') {
      jquery__WEBPACK_IMPORTED_MODULE_0__('.character').css('transform', 'scaleX(-1)');
      jquery__WEBPACK_IMPORTED_MODULE_0__('.btns_character').css('left', '3%');
    } else {
      jquery__WEBPACK_IMPORTED_MODULE_0__('.character').css('transform', 'scaleX(1)');
      jquery__WEBPACK_IMPORTED_MODULE_0__('.btns_character').css('left', '56%');
    }
  }
  hoverBtnPostMenu() {
    if (this.stateSoundMenu === 'active') {
      this.hoverAudio.play();
    }
  }
  leaveBtnPostMenu() {
    this.hoverAudio.pause();
    this.hoverAudio.currentTime = 0;
    //this.clickAudio.play();
  }
  /*  */
  btnCharacterText() {
    this.clickAudio.currentTime = 0;
    this.clickAudio.play();
    if (this.stateCharacterText === 'active') {
      this.stateCharacterText = 'inactive';
    } else {
      this.stateCharacterText = 'active';
    }
  }
  ngOnInit() {
    //this.mouthCharacter = $('div.character div.mouth');
    this.mouthCharacter = document.getElementsByClassName('mouth');
    console.log('OnInit mouthCharacter: ', this.mouthCharacter);
  }
  /*
   * Reproduce o detiene el audio del personaje
   * Reproduce o detiene la gesticulación de la boca del personaje
   */
  btnCharacterPlayPause() {
    // console.log(' ++ character btnCharacterPlay');
    // this.clickAudio.currentTime = 0;
    // this.clickAudio.play();
    // this.mouthCharacter = $('div.character div.mouth');
    // console.log(this.mouthCharacter.hasClass('close'));
    // console.log(this.mouthCharacter.target);
    if (this.stateCharacterPlay === 'active') {
      this.btnCharacterPlay();
    } else {
      // this.stateCharacterPlay = 'active';
      this.btnCharacterPause();
    }
  }
  btnCharacterPlay() {
    console.log('PLAY mouthCharacter: ', this.mouthCharacter);
    this.stateCharacterPlay = 'inactive';
    this.audioCharacter.play();
    if (this.stateCharacterSound == 'inactive' || this.stateSoundMenu == 'inactive') {
      this.audioCharacter.muted = true;
    }
    this.mouthCharacter[0].classList.remove('close');
    this.mouthCharacter[0].classList.add('gesture');
    //let mouth = this.mouthCharacter;
    this.audioCharacter.onended = () => {
      this.stateCharacterPlay = 'active';
      //mouth[0].classList.remove('gesture');
      //mouth[0].classList.add('close');
      this.mouthCharacter[0].classList.remove('gesture');
      this.mouthCharacter[0].classList.add('close');
    };
  }
  btnCharacterPause() {
    this.stateCharacterPlay = 'active';
    this.audioCharacter.pause();
    this.mouthCharacter[0].classList.remove('gesture');
    this.mouthCharacter[0].classList.add('close');
  }
  btnCharacterReset() {
    // this.stateCharacterPlay = 'active';
    this.btnCharacterPause();
    this.audioCharacter.currentTime = 0;
    // this.mouthCharacter[0].classList.remove('gesture');
    // this.mouthCharacter[0].classList.add('close');
  }
  /*
   * Mutea o activa el sonido del audio del personaje
   */
  btnCharacterSound() {
    this.clickAudio.currentTime = 0;
    this.clickAudio.play();
    if (this.stateCharacterSound === 'active') {
      this.stateCharacterSound = 'inactive';
      this.audioCharacter.muted = true;
    } else {
      this.stateCharacterSound = 'active';
      this.audioCharacter.muted = false;
    }
  }
  drawBoxCharacter(characterObj) {
    /* console.log('++ character drawBox characterObj: ', characterObj);
    console.log('++ character drawBox characterObj.left: ', characterObj.left); */
    // $('.box_character').css('left', characterObj.left);
    jquery__WEBPACK_IMPORTED_MODULE_0__('.box_character').css({
      "left": characterObj.left,
      "top": characterObj.top,
      "height": characterObj.height,
      "width": characterObj.width
    });
  }
}
CharacterComponent.ɵfac = function CharacterComponent_Factory(t) {
  return new (t || CharacterComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_services_audio_service__WEBPACK_IMPORTED_MODULE_1__.AudioService));
};
CharacterComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({
  type: CharacterComponent,
  selectors: [["app-character"]],
  decls: 7,
  vars: 2,
  consts: [["bindPosition", "", 1, "box_character"], [1, "character"], [1, "mouth", "close"], [1, "btns_character"], ["type", "button", 1, "button", "button_character_play", 3, "ngClass", "mouseenter", "mouseleave", "click"], ["type", "button", 1, "button", "button_character_sound", 3, "ngClass", "mouseenter", "mouseleave", "click"], [1, "message_box_character"]],
  template: function CharacterComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "softi-div", 0)(1, "div", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](2, "div", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "div", 3)(4, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("mouseenter", function CharacterComponent_Template_div_mouseenter_4_listener() {
        return ctx.hoverBtnPostMenu();
      })("mouseleave", function CharacterComponent_Template_div_mouseleave_4_listener() {
        return ctx.leaveBtnPostMenu();
      })("click", function CharacterComponent_Template_div_click_4_listener() {
        return ctx.btnCharacterPlayPause();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "div", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("mouseenter", function CharacterComponent_Template_div_mouseenter_5_listener() {
        return ctx.hoverBtnPostMenu();
      })("mouseleave", function CharacterComponent_Template_div_mouseleave_5_listener() {
        return ctx.leaveBtnPostMenu();
      })("click", function CharacterComponent_Template_div_click_5_listener() {
        return ctx.btnCharacterSound();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](6, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngClass", ctx.stateCharacterPlay);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngClass", ctx.stateCharacterSound);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.NgClass, _directives_softi_div_directive__WEBPACK_IMPORTED_MODULE_2__.SoftiDivDirective],
  styles: [".box_character[_ngcontent-%COMP%] {\n    position: absolute;\n    height: 64%;\n    left: 76.5%;\n    top: 26%;\n    width: 14.5%;\n    z-index: 3;\n}\n.box_character[_ngcontent-%COMP%]   .character[_ngcontent-%COMP%] {\n    position: absolute;\n    background: url('personaje.png') no-repeat;\n    background-size: 100% 100%;\n    bottom: 3%;\n    height: 94%;\n    left: 5%;\n    width: 63%;\n}\n.box_character[_ngcontent-%COMP%]   .character[_ngcontent-%COMP%]   .mouth.close[_ngcontent-%COMP%]{\n    position: absolute;\n    background: url('mouth_close.png') no-repeat;\n    background-size: 100% 100%;\n    height: 2%;\n    left: 37.2%;\n    top: 15.3%;\n    width: 13%;\n}\n.box_character[_ngcontent-%COMP%]   .character[_ngcontent-%COMP%]   .mouth.gesture[_ngcontent-%COMP%]{\n    position: absolute;\n    background: url('mouth_gesture.gif') no-repeat;\n    background-size: 100% 100%;\n    height: 19%;\n    left: 16.3%;\n    top: 7%;\n    width: 56%;\n}\n\n\n.fadeInRightBig[_ngcontent-%COMP%] {\n    animation-name: fadeInRightBig;\n  }\n  .animated[_ngcontent-%COMP%] {\n    animation-duration: 1s;\n    animation-fill-mode: both;\n  }\n\n  \n\n  .fadeOutLeftBig[_ngcontent-%COMP%] {\n    animation-name: fadeOutLeftBig;\n  }\n\n\n\n.btns_character[_ngcontent-%COMP%] {\n    position: absolute;\n    \n    width: 44%;\n    height: 57%;\n    width: 15%;\n    height: 27%;\n    top: 8%;\n    left: 56%;\n    display: flex;\n    justify-content: center;\n}\n.btns_character[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]{\n    position: absolute;\n    width: 45%;\n    height: 17%;\n    width: 88%;\n    height: 24%;\n    \n    cursor: pointer;\n}\n.btns_character[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]:active {\n    transform: translateY(3px);\n   \n}\n\n.btns_character[_ngcontent-%COMP%]   .button.button_character_text.active[_ngcontent-%COMP%] {\n    background: url('dialogue.png') no-repeat;\n    background-size: 100% 100%;\n    top: 6%;\n}\n.btns_character[_ngcontent-%COMP%]   .button.button_character_text.active[_ngcontent-%COMP%]:hover{\n    background: url('dialogue_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 6%;\n}\n.btns_character[_ngcontent-%COMP%]   .button.button_character_text.inactive[_ngcontent-%COMP%] {\n    background: url('dialogue_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 6%;\n}\n.btns_character[_ngcontent-%COMP%]   .button.button_character_text.inactive[_ngcontent-%COMP%]:hover{\n    background: url('dialogue.png') no-repeat;\n    background-size: 100% 100%;\n    top: 6%;\n}\n\n.btns_character[_ngcontent-%COMP%]   .button.button_character_play.active[_ngcontent-%COMP%] {\n    background: url('play.png') no-repeat;\n    background-size: 100% 100%;\n    top: 38%;\n}\n.btns_character[_ngcontent-%COMP%]   .button.button_character_play.active[_ngcontent-%COMP%]:hover{\n    background: url('play_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 38%;\n}\n.btns_character[_ngcontent-%COMP%]   .button.button_character_play.inactive[_ngcontent-%COMP%] {\n    background: url('play_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 38%;\n}\n.btns_character[_ngcontent-%COMP%]   .button.button_character_play.inactive[_ngcontent-%COMP%]:hover{\n    background: url('play.png') no-repeat;\n    background-size: 100% 100%;\n    top: 38%;\n}\n\n.btns_character[_ngcontent-%COMP%]   .button.button_character_sound.active[_ngcontent-%COMP%] {\n    background: url('_-_-_-_-assets-section1-character-images-sound.png') no-repeat;\n    background-size: 100% 100%;\n    top: 71%;\n}\n.btns_character[_ngcontent-%COMP%]   .button.button_character_sound.active[_ngcontent-%COMP%]:hover{\n    background: url('_-_-_-_-assets-section1-character-images-sound_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 71%;\n}\n.btns_character[_ngcontent-%COMP%]   .button.button_character_sound.inactive[_ngcontent-%COMP%] {\n    background: url('_-_-_-_-assets-section1-character-images-sound_off_hover.png') no-repeat;\n    background-size: 100% 100%;\n    top: 71%;\n}\n.btns_character[_ngcontent-%COMP%]   .button.button_character_sound.inactive[_ngcontent-%COMP%]:hover{\n    background: url('_-_-_-_-assets-section1-character-images-sound.png') no-repeat;\n    background-size: 100% 100%;\n    top: 71%;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hvcC9vdXRzaWRlL2NoYXJhY3Rlci9jaGFyYWN0ZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxtQ0FBbUM7QUFDbkM7SUFDSSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFdBQVc7SUFDWCxRQUFRO0lBQ1IsWUFBWTtJQUNaLFVBQVU7QUFDZDtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDBDQUF5RjtJQUN6RiwwQkFBMEI7SUFDMUIsVUFBVTtJQUNWLFdBQVc7SUFDWCxRQUFRO0lBQ1IsVUFBVTtBQUNkO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNENBQXVGO0lBQ3ZGLDBCQUEwQjtJQUMxQixVQUFVO0lBQ1YsV0FBVztJQUNYLFVBQVU7SUFDVixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw4Q0FBeUY7SUFDekYsMEJBQTBCO0lBQzFCLFdBQVc7SUFDWCxXQUFXO0lBQ1gsT0FBTztJQUNQLFVBQVU7QUFDZDs7QUFFQSwrQ0FBK0M7QUFDL0M7SUFFSSw4QkFBOEI7RUFDaEM7RUFDQTtJQUVFLHNCQUFzQjtJQUV0Qix5QkFBeUI7RUFDM0I7O0VBRUEsaURBQWlEOztFQUVqRDtJQUVFLDhCQUE4QjtFQUNoQzs7O0FBR0YsZ0RBQWdEO0FBQ2hEO0lBQ0ksa0JBQWtCO0lBQ2xCLGdDQUFnQztJQUNoQyxVQUFVO0lBQ1YsV0FBVztJQUNYLFVBQVU7SUFDVixXQUFXO0lBQ1gsT0FBTztJQUNQLFNBQVM7SUFDVCxhQUFhO0lBQ2IsdUJBQXVCO0FBQzNCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFdBQVc7SUFDWCxVQUFVO0lBQ1YsV0FBVztJQUNYOzs7Ozs7OztLQVFDO0lBQ0QsZUFBZTtBQUNuQjtBQUNBO0lBQ0ksMEJBQTBCO0dBQzNCOzs7O3VCQUlvQjtBQUN2QjtBQUNBLGlDQUFpQztBQUNqQztJQUNJLHlDQUFvRjtJQUNwRiwwQkFBMEI7SUFDMUIsT0FBTztBQUNYO0FBQ0E7SUFDSSwrQ0FBMEY7SUFDMUYsMEJBQTBCO0lBQzFCLE9BQU87QUFDWDtBQUNBO0lBQ0ksK0NBQTBGO0lBQzFGLDBCQUEwQjtJQUMxQixPQUFPO0FBQ1g7QUFDQTtJQUNJLHlDQUFvRjtJQUNwRiwwQkFBMEI7SUFDMUIsT0FBTztBQUNYO0FBQ0EsK0NBQStDO0FBQy9DO0lBQ0kscUNBQWdGO0lBQ2hGLDBCQUEwQjtJQUMxQixRQUFRO0FBQ1o7QUFDQTtJQUNJLDJDQUFzRjtJQUN0RiwwQkFBMEI7SUFDMUIsUUFBUTtBQUNaO0FBQ0E7SUFDSSwyQ0FBc0Y7SUFDdEYsMEJBQTBCO0lBQzFCLFFBQVE7QUFDWjtBQUNBO0lBQ0kscUNBQWdGO0lBQ2hGLDBCQUEwQjtJQUMxQixRQUFRO0FBQ1o7QUFDQSxzQ0FBc0M7QUFDdEM7SUFDSSwrRUFBaUY7SUFDakYsMEJBQTBCO0lBQzFCLFFBQVE7QUFDWjtBQUNBO0lBQ0kscUZBQXVGO0lBQ3ZGLDBCQUEwQjtJQUMxQixRQUFRO0FBQ1o7QUFDQTtJQUNJLHlGQUEyRjtJQUMzRiwwQkFBMEI7SUFDMUIsUUFBUTtBQUNaO0FBQ0E7SUFDSSwrRUFBaUY7SUFDakYsMEJBQTBCO0lBQzFCLFFBQVE7QUFDWiIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqICAgQ2FqYSBQZXJzb25hamUgICAqKioqKioqL1xuLmJveF9jaGFyYWN0ZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDY0JTtcbiAgICBsZWZ0OiA3Ni41JTtcbiAgICB0b3A6IDI2JTtcbiAgICB3aWR0aDogMTQuNSU7XG4gICAgei1pbmRleDogMztcbn1cbi5ib3hfY2hhcmFjdGVyIC5jaGFyYWN0ZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2dlbmVyYWxfdmlld19sZXNzb25zL3BlcnNvbmFqZS5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICBib3R0b206IDMlO1xuICAgIGhlaWdodDogOTQlO1xuICAgIGxlZnQ6IDUlO1xuICAgIHdpZHRoOiA2MyU7XG59XG4uYm94X2NoYXJhY3RlciAuY2hhcmFjdGVyIC5tb3V0aC5jbG9zZXtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaGFyYWN0ZXIvaW1hZ2VzL21vdXRoX2Nsb3NlLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIGhlaWdodDogMiU7XG4gICAgbGVmdDogMzcuMiU7XG4gICAgdG9wOiAxNS4zJTtcbiAgICB3aWR0aDogMTMlO1xufVxuLmJveF9jaGFyYWN0ZXIgLmNoYXJhY3RlciAubW91dGguZ2VzdHVyZXtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaGFyYWN0ZXIvaW1hZ2VzL21vdXRoX2dlc3R1cmUuZ2lmKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgaGVpZ2h0OiAxOSU7XG4gICAgbGVmdDogMTYuMyU7XG4gICAgdG9wOiA3JTtcbiAgICB3aWR0aDogNTYlO1xufVxuXG4vKioqKioqKiAgIEFuaW1hY2nDg8KzbiBkZSBsbGVnYWRhIHZpc3RhICAgKioqKioqKi9cbi5mYWRlSW5SaWdodEJpZyB7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tbmFtZTogZmFkZUluUmlnaHRCaWc7XG4gICAgYW5pbWF0aW9uLW5hbWU6IGZhZGVJblJpZ2h0QmlnO1xuICB9XG4gIC5hbmltYXRlZCB7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tZHVyYXRpb246IDFzO1xuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMXM7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tZmlsbC1tb2RlOiBib3RoO1xuICAgIGFuaW1hdGlvbi1maWxsLW1vZGU6IGJvdGg7XG4gIH1cblxuICAvKioqKioqKiAgIEFuaW1hY2nDg8KzbiBkZSBzYWxpZGEgZGUgdmlzdGEgICAqKioqKioqL1xuXG4gIC5mYWRlT3V0TGVmdEJpZyB7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tbmFtZTogZmFkZU91dExlZnRCaWc7XG4gICAgYW5pbWF0aW9uLW5hbWU6IGZhZGVPdXRMZWZ0QmlnO1xuICB9XG5cblxuLyoqKioqKiogICBDYWphIE1lbnUgQm90b25vcyBQZXJzb25hamUgICAqKioqKioqL1xuLmJ0bnNfY2hhcmFjdGVyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgLyogYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7ICovXG4gICAgd2lkdGg6IDQ0JTtcbiAgICBoZWlnaHQ6IDU3JTtcbiAgICB3aWR0aDogMTUlO1xuICAgIGhlaWdodDogMjclO1xuICAgIHRvcDogOCU7XG4gICAgbGVmdDogNTYlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uYnRuc19jaGFyYWN0ZXIgLmJ1dHRvbntcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDQ1JTtcbiAgICBoZWlnaHQ6IDE3JTtcbiAgICB3aWR0aDogODglO1xuICAgIGhlaWdodDogMjQlO1xuICAgIC8qIFxuICAgIGxlZnQ6IDE2JTtcbiAgICBib3gtc2hhZG93OiAwIDRweCAwIGNhbGModmFyKC0tZnJhbWVIZWlnaHQpKjAuMDAxNCkgIzNiM2MzZDtcbiAgICBib3JkZXItdG9wOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwNSkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItbGVmdDogY2FsYyh2YXIoLS1mcmFtZUhlaWdodCkqMC4wMDUpIHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLXJpZ2h0OiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwNSkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItYm90dG9tOiBjYWxjKHZhcigtLWZyYW1lSGVpZ2h0KSowLjAwNSkgc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7IFxuICAgICovXG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmJ0bnNfY2hhcmFjdGVyIC5idXR0b246YWN0aXZlIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoM3B4KTtcbiAgIC8qICBib3JkZXItdG9wOiAycHggc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItbGVmdDogMnB4IHNvbGlkICMzYjNjM2Q7XG4gICAgYm9yZGVyLXJpZ2h0OiAycHggc29saWQgIzNiM2MzZDtcbiAgICBib3JkZXItYm90dG9tOiAycHggc29saWQgIzNiM2MzZDtcbiAgICBib3gtc2hhZG93OiBub25lOyAqL1xufVxuLyogQm90w4PCs24gY3VhZHJvIHRleHRvIHBlcnNvbmFqZSAqL1xuLmJ0bnNfY2hhcmFjdGVyIC5idXR0b24uYnV0dG9uX2NoYXJhY3Rlcl90ZXh0LmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaGFyYWN0ZXIvaW1hZ2VzL2RpYWxvZ3VlLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogNiU7XG59XG4uYnRuc19jaGFyYWN0ZXIgLmJ1dHRvbi5idXR0b25fY2hhcmFjdGVyX3RleHQuYWN0aXZlOmhvdmVye1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvY2hhcmFjdGVyL2ltYWdlcy9kaWFsb2d1ZV9ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB0b3A6IDYlO1xufVxuLmJ0bnNfY2hhcmFjdGVyIC5idXR0b24uYnV0dG9uX2NoYXJhY3Rlcl90ZXh0LmluYWN0aXZlIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2NoYXJhY3Rlci9pbWFnZXMvZGlhbG9ndWVfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdG9wOiA2JTtcbn1cbi5idG5zX2NoYXJhY3RlciAuYnV0dG9uLmJ1dHRvbl9jaGFyYWN0ZXJfdGV4dC5pbmFjdGl2ZTpob3ZlcntcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2NoYXJhY3Rlci9pbWFnZXMvZGlhbG9ndWUucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdG9wOiA2JTtcbn1cbi8qIEJvdMODwrNuIHJlcHJvZHVjaXIvZGV0ZW5lciBkaWFsb2dvIHBlcnNvbmFqZSAqL1xuLmJ0bnNfY2hhcmFjdGVyIC5idXR0b24uYnV0dG9uX2NoYXJhY3Rlcl9wbGF5LmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9zZWN0aW9uMS9jaGFyYWN0ZXIvaW1hZ2VzL3BsYXkucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdG9wOiAzOCU7XG59XG4uYnRuc19jaGFyYWN0ZXIgLmJ1dHRvbi5idXR0b25fY2hhcmFjdGVyX3BsYXkuYWN0aXZlOmhvdmVye1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvY2hhcmFjdGVyL2ltYWdlcy9wbGF5X2hvdmVyLnBuZykgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICAgIHRvcDogMzglO1xufVxuLmJ0bnNfY2hhcmFjdGVyIC5idXR0b24uYnV0dG9uX2NoYXJhY3Rlcl9wbGF5LmluYWN0aXZlIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2NoYXJhY3Rlci9pbWFnZXMvcGxheV9ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB0b3A6IDM4JTtcbn1cbi5idG5zX2NoYXJhY3RlciAuYnV0dG9uLmJ1dHRvbl9jaGFyYWN0ZXJfcGxheS5pbmFjdGl2ZTpob3ZlcntcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL3NlY3Rpb24xL2NoYXJhY3Rlci9pbWFnZXMvcGxheS5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB0b3A6IDM4JTtcbn1cbi8qIEJvdMODwrNuIHNpbGVuY2lhciBkaWFsb2dvIHBlcnNvbmFqZSAqL1xuLmJ0bnNfY2hhcmFjdGVyIC5idXR0b24uYnV0dG9uX2NoYXJhY3Rlcl9zb3VuZC5hY3RpdmUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvY2hhcmFjdGVyL2ltYWdlcy9zb3VuZC5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB0b3A6IDcxJTtcbn1cbi5idG5zX2NoYXJhY3RlciAuYnV0dG9uLmJ1dHRvbl9jaGFyYWN0ZXJfc291bmQuYWN0aXZlOmhvdmVye1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvY2hhcmFjdGVyL2ltYWdlcy9zb3VuZF9ob3Zlci5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB0b3A6IDcxJTtcbn1cbi5idG5zX2NoYXJhY3RlciAuYnV0dG9uLmJ1dHRvbl9jaGFyYWN0ZXJfc291bmQuaW5hY3RpdmUge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvY2hhcmFjdGVyL2ltYWdlcy9zb3VuZF9vZmZfaG92ZXIucG5nKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgdG9wOiA3MSU7XG59XG4uYnRuc19jaGFyYWN0ZXIgLmJ1dHRvbi5idXR0b25fY2hhcmFjdGVyX3NvdW5kLmluYWN0aXZlOmhvdmVye1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvc2VjdGlvbjEvY2hhcmFjdGVyL2ltYWdlcy9zb3VuZC5wbmcpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICB0b3A6IDcxJTtcbn0iXSwic291cmNlUm9vdCI6IiJ9 */"]
});

/***/ }),

/***/ 4431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ 4497);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 6747);


_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__.platformBrowser().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule).catch(err => console.error(err));

/***/ }),

/***/ 5886:
/*!***********************************!*\
  !*** ./server/lesson1/texts.json ***!
  \***********************************/
/***/ ((module) => {

module.exports = JSON.parse('[{"title":"INTRODUCCIÓN","description":"A causa de una serie de intervenciones y/o intromisiones, efectuadas de forma desprolijas, por personas (maestros de oficio) y, por los distintos servicios que conviven en el subsuelo del Bien Nacional de Uso Público (BNUP), que generaron condiciones de inseguridad en las personas e instalaciones, se logró llegar a un Acuerdo de Mesa Técnica Interempresas, junto a los Municipios, SERVIU, MOP, Gobernación y otras empresas de servicios, con la firme intención de poder generar alguna modalidad que permita cautelar, prevenir y/o advertir el provocar cualquier tipo de incidente o acontecimiento, que exponga principalmente a las personas que trabajan directamente sobre ellas y ciudadanos que transitan sobre éstas."},{"title":"OBJETIVOS","description":"1. Conocer qué son los Bienes Nacionales de Uso Público y qué servicios básicos se ubican en el subsuelo de éstos.","description1":"2. Interiorizar a los colaboradores, respecto a las previsiones que se deben realizar, en las actividades o trabajos a efectuar en el subsuelo de los Bienes Nacionales de Uso Público","description2":"3. Tomar conciencia, de lo importante que es realizar una actividad, cumpliendo con los protocolos preventivos que se han dispuesto para ello, para la protección de las personas.","description3":"4. Promover   una   cultura   preventiva,   que   permita   evitar   accidentes, contribuyendo a la conservación de los Bienes Nacionales de Uso Público y, principalmente velando por la prevención y el cuidado de sus propias vidas y de los demás"}]');

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(4431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map